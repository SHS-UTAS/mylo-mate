const PuppeteerEnvironment = require('jest-environment-puppeteer');
const { testid } = require('./util.js');
const store = require('./test-store.json');

class CustomEnvironment extends PuppeteerEnvironment {
  async setup() {
    await super.setup();
    const { TEST_PLATFORM } = process.env;

    const base = (TEST_PLATFORM || '').trim();
    const original = this.global.page.goto;

    this.global.page.goto = async function goto(path, args = {}) {
      return original.call(this, path.startsWith('/') ? base + path : path, { waitUntil: 'networkidle0', ...args });
    };

    if (!this.global.page.testid) {
      this.global.page.testid = async id => {
        await this.global.page.waitForSelector(testid(id), { timeout: 60000 });
        return this.global.page.$(testid(id));
      };
    }

    const targets = await this.global.browser.targets();
    const ext = targets.find(target => target.type() == 'background_page');
    const background = await ext.page();
    await background.evaluate(store => new Promise(res => chrome.storage.sync.set(store, res)), store);
  }
}

module.exports = CustomEnvironment;
