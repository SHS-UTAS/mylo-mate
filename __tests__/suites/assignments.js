const { UNIT_ID, ASSESSMENT_ID } = process.env;

describe('Assignments', () => {
  beforeAll(async () => {
    await page.goto(`/d2l/lms/dropbox/admin/folders_manage.d2l?ou=${UNIT_ID}`);
    await page.waitForSelector('[mm-abort]');
    await page.waitFor(1000);
  });

  it('has the test assignment folder', async () => {
    expect(await page.$(`a[href*="db=${ASSESSMENT_ID}"]`)).toBeTruthy();
  });

  it.skip('has quicklink - [p]', async () => {
    await page.waitForSelector(`a[href*="db=${ASSESSMENT_ID}" i]`, { timeout: 15000 });
    const row = await page.$(`a[href*="db=${ASSESSMENT_ID}" i]`).then(e => e.$x('..')).then(e => e[0]);
    await row.waitForSelector('[title*="folder properties" i]', { timeout: 15000 });
    expect(await row.$('[title*="folder properties" i]')).toBeTruthy();
  });

  it.skip('has quicklink - [r]', async () => {
    await page.waitForSelector(`a[href*="db=${ASSESSMENT_ID}" i]`, { timeout: 15000 });
    const row = await page.$(`a[href*="db=${ASSESSMENT_ID}" i]`).then(e => e.$x('..')).then(e => e[0]);
    await row.waitForSelector('[title*="restrictions" i]', { timeout: 15000 });
    expect(await row.$('[title*="restrictions" i]')).toBeTruthy();
  });

  it.skip('has quicklink - [r]', async () => {
    await page.waitForSelector(`a[href*="db=${ASSESSMENT_ID}" i]`, { timeout: 15000 });
    const row = await page.$(`a[href*="db=${ASSESSMENT_ID}" i]`).then(e => e.$x('..')).then(e => e[0]);
    await row.waitForSelector('[title*="turnitin" i]', { timeout: 15000 });
    expect(await row.$('[title*="turnitin" i]')).toBeTruthy();
  });

  it.skip('has quicklink - [a]', async () => {
    await page.waitForSelector(`a[href*="db=${ASSESSMENT_ID}" i]`, { timeout: 15000 });
    const row = await page.$(`a[href*="db=${ASSESSMENT_ID}" i]`).then(e => e.$x('..')).then(e => e[0]);
    await row.waitForSelector('[title*="show assignment report" i]', { timeout: 15000 });
    expect(await row.$('[title*="show assignment report" i]')).toBeTruthy();
  });
});

describe('Assignment Submissions', () => {
  beforeAll(async () => {
    await page.goto(`/d2l/lms/dropbox/admin/mark/folder_submissions_users.d2l?db=${ASSESSMENT_ID}&ou=${UNIT_ID}`);
    await page.waitForSelector('[mm-abort]');
    await page.waitFor(1000);
  });

  it('has a submission', async () => {
    expect((await page.$$(`table[type="data"] > tbody > tr:not([header]):not([last-row])`)).length).toBeGreaterThan(0);
  });

  it('has quicklinks in breadcrumbs', async () => {
    await page.waitForSelector('[title*="folder properties" i]', { timeout: 15000 });
    expect(await page.$('[title*="folder properties" i]')).toBeTruthy();
    expect(await page.$('[title*="restrictions" i]')).toBeTruthy();
    expect(await page.$('[title*="turnitin" i]')).toBeTruthy();
  });

  it('has the Resubmit to Turnitin button', async () => {
    expect(await page.testid('turnitin-resubmit')).toBeTruthy();
  });

  it('has the "On This Page" table', async () => {
    await page.waitForSelector('.mm-analytics');
    expect(await page.testid('on-this-page')).toBeTruthy();
  });

  it('has the "Incomplete Rubrics" table', async () => {
    await page.waitForSelector('.mm-analytics');
    expect(await page.testid('incomplete-rubrics')).toBeTruthy();
  });

  it('has the Submission Status dropdown', async () => {
    expect(await page.testid('submissions-filter')).toBeTruthy();
  });
});