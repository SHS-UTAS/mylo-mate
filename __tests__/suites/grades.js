const { testid } = require('../config/util.js');

describe('Manage Grades', () => {
  beforeAll(async () => {
    const { UNIT_ID } = process.env;
    await page.goto(`/d2l/lms/grades/admin/manage/gradeslist.d2l?ou=${UNIT_ID}`);
    await page.waitForSelector('[mm-abort]');
    await page.waitFor(1000);
  });

  it('has User Progress settings dialog', async () => {
    expect(await page.testid('user-progress-settings-dialog')).toBeTruthy();
  });

  it('has the grade item quick link [r]', async () => {
    expect(await page.testid('grade-restriction')).toBeTruthy();
  });

  it('has the grade item quick link [a]', async () => {
    expect(await page.testid('grade-attached-item')).toBeTruthy();
  });

  it('has the Final Grade quick link [r]', async () => {
    expect(await page.testid('final-grade-quicklink')).toBeTruthy();
  });

  it('has a bulk Hide/Show buttons', async () => {
    expect(await page.testid('hide-selected-grades')).toBeTruthy();
    expect(await page.testid('show-selected-grades')).toBeTruthy();
  });

  it('can hide grade items', async () => {
    await page.waitForSelector('tr[data-type="item"] input[type="checkbox"]');
    await page.click('tr[data-type="item"] input[type="checkbox"]');
    await page.click(testid('hide-selected-grades'));
    await page.waitForNavigation({ timeout: 15000 });
    await page.waitForSelector('tr[data-type="item"] input[type="checkbox"]');
    expect(await page.$eval('tr[data-type="item"]', el => el.getAttribute('data-hidden') == 'true')).toBeTruthy();
  });

  it('can show grade items', async () => {
    await page.waitForSelector('tr[data-type="item"] input[type="checkbox"]');
    await page.click('tr[data-type="item"] input[type="checkbox"]');
    await page.click(testid('show-selected-grades'));
    await page.waitForNavigation({ timeout: 15000 });
    await page.waitForSelector('tr[data-type="item"] input[type="checkbox"]');
    expect(await page.$eval('tr[data-type="item"]', el => el.getAttribute('data-hidden') == 'false')).toBeTruthy();
  });
});

describe('Enter Grades', () => {
  beforeAll(async () => {
    const { UNIT_ID } = process.env;
    await page.goto(`/d2l/lms/grades/admin/enter/user_list_view.d2l?ou=${UNIT_ID}`);
    await page.waitForSelector('[mm-abort]');
  });

  it('has User Progress settings dialog', async () => {
    expect(await page.testid('user-progress-settings-dialog')).toBeTruthy();
  });

  it('has pagination at top of page', async () => {
    expect(await page.testid('cloned-pagination')).toBeTruthy();
  });

  it('has the grade item quick link [a]', async () => {
    expect(await page.testid('grade-attached-item')).toBeTruthy();
  });
});