describe('Quizzes', () => {
  beforeAll(async () => {
    const { UNIT_ID } = process.env;
    await page.goto(`/d2l/lms/quizzing/admin/quizzes_manage.d2l?ou=${UNIT_ID}`);
    await page.waitForSelector('[mm-abort]');
  });

  it('has all quicklinks', async () => {
    expect(await page.testid('quiz-grade')).toBeTruthy();
    expect(await page.testid('quiz-submissions')).toBeTruthy();
    expect(await page.testid('quiz-assessment')).toBeTruthy();
    expect(await page.testid('quiz-restrictions')).toBeTruthy();
  });

  it('has Bulk Uncheck Dates button', async () => {
    expect(await page.testid('bulk-uncheck-dates')).toBeTruthy();
  });
});