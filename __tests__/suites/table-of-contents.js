describe('Table of Contents', () => {
  beforeAll(async () => {
    const { UNIT_ID, TOC_ID } = process.env;
    await page.goto(`/d2l/le/content/${UNIT_ID}/Home?itemIdentifier=D2L.LE.Content.ContentObject.ModuleCO-${TOC_ID}`);
    await page.waitForSelector('[mm-abort]');
    await page.waitFor(2000);
  });

  it('has Unit Builder button', async () => {
    expect(await page.testid('unit-builder')).toBeTruthy();
  });

  it('has quicklinks [e] and [f] ', async () => {
    expect(await page.testid('show-in-manage-files')).toBeTruthy();
    expect(await page.testid('edit-web-page')).toBeTruthy();
  });
});