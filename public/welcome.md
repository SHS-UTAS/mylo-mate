[![Install MyLO MATE][logo]][download]
MyLO MATE is an extension for Google Chrome, [available here][download]. It is an extension that enhances MyLO, MyLO Manager and Echo360. The [Impact Chart][impact chart] details most of the features available.

For any support requests or suggestions please contact us through [service desk - select Digital Futures then MyLO MATE][support]. This product is maintained by Kevin Lyall and Connor Deckers with the support of Digital Futures, and invaluable support and assistance from members of the Building eLearning Community of Practice and the wider UTAS community.

# Contents

- [Overview](#overview)
  - [MyLO](#info-mylo)
  - [MyLO Widgets](#info-widgets)
  - [Echo360](#info-echo360)
  - [MyLO Manager](#info-mylo-manager)
- [Feature Highlight](#highlight)
- [Release Notes](#release-notes)

# Overview {overview}

## MyLO {info-mylo}

- Navigation bar icons have been added, including quick access to: `Unit outline`, `Manage Files`, `Unit Admin` and `Groups` tools. (The Groups icon will only appear if the standard Group link is missing from the navigation bar.)
- Shortcuts to popular tabs within the Quizzes, Assignment Folders and Grades tools are added (e.g. Restrictions, Assessment, Turnitin)
- Editing ICB templated pages opens a panel allowing easy addition of ICB template content, including in-place previews of ICB tools.
- A link checker tool is available on all web pages to check for broken links. An option is available to make all external links to open in a new tab. Links that would be invalid (e.g. target blank attribute missing) are automatically fixed when you save a file in the HTML Editor.
- Creating standard rubrics is much easier with integration with the [Rubric Generator](https://www.utas.edu.au/building-elearning/resources/mylo-rubric-generator), as well as the ability to download rubrics as Word documents.
- Unit in search are coloured according to approximate start and end date of unit.
- Removes some of the `Scroll Left` / `Scroll Right` arrows that appear throughout MyLO, and instead provides a horizontal scroll bar at the bottom of the screen.
- Adds a `Back to Top` and `Jump to Bottom` button on all pages in MyLO. Note that these will only appear if the page is long enough to require them.
- Enhanced Discussions to show the message of a Discussions post beneath the heading. Note that this is only available when viewing Discussions in Grid View, and needs to be turned on in the options.

## MyLO Widgets {info-widgets}

MyLO MATE introduces many widgets that appear on the Unit Homepage of MyLO units in the MyLO MATE Widget Dashboard. Certain widgets also appear when you visit your personal Sandpit.

### Widgets available in All Units

- **Search Unit**  
  This widget allows the user to search the entire unit's text content, including HTML, PDFs and Word documents (DOCX only, not DOC). Now also provides a shortcut to finding links that might be blocked overseas, including YouTube, Vimeo and Google.
- **Invalid Link Checker**  
  This widget automatically checks all of the content within the current MyLO unit, and check the links for issues. These issues include 404 (page not found) errors, and links that won't open correctly within MyLO. If issues are found, it will provide a link to each problem file. <br><br> This widget also appears when viewing MyLO HTML pages and when activated it will find any issues on that page, and highlight the invalid links.
- **Grades Analysis**  
  Offers charts comparing letter grades taken from Final Calculated and Final Adjusted Grades, as well as highlighting certain grade information.
- **Status Report**  
  This widget runs through a series of common checks for the unit (e.g. does the unit outline exist?).
- **Unit Outline Uploader**  
  This widget allows the user to easily replace unit outlines from the home page of a unit.
- **Word Cloud Widget**
  Provides a visual representation of the most commonly used words in your unit, in a specific discussion, in an assignment, in your unit content.

### Widgets available in your sandpit

- **Get Enrolled Staff**  
  This widget retrieves a list of enrolled staff from a unit (or range of units). Search terms include are inclusive, i.e. `ABC` or `ABC345` will match `ABC123 ABC345 Unit Name`.

- **Get Absent Students**  
  This widget retrieves a list of students what haven't logged into a unit. One spreadsheet per unit is created, and the resulting list of spreadsheets is bundled into a zip file for download. Search terms include are inclusive, i.e. `ABC` or `ABC345` will match `ABC123 ABC345 Unit Name`.

## Echo360 {info-echo360}

- Download with one click all videos in an Echo360 'course' by clicking the link in the MyLO MATE panel.

Please note that the developers of this extension have no control over changes to Echo360, MyLO Manager or MyLO, and as a result some aspects may stop working without notice. We'll endeavour to keep it updated. Please let us know if you have any suggestions for features, improvements or bugs. ![Please select Digital Futures and then MyLO MATE][support].

## MyLO Manager {info-mylo-manager}

- Allows creation of custom user groups to allow easy adding of staff to units
- Adds a Bulk User Management (BUM) CSV export button at bottom of list
- Rows coloured according to start and end date of unit
- Default rows per page is saved
- Expands the table width to 100%, allowing the user to widen columns and use the full width of the browser
- Re-enables the unit link (and unit quick links) where the status is Pending
- Adds a series of links below each unit link, hover for details. Each link goes directly to a part of MyLO without having to go via the home page. Adds a legend at the bottom of the screen.

# Feature Highlight {highlight}

MyLO MATE unit home page widgets are now all consolidated within a new pop-up called the Widget Dashboard. See the new button here:

![Open widget dashboard](/etc/whatsnew/open-widgets-dashboard.png)

The widgets themselves have not changed, they have just moved. Please get in touch if you have any issues; [select Digital Futures and then MyLO MATE][support].
# Release Notes {release-notes}

/--
/^^ Version 2.4.x
**Version 2.4.45 (current version)**  
_8 November 2023_

- Fix issue with rubric marking overlay

**Version 2.4.44**  
_9 October 2023_

- Fix issue with assignment retract feedback button sometimes failing

**Version 2.4.43**  
_27 September 2023_

- Fix issue with some unit outline shortcuts

**Version 2.4.42**  
_25 September 2023_

- Introduce warning icon for broken unit information widget unit outline links
- Update to unit outline widget messages

**Version 2.4.41**  
_14 September 2023_

- Fix grade rubrics button
- Update unit outline widget to support new process and pilot units

**Version 2.4.40**  
_31 August 2023_

- Fix rubric options shortcut scrolling issue
- Fix support link and advice

**Version 2.4.39**  
_17 August 2023_

- Adds filter support for observed assessments

**Version 2.4.38**  
_8 August 2023_

- Update delivery period display text to latest nomenclature to match recent MyLO Manager changes
- Add new icon to support online unit outlines

**Version 2.4.37**  
_28 June 2023_

-  Fix bug with online/off-campus unit outlines 

**Version 2.4.36**  
_28 June 2023_

-  Fix bug with some Akari unit outlines links 

**Version 2.4.35**  
_27 June 2023_

-  Fix bug with unit outlines links to old units

**Version 2.4.34**  
_27 June 2023_

- Add support for new unit outlines in the navigation bar icon link
- Fix issue with saved data

**Version 2.4.33**  
_14 June 2023_

- Fix status report widget issue when checking for templates
- Fix issue with row colouring not working in analyse grades widget

**Version 2.4.32**  
_25 May 2023_

- Remove default unit settings MyLO Manager feature which is now implemented natively

**Version 2.4.31**  
_23 May 2023_

- Fix issue where unit report was not opening

**Version 2.4.30**  
_17 May 2023_

- Fix issue where navigation bar unit outline link wasn't supporting web page type

**Version 2.4.29**  
_28 April 2023_

- Reintroduce discussions restore button
- Fix issue with expanded Grade Rubrics button not working

**Version 2.4.28**  
_6 April 2023_

- Reintroduce content template reporting
- Display asseessment submissions when showing filtered results
- Adjust callout stripping tool to convert to icon type
- Remove submission filter dropdown and On This Page dialog from Assessment Submissions pages

**Version 2.4.27**  
_22 March 2023_

- Fix issue `Show everyone` button on the assignment submissions page
- Fix issue where the edit web page `[e]` had disappeared from contents
- Introduce new Icon component into the D2L side bar
- Fix issue with group member names showing in `On This Page`
- Fix auditor warning colour
- Fix issue with contextual edit buttons not scrolling down to the point needing edited
- Adjust MyResults-related guidance in the Grades tool

**Version 2.4.26**  
_21 February 2023_

- Fix issue with dual coded unit outline updates

**Version 2.4.25**  
_30 January 2023_

- Fix issue with page editing sidebar shortcuts to insert H5P and Echo360

**Version 2.4.24**  
_24 January 2023_

- Fix issue with widget card layout

**Version 2.4.23**  
_17 January 2023_

- Fix bug where assessment tasks with numbers 10 or more weren't recognised as valid for MyResults
- Reintroduce sticky unit creation settings to MyLO Manager

**Version 2.4.22**  
_15 December 2022_

- Add Submission Status filter dropdown to Observed In Person assignments
- Fix PDF processing issues affecting certain MyLO MATE widgets
- Remove overlapping MyLO Manager features to coincide with central update

**Version 2.4.21**  
_22 November 2022_

- Adds support for assessment overview report on "Observed in person" assessments
- Fix issues for users on Chrome for MacOS

**Version 2.4.20**  
_13 September 2022_

- Add classlist shortcut icon to navigation bar if Classlist link is hidden
- Adjust text to display specific group restrictions for group discussions
- Fix issue with return from student view icon not appearing when Classlist is hidden
- Fix issue with unit search widget checking descriptions
- Fix issue with sidebar hover timing

**Version 2.4.19**  
_14 July 2022_

- Add `[e]` and `[o]` quick links to Rubrics list
- Add additional advanced columns to Rubrics list (attached to ed tech option)
- Change `Clone Topic` feature to `Copy Page`
- Add number of restricted groups to discussions list

**Version 2.4.18**  
_5 July 2022_

- Fix video embed hover getting stuck
- Reintroduce a direct Turnitin link to old interface, attached to ed tech option (note this is not reliable)

**Version 2.4.17**  
_28 June 2022_

- Fix issue with YouTube embeds
- Adds an `[e]` shortcut to edit assignment folders
- Ignores extra spaces in category names when providing advice related to MyResults

**Version 2.4.16**  
_20 June 2022_

- Fix issue with in-line video assignment submission buttons
- Fix issue with quiz attempt log export button

**Version 2.4.15**  
_8 June 2022_

- Escape feedback for assignment report export to CSV
- Update the direct link to edit rubric feature of the assignments tool to the new URL
- Add support to sidebar Insert Video YouTube links using the `/v/` format

**Version 2.4.14**  
_2 June 2022_

- Unit title now restricted to two lines to ensure unit info doesn't move off screen
- Assignment folder obsolete quick links removed due to new UX

**Version 2.4.13**  
_26 May 2022_

- Update the MyResults name generator to reduce confusion around hurdle checkbox
- Fix broken image link in welcome

**Version 2.4.12**  
_23 May 2022_

- Update the MyResults-related guidance to exclude units prior to S2, 2022

**Version 2.4.11**  
_20 May 2022_

- Update the MyResults-related guidance and turn on by default

**Version 2.4.10**  
_16 May 2022_

- Fix group enrol columns being overlapped by the username column
- Add vimeo support to sidebar video embed
- Add shortcut to dev2 MyLO Manager for dev users

**Version 2.4.9**  
_28 April 2022_

- Fix button overlap when viewing assignment submissions in group mode
- Fix issue when attempting to save changes to MyLO MATE user groups
- Fix grades merge error when grades whitesheets are manually edited incorrectly

**Version 2.4.8**  
_14 April 2022_

- Fix issues with the word cloud widget
- Fix issues with the embedded assessment video player

**Version 2.4.7**  
_21 March 2022_

- Change nondescript assignment rubrics icon into a button with text on it
- Fix bug where grades validation beta features sometimes didn't appear
- Fix bug in upload image dialogue used in banner management
- Default support method changed to Service Desk
- Other minor fixes

**Version 2.4.6**  
_7 March 2022_

- Reattach image preview to bottom of banner image selection dialogue
- Correct guidance text when inserting images
- Add Unit Coordinator role to Get Enrolled Staff sandpit widget
- Add direct links to Pebble+ and ATLAS into MyLO MATE panel
- Change guidance in MyLO Manager around Echo360 instructor and Unit Coordinator roles to exclude non-award and sandpit units

**Version 2.4.5**  
_10 February 2022_

- Add guidance in MyLO Manager around Echo360 instructor and Unit Coordinator roles
- Add MyResults-related grade validation and grades category name generator (turn on in Options)
- Add a feedback dialogue after editing sidebar tool `Open all links in new tab` is used
- Add improvements to the the Intellilearn synchronisation tool

**Version 2.4.4**  
_2 February 2022_

- Side panel now remembers which components are expanded and collapsed between page edits
- Introduce a `Use Default Banner` feature into the sidebar when editing pages
- Reintroduce `Open all links in new tab` feature into the sidebar when editing pages
- Special Access icon in Quiz and Assignment tools now displays last end date used
- Adds improvements to the the Intellilearn synchronisation tool so it is easier to manage previous deliveries
- Staff table in MyLO Manager is now sortable by clicking headings
- MyLO Manager Unit Details tab now allows the details to be saved and loaded 
- Fix issue with cancelling video embed

**Version 2.4.3**  
_24 January 2022_

- Fix issue where a zero row would appear in merged results
- Minor visual tweak to merged results table

**Version 2.4.2**  
_20 January 2022_

- Add inline video player for direct videos uploaded to assignments
- Add floating edit button so staff can edit easily regardless of scroll position (see options)
- Improve accessibility of Analyse Grades widget 
- Add Merge Grades settings button to main merge screen
- Fix issue where dual coded units caused issues in MyLO MATE
- Improvements to the check links widget to make it more robust and support more obscure errors
- Minor improvements to the strip tags tool
- Reduce caching time to improve speed by which changes are reflected in MyLO Mate features (e.g. linked rubrics)
- Various other minor visual improvements 

**Version 2.4.1**  
_8 December 2021_

- Add support for new Unit Coordinator role in MyLO Manager groups tool
- Fix issue where unit outline widget failed updating the unit outline
- Fix legend styling in Analyse Grades widget
- Fix occasional undefined values in Analyse Grades widget
- Various widget dashboard visual and usability improvements

**Version 2.4.0**  
_23 November 2021_

- Significant improvements to the UX for multiple widgets, including Check Links, Analyse Grades, Status Report, and the widget dashboard as a whole
- Fix issue with Blackboard Collaborate bulk downloading feature

/^^ Version 2.3.x 
**Version 2.3.18**  
_11 November 2021_

- Allow grades to be merged even if a unit doesn't have a unit code

**Version 2.3.17**  
_8 November 2021_

- Display latest submission date to users in assignment Special Access
- Manage Files select unlinked button now only selects unlinked HTML files
- Removes module export to Word feature - no longer deemed feasible
- Improves `Use D2L Basic Template` and default template setting functionality
- Fix issues with Manage Files unlinked and linked files selection buttons
- Fix issue where focus was in uneditable area, leading to potential uneditable content
- Fix non-functional folder button when in student view
- Fix issues with `Wrap videos with spaces` tool
- Various minor fixes and improvements

**Version 2.3.16**  
_12 October 2021_

- Introduce web page cloning feature
- Strip styles now has a confirmation dialogue
- Change the default initial path for adding files to a page to sit alongside the page itself
- Allow page link checking to occur when viewing in student role
- Fix clearfix display issue when format markers were on
- Fix Brightspace bug where waffle menu search box no longer has focus
- Fix issue where `Clear Content Dates` tool had stopped working
- Fix issue with bulk announements option default 
- Fix console error when viewing past units
- Fix issue with student letter grade preview displayed if empty 
- Fix results per page saving in assignment submissions
- Hide Page Link Checker panel tool in MyLO Manager
- Improvements to the beta tool to export a Brightspace module to Word
- Remove legacy references to jQuery
- Various minor fixes

**Version 2.3.15**  
_7 September 2021_

- Introduce new advanced ed dev feature to bulk delete all announcements
- Improvements to the beta tool to export a Brightspace module to Word
- Fix issue where rounding caused incorrect letter grade to show

**Version 2.3.14**  
_1 September 2021_

- Reintroduce letter grade preview to new assessment experience
- Improvements to the beta tool to export a Brightspace module to Word
- Fix issue where marking assignment by group unexpectedly reverts to view by users
- Fix issue with phantom `m` sometimes appearing alongside assignment titles in list
- Fix some minor issues when viewing past units

**Version 2.3.13**  
_25 August 2021_

- Add tool to allow Student View to be added and removed from groups in bulk
- Introduce support for new Brightspace WYSIWYG editor, including fixing sidebar Insert options
- Remove scroll arrows in new rubric tool
- Introduce alpha version of tool to export a Brightspace module to Word
- Reduce API cache to improve responsiveness of changes
- Fix LTI link checker in status report
- Fix bulk rubric status setting tool
- Fix issue where H5P editing caused issues when opened in multiple tabs
- Fix issue where empty `<p>` tags were sometimes removed erroneously
- Fix error when viewing the Manage Grades tool in past units
- Fix ICB quote citation conversion
- Fix sticky headers in Manage Files
- Various other minor fixes and tweaks

**Version 2.3.12**  
_21 July 2021_

- Improve H5P editing to allow more screen real estate
- Improve discussions role handling so Past Student role is displayed correctly
- Fix issues with Impersonate Student not working in certain tools
- Stop certain scripts erroneously running when in Impersonate Students
- Fix issues with pasting into accordions
- Fix issues with the accordion remover tool leaving bits behind
- Fix some false positives in the link checker widget

**Version 2.3.11**  
_19 July 2021_

- Improve H5P editing experience with wider editing dialogue
- Fix issues relating to accordion formatting and functionality
- Fix cleanup after removing accordion and keeping content
- Fix issue with banner being hidden after editing code

**Version 2.3.10**  
_14 July 2021_

- Add a link to the bottom of web pages to the Manage Files parent folder
- Add a callout style toggle option to switch icon on and off
- Improves unit status report to better display templating information, Talis reading lists and broken topics
- Fix Echo360 bulk downloader
- Fix impersonate view student feature when launched from Awards tool
- Fix `Get Enrolled Staff` sandpit widget
- Fix some banner cropping issues
- Fix issue with deselected tab lists nested within callout boxes
- Various minor fixes and improvements

**Version 2.3.9**  
_29 June 2021_

- Introduce new tool to allow tabs to have no selected default
- Fix issue with MyLO Manager User Groups disappearing and becoming 'undefined'
- Fix various minor issues with banner defaults and cropping
- Fix issue where sometimes banner failed if image file names were long and funky
- Migrate tools from echo360.org.au to echo360.net.au
- Minor changes to wording and layout of MyLO MATE Checker/widget dashboard

**Version 2.3.8**  
_22 June 2021_

- Combine extension checker widget with the Widgets Dashboard
- Add `Last Marked By` column to the assignment report tool, and change to icon
- Fix issue where discussion posts would sometimes extend beyond the table width
- Add clearfix visual indicator when format marks is on
- Fix issue where `Show Format Marks` wasn't toggling correctly
- Moves the `Use Default Banner` tool into the sidebar
- Change assignment report link from text to icon to make it stand out better
- Improves the Find Blocked tool (in the Search widget) to indicate Echo360 and YouTube types
- Fix issue where some loader spinners remained on screen

**Version 2.3.7**  
_7 June 2021_

- Add bulk LTI checker as new sandpit widget
- Add LTI link checker to Status Report widget
- Improve usability of the `Upload Page Image` dialogue
- Change wording and layout of some D2L sidebar tools to improve usability
- Fix issue where User Groups in MyLO Manager would sometimes encounter overflow error
- Fix issue where sometime two items were selected in the Choose Page Image dialogue
- Fix issues with layout of Grades Analysis tool
- Fix issues with the placement of some MyLO MATE buttons
- Fix issue with Turnitin bulk submission button not becoming enabled
- Fix issue with direct link for videos changing to embed link

**Version 2.3.6**  
_28 May 2021_

- Update to handle most recent Brightspace version HTML changes
- Editor sidebar location moved to local file
- Fix for when callout remove droplets remain in place
- Fix broken bulk discussion dates dialogue again
- Improve sidebar tool styling further

**Version 2.3.5**  
_25 May 2021_

- D2L sidebar items now contextually grouped
- Widget dashboard headings fixed
- `Get Staff from Unit` widget now supports Past Support role
- Fix issue when inserting quicklink
- Fix Retract Feedback button
- Fix broken bulk discussion dates dialogue
- Reduce chance of `Convert to D2L` button not appearing

**Version 2.3.4**  
_24 May 2021_

- Accessibility information (e.g. `(pdf, 123KB)`) now added to file links by default, and can be toggled on and off
- Allow images to be uploaded while using the `Choose Page Image` dialogue
- Introduce `Remove callout container` feature, appears in top-right corner of callout
- Override and hide new marking rubric slider functionality in prod
- `Unit banner image is selected` displays in `Choose Page Image` dialogue to make it clear
- When creating a new rubric in MyLO, `Hide scores from students` is now selected by default
- `Use D2L Basic Template` button now refreshes page so template list updates
- Various improvements to the ICB to D2L template conversion tool
- Improves usability of the widgets dashboard by adding scroll shadows to indicate more content
- Add `Clear all dates` button to bulk discussion dates dialogue
- Fix issue where sometimes cropped banner image didn't display due to caching
- Fix issue where rubric shortcut link sometimes didn't show next to assignment folder
- Fix issue where strip styles button didn't appear in TinyMCE
- Fix issues with the new TinyMCE in dev2
- Fix broken bulk discussion dates dialogue
- Fix issue where some passwords weren't accepted in the export to Intellilearn feature
- Introduce dev2 marking criterion ranges table for new marking rubric slider feature

**Version 2.3.3**  
_20 April 2021_

- Fix issue where HTML editor did not initialise correctly in some cases

**Version 2.3.2**  
_20 April 2021_

- Widgets consolidated and moved to Widgets Dashboard
- Add new ed tech feature `Change Grades Visibility` to quickly hide numerical grades from students
- Add widget support for Accelerated Study Periods 1-3
- Add support for TinyMCE v5
- Minor improvements and fixes to the Word Cloud widget

**Version 2.3.1**  
_31 March 2021_

- Add instructor role toggle to MyLO Manager
- Improve banner dialogue for smaller screens
- Improve visibility of open image in new tab feature
- Improve ICB to D2L conversion function to handle panels better
- Add `Use Default Banner` button if one is defined and it's not already in use
- Improve speed and accuracy of when edit web page button appears
- Fix bug where certain icons duplicated themselves when working in File Manager
- Improve loading speed of D2L page editing by excluding ICB sidebar
- Fix bug where editor buttons (e.g. view source) needed to be clicked twice

**Version 2.3.0**  
_23 March 2021_

- Add ability to define a unit-wide default D2L template for all new/converted pages
- Change default to banner images being off when pages are created/converted
- Add `Export Users Without Attempt` feature to Quiz tool
- Add alphabetical reordering feature to Discussions Reorder tool
- Add Echo360 role support to MyLO Manager User Groups tool
- Add new Blackboard Collaborate bulk download feature
- Add tab group of 5 to the sidebar
- Change `YouTube Embed` to `Video Embed`, and support both YouTube and Echo360 public links
- Remove non-functional edit button when impersonating
- Fix bug where `Toggle Format Marks` wouldn't persist between page edits
- Auto-fix for when D2LT pages become corrupted due to missing header or footer
- Improves floating save bar functionality in Manage Files
- Update UTAS logos to new branding

/^^ Version 2.2.x
**Version 2.2.34**  
_10 February 2021_

- Fix Assessment `Publish Feedback` button by moving the MyLO MATE buttons to the next row
- Allow MyLO MATE to support award units that don't have a unit code
- Echo360 bulk downloader fixed
- Hidden input tags removed on save
- Nested tab content panel visibility fixed
- Format marks are now persistent
- Introduce banner image crop and resize tool
- Improve Classlist Intellilearn sync feature

**Version 2.2.33**  
_20 January 2021_

- Fix issue where auto-guess path option wasn't able to be changed
- Update table of contents upload file path to best guess path
- Fix bulk visibility tool for discussions
- D2LT: Introduce format markers
- D2LT: Auto-convert attention panels to Jumbotron
- Strip tags no longer removes callout icons
- Introduce Intellilearn sync feature to Classlist
- Update MyLO MATE install image link

**Version 2.2.32**  
_16 December 2020_

- D2LT: Improve conversion to handle Wicking ICB customisations better
- D2LT: Fix issue with linked captions turning into plain text
- Reintroduce fixed assignments `Uncheck All Dates` button
- Default to using whitepaper grades with merging

**Version 2.2.31**  
_7 December 2020_

- Introduce release condition tooltips when hovering the icon for most tools
- D2LT: Change the default template for units when switching a new (blank) web page
- D2LT: Add new Case Study icon for icon callout panels

**Version 2.2.30**  
_30 November 2020_

- Fix bug in background script

**Version 2.2.29**  
_30 November 2020_

- Fix issue where sometimes images disappeared when converting between templates
- Adjust button availability to simplify web page transfer from ICB to D2L Template

**Version 2.2.28**  
_26 November 2020_

- Adjust defaults to allow easier web page transfer from ICB to D2L Template
- Fix issue with `Get Absent Students` returning invalid Email
- Fix `Show enrolled first` group sorting checkbox
- Fix issue with Rubric import/export opener
- Add Settings panel to Results Merge widget to allow for different school processes
- Introduce network request limiting in grades analysis widget
- View settings for assignment folders retained
- Add prompt for Intellilearn export classlist group id, allow for generic export

**Version 2.2.27**  
_4 November 2020_

- Fix issue where group restrictions caused results merge fail
- Fix issue where empty grade caused results merge fail
- Introduce Intellilearn export classlist button
- D2LT: Allow `Apply Template` button to be displayed even when D2LT not default, with the Always Show option

**Version 2.2.26**  
_28 October 2020_

- Results merging points numerator fixed to two decimal points
- Results merging feature now respects restricting to selected groups
- Bulk `Submit to Turnitin` disabled where there are no files that can be submitted

**Version 2.2.25**  
_26 October 2020_

- Fix issue where some column headings were missing in merged results spreadsheet
- Amend column widths in merged results spreadsheet to ensure data is more readable
- Adds groups support to assignment report and incomplete rubrics

**Version 2.2.24**  
_22 October 2020_

- Add new bulk `Retract Submissions` button to assignments tool
- Incomplete rubrics report now also detects text-only rubrics
- Allow columns to be sorted in results export preview
- Allow letter grading to be overwritten in results export preview
- Display numerator and denominator in results export preview
- D2LT: Option to add spacing around video components
- Add new optional dev1 shortcut to MyLO MATE panel
- Improve Search widget PDF searching efficiency

**Version 2.2.23**  
_2 October 2020_

- Fix issue where csv export was missing username column
- Fix issue where numerical columns in `Merge Results` were saved as text
- Fix issue where `Merge Results` failed if student ID was non-standard (e.g. sample students)

**Version 2.2.22**  
_1 October 2020_

- Various fixes related to updated Brightspace environment (mainly Groups-related)
- Fix issue with checkbox changes in search unit widget
- Improvements to the `Merge Results` beta in Grades Analysis widget
- D2LT: Make D2L default button more available
- D2LT: Fix issue with template change button

**Version 2.2.21**  
_21 September 2020_

- Introduce CoBE audit spreadsheet exporter
- Introduce basics of `MyLO MATE Missing` feature widget
- Fix for ICB components disappearing on mouse out
- Fix for when API cache gets too big

**Version 2.2.20**  
_16 September 2020_

- Introduce `Merge Results` beta to Grades Analysis widget
- Introduce content page Export User Progress button
- Introduce option to enrol in selected groups
- Move group enrolment pagination to the left side of the screen
- Fix issue where group assignments didn't show the group name in the assignment report
- D2LT: Fix issue where components could not be added to empty content
- Fix issue where Incomplete Rubrics dialog wouldn't fit on the screen if too many incomplete
- Fix unit subtitle being cut off if longer than unit title
- Change ICB components to display actual spacing

**Version 2.2.19**  
_28 August 2020_

- Fix for D2L Templates sidebar appearing when unneeded
- Fix for Group enrolment count
- Fix for grades mismatch indicator

**Version 2.2.18**  
_25 August 2020_

- Introduce `Merge Groups` tool to merge enrolled users in different groups into one group
- Introduce `Split Groups` tool to split enrolled users in one group into different groups
- Introduce page sorting options to Enrol Users tool to allow grouping enrolled, sorting by last accessed and post count
- Group enrolment count now includes restricted users
- Assignment visibility report improved, throbbing icon introduced while loading
- Fix for grades mismatch indicator to account for inactive items

**Version 2.2.17**  
_18 August 2020_

- Add topic and reply count for each student in `Enrol Users` tool
- Fix bug affecting enrolled student count, and tweaks text to be more helpful
- Add quicklink to jump from `Manage Groups` to `Enrol Users`

**Version 2.2.16**  
_11 August 2020_

- New feature added to show enrolled student count next to each group category
- Improves Assignments `Show Everyone` feature to work better with groups
- Fix issues with assignment grade visibility report
- Add impact survey notification

**Version 2.2.15**  
_4 August 2020_

- Fix issue where phantom button appeared in discussions when window was resized
- Improve Echo360 Insert Stuff shortcut button
- Fix issue where bulk uncheck dates tool was failing
- D2LT: Fix issue where last sidebar item was not accessible when window was resized

**Version 2.2.14**  
_28 July 2020_

- Improves embed blocks to make it harder to recursively embed blocks
- Fix issue in Status Checker where new ICB template path wasn't recognised
- Fix issue with widget visibility arrows
- Fix issue where discussions search defaults weren't overridden to check first and last names

**Version 2.2.13**  
_21 July 2020_

- Fix bug where rubric extraction was broken
- D2LT: Fix bug where sidebar Strip Tags didn't work in some circumstances
- D2LT: Fix issue where sidebar lead style sometimes didn't display correctly while editing
- D2LT: Change sidebar `Video Embed` to `YouTube Embed` and `Custom Embed` with `Other Embed`
- Add MyLO MATE badge to TinyMCE remove styles button
- Replace old Unit Outline widget with new, updated version
- Introduce a new feature that flags visibility differences between grade items and what they're linked to

**Version 2.2.12**  
_14 July 2020_

- Fix issue where Echo360 download clips tool was missing from panel
- Fix search widget not examining titles or descriptions
- Introduce new unit outline upload widget for testing
- Apply red colour to key symbol in Assignments when special access enabled

**Version 2.2.11**  
_7 July 2020_

- D2LT: Fix bug when pasting content

**Version 2.2.10**  
_7 July 2020_

- Introduce bulk content module date removal feature
- Fix bug where bulk group renaming renamed all rows
- Fix bug where rubric downloader had stopped working
- Fix Echo360 bulk downloader that failed when non-video files were encountered
- Fix issue where save bar scrolled off the screen when in enrol groups
- D2LT: Add a recommended image size message when changing banner images
- Update `Grades Analysis` widget to show a warning if the final grades are set up incorrectly
- Improve `Check page links` tool so it works on a wider variety of page templates
- Fix horrific typo in impersonate student prompt

**Version 2.2.9**  
_23 June 2020_

- Update Echo360 bulk downloader that had broken due to a recent update
- Add option to move Grades Analysis widget to the top of the unit home page
- New feature added to allow bulk download of quiz attempt logs
- Fix bug where style stripping icon wasn't appearing in some TinyMCE footer instances (e.g. edit web page)
- Fix bug where assignment conditional release hover text feature wasn't appearing
- Fix bug where discussions export feature wasn't appearing
- Fix bug where bulk restore deleted discussion posts feature wasn't appearing
- Fix issue where assignment submission evaluation report was sometimes showing out-of-date information
- D2LT: Sidebar `IFrame Embed` changed to `Custom Embed` to better reflect its functionality
- D2LT: Introduce ILO icon option to icon panel callouts
- Remove obsolete bulk delete bug fix (fixed in Brightspace itself now)
- Feature documentation table tweaks to add accordions per tool
- Various feature documentation text updates
- Other minor bug fixes

**Version 2.2.8**  
_12 June 2020_

- Custom embedded components now able to be deleted more easily
- Assessment report hidden on group-based submission folders
- Fix bug where console errors appeared with units that have no groups (non-award errors)

**Version 2.2.7**  
_9 June 2020_

- Introduce bulk `Set Rubric Status` feature
- Introduce `Export Submissions by Status` feature
- Introduce quick switch button to change existing page to D2L Templated page
- Add rubric student visibility icon
- Add custom iframe feature to D2LT sidebar
- Update Echo360 bulk downloader that had broken due to a recent update
- Fix bug where jump to top and bottom arrows were begin covered by the floating save bar
- Fix bug where edit page button didn't appear immediately upon creating a new web page

**Version 2.2.6**  
_2 June 2020_

- Start to introduce feature documentation to code for easy extraction
- Improve Status Report widget to better display information relating to D2L templates (as distinct from ICB templates)
- Restructure Release Notes section

**Version 2.2.5**  
_28 May 2020_

- Change Incomplete Rubrics functionality when looking at a group instead of the whole class
- Fix bug where image upload dialog wasn't removed when editing web pages
- Update status report widget to support multiple template types
- Fix bug affecting importing of settings

**Version 2.2.4**  
_26 May 2020_

- Add external link styling to D2L Template links
- Introduce divider component to D2L Templates sidebar
- Change paste behaviour so D2L Template content is more difficult to break
- Improving sidebar overlap handling
- Stop backspace and delete removing editable regions
- Add option to show template switch button even when template is already set to another system
- Fix sidebar disappearing when editing full screen
- Minor tweaks and improvements

**Version 2.2.3**  
_20 May 2020_

- Adjust sidebar so it doesn't overlap editor
- Change Chrome version checking to hard coded v80

**Version 2.2.2**  
_19 May 2020_

- Fix D2L Sidebar error accessing library

**Version 2.2.1**  
_19 May 2020_

- Introduce unit settings button that changes the default template to D2L templates
- Introduce image browser to make replacing D2L Templates banner image
- Moves D2L sidebar actions to shared central location
- Adjust D2L sidebar so components can't be added to heading region
- Update Find Lecturers widget to enable granular role filtering
- Override the sidebar caching frequency so stale data doesn't hang around
- Change navigation bar buttons default to ON
- Allows ICB and D2L sidebars to operate in full screen mode
- Fixes issue with ICB image containers
- Introduces legacy Echo360 embed option
- Fixes bug with path preset

**Version 2.2.0**  
_12 May 2020_

- Add a new optional Template Chooser widget to unit home pages
- Introduce UTAS-wide page link checker to MyLO MATE panel
- Discussions Search now has first and last name checked by default
- Improve layout of the Incomplete Rubric Report
- Add option to toggle horizontal scrolling arrows overriding
- Fix overriding horizontal scrolling arrows
- Add warning banner to MyLO MATE panel when Chrome version is old
- Improve file path prefill functionality, leaving it alone if a module has already set a path
- Add path display when creating new web pages
- Fix bug that stopped links being checked properly
- Fix page refresh bug related to page display count

/^^ Version 2.1.x
**Version 2.1.86**  
_27 April 2020_

- Fix bug in Import MyLO Manager User Groups
- Fix focus issue with discussion expand and collapse buttons
- Remove optional chaining which caused failure in older Chrome versions

**Version 2.1.85**  
_24 April 2020_

- Add a new option to expand and collapse discussion threads
- Replace FAQs link with a link to the Teaching and Learning web site which includes a summary of all resources available
- Fix various minor null check and MyLO Manager user groups issues

**Version 2.1.84**  
_8 April 2020_

- Fix bug where MyLO Manager group users were added as auditor instead of what had been selected
- Other minor MyLO Manager group improvements
- Fix typo in MyLO MATE options

**Version 2.1.83**  
_3 April 2020_

- Adds option to show full path of web pages at the bottom of the page without needing to edit
- Fix weird bug that affected the blocked links checker with malformed URLs
- Various minor feature fixes to handle broken content links better

**Version 2.1.82**  
_31 March 2020_

- Fix bug that broke Link Checker tool
- Fix null check in Find Blocked links tool
- Fix permissions issue that affected link checker
- Fix bug that affected Discussions enhancements
- Significant improvements in performance and reliability of unique group ID feature
- Significant improvements in performance and reliability of bulk group renaming feature

**Version 2.1.81**  
_27 March 2020_

- Assignment Report headers now sticky, so headings stay visible while scrolling down
- Introduce a bulk `Save` button to the `Enrol in Groups` page
- Introduce new `Impersonate View Student` method that is faster and more flexible, allow more opportunity to stay on the page you're viewing
- Improve the `Get Lecturers from Units` widget with better feedback when analysis completes
- Hide various MyLO MATE tools that don't make sense in Student View
- Fix bug where extension failed on class progress summary pages
- Fix MyLO Manager user groups `undefined` bug

**Version 2.1.80**  
_24 March 2020_

- Add link to online teaching FAQs in popup panel and in notification
- Introduces `Export Posts` to CSV button for Discussion Topics
- Link checker functionality added to web page HTML editor
- Minor usability tweaks to extension options window styling
- Fix various minor issues in the MyLO Manager user groups options
- Add new dialog providing more information on using MyLO Manager user groups through VPN
- Fixes console error on pages where impersonate view student button wasn't valid
- Fixes bug where withdrawn students were not being displayed correctly

**Version 2.1.79**  
_6 March 2020_

- Usability improvements to MyLO Manager groups functionality
- MyLO Manager `Manage Groups` button now expands the appropriate section of the options
- Introduces `Generate "Inactive Students" spreadsheet` button to Groups tool

**Version 2.1.78**  
_3 March 2020_

- Groups enrolment sorted to show checked students first
- Last accessed info shown under student names in `Enter Grades` tool
- MyLO Manager groups functionality update to allow a group to be favourited and thus preselected
- Update `Find Blocked Links` feature to search through PPTX files
- Extension options changed to work with Edge Chromium as well as Chrome
- Remove last remaining jQuery references
- Removes remaining references to the now obsolete `Editable Pages Checker` widget
- Fixes bug where Word Cloud would sometimes fail to build
- Other minor bugs and tweaks fixed

**Version 2.1.77**  
_19 February 2020_

- Update `Find Blocked Links` feature to search through checklists
- Update `Find Blocked Links` feature to add `Copy` and `Save` buttons to allow results to be used easily
- Adds student ID to Assignment Report tool, and withdrawn students now excluded from report
- Introduce new `Enrol and Open` option for faculty support to quickly enrol as support and open the unit

**Version 2.1.76**  
_18 February 2020_

- Update `Find Blocked Links` feature to search through module descriptions and to test internally linked PDFs and Word docs
- Improve layout of `Find Blocked Links` results to be more readable, and fixes order
- Improve `Impersonate View Student` function to stay on viewed page where possible
- If the `UnitOutline` folder is missing entirely, the `Unit Outline Uploader` widget will now create it

**Version 2.1.75**  
_17 February 2020_

- Update `Find Blocked Links` feature to search through module descriptions

**Version 2.1.74**  
_13 February 2020_

- Fix bug that occasionally caused the Manage Files tool to be blank
- Display warning banner for those with auditor role in a unit

**Version 2.1.73**  
_12 February 2020_

- Bulk hide/show feature introduced to Discussions
- `Find Blocked Links` button added to unit search widget
- Search widget now searches links in PDF and DOCX
- Assignment submissions system adjusted to be more flexible
- Removes now obselete `Editable Pages Checker` widget

**Version 2.1.72**  
_10 February 2020_

- Fixes bug where assignment report wouldn't appear

**Version 2.1.71**  
_28 January 2020_

- `Status Report` widget now combines the two unit outline checks (exists and is not placeholder)
- Fixes `Status Report` widget console error
- Update `Grades Analysis` widget to allow for past roles
- Fix bug where grades quick links didn't appear since last Brightspace update
- Fix bug where link checker dialog couldn't be dismissed with the cross
- Ensure search widget results are refreshed each time a search is performed
- Fixes bug in link checker which could flag images or videos as empty links

**Version 2.1.70**  
_7 January 2020_

- Move ICB domain from `health.utas.edu.au` to `icb.utas.edu.au`
- Fixes bug in link checker which could flag images or videos as empty links
- Discussion lists now have hover text explaining what the role badges (S, L, etc) mean
- Search unit widget now has two extra options: `Only check HTML pages` and `Only check visible content`
- A warning dialog has now been added when LTI links (e.g. Echo360) are followed in student view
- New `Email Lecturers` navigation button added that offers Emailing all active lecturers in a unit (off by default)

**Version 2.1.69**  
_29 November 2019_

- Fixes bug where Echo360 bulk downloads only downloaded the first video
- Hides several features that shouldn't be visible in student view
- Revamped in-page link checker introduced
- Updates rubric icon in assignments to ensure it always gets fresh data
- Updates search widget to allow element attribute searching
- Various other tweaks and fixes

**Version 2.1.68**  
_15 November 2019_

- Grades update widget: Bug fixed that affected groups retrieval
- Grades update widget: Grades download changed to standard Brightspace export structure

**Version 2.1.67**  
_12 November 2019_

- Hides in-page link checker temporarily while longer term fixes are implemented (please use unit home page widget in the interim)
- Introduce empty link zapper icon to replace the scary prompt on edit
- Introduce pages blacklist to stop running on, for example, error pages
- Permissions adjusted to ensure fast publishing. Users will get a permissions request on first use (e.g. link checker)
- Show/hide grades test added (for internal use)
- E2E code usage report added (for internal use)
- MyLO home page waffle unit date indication colours fixed
- Various minor fixes

**Version 2.1.66**  
_29 October 2019_

- Unit Outline Uploader updated to ensure any manually created links remain pointing to the correct file
- Get Absent Students widget now overhauled to use new API endpoint and is now almost instant
- Quizzes that have special access set to only allow specific users access now highlighted in red
- Empty text links now automatically removed (after prompt) when editing an affected page
- Page link checker now better flags empty text links for attention
- Link checker widget now better flags empty text links for attention
- Link checker widget displays the text link so the user can find it easier
- `Special Access` key icon now coloured red if `Allow only users with special access to see this quiz` is selected
- Introduces an `Editable Pages Checker` widget that allows staff to check their units for any units affected by the bug in `2.1.64`
- Move word cloud ignored words heading below cloud shape radio so it's clear what the box is for
- Fix `Show selected` grades which had stopped working in some circumstances
- Changes to the web page editor to work better with the new 'draft' checkbox
- Change icons and wording in discussions to reduce confusion due to the new proliferation of visibility eyeballs throughout Brightspace
- Other minor bug fixes and interface tweaks

**Version 2.1.65**  
_7 October 2019_

- Fix ICB bug

**Version 2.1.64**  
_4 October 2019_

- Update styles to ensure bulk edit tools in Groups tool are hidden when required
- Improves File Manager bulk rename functionality to hide buttons when required and tell user to select something when needed
- Fix issue where `Bulk Uncheck Dates` pretended to do something when nothing was selected
- `Unique Group IDs` tool now prompts telling the user what it's about to do, and allows them the opportunity to cancel
- Fixes bug in Grades tool where `Hide selected` and `Show selected` failed in some circumstances
- Updates discussion caching to re-request cached items where an intermittent error occurred
- Reintroduce a spinner while loading discussion content
- Fixes horizontal scrolling issue
- Widgets now are able to collapse and expand, and the state is saved between sessions
- Restricts ICB heading editing to H1 and H2

**Version 2.1.63**  
_27 September 2019_

- Fix for misbehaving `Save as draft and close`, `Publish` and `Save` buttons in `Edit Web Page`
- Fix for beta version display in extension panel
- Add support for accelerated units
- Introduce some automated UI testing through Jest and Puppeteer
- Fix for huge, intimidating eyes in discussions
- Fix role checking to be more robust
- Fixes console 403 errors when impersonating a student
- Minor visual tweaks

**Version 2.1.62**  
_20 September 2019_

- Optional link to H5P is added for trial participants
- Flagged rows in the assessment report are now highlighted so they stand out better
- Fix for bug where user groups were being removed when they were updated

**Version 2.1.61**  
_13 September 2019_

- Display the `Last accessed` information for each student in the group `Enrol Users` page
- Introduce new advanced feature: `Bulk Uncheck Dates` which allows ed dev teams to remove all release date information from a quiz
- Introduce new assignment report feature with the `[a]` link next to any assignment with submissions, and allows the report to be saved as CSV
- Attendance columns 'stickiness' fixed and enhanced
- Various minor fixes

**Version 2.1.60**  
_27 August 2019_

- Loading spinner added for `Impersonate Student` function
- Minor formatting fixes

**Version 2.1.59**  
_16 August 2019_

- Fix bug where `Edit Web Page` link disappeared
- Introduce new network caching method to speed up various caching areas, e.g. discussions
- Override MyLO Manager basic view unit status colours to be more accessible
- Hide `Rubric Report` button when no rubric found
- Fix for rubric exporter functions breaking

**Version 2.1.58**  
_23 July 2019_

- New tool added to allow content to be bulk published or put in draft
- Attendance table now has sticky columns
- Each tool now remembers the page count last used
- Fix for rubrics not loading into rubric generator
- `Manage Files` tool now has sticky toolbar and column headers
- Adds a bulk file renamer to the `Manage Files` tool, attached to Ed Tech option
- New tool added to assessments that automatically highlights rubrics that are incomplete
- Improvements to the `Word Cloud` widget, now defaults to being on for all users
- Fixes missing `View Rubrics` button when importing a rubric
- Now supports d2ldev1 (in addition to d2ldev2)
- Introduces new common dialog for MyLO Mate
- Various minor bug fixes and improvements

**Version 2.1.57**  
_28 June 2019_

- Spinner added to discussions loading text to better indicate to user that something is happening
- Widens the group name text boxes when performing a group bulk edit
- Introduces a beta `Word Cloud` widget to unit home pages (off in options by default for now)
- Re-introduces `Import Rubric from File` button that a recent Brightspace update destroyed
- Introduces a new `Report a Bug` feature to the MyLO MATE panel (bottom-right corner)
- In discussions, the hover text of a student profile icon now includes the groups they're enrolled in
- Email address added the output of the `Get Absent Students` widget
- `Export Grades` shortcut link added to `Manage Grades` tool under the `More Actions` drop-down
- Link checker widget error messages improved

**Version 2.1.56**  
_24 May 2019_

- Option added to change panel link to MyLO Manager to open Advanced View
- Link checker now opens links in a new window by default
- Show rubric buttons regardless of whether the unit is open or not
- Fixes bug where duplicate user dialogues were sometimes opened in the Options
- Various other minor bug fixes

**Version 2.1.55**  
_14 May 2019_

- File Manager now displays content location on hover
- Submission filter is now remembered between sessions
- Grade items that have date restrictions now have the dates displayed on hover

**Version 2.1.54**  
_3 May 2019_

- Fixes bottom `Options` link in extension popup
- Fixes dodgy loading spinner in grades area

**Version 2.1.53**  
_30 April 2019_

- Resolves formatting problems in the assignment marking areas
- Fixes issue where Ed Tech options were enabled despite the parent checkbox being off in the options
- Adds tooltip text to explain what the red outline in grades means (rounding will cause grade change)
- Adds an extra `MyLO MATE Options` link in the extension popup
- Fixes a formatting error in the grades list
- Tech debt

**Version 2.1.52**  
_16 April 2019_

- Reintroduce rubric export functionality with more features and flexibility than before
- Status Report widget error handling improved
- Old SEA widget removed
- User groups dialog formatting improved in MyLO Manager
- Groups are now able to have unique IDs assigned in bulk (this allows central bulk enrolment to be possible)

**Version 2.1.51**  
_29 March 2019_

- User Groups now have the option to override roles for staff already in the unit
- References to Rubric Generator Beta removed
- Absent students widget no longer downloads empty zip files if no absent students were found
- Various bug fixes related to the new release of Brightspace
- Temporarily remove `Element.createShadowRoot` related functions (e.g. export rubric)

**Version 2.1.50**  
_26 March 2019_

- Fixes bug introduced by auto-formatting
- Adds error checking for rubric pages in older units

**Version 2.1.49**  
_26 March 2019_

- Introduces `Download as Word Document` link next to each available rubric
- Adds a new `Restore Posts` to bulk restore deleted discussion posts
- Adds links to student profiles next to their names in discussion posts
- Corrects the wording of the checkbox in the unit outline uploader widget
- A couple of minor fixes and text changes

**Version 2.1.48**  
_12 March 2019_

- `Unit Outline Uploader` widget now supports multiple cohorts
- `Unit Outline Uploader` widget now on by default
- `Unit Outline Uploader` widget defaults to archiving old unit outlines
- Bulk Group Renaming feature now supports automatic renaming, prefixing and suffixing
- Various wording tweaks and changes

**Version 2.1.47**  
_1 March 2019_

- New `Status Report` widget is introduced
- New `Unit Outline Uploader` widget is introduced
- Discussions jump arrows fixed

**Version 2.1.46**  
_15 February 2019_

- Adjust MyLO rubrics to rubric generator interface to better deal with recent Brightspace changes
- Now exports to new (beta) version of rubric generator by default
- Fix bug where Ed Tech options weren't turned off when the parent checkbox in the options was unticked
- Introduce `Bulk Email Students` button allowing bulk Emailing class sizes greater than 3000
- Introduce new `Create Hover Text` feature in ICB editor

**Version 2.1.45**  
_1 February 2019_

- Integrate both the old and the new rubric maker export methods
- Update the import rubric button

**Version 2.1.44**  
_29 January 2019_

- Long titles no longer truncated
- Adds bulk hide/show button for grade items
- Adds bulk `Uncheck all dates` button when editing multiple assignments
- Introduces a new `Enable advanced Ed Tech features` option in `Other` to enable the above two potentially dangerous features
- Removes obsolete MyLO Manager features to coincide with release of new version (row date colouring, quicklinks, etc)
- Introduces accessibility override for green row colours in MyLO Manager advanced view
- Multiple minor fixes to coincide with release of new version of MyLO Manager
- Minor reorganisation and wording changes for clarity in the extension options
- Fixes relating to new release of Brightspace (classes relating to Rubric linking and export, User Progress settings, SEA widget)

**Version 2.1.43**  
_11 January 2019_

- Adds pagination (Results per page) drop-down at the top of the list of Assignments
- Fixes bug where `Impersonate Student View` had stopped working with the new version of Brightspace
- Adds option to show formatting marks in ICB HTML editor automatically
- Fixes bug where assignments appeared in a tiny window with the new version of Brightspace

**Version 2.1.42**  
_14 December 2018_

- Fixes bug where archived rubrics were no longer showing
- `Save as Draft and Close` button fixed
- Home page and unit heading coloured circles fixed
- Fixes several issues that would have caused problems with the upcoming version of Brightspace

**Version 2.1.41**  
_30 November 2018_

- Re-introduces the `Download Clips` function for Echo360, updated solution implemented
- Assignment release conditions now show more detail on the conditions attached on hover
- Adds new `Absent Students` sandpit-specific widget

**Version 2.1.40**  
_23 November 2018_

- Removes the `Download Clips` function for Echo360 as the site functionality has changed, making our solution break
- Corrects occasional positioning problems with the User Progress settings information panel
- Introduces coloured role indicator lines to staff profile images
- Adds support for paragraph markers in the HTML editor
- Fixes rogue background colour in My Units widget
- Repairs `Show All Students` feature in Assignment Folders that broke with the latest release
- Adds a `m` feature badge to the `Submit files to Turnitin` button

**Version 2.1.39**  
_6 November 2018_

- Adds a new `[a]` attachment quicklink to each grade item opens the attached quiz, discussion or assignment folder
- Adds a new `[g]` grade quicklink to each quiz title
- Adds a new `[f]` folder quicklink next to local content pages, that opens the containing folder in `Manage Files`
- Adjust MyLO MATE beta to never show update notification panel
- Introduces the option to default to export to the new beta rubric generator
- Fixes bug where `Email Users Without Submissions` was not working
- Fixes bug where some characters in a criterion caused issues exporting a rubric to the generator
- Fixes bug where sometimes `[s]` appeared next to student names in discussions

**Version 2.1.38**  
_23 October 2018_

- Fixes `Save as Draft and Close` button for OER instance
- Fixes style problem where some ICB links were not clickable
- Improves functionality of link checker when examining domains that time out
- Handles logging out better
- Popup dialog no longer relies on JQuery
- Fixes Grades Analysis widget bug
- Removes some old API references

**Version 2.1.37**  
_10 October 2018_

- Reorganises the MyLO section in the options - it was getting to be too much in the one tab
- Fixes widget rendering bug in dev2
- `Import/Export Components` tool now has date row colouring when searching for a unit to import from
- Fixes bug where icon generator was trying to apply itself to PDF files
- Changes the way options are managed to make it easier to maintain
- Introduces option to `Default to 'Show everyone' in Assignment Submissions`. Off by default
- Introduces option to `Always show archived rubrics`. Off by default
- Introduces better handling for certain dodgy web links in the link checker (including timeouts)
- Introduces a new `Submit files to Turnitin` button which submits unsubmitted files in bulk
- Defaults to check all `Question in use` checkboxes when updating a question in the question library
- Adds a quick link to edit web pages from content tool
- Fixes a minor bug where clicking back in `Manage Files` wouldn't correctly highlight the root node

**Version 2.1.36**  
_2 October 2018_

- Fixes a bug where `Save as draft and close` decided to publish the file
- In link checker widget, introduce a 10 second timeout for problematic URLs
- When checking page status (e.g. link checker), include check to guard against double encoding URLs

**Version 2.1.35**  
_28 September 2018_

- Updates the link types that will be modified when saving in the HTML editor (i.e. ignoring `mailto:` and `javascript:` and so on)
- Fixes bug where units with multiple unit outlines didn't show the relevant unit outline links correctly
- Default options now stored internally as standard JSON files
- Fixes bug where rubric XML export failed if a criterion name included an `&` symbol
- Fixes group enrol-related functionality due to Brightspace URL change
- Improves `Select All` functionality in group `Enrol Users` tool
- Introduces optional browser history management to the manage files tool
- Introduces Import/Export Settings functionality for all settings and for User Groups
- Fixes bug where sometimes ezproxy (reading list) links weren't corrected
- Various other minor tweaks and fixes

**Version 2.1.34**  
_7 September 2018_

- Introduces automated beta branch for early access and testing purposes
- Title attribute added to discussion quick links
- Adds red `M` to grades quick links
- Fixes enhanced discussions where sample students (not real students) are posting
- Adjust discussions nomenclature, 'Messages' to 'Posts', adds Title attribute
- Fixes bug with `Get Lecturers from Units` widget where only one user was returned
- Fixes bug where link checker widget would fail while examining some links
- Removes now obsolete Echo360 embedding method from ICB panel

**Version 2.1.33**  
_24 August 2018_

- Fixes bug that stopped `Impersonate Student View` links from appearing (caused by unexpected Brightspace style change)
- Fixes back-to-top buttons which had stopped working
- Excludes all non-UTAS domains for role checking (was causing silent console errors)
- Fixes missing rubric attached icon where single assessment folder found
- Reintroduce `Unit Builder` button to Content tool
- Improve API request function to be more robust

**Version 2.1.32**  
_10 August 2018_

- Fixes bug where unit row colouring didn't work on MyLO home page and was slow on other pages
- Fixes bug where search would fail when broken links were encountered
- Introduces scrollbar fixing as an option for Assignment Folder Evaluate pages
- Adjusts web page HTML editor to not exceed available height
- Introduces alert for when Student View not found and impersonate not possible
- Introduces alternative method of embedding Echo360 videos

**Version 2.1.31**  
_8 August 2018_

- Discussion boards now have user roles indicated next to names (e.g. L for lecturer). Note this is not yet possible in OER
- Quiz editor now has a Clear Formatting button (new quiz editor only)
- Adjust timing of orange unit date colouring - two weeks before and three weeks after unit expiry
- Increases the default web page HTML editor height
- Fixes `Save as Draft and Close`, `Advanced Search Options` and `Unit Builder` buttons
- Fixes bug where `Groups > Enrol Users` pagination duplication didn't work
- Fixes visibility of some functionality in Student View
- Floating arrow horizontal scrolling override removed (as it is no longer used in Brightspace)
- Fixes more styles that changed with the new version of Brightspace
- Fixes caching issue for new discussion posts
- Improves search widget's handling of dodgy file names, and checks links in web pages
- Improves Impersonate Student View functionality
- Improves jump arrows on some long content pages
- Fixes navigation bar buttons in template units
- Fixes multiple non-critical console errors
- Link checker widget handles unusual file names, and large numbers of links better
- Link checker widget provides more information on problem links
- Fixes and improves Assignment Folders evaluate scrollbars and sidebar positioning
- Fixes full screen rubric editing
- Fixes rubric toolbar focus sometimes incorrect
- Fixes a few other minor bugs

**Version 2.1.30**  
_1 August 2018_

- Fixes related to the new version of Brightspace

**Version 2.1.29**  
_27 July 2018_

- Migrated from GitHub to GitLab
- Introduce new bulk Group name editing feature
- Enrolled Users sandpit widget: Reworked to allow copying of lecturer Emails to clipboard as well as CSV download
- Enrolled Users sandpit widget: Allows Term selection as well as Semester
- Student Engagement widget copy to clipboard bug fixed
- Allow wider ICB style subdomain support
- Search unit widget: Now examines .htm as well as .html content
- Search unit widget: Fix bug where the search would fail on some units that had broken content links or cross-origin problems
- Combine unit outline navigation bar icon option in with the other navigation bar options
- Enhanced discussion view automatically gathers forum id if not available in URL
- Minor internal structural changes consolidating common functions

**Version 2.1.28**  
_23 July 2018_

- Fix bug in Unit Search where it fails if File.Url is null

**Version 2.1.27**  
_20 July 2018_

- Advanced (Unit) Search now parses query string and displays results
- Adds an icon next to as Assignment Submission folder showing whether rubrics attached, and allowing click to edit
- Fix date dots on home page of DEV2, adjust style
- Fix bug where SEA Widget loading animation wouldn't disappear
- Fix bug related to up-down buttons where general page responsiveness was affected (particularly drop-down lists)
- Link Checker widget improved, now examines all Link content items as well as web page content, results displayed to show the difference
- Introduce new styling for widgets and buttons to account for new DEV2 version. Concurrent support for new and old
- Search widget now examines all content titles as well as web page content
- Correct a bug with the Search Unit widget where case sensitivity state was stated incorrectly in RegEx

**Version 2.1.26**  
_10 July 2018_

- Introduce options for Regex and case sensitivity for the Unit Search widget
- Removes failing attempts to search text for .doc files (.docx works fine). User notified of unsupported files in unit
- Consistency in pagination controls introduced - always duplicated at top in `Enter Grades`, `Classlist` and `Groups - Enrol Users` and aligned to the right
- Consistency in confirmations applied (e.g. `Saved successfully`) so they shouldn't overlap buttons any more
- Expand the availability of User Progress panel to now display above `Classlist` and `Enter Grades`
- Various minor text, appearance and functionality tweaks

**Version 2.1.25**  
_6 July 2018_

- `User Progress` settings are detailed in an informational panel in the `Manage Grades` tool
- `User Progress` common settings can now be set at the click of a button
- Introduce a `Not Flagged` filter when marking assignment submissions
- Override the assignment submissions flagged icon to be bright red instead of nondescript grey
- Assignment submission folder `Saved successfully` notification moved so it doesn't overlap buttons
- Introduce `Grades Analysis` widget for each unit
- Introduce a `Search Unit` widget for each unit
- Custom shared loader element introduced
- `Save` button now changed to a `Save as draft and close` button, and `DRAFT` indicator displayed when a page is in draft
- `Strip Tags` sidebar function now removes `style` attributes from web page content
- ICB video embed now allows any URL (not just YouTube and Echo360)
- Introduce a navigation bar `Impersonate View Student` button
- Introduce jump to top/bottom buttons to the `Manage Files` tool
- Fixed a bug where the `Edit Page` and `Link Checker` buttons would sometimes swap positions
- Fixed a bug where sometimes the `Unit Builder` link did not appear
- Fixed a bug where some navigation bar shortcut icons disappeared on some pages
- Introduce an `About MyLO MATE` tab to the extension options
- Various minor text, appearance and functionality tweaks

**Version 2.1.24**  
_13 June 2018_

- Minor text changes

**Version 2.1.23**  
_12 June 2018_

- Reintroduce the Unit Outline shortcut icon in the nav bar
- Fix bug where if a grade was called `Quiz` it was erroneously given quiz shortcut links
- Remove option for `Rubric Generator` link, making it on by default
- Introduce direct links to export rubrics to `Rubric Generator`
- Improve `M` feature identification appearance, remove from home page date coloured dots
- Adjust height of modal dialogues (e.g. Restrictions) so vertical scrolling is less likely to be needed
- Add quick link next to `Final Adjusted Grade` or `Final Calculated Grade` (depending on settings)
- Waffle row colouring functionality and date accuracy improved
- Waffle row colouring introduced for OER
- Fixes bug where row colouring wasn't appearing in the unit `Advanced Search` tool
- Home page unit availability colour indicating icons introduced to OER
- Improve speed of `Manage Grades` tool link override (reducing chance of clicking `Grades` in nav bar and going to `Enter Grades` before override)
- Ignore invisible links in link checker widget, adding option to override this and check all links

**Version 2.1.22**  
_1 June 2018_

### MyLO

- Improve on/off button functionality and introduce off badge icon overlay
- Introduce MyLO MATE enhancement `M` badge to indicate which features are not standard
- Add new button on the Rubrics page linking to `Rubric Generator` tool
- Increase sizes of editable textboxes in the Rubric tool
- Fixed bug where discussion enhancement-related buttons wouldn't work first click

**Version 2.1.21**  
_25 May 2018_

### MyLO

- Improve placement of ICB in-place preview
- Enhanced Discussions now has a loading animation to attract the eye when processing large discussions
- Assignment Folders now have a summary totals page for the various states (Published, Evaluate, etc.)
- Navigation bar shortcuts now placed only when matching links are absent (supporting custom navigation bars)
- MyLO home page now shows date-related dots next to units (unless enrolled as student)

### Echo360

- File download initiation spaced out a bit so the browser doesn't seize up when many files are in the unit

### Other

- Introduce global `Off` button in MyLO popup panel

**Version 2.1.20**  
_18 May 2018_

### MyLO

- Fix bug where assignment folder filter didn't work in some units
- Allow notification panel to be disabled for minor updates

**Version 2.1.19**  
_10 May 2018_

### MyLO

- ICB sidebar for the OER now has improved features.
- Post content now appear in Discussion under the headings [Grid view only].
- Widgets now detect sandpits more accurately.

**Version 2.1.18**  
_4 May 2018_

### MyLO

- Introduce compatibility changes for the MOOC instance of Brightspace
- `Student Engagement` widget adjusted to include wait animation and close and back to top buttons at the bottom
- Duplicate Edit HTML button added above content of a web page
- Update notifications now show using chrome.notifications instead of a dodgy panel
- Invalid link checker now shows list of links on hover
- Text is properly escaped so User Group names with apostrophes store correctly
- ICB in-place preview introduced
- Fix left-right scrolling bug in discussions
- Various button styling changes to fix issues introduced with the latest version of Brightspace
- Permissions changes for future compatibility

**Version 2.1.17**  
_24 April 2018_

### MyLO

- Introduce filters (`Evaluate`, `Published`, etc) to assignment submissions list
- Ensure widgets are hidden if role doesn't allow their use
- Added a new Widgets tab to `Options` to allow individual widgets to be turned on and off
- Adjust layout of the assignment evaluation tool to reduce the number of scrollbars from 3 down to 1
- Change the student engagement widget to pop up results modally
- Fix ICB previews which had broken due to a recent Chrome update

**Version 2.1.16**  
_13 April 2018_

### MyLO

- Introduce a new `Student Engagement` widget that prepares a summary of the #SEA status for a unit
- Introduce a new `Invalid Link Checker` widget that allows a staff member to check for invalid external links en masse for a whole unit
- Introduce a new `Get Lecturers from Units` widget that downloads unit enrollments to a CSV spreadsheet
- Fix rare bug where semester sub-heading sometimes wrapped incorrectly, causing overlapping text under the unit title
- The word `Draft` in the content is now italicised in real time, rather than just on page refresh
- Add a quick link to assignment folder properties
- Pagination controls in Grades tool are now only cloned if they exist

**Version 2.1.15**  
_16 March 2018_

### Echo360

- Improve Echo360 bulk download function to better handle grouped videos

### MyLO

- Improve Unit Builder link availability in Content section
- Adjust the horizontal scrolling method of the grades tool
- Duplicate the pagination controls at the top of the grades tool

**Version 2.1.14**  
_9 March 2018_

### Echo360

- Fix Echo360 bulk download function which had stopped working due to a change at Echo360

### MyLO

- Change role detection method so certain functionality disappears when impersonating `Student View` and `View as Student`
- Fix bug where `Back to Top` and `Jump to Bottom` buttons were sometimes behind other elements
- Duplicate pagination controls at the top of the Groups enrollment tool
- Add button direct to rubrics page after a rubric import
- Remove more of the awkward left-right scrolling buttons, reverting to a standard horizontal scrollbar
- Add a `Remove Styles` button to the `Apply to All` panel in the rubric editors

**Version 2.1.13**  
_27 February 2018_

### MyLO

- Fix bug where some discussion quicklinks disappeared after editing a discussion topic
- Fix bug where months were 1-based instead of 0-based, causing incorrect unit background colouring
- For improved accessibility, adjust style of unit status disc so past units are square, current and future are round
- Introduce `Strip Styles` button and `Remove Style Attributes` button

**Version 2.1.12**  
_23 February 2018_

### MyLO

- Every web page in content now has a target="\_blank" checker for links. Solid link is all good, broken link icon means issues
- Fix bug where quicklinks on discussions only appeared next to the first topic
- Unit status disc appears next to unit title. Different colours according to whether unit is current, future, past
- Direct link to Unit Builder added to Content page
- Adjusted panel info and supporting pages to make it clear MyLO MATE is not centrally supported, and make it clearer how to get assistance
- Option added to allow navigation bar unit outline link to open the unit outline in a new window
- Sometimes people hide the Groups link in the navigation bar. This affects students and staff. If the main navigation bat Groups link is missing, MyLO MATE now adds an extra small icon into the navigation bar so that staff can still access the tool easily
- HTML Editors now include a strip styles button (e.g. in announcements)

### Other

- Various bugs and fixes, and documentation and nomenclature tweaks

**Version 2.1.11**  
_14 February 2018_

### MyLO

- Unit Outline icon support enhanced, should now work for all types
- Unit Outline icon now added by default
- Fixed overlapping Semester/Award text on unit home pages
- Introduced Jump to Bottom and Jump to Top buttons on long pages
- Duplicates pagination controls that appear below the list of students on the `Classlist` page, so they can be used at the top as well
- Introduce a single click `Impersonate View Student` button on the `Classlist` page

### Other

- Minor formatting and documentation changes

**Version 2.1.10**  
_9 February 2018_

### MyLO

- The word 'Draft' in the `Content` menu is italicised so it doesn't get lost
- Unit Outlines now much more robust, and should support all unit outline variations

### MyLO Manager

- Pale orange shading introduced for units falling within the exam period

### Other

- MyLO MATE licensed as GPLv3 and supporting web pages now moved to the [Building eLearning Community of Practice](http://www.utas.edu.au/building-elearning/resources/mylo-mate) site

**Version 2.1.9**  
_31 January 2018_

- Add to Top button now optional
- Override scrolling behaviour on `Enter Grades` screen now optional
- Some options rewording and reorganisation for clarity

**Version 2.1.8**  
_17 January 2018_

- Add direct link on the `Manage Groups` page to the `Advanced Search Options` (aka `Demographics Search`) tool
- Adjust the unit colouring by date to be more accurate
- Override the horizontal scrolling method introduced with Daylight to be more intuitive
- Introduce a Back to Top button which appears on all long pages

**Version 2.1.7**  
_21 December 2017_

### Echo360

- Implement a new method for bulk downloading videos

**Version 2.1.6**  
_7 December 2017_

### MyLO

- Shortcuts to `Restrictions` _[r]_ and `Assessment` _[a]_ added to the Discussions screen
- Fixed bug where navigation bar enhancements (unit outline, file manager, unit admin) weren't appearing on certain pages in MyLO
- Fixed bug where some developer features appeared when in a unit as a student (e.g. Manage Grades override)

### MyLO Manager

- Sometimes `Undefined` would appear in the User Groups tool (e.g. when creating a new unit)

### Other

- Update notification panel disappears when link is clicked, and can easily be disabled for certain releases (like this one)

**Version 2.1.5**  
_1 December 2017_

### MyLO

- The dates used for highlighting current units changed to extend beyond default semester dates to ensure they remain green (or blue) after exam period finishes (while staff are still working with the unit)
- Grades link no longer goes to a forbidden page if current role is student
- Unit outline link corrected where unit code changed after being initially ordered

### Echo360

- Fix bug where downloads would fail where unit title contained certain punctuation (e.g. backslash)

### Other

- Context added to references to the `Bulk User Management` (BUM) Tool
- Options storage method adjusted to be more robust

**Version 2.1.4**  
_17 November 2017_

### MyLO

- Allow ICB panel to operate when editing in full screen mode
- When bulk deleting discussion forums and topics, the Delete button remains disabled when Select All is ticked. MyLO MATE fixes this D2L bug
- Introduces three new buttons in File Manager: `Select all folders`, `Select all linked files` and `Select all unlinked files`
- Future units no longer highlighted red. Instead, they are left with a white background
- `Strip Tags` option introduced with ICB panel (quickly removes unneeded tags in the HTML). Enabled as part of Options > Enable Developer Tools

### Other

- In previous versions when a new version of MyLO MATE was released, a mandatory new tab was shown. Now we have a much less intrusive panel notification instead

**Version 2.1.3**  
_27 October 2017_

### MyLO

- Adjust contrast of current units that are highlighted in accessibility mode to make it easier to read
- Streamline rubric editing functionality and add single _clear format_ button

### Other

- Change wording of error message that is shown when user group adjustment is attempted when not logged into MyLO Manager, so the user knows what's wrong
- Change wording and function of ICB secondary headings option (tick to enable, instead of tick to disable)

**Version 2.1.2**  
_13 October 2017_

### MyLO

- Background colours for units are changed according to approximate semester start and end dates
- Consolidated rubric editing toolbar introduced
- Subtitle now appears under unit title in Daylight indicating semester and unit type

### Other

- Various bug fixes and visual improvements

**Version 2.1.1**  
_6 October 2017_

### Other

- Remove weird _Message_ link in MyLO Mate panel
- Various bug fixes and visual improvements

**Version 2.1.0**  
_29 September 2017_

### MyLO Manager

- Groups management functionality in the Staff tab now improved to be much more intuitive

### Other

- Help (?) icons now show help on click as well as on hover
- Extension name changed to `MyLO MATE`
- Notifications of new features introduced
- Various bug fixes and visual improvements

/^^ Version 2.0.x
**Version 2.0.10**  
_15 September 2017_

### MyLO

- Fixes direct Reading List links: After editing an HTML page, any corrupted ezproxy links will automatically be fixed when you Update (save) the file

### MyLO Manager

- In the `Staff` tab, you can now add a comma separated list of staff to a unit using the Add Username List to Unit... button
- Improved user groups usability

### Other

- `Quiz`, `Assignment Folders`, `Grades` link enhancement option now controlled by the one checkbox in the options
- Various bug fixes and visual improvements

**Version 2.0.9**  
_8 September 2017_

### Other

- Introduced 'Accessibility mode' which changes MyLO Manager unit status colouring to be easier for those with colour blindness
- Various bug fixes and visual improvements

**Version 2.0.8**  
_6 September 2017_

### MyLO Manager

- Custom User Groups can now be created to allow adding multiple users of varying roles to a unit with one click
- Department drop-down list in Find Staff window reduced to a manageable width

### MyLO

- Option to add link to unit outline in navigation bar introduced
- Invalid links to external HTTP sites are now automatically fixed upon saving an HTML file (changed to open in new tab)
- Options introduced to allow all external links to open in a new tab either automatically or on demand

### Other

- Various bug fixes and visual improvements

**Version 2.0.7**  
_11 August 2017_

### MyLO

- Add `Submission Views` link alongside `Assessment` and `Restrictions` links next to each quiz title
- Add link to ICB home page in ICB panel
- Now Daylight compatible

### Other

- Introduce `Developer Defaults` button, default on install is for most options to be off
- Add option to include link to `dev2` (Daylight) in extension panel
- Various bug fixes and visual improvements

**Version 2.0.6**  
_28 July 2017_

### Other

- Add Options shortcut link to icon popup panel
- Various bug fixes and visual improvements

**Version 2.0.5**  
_21 July 2017_

### MyLO

- ICB panel introduced, visual 'preview' representation of each ICB element in panel on hover

### Other

- Reworked Options panel to tabbed system
- Various bug fixes

**Version 2.0.4**  
_14 July 2017_

### MyLO

- Assignment Folders links now has direct links to `Restrictions` and `TurnItIn`

### Other

- Extension icon now opens panel of relevant links
- Add Echo360 download all videos link
- Various bug fixes, including a nasty one that caused all options to be turned off

**Version 2.0.3**  
_7 July 2017_

### MyLO

- Add direct links to `Restrictions` and `Assessment` in the Quizzes page
- Initial release of ICB direct interface

**Version 2.0.2**  
_30 June 2017_

### Other

- Options revamped to allow individual features to be turned off

### MyLO Manager

- `Bulk Unit Management` (BUM) tool button added at bottom of unit list
- Number of rows per page now able to be overridden either in Options or in MM

**Version 2.0.1**  
_23 June 2017_

### Other

- Extension options introduced
- Clicking extension icon opens `MyLO Manager` in a new tab
- Other miscellaneous bug-fixes and improvements

### MyLO Manager

- Links to MyLO units re-inserted even if they are Pending
- `Import/Export Components` Quicklink added
- Number of rows per page able to be set in Options

### MyLO

- Icon added to link directly to Unit Admin
- Icon added to link directly to Manage Files

**Version 2.0.0**  
_20 June 2017_

### Initial Release based on Excel tool

- Quicklinks control panel added to MyLO Manager
- Force MyLO Manager table to extend the full width of the browser

/^^ Version 1.0.0
_30 January 2015_

### Initial Release: Excel tool

- Excel tool Generates HTML quicklinks page

--/

[logo]: https://www.utas.edu.au/?a=1070208 'MyLO MATE'
[download]: https://www.utas.edu.au/building-elearning/resources/mylo-mate/download
[user guide]: https://www.utas.edu.au/building-elearning/resources/mylo-mate/user-guide
[impact chart]: https://www.utas.edu.au/building-elearning/resources/mylo-mate/impact-chart
[support]: https://utas1.service-now.com/selfservice/?id=sc_cat_item_v2&sys_id=bdce1e5fdbf00300f32d75a9bf961943
