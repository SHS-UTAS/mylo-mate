let pool = [];
const filezone = document.querySelector('mm-file-zone');

const isSpreadsheet = name => name.endsWith('.xlsx') || name.endsWith('.csv');
const workbookToCSV = sheet => {
  let csv = [];
  sheet._rows.forEach(row =>
    csv.push(
      row.values
        .filter(v => v)
        .map(v => `"${v}"`)
        .join(',')
    )
  );
  return csv.join('\r\n');
};

async function ProcessFile(file) {
  const process = async (buffer, path, file) => {
    try {
      if (path.endsWith('.xlsx')) {
        const workbook = new ExcelJS.Workbook();
        await workbook.xlsx.load(buffer);

        const sheet = workbook.worksheets[0];

        if (sheet.getCell('A1').value == 'FORMAT STUDY_PACKAGE_RESULT STANDARD' && sheet.getCell('R2').value == 'Comments') {
          sheet.spliceColumns(18, 1);
        }

        pool.push([path, workbookToCSV(sheet)]);
      } else if (path.endsWith('.csv')) {
        const csv = Papa.parse(file).data;
        csv.forEach(row => {
          if (row.length == 18) {
            // There's a comments cell
            delete row[17]; // Remember 0-offset arrays
          }
        });
        pool.push([
          path,
          csv
            .map(v =>
              v
                .filter(v => v)
                .map(v => `"${v}"`)
                .join(',')
            )
            .join('\r\n'),
        ]);
      }
    } catch (e) {
      console.error(e);
    }
  };

  if (isSpreadsheet(file.name)) {
    await process(await file.arrayBuffer(), file.name, await file.text());
  } else if (file.name.endsWith('.zip')) {
    const zip = await JSZip.loadAsync(file);
    await Promise.all(
      Object.entries(zip.files).map(async ([name, z]) => {
        if (isSpreadsheet(name)) {
          await process(await z.async('arraybuffer'), name, await z.async('text'));
        }
      })
    );
  }
}

async function CompileAndSave() {
  const zip = new JSZip();
  if (pool.length > 0) {
    if (pool.length == 1) {
      const [path, csv] = pool[0];
      saveAs(new Blob([csv]), path.replace('.xlsx', '.csv'));
    } else {
      for (const [path, csv] of pool) {
        await zip.file(path.replace('.xlsx', '.csv'), csv);
      }

      const blob = await zip.generateAsync({ type: 'blob' });
      saveAs(blob, `MyLO MATE Results Merge Fix.zip`);
    }
  } else {
    filezone.error('No suitable documents identified.');
  }
}

async function ProcessUpload({ detail }) {
  pool = [];
  const { files } = detail;
  if (files.length > 1 && Array.from(files).find(file => file.name.endsWith('.zip'))) {
    filezone.error(
      'Please either upload one or more .xlsx or .csv documents, OR a single .zip file, containing one or more .xlsx or .csv documents.'
    );
    return;
  }

  for (const file of files) {
    await ProcessFile(file);
  }
  await CompileAndSave();
}

filezone.addEventListener('file-drop', ProcessUpload);
