/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-underscore-dangle */
(async function () {
  document.querySelectorAll('.close-settings').forEach(el => {
    if (navigator.userAgent.includes('Edg/')) {
      el.remove();
    } else {
      el.addEventListener('click', e => {
        window.close();
      });
    }
  });

  const GetRequestPayload = () =>
    chrome.runtime.sendMessage({ section: 'options' }, res => {
      if (res) {
        if (res.tab) {
          const header = document.querySelector(`#${res.tab} header`);
          const active = document.querySelector(`header[active]`);

          if (active) active.removeAttribute('active');
          if (header) {
            header.setAttribute('active', true);
            header.scrollIntoView();
          }
        }
      }
    });

  window.onfocus = GetRequestPayload;
  GetRequestPayload();

  const store = async name => (await fetch(chrome.runtime.getURL(`config/${name}.json`))).json();
  const defaults = await store('default');
  const devDefaults = await store('devdefault');

  const ExtensionID = {
    Sandpit: 'epmdgcklgcgmidboecddiapdhdokceok',
    Beta: 'maianiginahgggelicglpcgcpjoefofi',
    Live: 'bbmpfljhlpdmialnpoakplledongdcom',
  };

  const IS_DEV = chrome.runtime.getManifest().update_url == null;

  const oldShowModal = HTMLDialogElement.prototype.showModal;
  const oldClose = HTMLDialogElement.prototype.close;

  HTMLDialogElement.prototype.showModal = function () {
    document.body.classList.add('dialog-modal');
    oldShowModal.call(this);
  };

  HTMLDialogElement.prototype.close = function () {
    document.body.classList.remove('dialog-modal');
    oldClose.call(this);
  };

  const _dialog = (text, type, buttons, opts = {}) =>
    new Promise(res => {
      // console.log('[%s] ', type, text);
      const d = document.createElement('dialog');
      d.classList.add(type);
      d.classList.add('mm-dialog-construct');
      document.body.appendChild(d);
      const t = document.createElement('div');
      t.innerHTML = text;
      d.appendChild(t);
      const btns = [];
      let i = null;
      if (opts.textfield) {
        i = document.createElement('input');
        i.type = 'text';
        i.placeholder = opts.placeholder || '';
        i.value = opts.value || '';
        d.appendChild(i);
        i.addEventListener('keydown', e => {
          if (e.key == 'Enter') {
            e.preventDefault();
            btns.find(b => b.primary).el.click();
          }
        });
      }
      const button_bar = document.createElement('div');
      button_bar.classList.add('buttons');
      Array.from(buttons).forEach(btn => {
        let primary;
        let text;
        if (typeof btn === 'string') {
          primary = false;
          text = btn;
        } else {
          text = Object.keys(btn)[0];
          primary = btn[text];
        }
        const b = document.createElement('button');
        if (primary) {
          b.setAttribute('primary', true);
        }
        b.innerText = text;
        b.addEventListener('click', e => {
          d.close();
          d.remove();
          res(opts.textfield ? { action: text, value: i.value } : text);
        });
        button_bar.appendChild(b);
        btns.push({ primary, el: b });
      });
      d.appendChild(button_bar);
      d.showModal();
    });

  const alert = async text => (await _dialog(text, 'alert', [{ OK: true }])) == 'OK';
  const confirm = async (text, buttons = [{ Yes: true }, 'No']) => (await _dialog(text, 'confirm', buttons)) == 'Yes';
  // const dialog = async (text, buttons) => _dialog(text, 'dialog', buttons);
  // const prompt = async (text, buttons = [{ OK: true }, 'Cancel'], opts = {}) => _dialog(text, 'prompt', buttons, { textfield: true, ...opts });

  // A reference to the confirmation dialog.
  const dev_confirmdialog = document.querySelector('#confirmDevOptions');
  const defaults_confirmdialog = document.querySelector('#confirmDefaults');

  /**
   * Sets the defaults that an EdTech or EdDev would use, as opposed to a lecturer.
   * Once the settings have been saved, it will 'restore' these options to the users screen.
   * @memberof Chrome/Options#
   */
  function setDevDefaults() {
    dev_confirmdialog.close();
    chrome.storage.sync.set(
      {
        ...defaults,
        ...devDefaults,
        // usergroups: { enabled: true },
      },
      restore_options
    );
  }

  /**
   * Sets the defaults to the default install state.
   * Once the settings have been saved, it will 'restore' these options to the users screen.
   * @memberof Chrome/Options#
   */
  function setDefaults() {
    defaults_confirmdialog.close();
    chrome.storage.sync.set(defaults, restore_options);
    // usergroups: { enabled: defaults.usergroups.enabled },
  }

  const getFileData = file =>
    new Promise((resolve, reject) => {
      const r = new FileReader();
      r.onload = e => resolve(r.result);
      r.onerror = reject;
      r.onabort = reject;
      r.readAsText(file);
    });

  async function handleImport(files) {
    // If there weren't any files selected, don't do anything.
    if (files.length == 0) return null;
    const file = Array.from(files).find(f => f.type == 'application/json');
    // It's not a JSON file.
    if (file == null) {
      alert('This is not the correct type of file.');
      return null;
    }

    const data = JSON.parse(await getFileData(file));
    if (data._metadata == null || data._metadata.exported == null) {
      alert("There's something wrong with this file.");
      return null;
    }

    // const manager = new UserGroups();

    const { exported } = data._metadata;
    if (exported.type == 'datastore') {
      if (data.store == null) {
        alert("There's something wrong with this file.");
        return null;
      }
      const notice = `
        <p><strong>MyLO MATE Settings Importer</strong></p>
        <p>
          <dl>
            <dt>Data exported</dt>
            <dd>${new Date(exported.at).format('{hour.pad}:{minute}{ampm} {date.pad}/{month.num.pad}/{year.short}')}</dd>
            
            <dt>Exported from</dt>
            <dd>MyLO MATE ${exported.branch}</dd>
            
            <dt>Version</dt>
            <dd>${exported.version} => <span${
        exported.version != chrome.runtime.getManifest().version ? ' style="font-weight: bold;"' : ''
      }>${chrome.runtime.getManifest().version}</span></dd>
          </dl>
        </p>
        <p>Importing this settings file will <strong>overwrite all your existing settings</strong>.</p>
        <p>Do you wish to continue?</p>
      `;

      if (await confirm(notice)) {
        // The user has confirmed their choice. Let's nuke their storage and overwrite our own.
        // const groups = new UserGroups();

        await promiseMe(chrome.storage.sync.clear.bind(chrome.storage.sync))();
        await promiseMe(chrome.storage.sync.set.bind(chrome.storage.sync))(data.store);
        // await groups.saveAll(data.groups);

        alert('Data imported successfully.');
        restore_options();
      } else {
        // Don't do anything.
        return null;
      }
    } else if (exported.type == 'usergroup') {
      // if (data.group == null) {
      //   alert("There's something wrong with this file.");
      //   return null;
      // }
      // const notice = `
      //   <p><strong>MyLO MATE User Group Importer</strong></p>
      //   <p>
      //     <dl>
      //       <dt>Data exported</dt>
      //       <dd>${new Date(exported.at).format('{hour.pad}:{minute}{ampm} {date.pad}/{month.num.pad}/{year.short}')}</dd>
      //       <dt>Exported from</dt>
      //       <dd>MyLO MATE ${exported.branch}</dd>
      //
      //       <dt>Version</dt>
      //       <dd>${exported.version} => <span${
      //   exported.version != chrome.runtime.getManifest().version ? ' style="font-weight: bold;"' : ''
      // }>${chrome.runtime.getManifest().version}</span></dd>
      //     </dl>
      //   </p>
      //   <p>Do you wish to import <strong>${data.group.Name}</strong>, containing ${data.group.Users.length} users?</p>
      // `;
      //
      // if (await confirm(notice)) {
      //   chrome.storage.local.get(null, async store => {
      //     if (Object.keys(store?.usergroups).includes(data.group.ID)) {
      //       const action = await dialog(
      //         'A group with this name already exists. Would you like to rename, overwrite or cancel?',
      //         [
      //           {
      //             Rename: true,
      //           },
      //           {
      //             Overwrite: true,
      //           },
      //           'Cancel',
      //         ]
      //       );
      //       if (action == 'Rename') {
      //         const nameExistsInStore = name =>
      //           Object.keys(store?.usergroups).find(id => store.usergroups.groups[id].Name == name) != null;
      //         const rename = async () =>
      //           new Promise(resolve => {
      //             const check = async () => {
      //               const res = await prompt('Please choose a new name.', undefined, {
      //                 value: data.group.Name,
      //               });
      //               // We hit Cancel instead.
      //               if (res.action != 'OK') resolve(null);
      //               else {
      //                 // Check the rules below. If they fail, alert and recheck.
      //
      //                 // Name is the same
      //                 if (res.value.trim() == data.group.Name.trim()) {
      //                   await alert('You need to choose a different name.');
      //                   check();
      //                 }
      //                 // Name is empty
      //                 else if (res.value.trim() == '') {
      //                   await alert('You need to enter a name.');
      //                   check();
      //                 }
      //                 // Name is used by something else
      //                 else if (nameExistsInStore(res.value.trim())) {
      //                   await alert('That name is already in use. Please try another one.');
      //                   check();
      //                 }
      //                 // This name is fine to use
      //                 else resolve(res.value.trim());
      //               }
      //             };
      //             check();
      //           });
      //
      //         // The new name we want to use.
      //         //  We know that this is unique and non-empty.
      //         const newName = await rename();
      //         // If we get null back, it means we hit Cancel. Return here and don't go on.
      //         if (newName == null) return null;
      //
      //         const id = newName
      //           .toLowerCase()
      //           .replace(/[^\w\s\d]/gi, '')
      //           .replace(/[\s]/g, '-');
      //         store.usergroups[id] = {
      //           ...data.group,
      //           ID: id,
      //           Name: newName,
      //         };
      //
      //         // Push the new group object, and update the UI elements.
      //         await manager.saveAll(store.usergroups);
      //       } else if (action == 'Overwrite') {
      //         // Just replace the group with the one from the imported file.
      //         store.usergroups[data.group.ID] = data.group;
      //
      //         // Push the new group object, and update the UI elements.
      //         await manager.saveAll(store.usergroups);
      //       } else if (action == 'Cancel') {
      //         // Do nothing.
      //         return null;
      //       }
      //     } else {
      //       // Just replace the group with the one from the imported file.
      //       store.usergroups[data.group.ID] = data.group;
      //
      //       // Push the new group object, and update the UI elements.
      //       await manager.saveAll(store.usergroups);
      //     }
      //   });
      // } else {
      //   // Don't do anything.
      //   return null;
      // }
    } else {
      alert('Unknown file type.');
      return null;
    }
  }

  function import_options() {
    const file = document.createElement('input');
    file.style.display = 'none';
    file.type = 'file';
    file.accept = '.json';
    file.addEventListener('change', e => handleImport(file.files));
    document.body.appendChild(file);
    file.click();
  }

  const branch =
    chrome.runtime.id == ExtensionID.Live
      ? 'Stable'
      : chrome.runtime.id == ExtensionID.Beta
      ? 'Beta'
      : IS_DEV
      ? 'Developer installation'
      : 'Unknown source';

  const DateFormat = '{year.short}{month.num.pad}{date.pad}{hour.24.pad}{minute}';

  async function export_options() {
    const group_manager = new UserGroups();
    const groups = await group_manager.get();
    const store = await promiseMe(chrome.storage.sync.get.bind(chrome.storage.sync))(null);

    const name = `MyLO-MATE SettingsExport ${new Date().format(DateFormat)}.json`;
    const o = {
      _metadata: {
        exported: {
          at: Date.now(),
          version: chrome.runtime.getManifest().version,
          branch,
          type: 'datastore',
        },
      },
      store,
      groups,
    };

    const file = new Blob([JSON.stringify(o)], { type: 'application/json' });
    const link = document.createElement('a');
    link.setAttribute('href', URL.createObjectURL(file));
    link.setAttribute('download', name);
    link.click();
  }

  async function export_group() {
    const groups = new UserGroups();
    const group = await groups.get(document.querySelector('input[name="group"]:checked').value);

    const name = `MyLO-MATE ${group.ID} ${new Date().format(DateFormat)}.json`;
    const o = {
      _metadata: {
        exported: {
          at: Date.now(),
          version: chrome.runtime.getManifest().version,
          branch,
          type: 'usergroup',
        },
      },
      group,
    };

    const file = new Blob([JSON.stringify(o)], { type: 'application/json' });
    const link = document.createElement('a');
    link.setAttribute('href', URL.createObjectURL(file));
    link.setAttribute('download', name);
    link.click();
  }

  /**
   * Stores the form options into the chrome storage area, then closes the options panel.
   * @memberof Chrome/Options#
   */
  async function save_options() {
    /** const data = await FetchMMSettings(); */

    // An empty, persistant object.
    const base = {};
    // Find anything with our [data-option] attribute.
    document.querySelectorAll('[data-option]').forEach(el => {
      // Get the path of our property.
      const key = el.getAttribute('data-option');
      // Get the value of our option that will be stored.
      // This will support checkboxes, radio buttons and `select` dropdowns.
      const value =
        el.localName == 'select'
          ? el.value
          : el.type == 'radio'
          ? document.querySelector(`[data-option="${key}"]:checked`).value
          : el.type == 'checkbox'
          ? el.checked
          : el.type == 'text'
          ? el.value
          : null;

      // Break the key down into properties, with each period element being nested in the last.
      // ie.
      //     something.somethingelse.aprop.key
      //       ===
      //    { something: { somethingelse: { aprop: { key: <<value>> } } } };
      key.split('.').reduce((obj, key, index, array) => {
        // If this is the last element of this path, then we know we've hit the `<<value>>` portion.
        //  This means that we're going to want to use the value retrieved above.
        // If it's not the last element, then we know that we're going to need to go deeper.
        //  In this case, check to see if we already have an object at that level;
        //  If we do, use it. If we don't, create a new one.
        obj[key] = index == array.length - 1 ? value : obj[key] || {};
        // Return the new level of the object, so that we can nest into it iteratively.
        return obj[key];
      }, base); // Start it working on our `base` object defined outside of the querySelectorAll.forEach loop.
    });

    // Make sure that our custom created usergroups are persisted.
    // These aren't part of the normal options flow, and are handled elsewhere.
    /** base.usergroups.groups = data.usergroups.groups; */

    // Store our new values.
    await SaveMMSettings(base);
  }

  /**
   * Restores the users current settings to the options form.
   * @memberof Chrome/Options#
   */
  async function restore_options() {
    const items = await FetchMMSettings();
    document.querySelectorAll('[data-option]').forEach(el => {
      // Gets the key from the `data-option` attribute, and then breaks it down into components and gets the property value based on the path.
      //    let item = { prop: { value: 42 } };
      //    item.prop.value === 42
      const key = el.getAttribute('data-option');
      const property = key.split('.').reduce((s, prop) => s[prop], items);

      if (el.localName == 'select' || el.type == 'text') {
        el.value = property;
      } else if (el.type == 'radio' && el.value == property) {
        el.checked = true;
      } else if (el.type == 'checkbox') {
        el.checked = property;
      }
    });

    // document.querySelector('#user-groups').style.display = items.usergroups.enabled ? 'block' : 'none';
    document.dispatchEvent(
      new CustomEvent('Ready', {
        detail: {
          data: items,
        },
      })
    );

    document.querySelectorAll('[depends-on]').forEach(d => {
      const toggle = document.querySelector(d.getAttribute('depends-on'));

      function setDisabled(state) {
        state ? d.setAttribute('disabled', 'disabled') : d.removeAttribute('disabled');
      }
      setDisabled(!toggle.checked);
      toggle.addEventListener('change', e => {
        setDisabled(!e.target.checked);
      });
    });
  }

  // Restore the current settings.
  restore_options();

  // When 'save' is clicked, save the settings into the storage area.
  // document.querySelector('#save').addEventListener('click', save_options);

  // When the Import Settings icon/button is clicked, export the store to a JSON file.
  document.querySelector('.icon.action-all.open').addEventListener('click', import_options);
  document.querySelector('.icon.action.open').addEventListener('click', import_options);

  // When the Export Settings icon/button is clicked, export the store to a JSON file.
  document.querySelector('.icon.action-all.save').addEventListener('click', export_options);
  // document.querySelector('.icon.action.save').addEventListener('click', export_group);

  // If the 'Developer Defaults' button is selected, open the dialog.
  document.querySelector('#dev-enabled').addEventListener('click', () => {
    dev_confirmdialog.showModal();
  });

  // When the 'OK' button is clicked in the Developer Options dialog, set the developer defaults.
  document.querySelector('#confirmDevOptions .ok').addEventListener('click', setDevDefaults);

  // If the 'Close' button is clicked in the Developer Options dialog, close the dialog.
  document.querySelector('#confirmDevOptions .cancel').addEventListener('click', () => {
    dev_confirmdialog.close();
  });

  // If the 'Defaults' button is selected, open the dialog.
  document.querySelector('#defaults').addEventListener('click', () => {
    defaults_confirmdialog.showModal();
  });

  // When the 'OK' button is clicked in the Options dialog, set the defaults.
  document.querySelector('#confirmDefaults .ok').addEventListener('click', setDefaults);

  // If the 'Close' button is clicked in the Options dialog, close the dialog.
  document.querySelector('#confirmDefaults .cancel').addEventListener('click', () => {
    defaults_confirmdialog.close();
  });

  // document.querySelector('#mm_displayUserGroups').addEventListener('click', e => {
  //   document.querySelector('#user-groups').style.display = e.target.checked ? 'block' : 'none';
  // });

  document.querySelectorAll('.mm-version').forEach(node => {
    node.innerText = chrome.runtime.getManifest().version;
  });

  let timer;
  const file_zone = document.body;

  const DropAreaVisible = (state, error) => {
    if (state) {
      file_zone.classList.add('file-enter');
      clearTimeout(timer);
    } else {
      timer = setTimeout(() => file_zone.classList.remove('file-enter'), 50);
    }
  };

  file_zone.addEventListener(
    'dragover',
    e => {
      e.stopPropagation();
      e.preventDefault();
      DropAreaVisible(true);
    },
    true
  );
  file_zone.addEventListener(
    'dragleave',
    e => {
      e.stopPropagation();
      e.preventDefault();
      DropAreaVisible(false);
    },
    true
  );
  file_zone.addEventListener(
    'drop',
    e => {
      e.stopPropagation();
      e.preventDefault();
      file_zone.classList.remove('file-enter');
      handleImport(e.dataTransfer.files);
    },
    true
  );

  document.addEventListener('click', e => {
    if (e.target.matches('#panels header')) {
      if (e.target.hasAttribute('active')) {
        e.target.removeAttribute('active');
      } else {
        if (document.querySelector('#panels header[active]')) {
          document.querySelector('#panels header[active]').removeAttribute('active');
        }
        e.target.setAttribute('active', true);
      }
    }
  });

  document.addEventListener('change', e => {
    if (e.target.matches('[data-option]')) {
      save_options();
    }
  });

  // Import/export

  // Confirm for any required checkboxes
  document.querySelectorAll('[data-confirm]').forEach(el => {
    el.addEventListener('click', async e => {
      if (e.target.checked) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        if (await confirm(el.getAttribute('data-confirm'))) {
          e.target.checked = true;
          e.target.dispatchEvent(new Event('change'));
        }
      }
    });
  });

  // When these are disabled, they should also disable other objects.
  document.querySelectorAll('[data-also-disables]').forEach(el => {
    el.addEventListener('click', e => {
      if (!e.target.checked) {
        const elements = e.target.getAttribute('data-also-disables').split(',');
        elements
          .map(elem => document.querySelector(`[data-option="${elem.trim()}"]`))
          .forEach(item => (item.checked = false));
      }
    });
  });
})();
