(async function () {
  // Open a communications port to the background script, named 'popup' for handling purposes.
  const p = chrome.runtime.connect({ name: 'popup' });
  const store = await new Promise(res => chrome.storage.sync.get(null, res));

  let currentTab = new URL((await new Promise(res => chrome.tabs.query({ active: true, currentWindow: true }, res)))[0].url);
  const isD2L = ['mylo.utas.edu.au', 'd2ldev1.utas.edu.au', 'd2ldev2.utas.edu.au'].includes(currentTab.hostname);

  // When an <li> element is clicked, post a message to the background script with the selected items information.
  document.addEventListener('click', e => {
    let li = e.composedPath().find(el => el?.localName?.toLowerCase() == 'li');
    if (li != null) {
      p.postMessage({
        action: li.getAttribute('action'),
        data: li.getAttribute('function'),
      });
      window.close();
    }
  });

  if (store.manager.advancedView) {
    let li = document.querySelector('[action="open"][function="mylo-manager"] span');
    li.innerText = `Advanced ${li.innerText}`;
  }

  // When the options link text is clicked, open the options page.
  document.querySelectorAll('.options').forEach(n =>
    n.addEventListener('click', e => {
      chrome.runtime.openOptionsPage();
    })
  );

  // document.querySelector('a.how-to-use').addEventListener('click', e => {
  // 	chrome.tabs.create({ url: 'http://www.utas.edu.au/building-elearning/resources/mylo-mate/user-guide' });
  // });

  document.querySelectorAll('.support').forEach(n =>
    n.addEventListener('click', e => {
      chrome.extension.getBackgroundPage().openSupportTicket();
    })
  );

  const updateChromeLink = document.querySelector('.update-chrome');
  updateChromeLink.addEventListener('click', e => {
    e.preventDefault();
    chrome.tabs.create({ url: e.target.href });
  });

  const thisMajorVer = parseInt(/Chrome\/([0-9.]+)/.exec(navigator.userAgent)[1].split('.')[0]);
  const newestMajorVer = 80;
  updateChromeLink.hidden = thisMajorVer >= newestMajorVer;

  /**
   * Get the latest version of Chrome from the official versions JSON file,
   *  and then decide whether to show the banner based on comparing the major versions.
   */

  // api(`https://omahaproxy.appspot.com/all.json`)
  //   .then(async versions => {
  //     const platform = await new Promise(res => chrome.runtime.getPlatformInfo(res));
  //     const info = (versions.find(ver => ver.os == platform.os) || versions[0]).versions.find(
  //       ({ channel }) => channel == 'stable'
  //     );
  //     const parseInt(info.version.split('.')[0]);
  //   })
  //   .catch();

  // document.querySelectorAll('.bug-report').forEach(n => {
  //   if (!isD2L) n.remove();
  //   else
  //     n.addEventListener('click', e => {
  //       e.preventDefault();
  //       chrome.extension.getBackgroundPage().generateBugReport();
  //       window.close();
  //     });
  // });

  document.querySelector('a.whats-new').addEventListener('click', e => {
    chrome.tabs.create({ url: 'http://www.utas.edu.au/building-elearning/resources/mylo-mate/#highlight' });
  });

  document.querySelector('a#refresh').addEventListener('click', e => {
    e.preventDefault();
    chrome.extension.getBackgroundPage().refreshCurrentPage();
    window.close();
  });

  const { name, version, version_name } = chrome.runtime.getManifest();

  // Set the app text, including the current version.
  document.querySelector('#app_title').innerText = name + ' v' + (version_name || version).split('beta')[0].trim();
  document.querySelector('.whats-new').innerText += version_name || version;

  // If the `other.showDev2` value is enabled, show the DEV2 list item.
  if (!store.other.showDev2) {
    const els = ['li[function="DEV1"]', 'li[function="DEV2"]', 'li[function="MMD2"]'];

    document.querySelectorAll(els.join(',')).forEach(el => el.remove());
  }
  if (!store.other.showH5P) document.querySelector('li[function="h5p"]').remove();

  // Responsible for slider checkbox
  let sliders = document.querySelectorAll('input[type="checkbox"].slider');
  if (sliders.length > 0) {
    let link = document.createElement('link');
    link.href = 'slider-checkbox.css';
    link.rel = 'stylesheet';
    link.type = 'text/css';
    document.head.appendChild(link);
  }

  sliders.forEach(checkbox => {
    checkbox.style.display = 'none';

    let container = document.createElement('div');
    container.classList.add('switch');

    checkbox.parentNode.insertBefore(container, checkbox);
    container.appendChild(checkbox);

    let slider = document.createElement('span');
    slider.classList.add('slider');
    if (checkbox.classList.contains('round')) slider.classList.add('round');
    container.appendChild(slider);

    container.addEventListener('click', e => {
      checkbox.checked = !checkbox.checked;
      chrome.storage.sync.set({ enabled: checkbox.checked });
      chrome.extension.getBackgroundPage().setDisabledBadgeState(!checkbox.checked);
      let reloadText = document.querySelector('p#reloadText');
      if (reloadText.style.display == 'none') reloadText.style.display = 'block';
    });
  });

  document.querySelector('input[type="checkbox"][name="enabled"]').checked = store.enabled !== false;
})();
