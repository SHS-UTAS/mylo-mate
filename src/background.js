const ExtensionID = {
  Sandpit: 'epmdgcklgcgmidboecddiapdhdokceok',
  Beta: 'maianiginahgggelicglpcgcpjoefofi',
  Live: 'bbmpfljhlpdmialnpoakplledongdcom',
};
// const IS_SANDPIT = chrome.runtime.id === ExtensionID.Sandpit;
const IS_BETA = chrome.runtime.id === ExtensionID.Beta;
// const IS_LIVE = chrome.runtime.id === ExtensionID.Live;

(async function () {
  /** @namespace Chrome/Background */

  const store = () => new Promise(res => chrome.storage.sync.get(null, res));

  /** The default storage object. */
  const store_defaults = await fetch(chrome.runtime.getURL('/config/default.json')).then(d => d.json());

  if (IS_BETA) {
    chrome.browserAction.setBadgeText({ text: 'BETA' });
    chrome.browserAction.setBadgeBackgroundColor({ color: '#80251a' });
  }

  /**
   * Returns a regexp string based on * wildcards in the argument.
   *
   * @author Connor Deckers (cdeckers)
   * @param {String} rule
   * @returns new RegExp
   * @memberof Chrome/Background#
   */
  const wildcard = rule => new RegExp(`^${rule.split('*').join('.*')}$`, 'i');

  // The URL for a new tab.
  // This may change in future, and at the moment, Chrome doesn't
  // have a way of using the API to detect a new tab any other way.
  const NEWTAB = 'chrome://newtab/';

  // A list of urls that are considered valid for the plugin to attach to.
  // 		These are just the base URLS, paths aren't used here.
  // This are used for the links panel, with the ID referencing the id passed from the popup panel.
  const urls = {
    'mylo': 'https://mylo.utas.edu.au/',
    'OER': 'https://oer.utas.edu.au/',
    'mylo-manager': 'https://mylo-manager.utas.edu.au/Home/',
    'echo360': 'https://echo360.net.au',
    'h5p': 'https://mylo-utas.h5p.com/',
    'DEV1': 'https://d2ldev1.utas.edu.au',
    'DEV2': 'https://d2ldev2.utas.edu.au',
    'MMD2': 'https://its-test-ws-mylomanager.its.utas.edu.au/Home/',
    'pebble+': 'https://v3.pebblepad.com.au/login/utas/',
    'atlas': 'https://v3.pebblepad.com.au/atlas/utas',
    'TandL': 'https://www.teaching-learning.utas.edu.au/',
  };

  // A list of Brightspace domains that each MyLO MATE tool should work on. These are the prefixes for the Brightspace URLs used in tabs.urls and tabs.blacklist
  const domainURLs = [
    'https://mylo.utas.edu.au/d2l',
    'https://d2ldev1.utas.edu.au/d2l',
    'https://d2ldev2.utas.edu.au/d2l',
    'https://oer.utas.edu.au/d2l',
  ];

  const globalBlacklist = ['/error/*'];

  /**
   * An object containing the list of scripts to load, depending on the current tabs URL.
   * Note, you can use more than one entry per domain.
   */
  const tabs = [
    {
      id: 'Messaging',
      urls: [
        // 'https://mylo-manager.utas.edu.au/*',
        '/*',
        'https://echo360.net.au/*',
        'https://mylo-utas.h5p.com/lti/*/content/*',
      ],
      injected: ['js/Messaging.js'],
      content: ['js/Messaging.js'],
      frames: true,
      beforeAll: true,
    },
    {
      id: 'Utilities',
      urls: ['/*'],
      injected: [
        {
          url: 'lib/elements.bundled.js',
          type: 'module',
        },
        'lib/merge.browser.min.js',
        'lib/Utils.js',
        'js/CustomElements.js',
        'js/mylo/role-check.js',
        'js/mylo/detect-tiny-mce.js',
      ],
      content: [
        {
          url: 'lib/elements.bundled.js',
          type: 'module',
        },
        'lib/merge.browser.min.js',
        'lib/Utils.js',
        'js/AccessMode.js',
        'lib/custom-elements.min.js',
        'js/CustomElements.js',
      ],
      frames: true,
      beforeAll: true,
    },
    // {
    //   id: 'Impersonate Student Injector',
    //   urls: ['/lms/classlist/classlist.d2l*'],
    //   injected: ['js/mylo-classlist/impersonate-student-view.js'],
    //   content: [],
    //   frames: true,
    // },
    {
      id: 'mylo',
      urls: ['/*'],
      injected: ['css/loader.css', 'css/submission-folder-settings.css', 'js/mylo/style-stripper.js'],
      content: [
        'js/mylo/content.js',
        'js/mylo-navigation/email-lecturers.js',
        'js/mylo/scroll-arrow-remover.js',
        'js/mylo/dialog-height-fixer.js',
        'js/all-frames/bug-report.js',
        'js/all-frames/remember-page-count.js',
      ],
    },
    {
      id: 'mylo-unit-homepage',
      urls: ['/home/\\d+'],
      injected: [],
      content: ['js/mylo/test-unit-outline.js'],
    },
    {
      id: 'mylo-css',
      urls: ['/*'],
      injected: ['css/mylo.css'],
      content: [],
      frames: true,
    },
    {
      id: 'impersonate-student-button',
      urls: ['/*'],
      blacklist: ['/home'],
      injected: [],
      content: ['js/mylo-navigation/impersonate-student.js'],
    },
    {
      id: 'mylo-popups',
      urls: ['/common/popup/popup.d2l?*'],
      injected: [],
      content: ['js/mylo-popups/colour-unit-rows.js'],
      frames: true,
    },
    {
      id: 'mylo-units',
      urls: ['/*'],
      blacklist: ['/home'],
      injected: ['css/mylo-units.css'],
      content: ['js/mylo-units/content.js', 'js/mylo-units/status-checker.js'],
    },
    {
      id: 'mylo-homepage',
      urls: ['/home'],
      injected: ['js/mylo/homepage-colors.js'],
      content: [],
    },
    {
      id: 'mylo-announcements',
      urls: ['/lms/news/main.d2l*'],
      injected: [],
      content: ['js/mylo-news/show-release-conditions.js', 'js/mylo-announcements/bulk-delete.js'],
    },
    {
      id: 'mylo-announcements-edit',
      urls: ['/lms/news/newedit.d2l*'],
      injected: [],
      content: [],
    },
    {
      id: 'mylo-attendance',
      urls: ['/lms/attendance/attendance/data_list.d2l*'],
      injected: ['css/freeze-columns.css', 'js/mylo-attendance/freeze-columns.js'],
      content: [],
    },
    {
      id: 'mylo-user-progress',
      urls: ['/le/classlist/userprogress/*/settings/Edit'],
      injected: ['css/mylo-user-progress.css'],
      content: ['js/mylo-user-progress/user-progress.js'],
    },
    {
      id: 'mylo-content',
      urls: ['/le/content/*/viewContent/*/View*'],
      injected: ['js/mylo-content/edit-button.js', 'css/content-view.css'],
      content: [
        'js/mylo-content/link-checker.js',
        'js/mylo-content/draft-checker.js',
        'js/mylo-content/full-path.js',
        'js/mylo-content/export-progress.js',
        {
          url: 'js/mylo-content/floating-hover-button.js',
          type: 'module',
          isBrightspaceComponent: false,
        },
        'js/h5p/modal.mjs',
      ],
    },
    {
      id: 'mylo-unit-toc',
      urls: ['/le/content/*/Home*'],
      injected: ['js/mylo-unit-toc/unit-builder.js', 'js/mylo-unit-toc/auto-path-file.js'],
      content: [
        'lib/polyfill-urlpattern.js',
        'js/mylo-unit-toc/on-topic-change.js',
        'js/mylo-unit-toc/copy-page.js',
        'js/mylo-unit-toc/clear-content-dates.js',
        'js/mylo-unit-toc/warn-on-lti.js',
        'js/mylo-unit-toc/make-unconfigured-unit-d2l-ready.js',
        /** 'lib/docx.js', */
        /** 'lib/filesaver.min.js', */
        /** 'js/mylo-unit-toc/export-to-word.js', */
      ],
    },
    {
      id: 'mylo-editor',
      urls: ['/le/content/*/authorHtml*', '/le/content/*/EditFile*'],
      injected: [
        'https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css',
        'lib/rxjs.umd.min.js',
        'css/editor.css',
        'css/content-view.css',
        'js/mylo-editor/TemplateComponents.js',
        'js/mylo-editor/icb-editor.mjs',
        'js/mylo-editor/d2l-template-editor.mjs',
        'js/mylo-editor/drafted.js',
        'js/mylo-editor/link-zapper.js',
        'js/mylo-editor/resize-editor.js',
        'js/mylo-editor/link-checker.js',
        'js/mylo-editor/use-default-banner.mjs',
        'js/mylo-editor/scroll-to-selector.js',
      ],
      content: ['js/h5p/modal.mjs'],
    },
    {
      id: 'big-h5p',
      urls: ['/common/dialogs/isf/selectItem.d2l*'],
      content: [],
      injected: ['js/mylo-editor/bigger-h5p.js'],
      frames: true,
    },
    {
      id: 'big-h5p-preview',
      urls: ['/common/dialogs/isf/itemProperties.d2l*'],
      content: [],
      injected: ['js/h5p/bigger-preview.js'],
      frames: true,
    },
    {
      id: 'big-h5p-inline',
      urls: ['https://mylo-utas.h5p.com/lti/*/content/*'],
      content: ['js/h5p/in-editor.js'],
      injected: [],
      frames: true,
    },
    {
      id: 'mylo-editor',
      urls: ['/le/content/*/EditFile*'],
      content: ['js/mylo-content/draft-checker.js'],
      injected: ['js/mylo-editor/migrate-to-d2l.mjs'],
    },
    {
      id: 'mylo-quicklinks',
      urls: ['/lp/quicklinks/manage/*/CreateDialog*'],
      content: [],
      injected: ['js/mylo-editor/accessible-quicklinks.js'],
      frames: true,
    },
    {
      id: 'mylo-new-editor',
      urls: ['/le/content/*/authorHtml*'],
      content: ['js/mylo-editor/preset-new-path.js'],
      injected: [
        /* 'js/mylo-editor/use-d2l-button.js', */
      ],
    },
    {
      id: 'mylo-grades',
      urls: ['/lms/grades/admin/enter/user_list_view.d2l*'],
      injected: ['css/grades.css'],
      content: ['js/mylo-grades/enter-grades.js', 'lib/moment.min.js', 'js/mylo-grades/show-last-accessed.js'],
    },
    {
      id: 'mylo-manage-grades',
      urls: ['/lms/grades/admin/manage/gradeslist.d2l*'],
      injected: ['css/grades.css'],
      content: [
        'js/mylo-grades/add-shortcuts.js',
        'js/mylo-grades/bulk-hide-grades.js',
        'js/mylo-grades/get-date-restrictions.js',
        'js/mylo-grades/add-export-link.js',
        'lib/exceljs.min.js',
        'js/mylo-grades/export-for-audit.js',
        'js/mylo-grades/show-release-conditions.js',
      ],
    },
    {
      id: 'mylo-grades-link-to-item',
      urls: ['/lms/grades/admin/enter/user_list_view.d2l*', '/lms/grades/admin/manage/gradeslist.d2l*'],
      injected: [],
      content: ['js/mylo-grades/link-to-item.js'],
    },
    {
      id: 'mylo-update-student-view-options',
      urls: ['/lms/grades/admin/settings/*'],
      injected: [],
      content: ['js/mylo-grades/apply-grade-details.js'],
    },
    {
      id: 'mylo-manage-user-progress-alert',
      urls: ['/lms/grades/admin/manage/gradeslist.d2l*', '/lms/grades/admin/enter/*', '/lms/classlist/classlist.d2l*'],
      injected: [],
      content: ['js/mylo-grades/check-user-progress-settings.js'],
    },
    {
      id: 'mylo-validate-grades',
      urls: ['/lms/grades/admin/manage/gradeslist.d2l*'],
      injected: [],
      content: ['js/mylo-grades/grades-validation.js'],
    },
    {
      id: 'mylo-grades-rename-wizard',
      urls: ['/lms/grades/admin/manage/category_props_newedit.d2l*'],
      injected: [],
      content: ['js/mylo-grades/rename-wizard.js'],
      frames: true,
    },
    {
      id: 'mylo-assignment-folders',
      urls: ['/lms/dropbox/admin/folders_manage.d2l*'],
      injected: [
        'css/mylo-grades.css',
        'js/mylo-assignments/bulk-detach-rubrics.js',
        'js/mylo-assignments/moderator-tools.js',
      ],
      content: [
        'js/mylo-quizzes/highlight-special-access.js',
        'js/mylo-assignments/rubric-attached.js',
        'js/mylo-assignments/get-release-conditions.js',
        'js/mylo-assignments/pagination-fix.js',
        'js/mylo-assignments/report-visibility.js',
        'js/mylo-assignments/quicklinks.js',
      ],
    },
    {
      id: 'no-mobile-rubric',
      urls: ['/common/dialogs/nonModal/blank.d2l*assessDlg*'],
      injected: ['js/mylo-assignments/no-mobile-rubrics.js'],
      content: [],
    },
    {
      id: 'change-add-file-path',
      urls: ['https://mylo.utas.edu.au/d2l/common/dialogs/file/main.d2l*af=MyComputer,OuFiles,SharedFiles,Url*'],
      injected: ['js/mylo-editor/change-add-file-path.js'],
      content: [],
      frames: true,
    },
    {
      id: 'mylo-bulk-edit-assignments',
      urls: ['/lms/dropbox/admin/folders_quickedit.d2l*'],
      injected: ['js/mylo-assignments/bulk-unselect-dates.mjs', 'js/mylo-assignments/fix-overflow.js'],
      content: [],
    },
    {
      id: 'mylo-assignments-submissions',
      urls: [
        '/lms/dropbox/admin/mark/folder_submissions_users.d2l*',
        '/lms/dropbox/admin/mark/folder_submissions_observed.d2l*',
      ],
      injected: ['js/mylo-assignments/assignment-submissions.js', 'js/mylo-assignments/analytical-data.js'],
      content: [],
    },
    {
      id: 'mylo-assignments-submissions',
      urls: ['/lms/dropbox/admin/mark/folder_submissions_*.d2l*'],
      injected: [
        'css/assignment-submissions.css',
        'js/mylo-assignments/save-search.js',
        'js/mylo-assignments/pagination-fix.js',
        'js/mylo-assignments/show-everyone.js',
        'js/mylo-assignments/bulk-retract.js',
        'js/mylo-assignments/show-video-player-in-submissions.js',
      ],
      content: ['js/mylo-assignments/resubmit-to-turnitin.js'],
    },
    {
      id: 'mylo-assignments-submissions-new',
      urls: ['/le/activities/iterator/*assignment-submissions*'],
      injected: ['js/mylo-assignments/show-video-player-in-submissions.js'],
    },
    {
      id: 'mylo-assignments-marking',
      urls: ['/lms/dropbox/admin/mark/folder_user_mark.d2l*'],
      injected: [],
      content: ['js/mylo-assignments/hilite-unscored-elements.js'],
    },
    {
      id: 'mylo-assignments-restrictions',
      urls: ['/lms/dropbox/admin/modify/folder_newedit_restrictions.d2l*'],
      injected: [],
      content: ['lib/moment.min.js', 'js/mylo-assignments/restriction-extras.js'],
    },
    {
      id: 'assignment-marking',
      urls: ['/le/activities/iterator/*cft=assignment-submissions*'],
      injected: [
        'js/mylo-assignments/rubric-marking-overlay.js',
        'js/mylo-assignments/show-student-view-grade.js',
        'js/mylo-assignments/expanded-rubric-button.js',
      ],
      content: [],
    },
    {
      id: 'mylo-group-enrollments',
      urls: ['/lms/group/group_enroll.d2l?*', '/lms/group/group_enroll_expanded.d2l?*'],
      injected: ['lib/moment.min.js', 'js/mylo-groups/save-override.js', 'js/mylo-groups/sort-by-extra-data.js'],
      content: [
        'lib/moment.min.js',
        'js/mylo-groups/pagination-fix.js',
        'js/mylo-groups/script.js',
        'js/mylo-groups/enrol-column-filter.js',
      ],
    },
    {
      id: 'mylo-group-list',
      urls: ['/lms/group/group_list.d2l?*'],
      injected: ['css/group-edit.css'],
      content: [
        'js/mylo-groups/quicklinks.js',
        'js/mylo-groups/show-student-count.js',
        'js/mylo-groups/merge-groups.js',
        'js/mylo-groups/split-groups.js',
        'js/mylo-groups/bulk-edit-names.js',
        'js/mylo-groups/bulk-update-group-ids.js',
        'js/mylo-groups/move-inactive-students.js',
        'js/mylo-groups/enrol-selected-groups.js',
        'js/mylo-groups/twiddler.js',
      ],
    },
    {
      id: 'mylo-group-edit',
      urls: ['/lms/group/group_edit.d2l?*'],
      injected: [],
      content: ['js/mylo-groups/rename-group.js', 'js/mylo-groups/rename-group-id.js'],
      frames: true,
    },
    {
      id: 'mylo-manage-files',
      urls: ['/lp/manageFiles/main.d2l*'],
      injected: [
        'js/mylo-files/event-manager.js',
        'css/manageFiles.css',
        'js/mylo-files/sticky-headers.js',
        'js/mylo-files/history-manager.js|mylo.ManageFilesHistory=true',
        'js/mylo-files/breadcrumbs.js',
        'js/mylo-files/bulk-renamer.js|other.ManageFilesFeatures=true',
      ],
      content: ['js/mylo-files/script.js', 'js/mylo-files/jump-buttons.js'],
      // injected: [],
      // content: [],
    },
    {
      id: 'mylo-rubric-list',
      urls: ['/lp/rubrics/list.d2l?*'],
      injected: ['js/mylo-rubrics/autocheck-archived.js|mylo.AlwaysShowArchivedRubrics=true'],
      content: [
        'js/mylo-rubrics/bulk-manage-rubric-status.js',
        'js/mylo-rubrics/rubric-visible.js',
        'js/mylo-rubrics/link-to-importer.js',
        'js/mylo-rubrics/link-to-builder.js',
        'lib/jszip.min.js',
        'lib/html-docx.js',
        'lib/filesaver.min.js',
        'js/mylo-rubrics/rubric-zipper.js',
        'js/mylo-rubrics/rubric-xml-builder.js',
        'js/mylo-rubrics/quicklinks.js',
        'js/mylo-rubrics/extended-data.js',
      ],
    },
    {
      id: 'mylo-new-rubric',
      urls: ['/lp/rubrics/edit_structure.d2l?*'],
      injected: ['js/mylo-rubrics/hide-scores.js', 'js/mylo-rubrics/go-to-options-panel.js'],
      content: ['js/mylo-rubrics/remove-arrows.js'],
    },
    {
      id: 'mylo-rubric-new-edit',
      urls: [
        '/lp/rubrics/criterion_newEdit.d2l?*',
        '/lp/rubrics/level_newEdit.d2l?*',
        '/lp/rubrics/edit_overallLevels.d2l?*',
      ],
      injected: ['css/rubric.css', 'js/mylo-rubrics/rubric.js'],
      content: [],
    },
    {
      id: 'mylo-search',
      urls: ['/*'],
      injected: ['css/mylo-search.css'],
      content: ['js/mylo-search/content.js'],
    },
    {
      id: 'mylo-rubric-previews',
      urls: ['/common/dialogs/nonModal/blank.d2l*previewRubric_*'],
      injected: [],
      content: ['lib/html-docx.js', 'js/mylo-rubrics/preview-download.js'],
    },
    {
      id: 'mylo-search-screen',
      urls: ['/le/manageCourses/search/*'],
      injected: ['css/mylo-search.css', 'js/mylo-search/search-from-url.js'],
      content: ['js/mylo-search/search-units.js'],
    },
    {
      id: 'mylo-bulk-discussions',
      urls: ['/le/*/discussions/List*'],
      injected: [
        {
          url: 'https://s.brightspace.com/lib/bsi/20.21.3-184/unbundled/input-date-time-range.js',
          type: 'module',
          isBrightspaceComponent: true,
        },
        'js/mylo-discussions/bulk-visibility.js',
      ],
      content: [
        'js/mylo-discussions/show-group-restriction-count.js',
        'js/mylo-discussions/add-shortcuts.js',
        'js/mylo-discussions/add-link-data.js',
        'js/mylo-discussions/show-release-conditions.js',
        'js/mylo-discussions/bulk-date-manager.js',
      ],
    },
    {
      id: 'mylo-discussions-reorder',
      urls: ['/lms/discussions/admin/forum_topics_reorder.d2l*'],
      injected: [],
      content: ['js/mylo-discussions/sort-topics.js'],
    },
    {
      id: 'mylo-discussions-images',
      urls: ['/le/*/discussions/*'],
      injected: [],
      content: ['js/mylo-discussions/profile-image-role-add.js'],
    },
    {
      id: 'mylo-discussions-thread',
      urls: ['/lms/discussions/messageLists/frame.d2l*'],
      injected: [],
      content: ['js/mylo-discussions/make-fit.js'],
    },
    {
      id: 'mylo-discussions-thread',
      urls: ['/lms/discussions/messageLists/message_list_gridstyle.d2l*'],
      injected: ['css/discussions.css', 'js/mylo-discussions/restore-posts.js'],
      content: [
        'js/mylo-discussions/collapse-threads.js',
        'js/mylo-discussions/add-content.js',
        'js/mylo-discussions/export-topic.js',
        'js/mylo-discussions/fix-search.js',
      ],
      frames: true,
    },
    {
      id: 'mylo-import-export-tool',
      urls: ['/lms/importExport/import_export.d2l?*'],
      injected: [],
      content: ['js/mylo-import-export/view-rubric-button.js', 'js/mylo-import-export/auto-select.js'],
    },
    {
      id: 'mylo-classlist',
      urls: ['/lms/classlist/classlist.d2l*'],
      injected: [
        'js/mylo-classlist/pagination-fix.js',
        'js/mylo-classlist/email-all-students.js|other.EdTechsMode=true&other.BulkEmailStudents=true',
        'js/mylo-classlist/export-to-intellilearn.mjs|mylo.showIntelliLearn=true',
      ],
      content: ['js/mylo-classlist/export-classlist.js'],
    },
    // {
    //   id: 'Utilities',
    //   urls: ['https://mylo-manager.utas.edu.au/*'],
    //   injected: ['lib/merge.browser.min.js', 'lib/Utils.js'],
    //   content: ['lib/merge.browser.min.js', 'lib/Utils.js'],
    //   frames: true,
    //   beforeAll: true,
    // },
    // {
    //   id: 'mylo-manager-general',
    //   urls: ['https://mylo-manager.utas.edu.au/Home/*'],
    //   injected: ['css/content.css'],
    //   content: ['js/mylo-manager/access-friendly.js'],
    // },
    // {
    //   id: 'mylo-manager-general',
    //   urls: ['https://mylo-manager.utas.edu.au/Home/Units'],
    //   injected: ['css/manager-colors.css'],
    //   content: [],
    // },
    // {
    //   id: 'mylo-manager',
    //   urls: ['https://mylo-manager.utas.edu.au/Home/Advanced'],
    //   injected: [
    //     /** 'js/mylo-widgets/ProcessEnrolledUsers.js', */
    //     'js/mylo-manager/pagescript.js',
    //     'css/mylo-manager.css',
    //     'js/mylo-manager/enrol-and-open.js|manager.quickEnrol=true',
    //   ],
    //   content: [],
    // },
    // {
    //   id: 'mylo-manager-unit-details',
    //   urls: ['https://mylo-manager.utas.edu.au/UnitRequest/UnitDetails/*'],
    //   injected: [],
    //   content: ['js/mylo-manager/unit-defaults.js'],
    // },
    // {
    //   id: 'mylo-manager-staff',
    //   urls: ['https://mylo-manager.utas.edu.au/UnitRequest/Staff/*'],
    //   injected: ['css/manager.css', 'js/mylo-manager/stafftab.js', 'js/mylo-manager/make-all-echo-instructors.js'],
    //   content: [
    //     'app/dataset.js',
    //     'js/UserGroups.js',
    //     'js/mylo-manager/users.js',
    //     'js/mylo-manager/fix-search.js',
    //     'lib/sort-table.js',
    //     'js/mylo-manager/role-advice.js',
    //   ],
    // },
    {
      id: 'echo360',
      urls: ['https://echo360.net.au/*'],
      injected: ['js/echo360/pagescript.js'],
      content: [],
    },
    {
      id: 'mylo-widgets',
      urls: ['/home/*'],
      injected: [
        'js/mylo-home/role-notice.js',
        'lib/rxjs.umd.min.js',
        {
          url: 'https://s.brightspace.com/lib/bsi/20.21.3-184/unbundled/input-text.js',
          type: 'module',
          isBrightspaceComponent: true,
        },
        {
          url: 'https://s.brightspace.com/lib/bsi/20.21.3-184/unbundled/input-checkbox.js',
          type: 'module',
          isBrightspaceComponent: true,
        },
        'js/mylo-widgets/entry.mjs',
      ],
      content: ['config/store.js' /*'js/mylo-widgets/entry.js'*/],
    },
    {
      id: 'mylo-quizzes',
      urls: ['/lms/quizzing/admin/quizzes_manage.d2l*'],
      injected: ['css/mylo-grades.css'],
      content: [
        'js/mylo-quizzes/highlight-special-access.js',
        'js/mylo-quizzes/uncheck-dates.js',
        'js/mylo-quizzes/show-release-conditions.js',
        'lib/exceljs.min.js',
        'js/mylo-quizzes/get-students-with-no-attempts.js',
      ],
    },
    {
      id: 'mylo-surveys',
      urls: ['/lms/survey/admin/surveys_manage.d2l*'],
      injected: [],
      content: ['js/mylo-surveys/show-release-conditions.js'],
    },
    {
      id: 'mylo-quizzes-attempt-logs',
      urls: ['/lms/quizzing/admin/attemptLogs/*/Logs*'],
      injected: [],
      content: ['js/mylo-quizzes/export-attempt-logs.js'],
    },
    {
      id: 'mylo-quiz-questions',
      urls: ['*//s.brightspace.com/apps/quiz-question-authoring/*/index.html'],
      injected: ['js/mylo-quizzes/clear-formatting.js'],
      content: [],
      frames: true,
    },
    {
      id: 'mylo-quiz-questions-in-use',
      urls: ['/lms/qc/question/shared_question.d2l?*'],
      injected: [],
      content: ['js/mylo-quizzes/check-all.js'],
      frames: true,
    },
    {
      id: 'download-bbcollab-videos',
      urls: ['*//au-lti.bbcollab.com/collab/ui/*'],
      injected: ['js/bb-collab/download-videos.js'],
      content: [],
      frames: true,
    },
  ];

  /**
   * Gets an insertable URL, based on the file provided.
   * If this URL is an external file (starts with `http`), then it will return,
   * 	otherwise it will get the `chrome-extension://` URL.
   * @param {string} url The URL of the file to insert
   * @returns {string} The insertable URL link.
   * @memberof Chrome/Background#
   */
  const getURL = url => (url.substr(0, 4) === 'http' ? url : chrome.runtime.getURL(url));

  /**
   * Enables or disables the icon.
   *
   * @param {boolean} enableIcon The state to set the icon to.
   * @param {number} id The tabID to change the icon for.
   * @memberof Chrome/Background#
   */
  // const changeIcon = (enableIcon, id) => chrome.browserAction.setIcon({ path: `../images/icon${enableIcon ? '48' : '128d'}.png`, tabId: id });

  /**
   * Tests if a string matches an array of strings.
   *
   * @param {string} url The URL to test.
   * @param {string[]} checkURLs The array of URLs to test against.
   * @returns {boolean} Returns true if any URLs match.
   * @memberof Chrome/Background#
   */
  function matches(url, checkURLs) {
    const haystack = checkURLs
      .flatMap(url => (url.startsWith('/') ? domainURLs.map(base => base + url) : url))
      .map(wildcard);

    return haystack.some(check => check.test(url));
  }

  /**
   * Gets the appropriate dataset from our tabs object
   *
   * @param {any} url
   * @returns {boolean|Types#TabData} Returns false if the URL is not defined, otherwise will return an array containing all the {@link Types#TabData} results that match the current URL.
   * @see {@link Chrome/Background#tabs}
   * @memberof Chrome/Background#
   */
  function checkURL(url, dataset = tabs) {
    if (url === undefined) {
      return false;
    }
    const matching = [];
    for (const block of dataset) {
      // For each tab we want to compare against
      const whitelisted = matches(url, block.urls);
      const inGlobalBlacklist = matches(url, globalBlacklist);
      const blacklisted = block.blacklist ? matches(url, block.blacklist) : false;

      if (blacklisted || inGlobalBlacklist) continue;
      if (!whitelisted) continue;

      matching.push(block);
    }
    return matching;
  }

  /**
   * Sends a message to a tab, with a handshake message (the name on the envelope, if you will),
   * the data, and a response callback.
   *
   * @param {number} tabId The tab to send the message to.
   * @param {string} handshake The handshake message.
   * @param {any} data The data to send with the message.
   * @param {function} response The callback to run when there's a response.
   * @memberof Chrome/Background#
   */
  function sendMessage({ tabId, frameId = 0 }, handshake, data) {
    chrome.tabs.sendMessage(tabId, { handshake, data }, { frameId });
  }

  /**
   * A wrapper function to make changing our icon a little easier.
   *
   * @param {string} url The URL of the current tab.
   * @param {number} tabId The tab ID received from the Chrome event. This allows the extension to know what tab to change the appearance for.
   * @see {@link changeIcon} for more information.
   * @memberof Chrome/Background#
   */
  // const changeIconBasedOnTab = (url, tabId) => changeIcon(checkURL(url).length > 0, tabId);

  // When a tab is activated, check the URL of the tab and find out if we should make our icon appear 'enabled'.
  // chrome.tabs.onActivated.addListener(info => chrome.tabs.get(info.tabId, change => changeIconBasedOnTab(change.url, info.tabId)));

  const FrameURL = ({ tabId, frameId }) =>
    new Promise(resolve => chrome.webNavigation.getFrame({ tabId, frameId }, e => resolve(e ? e.url : '')));
  const OnPageNavigate = async ({ frameId, tabId, url }) => {
    const s = await store();
    const CheckRules = href => {
      if (!href.includes('|')) return true;
      const rules = href
        .split('|')[1]
        .split('&')
        .reduce((obj, p) => {
          const kv = p.split('=').map(v => v.trim());
          obj.push({ prop: kv[0], value: kv[1] });
          return obj;
        }, []);
      return rules.every(
        rule =>
          rule.prop
            .split('.')
            .reduce((s, prop) => s[prop], s)
            .toString() === rule.value
      );
    };

    const CheckForScripts = async ({ url: href, tabId, frameId, dataset }) => {
      if (s.enabled === false) return;
      const tabData = checkURL(href, dataset);

      console.log(href, tabData);

      const abs = u => (u.substr(1) === '/' ? u : `/${u}`);
      if (tabData.length > 0) {
        tabData.forEach(t => {
          (t.injected || []).forEach(data => {
            const u = typeof data == 'string' ? data : data.url;
            const inHead = data?.inHead ?? t.beforeAll ?? false;
            const isBrightspaceComponent = data?.isBrightspaceComponent ?? false;

            if (CheckRules(u)) {
              chrome.tabs.sendMessage(
                tabId,
                {
                  handshake: 'injectScript',
                  data: {
                    url: getURL(u.split('|')[0]),
                    inHead,
                    isBrightspaceComponent,
                    type: data.type ?? u.endsWith('mjs') ? 'module' : 'script',
                  },
                },
                { frameId }
              );
              // sendMessage({ tabId, frameId }, "injectScript", { url: getURL(u.split("|")[0]), inHead: t.beforeAll || false });
            }
          });
          (t.content || []).forEach(async data => {
            const u = typeof data == 'string' ? data : data.url;

            if (CheckRules(u)) {
              const file = abs(u.split('|')[0]);

              const loaded = await new Promise(res =>
                chrome.tabs.executeScript(
                  tabId,
                  {
                    frameId,
                    runAt: t.beforeAll ? 'document_start' : 'document_idle',
                    code: `
(() => { 
  window.__loadedcontentscripts__ = window.__loadedcontentscripts__ || new Set(); 
  if(window.__loadedcontentscripts__.has('${file}')) { return true; } 
  else { 
    window.__loadedcontentscripts__.add('${file}');
    return false; 
  } 
})()
`,
                  },
                  ([loaded]) => res(loaded)
                )
              );

              if (loaded) return;

              if (data.type == 'module') {
                chrome.tabs.executeScript(tabId, {
                  frameId,
                  runAt: t.beforeAll ? 'document_start' : 'document_idle',
                  code: `(async () => { await import("${chrome.runtime.getURL(file)}"); })()`,
                });
              } else {
                chrome.tabs.executeScript(tabId, {
                  file,
                  frameId,
                  runAt: t.beforeAll ? 'document_start' : 'document_idle',
                }); // chrome.tabs.executeScript(tabId, { file, frameId, runAt: "document_idle" });
              }
            }
          });
        });
      }
    };

    if (frameId === 0) {
      // changeIconBasedOnTab(url, tabId);
      await CheckForScripts({ url, tabId, frameId, dataset: tabs });
    } else {
      await CheckForScripts({
        url: await FrameURL({ tabId, frameId }),
        tabId,
        frameId,
        dataset: tabs.filter(tab => tab.frames),
      });
    }
  };
  chrome.webNavigation.onCompleted.addListener(OnPageNavigate);
  chrome.webNavigation.onHistoryStateUpdated.addListener(OnPageNavigate);

  const checkPageStatus = obj =>
    new Promise(async (resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', obj.href.includes('%') ? obj.href : encodeURI(obj.href));
      xhr.timeout = 60000;
      // Here, status code `0` can also mean that the XHR request timed out.
      xhr.addEventListener('loadend', () => {
        if (xhr.readyState === 4) {
          if (obj.status == null) {
            obj.status = xhr.status;
          }
          resolve(obj);
        }
      });
      xhr.addEventListener('timeout', () => {
        obj.status = 999;
      });
      xhr.send();
    });

  // Listens for messages from external sources (message requests from DOM-based scripts [injected scripts!] are considered external)
  // and handle them appropriately.
  const linkCheckerPermissionSet = {
    origins: ['<all_urls>'],
  };

  /**
   * Opens a URL, based on the ID passed to it.
   * 	Note: This will overwrite a "new tab" if open, otherwise will auto open in a new tab.
   * @param {string} id The ID of the URL to open.
   * @memberof Chrome/Background#
   */
  const openPage = id => {
    if (urls[id] !== undefined) {
      chrome.tabs.query(
        {
          active: true,
          currentWindow: true,
        },
        async tab => {
          const opts = await store();

          let url = urls[id];
          if (id === 'mylo-manager') {
            url += opts.manager.advancedView ? 'Advanced' : 'Units';
          }

          if (tab[0].url === NEWTAB) {
            chrome.tabs.update(tab[0].id, {
              url,
            });
          } else {
            chrome.tabs.create({
              url,
            });
          }
        }
      );
    }
  };

  // Messaging.Listen(async ({ data }, res) => {
  //   console.log(data);
  // });

  /**
   * Downloads the videos listed in the dataset.
   * Default action when encountering duplicate filenames is [uniquify]{@linkcode https://developer.chrome.com/extensions/downloads#type-FilenameConflictAction}.
   * @param {Types#VideoDownloads} msg The download dataset.
   * @memberof Chrome/Background#
   */
  const downloadVideos = async msg => {
    const permissionSet = {
      permissions: ['downloads'],
    };

    let canDownload = await new Promise(res => chrome.permissions.contains(permissionSet, res));
    if (!canDownload) canDownload = await new Promise(res => chrome.permissions.request(permissionSet, res));

    if (!canDownload) {
      console.error('Permission to download denied by user. Please allow permissions and try again.');
      return;
    }

    const safe = text => text.replace(/[\\/:*?"<>|]/g, '');
    const wait = time => new Promise(resolve => setTimeout(resolve, time));

    const suggestedDirectory = `Echo360 Downloads/${safe(msg.data.unit)}-${safe(msg.data.offering)}/`;

    for (const video of msg.data.videos) {
      const file = video.url.split('?')[0].trim();
      let ext = file.split('.');
      ext = ext[ext.length - 1].trim();
      chrome.downloads.download({
        url: video.url,
        filename: `${suggestedDirectory + safe(video.title).replace(/[\s]+/g, ' ')}${
          video.title.endsWith(ext) ? '' : `.${ext}`
        }`,
        conflictAction: 'uniquify',
      });
      await wait(500);
    }

    const removed = await new Promise(res => chrome.permissions.remove(permissionSet, res));
    if (!removed) {
      console.error(
        `Couldn't remove permission set %o; Permission denied. Is it possible that you tried removing a required permission?`,
        permissionSet
      );
    }
  };

  // A container to store active messaging ports in.
  const ports = {
    'echo360': null,
    'mylo-editor': null,
    'mylo-manager': null,
    'collaborate': null,
  };

  // Listens for requests from external sources; e.g. injected scripts!
  chrome.runtime.onConnectExternal.addListener(p => {
    switch (p.name) {
      case 'echo360-download-listener': {
        ports.echo360 = p;
        // When we get a handshake from our download port opening, store it so that we can send it the all-clear later on.
        p.onMessage.addListener(msg => {
          if (msg.flag === 'download') {
            downloadVideos(msg);
          }
        });
        break;
      }

      case 'icb-listener': {
        // If we get a handshake from our ICB tool port opening, store it so that we can toggle the ICB sidebar from our popup later.
        ports['mylo-editor'] = p;
        break;
      }

      case 'collaborate': {
        ports.collaborate = p;
        break;
      }
    }
  });

  const AsyncPostMessage = (port, message) => {
    return new Promise(res => {
      const handle = ev => {
        res(ev);
        port.onMessage.removeListener(handle);
      };
      port.onMessage.addListener(handle);
      port.postMessage(message);
    });
  };

  /**
   * Performs an action, based on an ID passed from our popup windows contextual pane.
   * @param {string} request The ID to parse against.
   * @memberof Chrome/Background#
   */
  const handleContextAction = async request => {
    switch (request) {
      case 'mylo-icb':
        if (ports['mylo-editor'] != null) {
          ports['mylo-editor'].postMessage({
            request: 'toggleVisibility',
          });
        }
        break;
      case 'all-links-in-new-tab':
        if (ports['mylo-editor'] != null) {
          ports['mylo-editor'].postMessage({
            request: 'all-links-in-new-tab',
          });
        }
        break;
      case 'echo360-download':
        if (ports.echo360 != null) {
          ports.echo360.postMessage({
            request: 'download',
          });
        }
        break;
      case 'check-utas-links': {
        const [tab] = await new Promise(res => chrome.tabs.query({ active: true, currentWindow: true }, res));
        const file = '/js/utas-wide/link-checker.js';
        chrome.tabs.executeScript(tab.id, { file });
        break;
      }
      case 'collab-download': {
        const msg = await AsyncPostMessage(ports.collaborate, { request: 'download' });
        console.log(msg);
        const permissionSet = {
          permissions: ['downloads'],
        };

        let canDownload = await new Promise(res => chrome.permissions.contains(permissionSet, res));
        if (!canDownload) canDownload = await new Promise(res => chrome.permissions.request(permissionSet, res));

        if (!canDownload) {
          console.error('Permission to download denied by user. Please allow permissions and try again.');
          return;
        }

        const safe = text => text.replace(/[\\/:*?"<>|]/g, '');
        /** const wait = time => new Promise(resolve => setTimeout(resolve, time)); */

        const suggestedDirectory = `Collaborate Downloads/${safe(msg.userContext.user.ltiLaunchDetails.context_title)}/`;

        for (const video of msg.recordings) {
          const ref = `https://au-lti.bbcollab.com/collab/api/csa/recordings/${video.id}/url?disposition=download`;
          const auth = msg.userContext.user.$response.config.headers.Authorization;
          const url = await fetch(ref, { headers: { Authorization: auth } })
            .then(d => d.json())
            .then(d => d.url);

          chrome.downloads.download({
            url,
            filename: `${suggestedDirectory + safe(video.name).replace(/[\s]+/g, ' ')}.mp4`,
            conflictAction: 'uniquify',
          });
          /** await wait(500); */
        }

        const removed = await new Promise(res => chrome.permissions.remove(permissionSet, res));
        if (!removed) {
          console.error(
            `Couldn't remove permission set %o; Permission denied. Is it possible that you tried removing a required permission?`,
            permissionSet
          );
        }
        break;
      }
      default:
        break;
    }
  };

  // This is what handles the popup page!
  chrome.runtime.onConnect.addListener(p => {
    if (p.name === 'popup') {
      p.onMessage.addListener(msg => {
        if (msg.action === 'open') {
          openPage(msg.data);
        }
        if (msg.action === 'context') {
          handleContextAction(msg.data);
        }
      });
    }
  });

  /**
   * Performs a deep merge of `source` into `target`.
   * Mutates `target` only but not its objects and arrays.
   *
   * @author inspired by [jhildenbiddle](https://stackoverflow.com/a/48218209).
   */
  function mergeDeep(target, source) {
    const isObject = obj => obj && typeof obj === 'object';

    if (!isObject(target) || !isObject(source)) {
      return source;
    }

    Object.keys(source).forEach(key => {
      const targetValue = target[key];
      const sourceValue = source[key];

      if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
        target[key] = targetValue.concat(sourceValue);
      } else if (isObject(targetValue) && isObject(sourceValue)) {
        target[key] = mergeDeep(Object.assign({}, targetValue), sourceValue);
      } else {
        target[key] = sourceValue;
      }
    });

    return target;
  }

  let optionsPayload = null;
  const handleRuntimeMessage = (req, s, res) => {
    if (!req) return null;

    switch (req.request) {
      case 'ExtensionMode': {
        if (chrome.runtime.id === ExtensionID.Live) return res('PROD');
        else if (chrome.runtime.id === ExtensionID.Beta) return res('BETA');
        else return res('DEV');
      }

      case 'IsDev': {
        return res(chrome.runtime.id !== ExtensionID.Live && chrome.runtime.id !== ExtensionID.Beta);
      }

      case 'GetRelease': {
        const { Beta, Live } = ExtensionID;

        switch (chrome.runtime.id) {
          case Live:
            return res('STABLE');
          case Beta:
            return res('BETA');
          default:
            return res('DEV');
        }
      }

      case 'store': {
        chrome.storage.sync.get(null, res);
        return true;
      }

      case 'cors-api': {
        fetch(req.url, req.body)
          .then(r => {
            r.ok ? r.text().then(res) : res(r.status);
          })
          .catch(() => res(null));
        return true;
      }

      // If we get a request to update records from MyLO Manager,
      // handle it here.
      case 'updateRecord':
        chrome.storage.sync.get(
          {
            manager: {},
          },
          data => {
            data.recordsPerPage = request.records;
            chrome.storage.sync.set({
              manager: data,
            });
          }
        );
        return false;

      case 'settings': {
        chrome.storage.sync.get(store_defaults, res);
        return true;
      }

      case 'settingsSave': {
        chrome.storage.sync.get(null, orig => {
          const payload = mergeDeep(orig, req.data);
          setDisabledBadgeState(!payload.enabled);
          chrome.storage.sync.set(payload, res);
        });
        return true;
      }

      case 'link-check':
        if (Array.isArray(req.data)) {
          Promise.all(req.data.map(checkPageStatus)).then(res);
        } else {
          checkPageStatus(req.data).then(res);
        }
        return true;

      case 'link-checker-has-permissions':
        chrome.permissions.contains(linkCheckerPermissionSet, res);
        return true;

      case 'link-checker-get-permission':
        (async function () {
          let canDownload = await new Promise(res => chrome.permissions.contains(linkCheckerPermissionSet, res));
          if (!canDownload)
            canDownload = await new Promise(res => chrome.permissions.request(linkCheckerPermissionSet, res));
          if (!canDownload) {
            const message = 'Permission to check links denied by user. Please allow permissions and try again.';
            console.error(message);
          }
          return canDownload;
        })().then(res);
        return true;

      case 'link-check-revoke-permission':
        (async function () {
          const removed = await new Promise(res => chrome.permissions.remove(linkCheckerPermissionSet, res));
          if (!removed) {
            console.error(
              `Couldn't remove permission set %o; Permission denied. Is it possible that you tried removing a required permission?`,
              linkCheckerPermissionSet
            );
          }
          return removed;
        })().then(res);
        return true;

      case 'dom':
        fetch(req.url)
          .catch(() => res(null))
          .then(d => (d?.ok ? d.text() : ''))
          .then(res);
        return true;

      case 'check-status': {
        const urls = Array.isArray(req.payload) ? req.payload : [req.payload];
        const unique = urls.filter((v, i, a) => a.indexOf(v) == i);

        let results = {};
        Promise.all(
          unique.map(async url => {
            try {
              results[url] = await fetch(url, { method: 'head' })
                .then(res => res.status)
                .catch(e => 404);
            } catch (e) {
              results[url] = 418;
            }
          })
        ).then(() => res(results));

        return true;
      }

      default:
        break;
    }

    if (req.section === 'options') {
      res(optionsPayload);
      optionsPayload = null;
    }

    if (req.open === 'options') {
      optionsPayload = req.data;
      chrome.runtime.openOptionsPage();
      return false;
    }

    if (req.flag === 'checkURL') {
      Promise.all(req.payload.map(checkPageStatus)).then(res);
      return true;
    }

    return true;
  };
  chrome.runtime.onMessage.addListener(handleRuntimeMessage);
  chrome.runtime.onMessageExternal.addListener(handleRuntimeMessage);
  // try {
  //   let reg = await navigator.serviceWorker.register('sw-cache.js', { scope: '/' });
  //   console.log('Service worker registered: ', reg);
  // } catch (e) {
  //   console.error(`Couldn't register the service worker: `, e);
  // }
})();

// eslint-disable-next-line no-unused-vars
function openSupportTicket() {
  const url = 'https://utas1.service-now.com/selfservice/?id=sc_cat_item_v2&sys_id=bdce1e5fdbf00300f32d75a9bf961943';
  chrome.tabs.create({ url });
}

// eslint-disable-next-line no-unused-vars
function setDisabledBadgeState(disabled) {
  const text = disabled ? 'OFF' : IS_BETA ? 'BETA' : '';
  chrome.browserAction.setBadgeText({
    text,
  });
  chrome.browserAction.setBadgeBackgroundColor({
    color: '#80251a',
  });
}

// eslint-disable-next-line no-unused-vars
function refreshCurrentPage() {
  chrome.tabs.executeScript({
    code: 'window.location.reload()',
  });
}

// eslint-disable-next-line no-unused-vars
async function generateBugReport() {
  const activeTab = await new Promise(res =>
    chrome.tabs.query(
      {
        active: true,
        currentWindow: true,
      },
      res
    )
  );
  if (activeTab) {
    const tab = activeTab[0];
    chrome.tabs.sendMessage(tab.id, {
      request: 'bug-report',
    });
  }
}
