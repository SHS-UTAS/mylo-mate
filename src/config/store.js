(async function () {
  const store = async name => (await fetch(chrome.runtime.getURL(`config/${name}.json`))).json();
  const defaults = await store('default');

  // Gets all settings for the extension
  chrome.storage.sync.get(null, function (items) {
    // This is reproduced and commented in `Utils.js`
    // const extend = () => { const a = arguments; for (let i = 1; i < a.length; i++) { for (const k in a[i]) { if (a[i].hasOwnProperty(k)) { if (typeof a[0][k] === 'object' && typeof a[i][k] === 'object') { extend(a[0][k], a[i][k]); } else { a[0][k] = a[i][k]; } } } } return a[0]; };

    // Fill the existing data with any blanks from our default object.
    const data = $.extend(true, {}, defaults, items);
    // const data = extend({}, defaults, items);

    // Set the inline variables that need to be called from within injected scripts.
    // These are currently a stringified data object (all our settings), and the extension ID for message handling.
    const inlineVariables = `var store = JSON.parse('${JSON.stringify(data).replace(
      /[\/\(\)\']/g,
      '\\$&'
    )}'); var extensionID = "${chrome.runtime.id}"`;

    // If we're not looking at a page with the chrome-extension protocol, then inject our inline variables.
    if (!window.location.href.startsWith('chrome-extension://')) {
      document.head.appendChild(document.createElement('script')).innerHTML = inlineVariables;
    }

    // Make sure that we update our dataset to fill any blanks from earlier.
    chrome.storage.sync.set(data);
  });
})();
