(async function () {
  const store = await FetchMMSettings();
  if (store.other.makeAccessible) {
    document.body.setAttribute('mm-accessible', true);
    document.body.classList.add('mm-access-friendly');
  }
})();

