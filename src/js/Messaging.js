// TODO:
// [x] Detect if in a content script or page script.
// [ ] Connect to background script with a new Port object.
// [ ] Expose a OnMessage and Broadcast method
// [ ] Listen for `window.postMessage` events, ensure they have the right signature (to ensure that they are ours), and if so,
// [ ] Pass them along the same chain.

// If we're clever, we can detect if we're in a page or a content script.
// Then, we can wrap the 'if in page, use `window.postMessage`, otherwise use `Port` into the same handlers.

const struct = {
  data: null,
  action: null,
  originalAction: null,
  signature: '',
  target: '',
  motion: '',
};

const noop = () => console.log('Calling noop');
const uuid = () =>
  ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
  );

const isContentScript = typeof chrome?.runtime?.getURL != 'undefined';

class ContentMessaging {
  pool = [];
  port = chrome.runtime.connect();

  constructor() {
    window.addEventListener('message', ({ data }) => {
      if (data.action == 'broadcast:page') {
        this.port.postMessage(data);
      }
    });

    this.port.onMessage.addListener(data => {
      if (data.action == 'broadcast:background') {
        const obj = data.data;
        if (obj.motion == 'response') return;

        const packet = {
          ...struct,
          ...obj,
          originalAction: data.action,
          action: 'broadcast:content',
          motion: 'response',
        };

        const res = args => {
          packet.data = args;
          this.port.postMessage(packet);
        };

        this.pool.forEach(cb => cb(obj, res));
        window.postMessage(packet, '*');
      }
    });
  }

  Broadcast(data, cb, { target = 'all' } = {}) {
    const id = uuid();
    const handler = ({ data }) => {
      try {
        if (data && data.signature == id && data.motion == 'response') {
          const res = args => {
            const packet = {
              ...struct,
              ...obj,
              originalAction: data.action,
              action: 'broadcast:content',
              motion: 'response',
              signature: id,
              data: args,
            };

            this.port.postMessage(packet);
          };

          cb(data.data, res);
        }
      } catch (e) {
        console.error(
          'An error occured whilst broadcasting a message from Messaging [Content]. The payload being sent was: ',
          data,
          e
        );
      }
    };

    this.port.onMessage.addListener(handler);
    this.port.postMessage({
      ...struct,
      data,
      target,
      signature: id,
      motion: 'request',
      action: 'broadcast:content',
    });
  }

  Listen(cb) {
    this.pool.push(cb);
  }
}

class WindowMessaging {
  pool = [];

  constructor() {
    window.addEventListener('message', ({ data }) => {
      if (data.motion == 'request') {
        let respond = payload => {
          let packet = {
            ...struct,
            ...data,
            data: payload,
            action: 'broadcast:page',
            motion: 'response',
          };

          window.postMessage(packet, '*');
        };

        this.pool.forEach(cb => cb(data, respond));
      }
    });
  }

  Broadcast(data, cb, { target = 'all' } = {}) {
    const id = uuid();
    const handler = ({ data }) => {
      if (data && data.signature == id && data.motion == 'response') {
        cb(data, noop);
        window.removeEventListener('message', handler);
      }
    };

    window.addEventListener('message', handler);
    window.postMessage(
      {
        ...struct,
        data,
        target,
        signature: id,
        motion: 'request',
        action: 'broadcast:page',
      },
      '*'
    );
  }

  Listen(cb) {
    this.pool.push(cb);
  }
}

const Messaging = isContentScript ? new ContentMessaging() : new WindowMessaging();

