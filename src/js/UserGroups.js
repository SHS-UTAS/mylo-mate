const JSONtoSearchParams = json => {
  let params = new URLSearchParams();
  Object.entries(json).forEach(([key, value]) => params.append(key, value));
  return params;
};

const get = defaults => new Promise(res => chrome.storage.local.get(defaults, res));
const set = data => new Promise(res => chrome.storage.local.set(data, res));

class UserGroups extends EventTarget {
  constructor() {
    super();
    chrome.storage.local.onChanged.addListener(change => {
      if (change.usergroups?.newValue) {
        this.dispatchEvent(new CustomEvent('update', { detail: change.usergroups.newValue }));
      }
    });
  }

  static UserRoles = ['Auditor', 'Lecturer', 'Student', 'Support', 'Tutor', 'UnitCoordinator'];

  _store() {
    return get({ usergroups: {} });
  }

  static get CanConnect() {
    return new Promise(async res => {
      try {
        await fetch('https://mylo-manager.utas.edu.au/', { method: 'head', mode: 'no-cors' });
        res(true);
      } catch (e) {
        res(false);
      }
    });
  }

  static get SignedIn() {
    return new Promise(async res => {
      const req = await fetch('https://mylo-manager.utas.edu.au/Home/Units', { mode: 'no-cors' });
      if (!req.ok || req.url.includes('login.microsoftonline.com')) {
        res(false);
      } else {
        const body = await req.text();
        res(!body.toLowerCase().includes('sign in'));
      }
    });
  }

  async _groups() {
    const store = await this._store();
    return store.usergroups ?? {};
  }

  async size() {
    let groups = await this.get();
    return groups.length;
  }

  async get(id = null) {
    const groups = await this._groups();
    if (id != null) return groups[id];
    else
      return Object.entries(groups)
        .filter(([k, v]) => !k.startsWith('_'))
        .map(([k, v]) => v);
  }

  async save(id, group, updateId) {
    const collection = await this._groups();
    if (updateId) {
      delete collection[id];
      id = group.ID;
    }

    return set({ usergroups: { ...collection, [id]: group } });
  }

  async saveAll(groups) {
    if (Array.isArray(groups)) {
      groups = groups.reduce((o, c) => {
        o[c.ID] = c;
        return o;
      }, {});
    }

    return set({ usergroups: groups });
  }

  async delete(id) {
    const collection = await this._groups();
    delete collection[id];

    return set({ usergroups: collection });
  }

  static async SearchForUser(username) {
    try {
      const body = JSONtoSearchParams({
        _search: false,
        nd: 1501635126034,
        rows: 100,
        page: 1,
        sidx: 'givenname',
        sord: 'asc',
      });

      const req = await fetch(
        `https://mylo-manager.utas.edu.au/User/StaffSearch?Surname=&GivenName=&Username=${username}&DepartmentCode=&StaffId=`,
        { method: 'POST', body }
      );
      if (req.ok) {
        const res = await req.json();

        if (res.records == 0) return null;

        const user = res.rows.find(row => row.id === username);
        if (!user) return null;

        const [PreferredName, Surname, Username, StaffId, Schools] = user.cell;
        return {
          id: user.id,
          PreferredName,
          Surname,
          Username,
          StaffId,
          Schools,
        };
      }
    } catch (e) {
      console.error(e);
      return null;
    }
  }
}

class OptionsUI {
  static prompt(title, desc, placeholder, defaultValue) {
    return new Promise(resolve => {
      let dialog = document.createElement('dialog');
      dialog.classList.add('dialog', 'dialog-prompt');

      let name = document.createElement('p');
      name.className = 'name';
      name.innerText = title || '';

      let info = document.createElement('div');
      info.className = 'info';
      info.innerHTML = desc || '';

      let input = document.createElement('input');
      input.type = 'text';
      input.placeholder = placeholder || '';
      input.value = defaultValue || '';

      input.addEventListener('keyup', e => {
        if (e.keyCode === 13) {
          dialog.close();
          resolve(input.value);
          document.body.removeChild(dialog);
        }
      });

      let dialog_yes = document.createElement('button');
      dialog_yes.innerText = 'OK';
      dialog_yes.classList.add('btn', 'okay_btn');
      dialog_yes.addEventListener('click', () => {
        dialog.close();
        resolve(input.value);
        dialog.remove();
      });

      let dialog_no = document.createElement('button');
      dialog_no.innerText = 'Cancel';
      dialog_no.classList.add('btn', 'cancel_btn');
      dialog_no.addEventListener('click', () => {
        dialog.close();
        resolve(null);
        dialog.remove();
      });

      dialog.append(name, info, input, dialog_yes, dialog_no);
      document.body.append(dialog);
      dialog.showModal();
    });
  }

  static alert(title, desc) {
    let dialog = document.createElement('dialog');
    dialog.classList.add('dialog', 'dialog-alert');

    let name = document.createElement('p');
    name.className = 'name';
    name.innerText = title || '';

    let info = document.createElement('div');
    info.className = 'info';
    info.innerHTML = desc || '';

    let okay = document.createElement('button');
    okay.innerText = 'OK';
    okay.classList.add('btn', 'okay_btn');
    okay.addEventListener('click', () => {
      dialog.close();
      dialog.remove();
    });

    dialog.append(name, info, okay);
    document.body.append(dialog);
    dialog.showModal();
  }

  static confirm(title, desc, yes, no) {
    return new Promise(resolve => {
      let dialog = document.createElement('dialog');
      dialog.classList.add('dialog', 'dialog-confirm');

      let name = document.createElement('p');
      name.className = 'name';
      name.innerText = title || '';

      let info = document.createElement('div');
      info.className = 'info';
      info.innerHTML = desc || '';

      let dialog_yes = document.createElement('button');
      dialog_yes.innerText = yes || 'OK';
      dialog_yes.classList.add('btn', 'okay_btn');
      dialog_yes.addEventListener('click', () => {
        dialog.close();
        dialog.remove();
        resolve(true);
      });

      let dialog_no = document.createElement('button');
      dialog_no.innerText = no || 'Cancel';
      dialog_no.classList.add('btn', 'cancel_btn');
      dialog_no.addEventListener('click', () => {
        dialog.close();
        dialog.remove();
        resolve(false);
      });

      dialog.append(name, info, dialog_yes, dialog_no);
      document.body.append(dialog);

      dialog.showModal();
    });
  }

  static notify(title, info, { classes = '', timeout = 5, parent = document.body } = {}) {
    let n = document.createElement('div');
    n.className = 'visible ' + classes;
    n.id = 'notify';
    n.innerHTML = `<p class="title">${title}<span class="close" title="Dismiss message">x</span></p><p class="desc">${info}</p>`;
    parent.append(n);

    let timer = setTimeout(() => n.classList.add('fade'), timeout * 1000);
    n.querySelector('.close').addEventListener('click', () => {
      clearTimeout(timer);
      n.remove();
    });
    n.addEventListener('animationend', () => n.remove());
  }

  static promptRoles(title, desc, users) {
    return new Promise(resolve => {
      let dialog = document.createElement('dialog');
      dialog.classList.add('dialog', 'dialog-prompt');

      let name = document.createElement('p');
      name.className = 'name';
      name.innerText = title || '';

      let info = document.createElement('div');
      info.className = 'info';
      info.innerHTML = desc || '';

      let table = document.createElement('table');
      table.className = 'userRoles';
      table.setAttribute('cellspacing', 0);

      let headers = document.createElement('tr');

      let headName = document.createElement('th');
      headName.innerText = 'User';

      let headRole = document.createElement('th');
      headRole.innerText = 'Role';

      let headEcho = document.createElement('th');
      headEcho.innerText = 'Echo360 Level';

      headers.append(headName, headRole, headEcho);

      table.append(headers);

      let roles = document.createElement('select');
      roles.className = 'roleSelect';
      UserRoles.forEach(role => {
        let r = document.createElement('option');
        r.value = r.innerHTML = role;
        roles.append(r);
      });

      const echoRoles = document.createElement('select');
      echoRoles.className = 'echoRoleSelect';
      echoRoles.innerHTML = `<option value="0">None</option> <option value="2">Instructor</option>`;

      users.forEach(user => {
        let row = document.createElement('tr');

        let u = document.createElement('td');
        u.innerText = `${user.PreferredName} ${user.Surname} (${user.Username})`;

        let r = document.createElement('td');
        r.append(roles.cloneNode(true));
        r.querySelector(`select option[value="${user.Role || 'Support'}"]`).selected = true;

        const e = document.createElement('td');
        const er = echoRoles.cloneNode(true);
        e.append(er);
        er.querySelector(`select option[value="${user.EchoRole || '0'}"]`).selected = true;

        row.append(u, r, e);

        row.setAttribute('user', user.Username);
        table.append(row);
      });

      let dialog_done = document.createElement('button');
      dialog_done.innerText = 'Close';
      dialog_done.classList.add('btn', 'okay_btn');
      dialog_done.addEventListener('click', function () {
        dialog.close();
        let roles = [];
        document.querySelectorAll('tr[user]').forEach(row => {
          const role = row.querySelector('.roleSelect').value;
          const echo = row.querySelector('.echoRoleSelect').value;

          let user = users.find(u => u.Username === row.getAttribute('user'));
          user.Role = role;
          user.EchoRole = echo;

          roles.push(user);
        });
        resolve(roles);
        dialog.remove();
      });

      dialog.append(name, info, table, dialog_done);
      document.body.append(dialog);
      dialog.showModal();
    });
  }

  static promptUserAdd(title, desc, placeholder, defaultValue, roleText, defaultForceOverride) {
    return new Promise(resolve => {
      let dialog = document.createElement('dialog');
      dialog.classList.add('dialog', 'dialog-prompt');

      let name = document.createElement('p');
      name.className = 'name';
      name.innerText = title || '';

      let info = document.createElement('div');
      info.className = 'info';
      info.innerHTML = desc || '';

      let input = document.createElement('input');
      input.type = 'text';
      input.placeholder = placeholder || '';
      input.value = defaultValue || '';

      input.addEventListener('keyup', e => {
        if (e.code == '13') {
          dialog.close();
          resolve({ users: input.value, role: roles.value, forceRole: forceRoleCb.checked, echoRole: echoLevels.value });
          dialog.remove();
        }
      });

      let role = document.createElement('p');
      role.innerHTML = roleText;

      let roles = document.createElement('select');
      roles.style.marginLeft = '1em';
      roles.style.display = 'inline-block';
      roles.innerHTML = UserGroups.UserRoles.map(
        role => `<option value="${role}"${role === 'Support' ? ' selected' : ''}>${role}</option>`
      ).join('\n');

      let forceRole = document.createElement('label');
      forceRole.innerText = 'Override existing role?';
      forceRole.style.marginLeft = '20px';

      let forceRoleCb = document.createElement('input');
      forceRoleCb.type = 'checkbox';
      forceRoleCb.checked = defaultForceOverride;
      forceRole.prepend(forceRoleCb);

      role.append(roles, forceRole);

      let echoLevel = document.createElement('p');
      echoLevel.innerHTML = 'Echo360 Instructor';

      let echoLevels = document.createElement('select');
      echoLevels.style.display = 'inline-block';
      echoLevels.style.marginLeft = '1em';
      echoLevels.innerHTML = `<option value="0" selected>None</option> <option value="2">Instructor</option>`;

      echoLevel.append(echoLevels);

      let dialog_yes = document.createElement('button');
      dialog_yes.innerText = 'OK';
      dialog_yes.classList.add('btn', 'okay_btn');
      dialog_yes.addEventListener('click', function () {
        dialog.close();
        resolve({ users: input.value, role: roles.value, forceRole: forceRoleCb.checked, echoRole: echoLevels.value });
        dialog.remove();
      });

      let dialog_no = document.createElement('button');
      dialog_no.innerText = 'Cancel';
      dialog_no.classList.add('btn');
      dialog_no.classList.add('cancel_btn');
      dialog_no.addEventListener('click', function () {
        dialog.close();
        resolve(null);
        dialog.remove();
      });

      dialog.append(name, info, input, role, echoLevel, dialog_yes, dialog_no);
      document.body.append(dialog);
      dialog.showModal();
    });
  }
}
