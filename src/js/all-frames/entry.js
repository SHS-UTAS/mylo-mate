// /**
//  * @purpose Injects the "Impersonate Student View" into any classlist iframes that appear. 
//  */
// (async function() {
// 	// Test that we're definitely within an iframe.
// 	// If we are, then add our javascript file.
// 	const IFRAME = window.self !== window.top;
// 	if (window.location.href.includes('d2l/lms/classlist/classlist.d2l?ou=') && IFRAME) {
// 		let script = document.createElement('script');
// 		script.src = chrome.runtime.getURL('js/mylo-classlist/impersonate-student-view.js');
// 		document.head.appendChild(script);
// 	}
// })();