(async function () {
  // Set to true to show notification panel with this update
  let showUpdateNotificationPanel = false;
  const notification = {
    title: `MyLO MATE updated to v${chrome.runtime.getManifest().version}!`,
    message: 'Creating rubrics? Check out the recent changes.',
  };
  const ver = chrome.runtime.getManifest().version.substr(chrome.runtime.getManifest().version.lastIndexOf('.') + 1);
  if (chrome.runtime.id == ExtensionID.Beta) {
    // This fetches a list of all the other installed extensions.
    const all_exts = await new Promise(res => chrome.management.getAll(res));
    // If our live version is installed, then disable it.
    // Without this check, we get an error when it's not installed and we try to set it's enabled state.
    if (all_exts.find(ext => ext.id == ExtensionID.Live)) {
      // The Chrome Management system is enabled. Lets listen for onInstalled and onEnabled to check for our MyLO MATE Stable version.
      const DisableStable = ext => {
        chrome.management.setEnabled(ExtensionID.Live, false);
      };
      // Whenever an extension is installed or enabled, use that as a flag to make sure the live extension is disabled.
      //  Also do this whenever this extension is installed itself.
      chrome.management.onInstalled.addListener(DisableStable);
      chrome.management.onEnabled.addListener(DisableStable);
      chrome.runtime.onInstalled.addListener(DisableStable);
    }

    showUpdateNotificationPanel = false;
  }

  chrome.runtime.onInstalled.addListener(async details => {
    switch (details.reason) {
      case 'install':
        chrome.notifications.create(
          {
            type: 'basic',
            iconUrl: '/images/icon48.png',
            title: 'Welcome to MyLO MATE!',
            message: 'For information on how to get started with MyLO MATE, click this notification.',
          },
          id => {
            chrome.notifications.onClicked.addListener(notificationID => {
              if (notificationID !== id) return;
              chrome.tabs.create({
                url: 'http://www.utas.edu.au/building-elearning/resources/mylo-mate',
              });
              chrome.notifications.clear(id);
            });
          }
        );
        break;
      case 'update':
        if (details.previousVersion !== chrome.runtime.getManifest().version) {
          if (showUpdateNotificationPanel) {
            chrome.notifications.create(
              {
                type: 'basic',
                iconUrl: '/images/icon48.png',
                title: notification.title,
                message: notification.message,
                buttons: [
                  {
                    //title: `Updates in v${ver}`,
                    title: `More Information`,
                  },
                  {
                    title: 'Close',
                  },
                ],
                requireInteraction: true,
              },
              id => {
                chrome.notifications.onButtonClicked.addListener((notificationID, btnIndex) => {
                  if (notificationID !== id) return;
                  if (btnIndex == 0) {
                    chrome.tabs.create({
                      url: 'https://www.utas.edu.au/building-elearning/resources/mylo-rubric-generator/may-2021-changes', // 'http://www.utas.edu.au/building-elearning/resources/mylo-mate#highlight',
                    });
                  }
                  chrome.notifications.clear(id);
                });
              }
            );
          }

          /**
           * TEMPORARY
           * Migrate user groups from sync storage to local storage.
           */
          /**
           * chrome.storage.sync.get(null, data => {
           *   chrome.storage.local.set({ usergroups: data.usergroups.groups });
           *   delete data.usergroups.groups;
           *   chrome.storage.sync.set(data);
           * });
           */

          // Overlay our existing settings onto our settings structure,
          //	clear the existing settings and then apply our fresh structure (with the existing settings in place!)
          let newData = await exportSettings();
          chrome.storage.sync.clear(() => chrome.storage.sync.set(newData));
        }
        break;
    }
  });

  function apply(structure, data) {
    let o = {};

    function iterate(a, b, base = o) {
      if (!a || !b) return;
      for (let k in a) {
        if (a.hasOwnProperty(k)) {
          if (typeof a[k] === 'object') {
            base[k] = base[k] || {};
            if (a[k]['_raw']) {
              base[k] = {
                ...a[k],
                ...b[k],
              };
            } else iterate(a[k], b[k], base[k]);
          } else {
            base[k] = b.hasOwnProperty(k) ? b[k] : a[k];
          }
        }
      }
    }
    iterate(structure, data);
    return o;
  }

  const store = async name => (await fetch(chrome.runtime.getURL(`config/${name}.json`))).json();
  const exportSettings = async () => {
    let current = await new Promise(res => chrome.storage.sync.get(null, res));
    let defaults = await store('default');
    return apply(defaults, current);
  };
})();
