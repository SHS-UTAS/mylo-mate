export {
  faArrowUp,
  faInfoCircle,
  faSearch,
  faLink,
  faUserGraduate,
  faUsers,
  faSchool,
  faChartBar,
  faFileUpload,
  faCloud,
  faBookOpen,
} from '@fortawesome/free-solid-svg-icons';
