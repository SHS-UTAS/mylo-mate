/**
 * @namespace Echo360
 */
(async () => {
  // The window guard prevents this script from being run more than once without being reloaded.
  // This is to prevent any duplications issues.
  if (window.guardactive) return;
  window.guardactive = true;

  /**
   * Finds the React properties attached to a DOM element.
   *
   * @param {any} dom The DOM element to get React properties from.
   * @returns {(object|null)} The React properties of the object, or null if none found.
   * @memberof Echo360#
   */
  function FindReact(dom) {
    for (var key in dom) {
      if (key.startsWith('__reactInternalInstance$')) return dom[key].alternate;
    }
    return null;
  }

  /**
   * Checks the properties of the rows, and retrieves the downloadable links and names from each element.
   *
   * @returns {object} Object containing the unit name, offering period (e.g. 2017_S2-ALL), and the videos found.
   * @memberof Echo360#
   */
  function checkProperties(port) {
    return new Promise((resolve, reject) => {
      // An array to hold our video elements in.
      let videos = [];
      const rows = [];

      const react = FindReact(document.querySelector('.section-home > .class-list > .contents-wrapper'));
      if (!react) return;
      const dataset = react.memoizedProps.children;
      dataset.forEach(v => {
        if (v.props.type == 'LessonGroup') v.props.children.props.children.forEach(c => rows.push(c.props));
        else rows.push(v.props);
      });

      const title = document.querySelector('.course-section-header h1');
      let offering = title?.innerText.match(/20[0-9]{2}_[^-]+(-|_)[^\s]+/)?.[0] ?? 'Unknown Echo360 Unit';

      let stack = [];
      rows.forEach(p => {
        try {
          if (p.lesson.medias.length == 0) return;
          let media = p.lesson.medias[0];
          if (!media.isAvailable) return;
          if (media.mediaType != 'Video') return;

          stack.push({ title: media.title, url: `https://echo360.net.au/media/download/${media.id}/hd1.mp4` });
        } catch (e) {
          console.warn('Could not get media file from %o', p);
        }
      });

      port.postMessage({
        flag: 'download',
        data: {
          unit: title?.innerText.split('-')?.[0].trim() ?? offering,
          offering,
          videos: stack,
        },
      });
    });
  }

  // Open a communications channel with the background script.
  const p = chrome.runtime.connect(extensionID, { name: 'echo360-download-listener' });

  // Post a greeting to open the network on the other side, so it can start sending things back!
  p.postMessage({ flag: 'init' });

  // When we get the request to start downloading..
  p.onMessage.addListener(function (msg) {
    // If the request is specifically for a download,
    // then send back a message with the same flag,
    // and with our collection of videos.
    if (msg.request == 'download') {
      checkProperties(p);
    }
  });
})();
