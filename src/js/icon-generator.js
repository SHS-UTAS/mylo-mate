(async function () {
  if (window.location.pathname.endsWith('.pdf')) return;

  function check() {
    if (!document.head.querySelector('link#icon-gen')) {
      const link = document.createElement('link');
      link.href = chrome.runtime.getURL('css/mylo-mate-icons.css');
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.id = 'icon-gen';
      document.head.appendChild(link);
    }
    document.querySelectorAll('[mm-icon-type]:not([mm-generated="true"])').forEach((item) => {
      const parent = item;
      // if (item.getAttribute('mm-icon-render') == 'background') {
      // } else {
      const o = (item.getAttribute('mm-icon-offset') || '').split(':');
      const offset = {
        top: parseInt(o[1] || 0),
        left: parseInt(o[0] || 0)
      };

      const handle = document.createElement('span');
      handle.style.position = 'absolute';
      handle.style.left = `${offset.left}px`;
      handle.style.top = `${offset.top + 5}px`;
      handle.style.margin = 0;
      handle.style.padding = 0;

      Array.from(parent.attributes).forEach((attr) => {
        if (attr.name.startsWith('mm-icon')) {
          handle.setAttribute(attr.name, attr.value);
          parent.removeAttribute(attr.name);
        }
      });

      handle.setAttribute('mm-generated', 'true');
      parent.style.position = parent.style.position || 'relative';
      parent.insertAdjacentElement('afterbegin', handle);
      // }

      item.setAttribute('mm-generated', 'true');
    });
  }

  // check();
  setInterval(check, 500);
}());
