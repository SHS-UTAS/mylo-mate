(async () => {
  const config = await FetchMMSettings();
  if (!config.other.BulkDeleteAnnouncements) return;

  const button = document.createElement('d2l-button-subtle');
  button.setAttribute('icon', 'tier1:discussions');
  button.setAttribute('text', 'Delete All Announcements...');
  button.setAttribute('description', 'Delete all announcements');

  button.setAttribute('mm-icon-type', 'action-button');
  button.setAttribute('mm-icon-offset', '4:4');

  document.querySelector('d2l-overflow-group')?.append(button);

  button.onclick = async e => {
    if (confirm('Are you sure you want to delete every announcement? This cannot be undone.')) {
      button.toggleAttribute('disabled', true);
      const ou = window.location.searchParams.ou;

      const posts = await api(`/d2l/api/le/1.31/${ou}/news/`, { fresh: true });
      await Promise.all(posts.map(post => apisend(`/d2l/api/le/1.31/${ou}/news/${post.Id}`, null, { verb: 'DELETE' })));

      button.toggleAttribute('disabled', false);

      window.location.reload();
    }
  };
})();
