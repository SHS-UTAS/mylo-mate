import jss from '../../lib/jss.bund.js';
import preset from '../../lib/jss-preset-default.bund.js';
const store = FetchMMSettings();
jss.setup(preset());

/**
 * @affects Assignment Folders
 * @problem Manually removing previous delivery assignment dates when starting a new delivery is tedious and error-prone.
 * @solution An `Uncheck all dates` button that automatically unchecks all assignment availability dates on the Bulk Edit Assignments page.
 * @target Support staff
 * @impact Moderate
 */

const { classes } = jss
  .createStyleSheet({
    button: {
      'backgroundColor': '#e3e9f1',
      'color': '#494c4e',
      'borderRadius': '.3rem',
      'borderStyle': 'none',
      'borderWidth': '1px',
      'boxSizing': 'border-box',
      'cursor': 'pointer',
      'display': 'inline-block',
      'margin': '0',
      'minHeight': 'calc(2rem + 2px)',
      'outline': 'none',
      'textAlign': 'center',
      'transition': 'box-shadow .2s',
      'userSelect': 'none',
      'verticalAlign': 'middle',
      'whiteSpace': 'nowrap',
      'width': 'auto',
      'boxShadow': '0 0 0 4px transparent',
      'fontFamily': 'inherit',
      'padding': '.55rem 1.5rem',
      'fontSize': '.7rem',
      'lineHeight': '1rem',
      'fontWeight': '700',
      'letterSpacing': '.2px',
      'marginRight': '.75rem',
      'width': 'auto',

      '&:hover,&:focus,&[active]': {
        background: '#cdd5dc',
      },

      '&:focus': {
        boxShadow: '0 0 0 2px #fff, 0 0 0 4px #006fbf',
      },
    },
  })
  .attach();

(async function () {
  const s = await store;
  if (!(s.other.EdTechsMode && s.other.BulkUncheckAssignmentDates)) return;

  let container = document.querySelector('.d2l-grid-container');

  let button = document.createElement('button');

  button.innerHTML = `
    <d2l-icon icon="tier1:disable" style="color: inherit;"></d2l-icon>
    &nbsp;
    Uncheck all dates
  `;

  button.className = classes.button;

  button.setAttribute('mm-icon-render', 'icon');
  button.setAttribute('mm-icon-type', 'action-button');
  button.setAttribute('mm-icon-offset', '-5:-5');
  button.style.marginBottom = '20px';

  container.insertBefore(button, container.querySelector('d2l-table-wrapper'));

  button.addEventListener('click', e => {
    document.querySelectorAll('input[type="checkbox"]:checked').forEach(cb => cb.click());
  });
})();
