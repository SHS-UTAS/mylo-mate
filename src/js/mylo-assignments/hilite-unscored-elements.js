(async function () {
  const style = document.createElement('style');
  style.innerHTML = `
	.rubric-box-score.missed-criteria {
    font-size: 0.8em;
    color: black;
    background: rgba(255,0,0,0.3);
    padding: 0.7em;
    margin-top: 10px;
	}
	`;
  document.head.append(style);

  const checkForMissedItems = () =>
    document.querySelectorAll('.rubric-box-score').forEach(s => {
      if (s.innerText.includes('not scored')) {
        s.classList.add('missed-criteria');
      } else {
        s.classList.remove('missed-criteria');
      }
    });
  checkForMissedItems();

  const mo = new MutationObserver(checkForMissedItems);
  mo.observe(document.body.querySelector('.d2l-assignment-mark-user'), { childList: true, subtree: true });
})();
