/**
 * @affects Assignment Folders
 * @problem No way to see an overview of students' assignment progress.
 * @solution Automatically generate an assignment report that details submission completion, grades and any provided feedback.
 * @target Academic staff
 * @impact High
 * @savings 20 minutes per folder
 */

(async function () {
  const DropboxStatus = {
    0: 'Unsubmitted',
    1: 'Submitted',
    2: 'Draft',
    3: 'Published',
  };

  await customElements.whenDefined('mm-dialog');

  const links = document.querySelectorAll('a.d2l-link[href*="folder_submissions_users.d2l" i]');
  links.forEach(async link => {
    const dataset = [];

    const dialog = document.createElement('mm-dialog');
    dialog.innerHTML =
      '<div style="width: 100%; height: 100%; display: flex; justify-content: center; align-items: center;"><mm-loader></mm-loader></div>';

    document.body.append(dialog);

    dialog.addEventListener('open', () => {
      document.body.style.overflow = 'hidden';
    });
    dialog.addEventListener('close', () => {
      document.body.style.overflow = '';
    });

    dialog.preserve = true;
    dialog.hideCancel = true;

    const { db, ou } = new URL(link.href).query;

    const opener = document.createElement('d2l-icon');
    opener.title = `Show assignment report for ${link.innerText}`;
    opener.setAttribute('icon', 'tier1:reporting');
    opener.classList.add('d2l-link');

    const wrapper = document.createElement('span');
    wrapper.setAttribute('mm-icon-type', 'action-button');
    wrapper.setAttribute('mm-icon-offset', '-6:0');
    wrapper.append(opener);
    link.closest('.d2l-foldername').querySelector('d2l-dropdown-context-menu').insertAdjacentElement('afterend', wrapper);

    opener.addEventListener('click', e => {
      dialog.heading = `Assignment Report for ${link.innerText}`;
      dialog.open = true;

      dialog.setAttribute('folder', db);
    });

    const folder = await api(`/d2l/api/le/1.48/${ou}/dropbox/folders/${db}`, { first: true });

    if (folder.TotalUsersWithSubmissions == 0) {
      dialog.innerHTML = 'No submissions';
      wrapper.remove();
    }

    const IsGroupSubmissionFolder = folder.GroupTypeId != null;

    // If this is a group-type submission folder, we're going to need to handle it differently.
    // For now, let's just bail out so we don't get errors.
    // if (IsGroupSubmissionFolder) return;

    const users = await api(`/d2l/api/lp/1.31/enrollments/orgUnits/${ou}/users/`, { fresh: true });
    const submissions = await api(`/d2l/api/le/1.48/${ou}/dropbox/folders/${db}/submissions/`, { fresh: true });

    if (IsGroupSubmissionFolder) {
      // console.log(submissions);
      submissions.forEach(sub => {
        dataset.push({
          flagged: sub.Submissions.filter(s => s.Files.find(f => f.IsFlagged)).length > 0,
          name: sub.Entity.Name,
          id: sub.Entity.EntityId,
          status: { code: sub.Status, text: DropboxStatus[sub.Status] },
          grade: { score: sub.Feedback?.Score, total: folder.Assessment.ScoreDenominator },
          feedback: sub?.Feedback?.Feedback?.Text,
          assessors: Array.from(
            new Set(
              sub.Feedback?.RubricAssessments.map(rubric =>
                users.find(({ User }) => User.Identifier == rubric.AssessorId)
              ) ?? []
            )
          ),
        });
      });
    } else {
      const students = await api(`/d2l/api/le/1.48/${ou}/classlist/`, { fresh: true });
      submissions.forEach(sub => {
        if (!sub.Entity.Active) return;
        try {
          const student = students.find(student => student.Identifier == sub.Entity.EntityId);
          if (!student) {
            console.warn(
              'MyLO MATE could not find a student in the classlist for this submission: %o Classlist: %o',
              sub,
              students
            );
            return;
          }
          dataset.push({
            flagged: sub.Submissions.filter(s => s.Files.find(f => f.IsFlagged)).length > 0,
            name: sub.Entity.DisplayName,
            id: student.OrgDefinedId,
            status: { code: sub.Status, text: DropboxStatus[sub.Status] },
            grade: { score: sub.Feedback ? sub.Feedback.Score || null : null, total: folder.Assessment.ScoreDenominator },
            feedback: sub.Feedback ? sub.Feedback.Feedback && sub.Feedback.Feedback.Text : null,
            assessors: Array.from(
              new Set(
                sub.Feedback?.RubricAssessments.map(rubric =>
                  users.find(({ User }) => User.Identifier == rubric.AssessorId)
                ) ?? []
              )
            ),
          });
        } catch (e) {
          console.error(e);
          /**
           * console.log(
           *   sub,
           *   students,
           *   students.find(student => student.Identifier == sub.Entity.EntityId)
           * );
           */
        }
      });
    }
    const formatter = new Intl.ListFormat('en', { style: 'long', type: 'conjunction' });

    const csv = ['ID,Flagged,Name,Status,Score,Assessors,Feedback'];
    dataset.forEach(({ id, flagged, name, status, grade, assessors, feedback }) => {
      csv.push(
        `"${id}","${flagged ? 'Yes' : 'No'}","${name}","${status.text}",${grade.score || ''},"${formatter.format(
          assessors.filter(Boolean).map(usr => usr.User.DisplayName)
        )}","${feedback?.split('"').join('""') || ''}"`
      );
    });

    dialog.innerHTML = `
        <style>
          table.student-report thead td {
            background-color: #f1f1f1;
            padding: 5px;
            font-weight: bold;
            text-align: center;
            position: sticky;
            top: 40px;
          }
  
          table.student-report tbody td {
            padding: 15px 25px;
          }
          
          table.student-report tbody td:not(.feedback) {
            white-space: nowrap;
            text-align: center;
          }
          
          table.student-report tbody tr:nth-child(even) {
            background-color: #f5f5f5;
          }
  
          .download-csv {
            margin-top: -20px;
            background: white;
            display: block;
            position: sticky;
            top: -20px;
            padding: 20px 0;
          }
  
          table.student-report tbody tr.flagged {
            background: #ffd3db;
          }
        </style>
        <div class="download-csv"><a class="d2l-link" download="${`Assignment Report - ${link.innerText}.csv`}" href="${URL.createObjectURL(
      new Blob([csv.join('\r\n')])
    )}">Save as spreadsheet</a></div>
        <table class="student-report">
          <thead>
            <tr>
              <td>Flagged</td>
              <td>Name</td>
              <td>Status</td>
              <td>Grade</td>
              <td>Last Marked By</td>
              <td>Feedback</td>
            </tr>
          </thead>
          <tbody>
            ${dataset
              .map(
                ({ flagged, name, status, grade, assessors, feedback }) => `
              <tr${flagged ? ' class="flagged"' : ''}>
                <td class="flagged">${flagged ? 'Yes' : 'No'}</td>
                <td class="name">${name}</td>
                <td class="status">${status.text}</td>
                <td class="score">${grade.score || '-'} / ${grade.total || '-'}</td>
                <td class="marked-by">${formatter.format(
                  assessors
                    .filter(Boolean)
                    .map(
                      usr =>
                        `<a href="mailto:${usr.User.EmailAddress}" class="d2l-link" title="Email ${usr.User.DisplayName}">${usr.User.DisplayName}</a>`
                    )
                )}</td>
                <td class="feedback">${feedback || ''}</td>
              </tr>
            `
              )
              .join('\r\n')}
          </tbody>
        </table>
      `;

    dialog.width = 'auto';
  });
})();
