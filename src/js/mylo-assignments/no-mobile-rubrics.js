(async () => {
  const makeNotMobile = () => {
    console.log('resize raf');

    const rubric = window.frames[0].document.querySelector('d2l-rubric');
    if (!rubric) return;

    setTimeout(() => {
      rubric._isMobile = false;
    }, 10);
  };

  window.addEventListener('resize', e => {
    window.requestAnimationFrame(makeNotMobile);
  });

  makeNotMobile();
})();
