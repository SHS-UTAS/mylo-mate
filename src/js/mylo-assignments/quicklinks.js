(async () => {
  const quicklink = (id, title, href, text, addMicon) => {
    const a = document.createElement('a');
    a.setAttribute('data-testid', id);
    a.classList.add('d2l-link');
    a.title = title;
    a.href = href;
    a.innerText = `[${text}]`;

    if (addMicon) {
      a.setAttribute('mm-icon-render', 'icon');
      a.setAttribute('mm-icon-type', 'action-navigation');
      a.setAttribute('mm-icon-offset', '-5:0');
    }

    return a;
  };

  const config = await FetchMMSettings();

  document
    .querySelectorAll(
      [
        '/d2l/lms/dropbox/admin/mark/folder_submissions_users.d2l',
        '/d2l/lms/dropbox/admin/mark/folder_submissions_observed.d2l',
      ]
        .map(v => `[href*="${v}"]:not([mm-generated])`)
        .join(',')
    )
    .forEach(el => {
      if (el.matches('d2l-breadcrumb')) return;

      const wrapper = document.createElement('span');
      wrapper.style.marginLeft = '1em';

      const search = new URL(el.href).search; //window.location.search;
      const title = el.innerText;

      /* const p = quicklink(
				'a-turnitin',
				`Folder Properties for ${title}`,
				`/d2l/lms/dropbox/admin/modify/folder_newedit_properties.d2l${search}`,
				'p',
				true
			);

			const r = quicklink(
				'a-restrictions',
				`Restrictions for ${title}`,
				`/d2l/lms/dropbox/admin/modify/folder_newedit_restrictions.d2l${search}`,
				'r'
			); */

      const e = quicklink(
        'a-edit',
        `Edit ${title}`,
        `/d2l/lms/dropbox/admin/modify/folder_newedit_properties.d2l${search}`,
        'e'
      );
      wrapper.append(e);

      if (config.other.EdTechsMode) {
        const t = quicklink(
          'a-turnitin',
          `Turnitin for ${title}`,
          `/d2l/lms/dropbox/admin/modify/folder_newedit_turnitin.d2l${search}`,
          't'
        );

        wrapper.append(t);
      }

      el.insertAdjacentElement('afterend', wrapper);
    });
})();
