(async function () {
  const orgid = window.location.searchParams.ou;

  const userprogress = new DOMParser().parseFromString(
    await fetch(`/d2l/le/classlist/userprogress/0/${orgid}/settings/Edit`).then(d => d.text()),
    'text/html'
  );

  const RubricBase = window.location.origin.includes('d2ldev2')
    ? '0e78be5b-5af6-4f4b-827b-568374e17a1d'
    : 'c0ef8d21-d21d-4bf1-b68f-054dc1a5e6fc';

  const showInUserProgress = userprogress.querySelector('input[value="Assignments"]').checked;
  const linebreak = '\u000A';

  const token = await GetOAuth2Token();

  const StatusCodes = {
    Visible: 'AlwaysVisible',
    Hidden: 'NeverVisible',
    VisibleAfterFeedback: 'VisibleOnceFeedbackPosted',
  };

  const GetVisibility = (ou, id) =>
    api(`https://${RubricBase}.rubrics.api.brightspace.com/organizations/${ou}/${id}`, {
      token,
      first: true,
      fresh: true,
    }).then(d => d.properties.visibility);

  document.querySelectorAll('.d2l-foldername').forEach(async row => {
    const a = row.querySelector('a[href*="mark/folder_submissions"]');
    if (!a) return;

    const icon = document.createElement('d2l-icon');
    icon.setAttribute('icon', 'tier1:visibility-conditional');
    icon.style.marginLeft = '15px';
    icon.setAttribute('loading', '');
    icon.title = 'Generating visibility report...';
    row.querySelector('.dco_c .clear').insertAdjacentElement('beforebegin', icon);

    const gradeid = new URL(a.href).searchParams.get('db');
    const grade = await api(`/d2l/api/le/1.46/${orgid}/dropbox/folders/${gradeid}`, { first: true, fresh: true });

    let visible = false;

    const rubrics = await Promise.all((grade.Assessment.Rubrics || []).map(rubric => GetVisibility(orgid, rubric.RubricId)));

    const items = ['Visibility report', '', `Assignment item visibility: ${grade.IsHidden ? 'hidden' : 'visible'}`];

    if (!grade.IsHidden) visible = true;

    if (grade.GradeItemId != null) {
      const gradevisibility = await api(`/d2l/api/le/1.46/${orgid}/grades/${grade.GradeItemId}/access/?roleId=103`, {
        fresh: true,
      });
      let total = gradevisibility.length;
      let hidden = gradevisibility.filter(vis => !vis.HasAccess).length;
      let visible = gradevisibility.filter(vis => vis.HasAccess).length;

      if (hidden != total) visible = true;

      items.push(`Grade item visibility: ${hidden == total ? 'hidden' : visible == total ? 'visible' : 'mixed'}`);
    }

    if (rubrics.length > 0) {
      let total = rubrics.length;
      let hidden = rubrics.filter(vis => vis == StatusCodes.Hidden || vis == StatusCodes.VisibleAfterFeedback).length;
      let visible = rubrics.filter(vis => vis == StatusCodes.Visible).length;

      if (hidden != total) visible = true;

      items.push(`Rubrics visibility: ${hidden == total ? 'hidden' : visible == total ? 'visible' : 'mixed'}`);
    }

    items.push(`Assignments visibility in User Progress: ${showInUserProgress ? 'visible' : 'hidden'}`);
    if (showInUserProgress) visible = true;

    icon.classList.toggle('mm-completely-hidden', !visible);
    icon.title = items.join(linebreak);
    icon.removeAttribute('loading');
  });

  const style = document.createElement('style');
  style.innerHTML = `d2l-icon.mm-completely-hidden::before {
    display: block;
    content: '/';
    position: absolute;
    color: red;
    font-weight: 900;
    font-size: 1.5em;
    margin-left: 2.5px;
    margin-top: 5px;
  }

  d2l-icon[loading] {
    animation: pulse 2s infinite;
    animation-timing-function: ease-in-out;
    opacity: 0.6;
  }

  @keyframes pulse {
    from { opacity 0.6; }
    50% { opacity: 0.3; }
  }
`;
  document.head.append(style);
})();
