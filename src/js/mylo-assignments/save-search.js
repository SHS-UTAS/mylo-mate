(async function () {
  const IsDifferentState = (previous, current) => {
    if (previous.pageId != current.pageId) return false;

    const prev = JSON.parse(previous?.state?.State?.search ?? 'null')?.Controls;
    const curr = JSON.parse(current?.state?.State?.search ?? 'null')?.Controls;
    if (!prev || !curr) return false;

    if (prev.length != curr.length) return true;

    for (let i = 0, len = prev.length; i < len; i++) {
      const p = prev[i];
      const c = curr[i];

      if (JSON.stringify(p.State) != JSON.stringify(c.State)) {
        return true;
      }
    }

    return false;
  };

  const { db, ou } = window.location.searchParams;
  const store = await FetchMMSettings();

  if (!store.mylo.SaveLastSearchState) return;

  const GetState = () => {
    const o = JSON.parse(window.localStorage.LastState ?? '{}');
    return o[ou] ?? {};
  };

  const StoreState = state => {
    const o = JSON.parse(window.localStorage.LastState ?? '{}');
    o[ou] = state;
    window.localStorage.LastState = JSON.stringify(o);
  };

  const ApplyState = () => {
    const s = GetState();
    if (s[db] == null) return;

    const { state, pageId } = s[db];
    const pagecount = window.localStorage[`pagecount-${db}`];

    if (pagecount) {
      const grid = JSON.parse(state.State.grid);
      grid.Controls[0].State.PageSize = pagecount;
      state.State.grid = JSON.stringify(grid);
    }

    const n = new D2L.NavInfo();
    n.SetParam('d2l_stateScopes', state.Scopes);
    n.SetParam('d2l_stateGroups', state.Groups);
    n.SetParam('d2l_statePageId', pageId);
    for (const group in state.State) {
      n.SetParam('d2l_state_' + group, state.State[group]);
    }

    n.SetParam('d2l_change', window.location.search.includes('d2l_change=1') ? 0 : 1);
    Nav.GoHelper(n.GetHref(), n.target);
  };

  const CurrentState = () => {
    return { state: UI.GetStateManager().GenerateState(true, true), pageId: UI.pageId };
  };

  const SaveState = () => {
    const state = GetState();
    state[db] = CurrentState();
    StoreState(state);
  };

  const ClearState = () => {
    const state = GetState();
    delete state[db];
    StoreState(state);
  };

  document.querySelector('d2l-input-search').addEventListener('d2l-input-search-searched', SaveState);
  document.querySelector('.d_tabs_tabcontent > div button')?.addEventListener('click', SaveState);

  document.querySelector('[onclick*=".Clear()"]')?.addEventListener('click', ClearState);
  if (IsDifferentState(GetState()[db], CurrentState())) ApplyState();
})();
