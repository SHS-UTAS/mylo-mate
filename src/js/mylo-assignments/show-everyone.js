/**
 * @affects Assignment Submission
 * @problem The Assignment Submissions page, by default, only shows students who have submissions.
 * @solution Add a button that automatically toggles between showing only students with submissions, and showing all students.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  // await until(() => window?.WebComponents?.ready);

  const filter = document.querySelector('select[title="view by" i]')?.selectedOptions[0]?.innerText ?? '';
  const submissions = document.querySelector(`select[name*="restrictUsers"]`);
  if (!submissions) return null;

  const showAll = submissions.value != '1';
  const isGroupList = /^groups$/i.test(filter);
  const groupsString = isGroupList ? 'groups' : 'students';
  const button = document.createElement('button');
  button.classList.add('d2l-button');
  button.innerText = showAll
    ? isGroupList
      ? 'Show all groups'
      : 'Show everyone'
    : `Hide ${groupsString} without submissions`;
  button.title = `${showAll ? 'Show' : 'Hide'} ${groupsString} that haven't submitted anything yet.`;
  button.setAttribute('mm-icon-type', 'action-button');
  button.setAttribute('mm-icon-offset', '-7:-2');

  const td = document.createElement('td');
  td.classList.add('d_tl', 'd_tm', 'd_tn');
  td.append(button);

  document.querySelector('.d_tabs_tabcontent tr').append(td);

  const searcher = document.querySelector('d2l-input-search');
  button.addEventListener('click', e => {
    submissions.value = showAll ? 1 : 0;
    searcher.search();
  });
})();
