(async () => {
  const container = document.querySelector('d2l-consistent-evaluation');
  const dummyPlayer = document.createElement('video');

  const videoFormats = Object.fromEntries(
    Object.entries({
      'video/3gpp': ['3gp'],
      'video/3gpp2': ['3g2'],
      'video/h261': ['h261'],
      'video/h263': ['h263'],
      'video/h264': ['h264'],
      'video/jpeg': ['jpgv'],
      'video/jpm': ['jpm', 'jpgm'],
      'video/mj2': ['mj2', 'mjp2'],
      'video/mp4': ['mp4', 'mp4v', 'mpg4'],
      'video/mpeg': ['mpeg', 'mpg', 'mpe', 'm1v', 'm2v'],
      'video/ogg': ['ogv'],
      'video/quicktime': ['qt', 'mov'],
      'video/vnd.dece.hd': ['uvh', 'uvvh'],
      'video/vnd.dece.mobile': ['uvm', 'uvvm'],
      'video/vnd.dece.pd': ['uvp', 'uvvp'],
      'video/vnd.dece.sd': ['uvs', 'uvvs'],
      'video/vnd.dece.video': ['uvv', 'uvvv'],
      'video/vnd.dvb.file': ['dvb'],
      'video/vnd.fvt': ['fvt'],
      'video/vnd.mpegurl': ['mxu', 'm4u'],
      'video/vnd.ms-playready.media.pyv': ['pyv'],
      'video/vnd.uvvu.mp4': ['uvu', 'uvvu'],
      'video/vnd.vivo': ['viv'],
      'video/webm': ['webm'],
      'video/x-f4v': ['f4v'],
      'video/x-fli': ['fli'],
      'video/x-flv': ['flv'],
      'video/x-m4v': ['m4v'],
      'video/x-matroska': ['mkv', 'mk3d', 'mks'],
      'video/x-mng': ['mng'],
      'video/x-ms-asf': ['asf', 'asx'],
      'video/x-ms-vob': ['vob'],
      'video/x-ms-wm': ['wm'],
      'video/x-ms-wmv': ['wmv'],
      'video/x-ms-wmx': ['wmx'],
      'video/x-ms-wvx': ['wvx'],
      'video/x-msvideo': ['avi'],
      'video/x-sgi-movie': ['movie'],
      'video/x-smv': ['smv'],
    }).filter(([k, v]) => dummyPlayer.canPlayType(k) != '') // Make sure this format is at least /maybe/ playable.
  );

  const extensions = Object.values(videoFormats).flat();

  const players = {};
  const toggleVideo = (metadata, parent) => {
    /**
     *     if (players[metadata.fileId]) {
     *       players[metadata.fileId].remove();
     *       delete players[metadata.fileId];
     *     } else {
     *       const video = document.createElement('video');
     *       video.controls = true;
     *       video.autoplay = true;
     *       video.src = `/d2l/api/le/1.60/${ou}/dropbox/folders/${db}/submissions/${metadata.submissionId}/files/${metadata.fileId}`;
     *       video.style.display = 'block';
     *
     *       parent.append(video);
     *       players[metadata.fileId] = video;
     *     }
     */
  };

  const makeVideoPlayer = (href, icon = 'tier1:video-assignment') => {
    const btn = document.createElement('d2l-icon');
    btn.setAttribute('icon', icon);
    btn.title = 'Toggle video player';
    btn.style.cursor = 'pointer';
    btn.style.marginRight = '0.5em';

    const wrapper = document.createElement('span');
    wrapper.setAttribute('mm-icon-type', 'action-button');
    wrapper.setAttribute('mm-icon-offset', '-6:-2');
    wrapper.append(btn);

    const video = document.createElement('video');
    video.src = href;
    video.controls = true;
    video.preload = 'none';
    video.style.maxHeight = '20em';
    video.style.maxWidth = '30em';

    return { btn: wrapper, video };
  };

  if (window.location.pathname.includes('d2l/le/activities/iterator')) {
    const submission = await until(() => sdqs.querySelectorDeep('d2l-consistent-evaluation-evidence-assignment'));
    await until(() => sdqs.querySelectorDeep('d2l-consistent-evaluation-assignments-submissions-page', submission));

    const subs = await until(() => {
      const items = sdqs.querySelectorAllDeep('d2l-consistent-evaluation-assignments-submission-item');
      return items.length > 0 && items;
    });

    for (const sub of subs) {
      for (const attachment of sub.__attachments) {
        const props = attachment.properties;

        if (!props) continue;
        if (!extensions.includes(props.extension)) continue;

        const listitem = await until(() => sub.shadowRoot.querySelector(`d2l-list > d2l-list-item [file-id="${props.id}"]`));

        const icon = 'tier1:video-assignment';

        const { btn, video } = makeVideoPlayer(props.href, icon);
        btn.onclick = () => {
          if (newItem.isConnected) newItem.remove();
          else {
            listitem.closest('d2l-list').append(newItem);
            video.play();
          }
        };

        const block = listitem.closest('.d2l-submission-attachment-list-item-flexbox');
        block.style.flexDirection = 'row';
        block.style.gap = '1em';
        block.style.alignItems = 'center';
        block.append(btn);

        const newItem = document.createElement('d2l-list-item');
        newItem.setAttribute('role', 'listitem');

        newItem.append(video);
      }
    }
  } else {
    const videos = [];
    const { db, ou } = window.location.searchParams;
    const submissions = await api(`/d2l/api/le/1.60/${ou}/dropbox/folders/${db}/submissions/`, { fresh: true });

    submissions.forEach(sub => {
      sub.Submissions?.forEach(s => {
        s.Files?.forEach(file => {
          if (extensions.some(ext => file.FileName.endsWith(ext.toLowerCase()))) {
            videos.push({
              submissionId: s.Id,
              fileId: file.FileId,
            });
          }
        });
      });
    });

    document.querySelectorAll('[onclick*="SetReturnPointAndEvaluateOrDownload"]').forEach(link => {
      // Parse the parameters passed to the function in the onclick handler.
      const [, fileId] = new Function(`return [${link.getAttribute('onclick').match(/.*?\((.*?)\)/)[1]}]`)();

      // If we don't have a video for this submission, move on.
      const metadata = videos.find(video => video.fileId == fileId);
      if (!metadata) return;

      const videoHost = link.closest('td');
      const buttonHost = link.closest('span');

      const href = `/d2l/api/le/1.60/${ou}/dropbox/folders/${db}/submissions/${metadata.submissionId}/files/${metadata.fileId}`;
      const { btn, video } = makeVideoPlayer(href);

      video.style.display = 'block';

      btn.style.marginLeft = '1em';
      buttonHost.append(btn);

      btn.onclick = () => {
        if (video.isConnected) {
          video.currentTime = 0; // Set the player back to the beginning.
          video.remove();
        } else {
          videoHost.append(video);
          video.play();
        }
      };
    });
  }
})();
