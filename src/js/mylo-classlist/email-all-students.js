(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  let container = document.querySelector('d2l-overflow-group');

  let btn = document.createElement('d2l-button-subtle');
  btn.setAttribute('data-testid', 'email-students');
  btn.setAttribute('text', 'Email all students');
  btn.setAttribute('icon', 'd2l-tier1:email');
  btn.setAttribute('title', 'Email all students');
  btn.setAttribute('mm-icon-render', 'icon');
  btn.setAttribute('mm-icon-type', 'action-navigation');
  btn.setAttribute('mm-icon-offset', '-5:0');

  container.insertBefore(btn, container.querySelector('.d2l-grid-action-wrapper:nth-child(2)'));

  btn.addEventListener('click', async e => {
    btn.setAttribute('disabled', true);

    let users = await api(`/d2l/api/lp/1.21/enrollments/orgUnits/${window.location.searchParams.ou}/users/`);
    let students = users.filter(({ Role }) => Role.Code == 'Student');

    let d = new D2L.Popup.Email();
    d.AddUsers(
      students.map(u => u.User.Identifier),
      D2L.Popup.Email.Location.Bcc
    );
    d.Open();

    btn.removeAttribute('disabled');
  });
})();
