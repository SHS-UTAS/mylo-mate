import jss from '../../lib/jss.bund.js';
import preset from '../../lib/jss-preset-default.bund.js';
jss.setup(preset());

const pw = email => {
  const hash = btoa(email).replace(/=/g, '').slice(0, 20);
  if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/i.test(hash)) return hash;
  else return hash.slice(0, 17) + 'aA1';
};

const { classes } = jss
  .createStyleSheet({
    buttonContainer: {
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'center',
      marginTop: '-0.8em',
      marginBottom: '0.5em',
    },
    collapse: {
      '&[collapsed]': {
        '& $collapseTitle:before': { transform: 'rotate(-90deg)' },
        '& $collapseBody': {
          transform: 'scaleY(0)',
          margin: 0,
          padding: 0,
        },
      },
    },
    collapseTitle: {
      'cursor': 'pointer',
      'fontWeight': 'bold',
      '&:before': {
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        content: '"\\25be"',
        transformOrigin: 'center',
        transform: 'rotate(0deg)',
        transition: 'transform 200ms ease-in-out',
        margin: '0 0.5em',
      },
    },
    collapseBody: {
      overflow: 'hidden',
      transformOrigin: 'top left',
      transitionProperty: 'height, transform, margin, padding',
      transitionDuration: '250ms',
      transitionTimingFunction: 'ease-in-out',
      border: 'thin solid #ccc',
      boxSizing: 'content-box',
      padding: '0.5em',
      marginTop: '0.5em',
      borderRadius: '5px',
      transform: 'scaleY(1)',
    },
    notice: {
      width: '90%',
      margin: '0 auto 1rem !important',
      fontSize: '0.9em',
    },
    sticky: { position: 'sticky' },
    pinBottom: { bottom: '-1px', paddingTop: '1em', paddingBottom: '12px' },
    whiteBackground: { background: 'white' },
    userIcon: { textAlign: 'center' },
    userExists: { color: 'green' },
    userNotExists: { color: 'grey' },
  })
  .attach();

const ou = window.location.searchParams.ou;
const classlist = api(`/d2l/api/le/1.48/${ou}/classlist/`, { fresh: true }).then(data =>
  data.filter(usr => usr.RoleId == 103)
);
const users = api(`/d2l/api/lp/1.29/enrollments/orgUnits/${ou}/users/`, { fresh: true });
const info = api(`/d2l/api/lp/1.21/enrollments/myenrollments/${ou}`, { first: true });

const me = api(`/d2l/api/lp/1.29/users/whoami`, { first: true }).then(async usr => {
  return users.then(d => d.find(u => u.User.Identifier == usr.Identifier));
});

let container = document.querySelector('d2l-overflow-group');

const meta = document.head.querySelector('meta[name="mylo-mate"]').content;
const BNGroup = 419446;

//const ext = src => `chrome-extension://${meta}/${src}`;
const post = data =>
  new Promise(res => {
    chrome.runtime.sendMessage(meta, data, res);
  });

let btn = document.createElement('d2l-button-subtle');
btn.setAttribute('text', 'Send to IntelliLearn');
btn.setAttribute('icon', 'tier1:classlist');
btn.setAttribute('title', 'Exports all students in the classlist into an IntelliLearn group');
btn.setAttribute('mm-icon-render', 'icon');
btn.setAttribute('mm-icon-type', 'action-navigation');
btn.setAttribute('mm-icon-offset', '-5:0');

class IntelliLearn {
  constructor() {
    this.remote = {
      async fetch(url, format = 'json') {
        const root = `https://utas.kineoportal.com.au/rest/${format}/`;
        const res = await post({
          request: 'cors-api',
          url: root + url,
          body: {
            method: 'get',
            headers: { Authorization: `Basic ${await this.token()}` },
          },
        });

        if (Number.isInteger(res)) throw res;
        return format == 'json' ? JSON.parse(res) : res;
      },
      async send(url, data, verb = 'post', type = 'application/json') {
        const root = `https://utas.kineoportal.com.au/rest/json/`;
        const isJSON = type == 'application/json';
        const res = await post({
          request: 'cors-api',
          url: root + url,
          body: {
            method: verb,
            headers: { 'Content-Type': type, 'Authorization': `Basic ${await this.token()}` },
            body: isJSON ? JSON.stringify(data) : data,
          },
        });
        if (Number.isInteger(res)) throw res;
        return isJSON ? JSON.parse(res) : res;
      },

      _token: '',
      async token() {
        if (this._token.length > 0) return this._token;

        const un = (await me).User.EmailAddress;
        const pass = pw((await me).User.EmailAddress);

        this._token = btoa(`${un}:${pass}`);
        return this._token;
      },
    };
  }

  async CanAccess() {
    const usr = await this.remote.fetch(`account/${(await me).User.EmailAddress.toLowerCase()}/get/?includeDisabled=true`);

    const pass = pw((await me).User.EmailAddress);

    const canAccess = usr.message != 'UNABLE_TO_AUTHENTICATE_REQUEST';

    if (!canAccess) {
      console.log(`Attn ${(await me).User.DisplayName}:
If you are positive that you have the correct account required to manage IntelliLearn, but the tool is informing otherwise, you may need to change your password.

Please change your password to the following:
      ${pass}`);
    }

    return canAccess;
  }

  /**
   * @param {string} name The name of the group
   * @param {string} description The descrption of the unit. Should be the Org ID
   */
  CreateGroup(name, description) {
    return this.remote.send(`group/-/newsubgroup/${name}/create?parent_group_id=${BNGroup}&description=${description}`);
  }

  /**
   * Create a group if it doesn't exist already.
   * Otherwise, just return the current group.
   */
  async UpsertGroup(name, ou) {
    const existing = await this.GroupByOrgId(ou);
    if (existing != null) return existing;

    // If we're here, there's no group yet.
    // Create it and return.

    const res = await this.CreateGroup(name, ou);

    if (res.data && res.message == 'OK') {
      return this.GroupByOrgId(ou);
    } else {
      return null;
    }
  }

  /**
   * Fetch the groups in the Bachelor of Nursing IntelliLearn Group
   */
  Groups() {
    return this.remote.fetch(`group/-/getchildgroups?group_id=${BNGroup}`);
  }

  /**
   * Delete a group in the Bachelor of Nursing IntelliLearn Group
   */
  async DeleteGroup(group) {
    const users = group.Enrollments;
    if (users.length > 0) {
      await Promise.all(
        users.map(usr => this.remote.send(`group/-/account/${usr.username}/remove?group_id=${group.groupId}`))
      );
    }

    return this.remote.send(`group/-/delete?group_id=${group.groupId}`);
  }

  /**
   * Get the immediate children of a particular group.
   */
  async GetImmediateChildGroups(parent) {
    const groups = await this.remote.fetch(`group/-/getchildgroups?group_id=${parent}`);
    return groups.group.filter(group => group.parent == parent);
  }

  /**
   * Fetch a group, identified with a specific Org ID
   */
  async GroupByOrgId(orgid) {
    const data = await this.Groups();
    return data.group.find(grp => grp.description?.includes(orgid.toString()));
  }

  /**
   * Fetch the users in a particular group
   * @param {string} group The group ID to fetch enrollments from
   */
  EnrolledUsers(group) {
    return this.remote.fetch(`group/-/getaccounts?group_id=${group.groupId}&includeDisabled=true`);
  }

  /**
   * Fetch specific user
   */
  User(email) {
    return this.remote.fetch(`account/${email.toLowerCase()}/get/?includeDisabled=true`);
  }

  async CreateStudent(user, group) {
    const SafeName = name => name.replace(/[^a-zA-Z-'.]/g, '.');
    const groups = [BNGroup, group.groupId];

    const dataset = {
      upsert: true,
      username: user.Email,
      password: pw(user.Email),
      email: user.Email,
      name: SafeName(user.FirstName),
      surname: SafeName(user.LastName),
      disabled: user.AccountDisabled ?? false,
    };

    const form = new URLSearchParams();
    for (const [key, data] of Object.entries(dataset)) {
      form.append(key, data);
    }

    // We can't send groups as comma-seperated values, so they have to be appended duplicate keys (form data is weird)
    for (const g of groups) {
      form.append('group.id', g);
      form.append('group.role', 'user');
    }

    return this.remote.send(`account/create`, form.toString(), 'post', 'application/x-www-form-urlencoded');
  }

  async RevokeStudentAccess(user, group) {
    return this.remote.send(`group/-/account/${user.Email}/remove?group_id=${group.groupId}`);
  }

  async SetStudentActiveStatus(user, active) {
    return this.CreateStudent({ ...user, AccountDisabled: !active });
  }
}

/**
 * Start creating a modal dialog to show things in.
 */
const modal = document.createElement('mm-dialog');
modal.setAttribute('heading', 'IntelliLearn');
modal.setAttribute('preserve', true);
document.body.append(modal);

const spinner = document.createElement('mm-loader');
spinner.toggleAttribute('indeterminate', true);
spinner.toggleAttribute('center', true);
spinner.style.display = 'flex';
spinner.style.justifyContent = 'center';
spinner.style.alignItems = 'center';
modal.append(spinner);

const il = new IntelliLearn();
(async () => {
  try {
    const priviledged = await il.CanAccess();
    if (!priviledged) throw null;
  } catch (e) {
    btn.disabled = true;
    btn.title = 'You do not have permission to modify IntelliLearn groups or users';
  } finally {
    container.append(btn);
  }
})();

const syncBtn = document.createElement('d2l-button-subtle');
const removeBtn = document.createElement('d2l-button-subtle');

syncBtn.setAttribute('text', 'Sync Student Access');
syncBtn.setAttribute('icon', 'tier1:download');

removeBtn.setAttribute('text', 'Revoke All Students');
removeBtn.setAttribute('icon', 'tier1:remove-user');

const collapse = document.createElement('div');
collapse.className = classes.collapse;

const collapseTitle = document.createElement('h3');
const collapseBody = document.createElement('div');

const handleBodyCollapse = isCollapsed => {
  collapseBody.style.height = `${!isCollapsed ? collapseBody.scrollHeight : 0}px`;
};

collapseTitle.innerText = 'Student Access';
collapseTitle.className = classes.collapseTitle;
collapseTitle.addEventListener('click', () => handleBodyCollapse(collapse.toggleAttribute('collapsed')));

collapse.append(collapseTitle, collapseBody);

collapseBody.className = classes.collapseBody;

const notice = document.createElement('p');
notice.className = classes.notice;

const loader = document.createElement('mm-loader');
loader.toggleAttribute('center', true);
loader.setAttribute('width', 20);

const btnContainer = document.createElement('div');
btnContainer.className = classes.buttonContainer;
btnContainer.append(syncBtn, removeBtn);

const GetUserCollection = async group => {
  // A collection of users with merged datasets.
  const UserList = new Map();
  const usermap = new Map();
  const intellimap = new Set();
  const { member: members = [] } = await il.EnrolledUsers(group);

  for (const { account } of members) {
    intellimap.add(account.email.toLowerCase());
  }

  for (const { Role, User } of await users) {
    if ([103, 120].includes(Role.Id)) {
      usermap.set(User.Identifier, User);
    }
  }

  for (const user of await classlist) {
    const existing = usermap.get(user.Identifier);
    usermap.set(user.Identifier, { ...existing, ...user });
  }

  for (const [_, user] of usermap) {
    if (!user.EmailAddress) continue;
    UserList.set(user.EmailAddress.toLowerCase(), {
      FirstName: user.FirstName,
      LastName: user.LastName,
      Email: user.EmailAddress,
      IsEnrolledInMyLO: true,
      IsEnrolledInIntellilearn: intellimap.has(user.EmailAddress.toLowerCase()),
      AccountDisabled: false,
    });
  }

  for (const { account } of members) {
    const email = account.email.toLowerCase();
    /** If the email already exists in the UserList,
     *    then we've parsed them as a MyLO student (and they've already confirmed if they exist in the IntelliLearn system)
     *  As such, we can safely assume that anything after this continue statement exists in intellilearn, but not in MyLO.
     */
    if (UserList.has(email)) continue;
    UserList.set(email, {
      FirstName: account.name,
      LastName: account.surname,
      Email: account.email,
      IsEnrolledInMyLO: false,
      IsEnrolledInIntellilearn: true,
    });
  }

  return UserList;
};

const checkDeliveriesMsg = document.createElement('p');
checkDeliveriesMsg.innerHTML = '<mm-loader width="30"></mm-loader> Checking for other delivieries...';
checkDeliveriesMsg.style.fontSize = 'smaller';
checkDeliveriesMsg.style.alignSelf = 'center';
checkDeliveriesMsg.style.display = 'inline-flex';
checkDeliveriesMsg.style.alignItems = 'center';
checkDeliveriesMsg.style.gap = '1em';

btn.addEventListener('click', async () => {
  btn.setAttribute('disabled', true);
  try {
    // Make the modal feel more responsive by showing it while things are loading
    modal.setAttribute('heading', `Loading...`);
    modal.toggleAttribute('open', true);

    const {
      OrgUnit: { Name },
    } = await info;

    /** An IntelliLearn group that represents this unit. */
    const group = await il.UpsertGroup(Name, ou);

    // Now that we know the name of the group, let's update the title
    modal.setAttribute('heading', `${group.name} - IntelliLearn`);

    // Get the students enrolled in our group.
    let UserList = await GetUserCollection(group);

    // Start building the table to show the students
    const table = document.createElement('table');
    table.classList.add('d2l-table');
    const headers = [/*'Student ID', */ 'Student Name', 'MyLO', 'IntelliLearn'];

    const hdr = document.createElement('tr');
    hdr.append(
      ...headers.map(hdr => {
        const cell = document.createElement('th');
        cell.innerText = hdr;
        cell.style.textAlign = 'center';
        return cell;
      })
    );

    // Just some semantic improvements
    const h = document.createElement('thead');
    h.append(hdr);

    const tbody = document.createElement('tbody');
    tbody.style.textAlign = 'center';
    table.append(h, tbody);

    const BoolToText = bool => (bool ? 'Y' : 'N');
    const BoolToIcon = bool => {
      const icon = document.createElement('d2l-icon');
      icon.setAttribute('icon', `tier1:${bool ? 'check' : 'close-default'}`);
      icon.classList.add(bool ? classes.userExists : classes.userNotExists, classes.userIcon);
      return icon;
    };

    const render = async list => {
      tbody.innerHTML = '';
      const boolTest = (key, a, b) => (a[key] === b[key] ? 0 : a[key] ? 1 : -1);
      const items = Array.from(list.values());

      items
        .sort((a, b) => {
          let mylo = boolTest('IsEnrolledInMyLO', a, b);
          if (mylo !== 0) return mylo;

          let intelli = boolTest('IsEnrolledInIntellilearn', a, b);
          if (intelli !== 0) return intelli;

          const asName = usr => `${usr.LastName} ${usr.FirstName}`;
          return asName(a).localeCompare(asName(b));
        })
        .forEach(student => {
          const row = document.createElement('tr');
          const cells = [
            //student.OrgDefinedId,
            //student.Email,
            document.createTextNode(`${student.FirstName} ${student.LastName}`),
            BoolToIcon(student.IsEnrolledInMyLO),
            BoolToIcon(student.IsEnrolledInIntellilearn),
          ].map(v => {
            const td = document.createElement('td');
            if (v.matches?.('d2l-icon')) td.style.textAlign = 'center';
            td.append(v);
            return td;
          });
          row.append(...cells);

          tbody.append(row);
        });

      const toAdd = items.filter(v => !v.IsEnrolledInIntellilearn && v.IsEnrolledInMyLO).length;
      const toRemove = items.filter(v => v.IsEnrolledInIntellilearn && !v.IsEnrolledInMyLO).length;
      const canRevoke = items.filter(v => v.IsEnrolledInIntellilearn).length;

      const classlist = await users.then(d => d.filter(u => [103, 120].includes(u.Role.Id)));
      const classlistSize = classlist.length;

      let flag = '';
      let shouldBeCollapsed = true;

      // If there's no students that need to be added or removed to bring in line with the MyLO classlist
      if (toAdd + toRemove == 0) {
        flag = ' matches';
        syncBtn.disabled = true;
      }

      // If there are some students that need either adding or removal to sync with the classlist
      if (toAdd + toRemove > 0) {
        flag = ' has differences';
        shouldBeCollapsed = false;
      }

      // If there aren't any students enrolled in IntelliLearn at all
      if (canRevoke == 0) {
        flag = ' has no students added';

        // There are no accounts that could be revoked
        removeBtn.disabled = true;
      }

      notice.innerText = `MyLO shows ${classlistSize} ${plural(classlistSize, 'student')}, IntelliLearn ${flag}. `;
      /** notice.append(checkDeliveriesMsg); */
      modal.shadowRoot.querySelector('.buttons').append(checkDeliveriesMsg);
      collapse.toggleAttribute('collapsed', shouldBeCollapsed);
      handleBodyCollapse(shouldBeCollapsed);
    };

    render(UserList);

    collapseBody.innerHTML = '';
    collapseBody.append(table);

    modal.innerHTML = '';

    loader.style.display = 'none';
    modal.append(notice, btnContainer, loader, collapse);

    const rerender = async () => {
      const users = await GetUserCollection(group);
      render(users);
      UserList = users;
    };

    const isLoading = loading => {
      [syncBtn, removeBtn].forEach(b => (b.disabled = loading));
      loader.style.display = loading ? '' : 'none';
    };

    // Do something when we click the Sync button
    syncBtn.onclick = async () => {
      const toAdd = Array.from(UserList.values()).filter(v => !v.IsEnrolledInIntellilearn && v.IsEnrolledInMyLO);
      const toRemove = Array.from(UserList.values()).filter(v => v.IsEnrolledInIntellilearn && !v.IsEnrolledInMyLO);

      if (toAdd.length + toRemove.length == 0) {
        return alert('Unit is in sync with IntelliLearn.');
      }

      isLoading(true);
      if (
        confirm(
          `This will ${toAdd.length == 0 ? '' : `add ${toAdd.length} ${plural(toAdd.length, 'student')}`}${
            toAdd.length > 0 && toRemove.length > 0 ? ' and ' : ''
          }${
            toRemove.length == 0 ? '' : `remove ${toRemove.length} ${plural(toRemove.length, 'student')}`
          }. Do you wish to continue?`
        )
      ) {
        await Promise.all([
          ...toAdd.map(usr => il.CreateStudent(usr, group)),
          ...toRemove.map(usr => il.RevokeStudentAccess(usr, group)),
        ]);
        rerender();
      }
      isLoading(false);
    };

    // Do something when we click the Revoke button
    removeBtn.onclick = async () => {
      const toRemove = Array.from(UserList.values()).filter(v => v.IsEnrolledInIntellilearn);
      isLoading(true);
      if (
        confirm(
          `This will revoke the access for ${toRemove.length} ${plural(
            toRemove.length,
            'student'
          )}. Do you wish to continue?`
        )
      ) {
        await Promise.all(toRemove.map(usr => il.RevokeStudentAccess(usr, group)));
        rerender();
      }

      isLoading(false);
    };

    // Handle the list of IntelliLearn groups that match this unit from past deliveries
    // Encapulsating in curly braces to avoid collisions of variable names
    {
      const period = (await info).OrgUnit.Code.split('_')[2];
      const isFullYear = period.endsWith('YR');
      const currentPeriod = period;
      let delivery = '';
      if (!/[0-9]{2}[a-z0-9]{2}/i.test(period)) delivery = 'a sandpit';
      else {
        const yr = `20${period.substr(0, 2)}`;
        delivery = 'the ' + (isFullYear ? `${yr} Full Year` : `NSP ${period.substr(3)}, ${yr}`);
        /** delivery = `the NSP ${period.substr(3)}, ${period.endsWith('YR') ? 'Full Year' : `20${period.substr(0, 2)}`}`; */
      }

      const container = document.createElement('div');
      container.classList.add(classes.collapse, classes.sticky, classes.pinBottom, classes.whiteBackground);

      const collapseTitle = document.createElement('h3');
      const collapseBody = document.createElement('div');
      collapseBody.className = classes.collapseBody;

      const handleBodyCollapse = isCollapsed => {
        collapseBody.style.height = `${!isCollapsed ? collapseBody.scrollHeight : 0}px`;
      };

      collapseTitle.innerText = 'Other deliveries';
      collapseTitle.className = classes.collapseTitle;
      collapseTitle.addEventListener('click', () => handleBodyCollapse(container.toggleAttribute('collapsed')));

      container.toggleAttribute('collapsed', true);
      handleBodyCollapse(true);

      const loadingMessage = html`<p>Loading...</p>`;

      // Start showing data
      collapseBody.append(loadingMessage);
      container.append(collapseTitle, collapseBody);

      // Get the unit code from the unit title; if there is no unit code, just use the whole name.
      const unitmatch = Name.match(/^([a-z]{3}[0-9]{3})/i)?.[0] ?? Name;

      let previous_deliveries = [];
      let prev_orgs = [];

      const UpdateList = async () => {
        // Get all the groups from the IntelliLearn API
        const all_groups = await il.Groups();

        // Using the matched unit code/name above, find all the groups from intellilearn that match.
        previous_deliveries = await Promise.all(
          all_groups.group
            .filter(group => group.name.toLowerCase().includes(unitmatch.toLowerCase()) && parseInt(group.description) != ou)
            .map(async group => {
              group.Enrollments = await il.EnrolledUsers(group).then(d => d.member ?? []);
              group.EnrolledUsers = group.Enrollments.length;
              return group;
            })
        );

        // Get the mylo data for the identified groups above
        prev_orgs = await Promise.all(
          previous_deliveries.map(prev =>
            api(`/d2l/api/lp/1.35/enrollments/myenrollments/${prev.description}`, { first: true })
          )
        );

        if (prev_orgs.length == 0) {
          container.remove();
          checkDeliveriesMsg.innerText = 'No other deliveries found.';
          return;
        } else {
          /** checkDeliveriesMsg.innerText = `${prev_orgs.length} other deliveries found.`; */
          checkDeliveriesMsg.remove();
          modal.append(container);
        }

        const table = html`
          <table>
            <tbody>
              <tr>
                <td colspan="2">You are currently syncing ${delivery} delivery.</td>
                <td></td>
              </tr>
            </tbody>
            ${prev_orgs.map(org => {
              const period = org.OrgUnit.Code.split('_')[2];
              const isFullYear = period.endsWith('YR');
              const yr = `20${period.substr(0, 2)}`;
              const delivery = isFullYear ? `${yr} Full Year` : `NSP ${period.substr(3)}, ${yr}`;
              const prev = previous_deliveries.find(group => group.description.includes(org.OrgUnit.Id));
              const isPastDelivery = parseInt(period) < parseInt(currentPeriod);

              return `<tr>
                <td>${delivery}</td>
                <td>${
                  isPastDelivery
                    ? `<a class="d2l-link" data-group-id="${prev.groupId}">Revoke access for ${prev.EnrolledUsers} students</a>`
                    : 'Cannot revoke access for future deliveries'
                }</td>
              </tr>`;
            })}
          </table>
        `;

        collapseBody.innerHTML = '';
        collapseBody.append(table);
      };

      collapseBody.addEventListener('click', async e => {
        if (e.target.matches('[data-group-id]')) {
          const group = previous_deliveries.find(g => g.groupId == e.target.dataset.groupId);
          const org = prev_orgs.find(org => org.OrgUnit.Id == group.description);

          const period = org.OrgUnit.Code.split('_')[2];
          const isFullYear = period.endsWith('YR');
          const yr = `20${period.substr(0, 2)}`;
          const delivery = isFullYear ? `${yr} Full Year` : `NSP ${period.substr(3)}, ${yr}`;

          const msg = `Select OK to revoke access for ${group.EnrolledUsers} students who accessed the ${delivery} delivery.`;

          if (!confirm(msg)) return;

          await il.DeleteGroup(group);
          collapseBody.innerHTML = '';
          collapseBody.append(loadingMessage);
          await UpdateList();
        }
      });

      await UpdateList();
    }
  } catch (e) {
    console.error(e);
  } finally {
    btn.removeAttribute('disabled');
  }
});
