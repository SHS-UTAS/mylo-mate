(function () {
  if (window.ClassListPaginationFix) return;
  window.ClassListPaginationFix = true;

  if (document.querySelector('[name="gridUsers_sl_pgS2"]')) {
    let old_pagination = document.querySelector('[name="gridUsers_sl_pgS2"]').parent('table');
    let new_pagination = old_pagination.cloneNode(true);
    let container = document.querySelector('.d2l-grid-container');

    old_pagination.parent('td.d_gact').style.paddingRight = 0;

    old_pagination.style.width = 'unset';
    old_pagination.style.float = 'right';

    new_pagination.style.width = 'unset';
    new_pagination.style.float = 'right';
    new_pagination.setAttribute('data-testid', 'cloned-pagination')

    new_pagination.querySelectorAll('select').forEach(sel => {
      let wrapper = document.createElement('div');
      sel.parentNode.insertBefore(wrapper, sel);
      wrapper.appendChild(sel);
      wrapper.setAttribute('mm-icon-type', 'action-select');
      wrapper.setAttribute('mm-icon-offset', '-6:0');
    });

    container.insertBefore(new_pagination, container.querySelector('.d_gact.d_gl'));

    new_pagination.addEventListener('change', function (e) {
      let r = old_pagination.querySelector('select[name="' + e.target.name + '"]');
      r.value = e.target.value;
      r.dispatchEvent(new Event('change'));
    });

  }
})();