/**
 * @affects Unit Content
 * @problem The `Edit Web Page` button is at the bottom of the page, requiring lots of scrolling.
 * @solution Add an `Edit Web Page` icon in the top right of each web page.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  /**
   *   const [, OrgID, , TopicID] = window.location.pathname.match(/content\/([0-9]+)\/(viewContent|contentfile)\/([0-9]+)\//i);
   *   const unit = await GetUnitModule({ OrgID, TopicID, Cached: false });
   *   if (!unit || unit.IsBroken) return;
   *
   *   // If we aren't an HTML file, bail.
   *   if (!(unit.TypeIdentifier == 'File' && unit.Url && (unit.Url.endsWith('.html') || unit.Url.endsWith('.htm')))) return;
   */

  const hasEditButton =
    Array.from(document.querySelectorAll('button.d2l-button')).find(btn => /edit web page/i.test(btn.innerText)) != null;

  if (!hasEditButton) return;

  const headerRow = document.querySelector('.d2l-page-header-side');

  let bar;
  if (!document.querySelector('div.mm-button-bar')) {
    bar = document.createElement('div');
    bar.classList.add('mm-button-bar');
    bar.style.display = 'inline-block';
    headerRow.insertBefore(bar, headerRow.firstChild);
  } else bar = document.querySelector('div.mm-button-bar');

  // The row to put the icon in
  const ids = /d2l\/le\/content\/([0-9]+?)\/viewContent\/([0-9]+?)\/View/gi.exec(window.location.href);

  const wrap = el => {
    const wrapper = document.createElement('span');
    wrapper.setAttribute('mm-icon-type', 'action-select');
    wrapper.setAttribute('mm-icon-offset', '10:0');
    wrapper.append(el);
    return wrapper;
  };

  const editIcon = document.createElement('d2l-button-icon');
  editIcon.setAttribute('data-testid', 'quick-edit-link');
  editIcon.setAttribute('icon', 'tier1:edit');
  editIcon.title = 'Edit Web Page';

  const linker = document.createElement('a');
  linker.href = `/d2l/le/content/${ids[1]}/contentfile/${ids[2]}/EditFile?fm=0`;
  linker.classList.add('d2l-inline');
  linker.style.margin = '0 10px';
  linker.append(editIcon);

  bar.insertAdjacentElement('afterbegin', wrap(linker));
})();
