(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const [, orgid, contentid] = window.location.pathname.match(/content\/([0-9]+)\/viewContent\/([0-9]+)/i);

  const wrap = el => {
    const wrapper = document.createElement('span');
    wrapper.setAttribute('mm-icon-type', 'action-select');
    wrapper.setAttribute('mm-icon-offset', '10:0');
    wrapper.append(el);
    return wrapper;
  };

  const ExportIcon = 'tier1:export';
  const PageName = document.querySelector('[title="Enter a Title" i]').innerText;
  const ToolTip = `Export User Progress information for "${PageName}".`;

  const button = document.createElement('button');
  button.type = 'button';
  button.classList.add('d2l-button');
  button.innerText = 'Export User Progress';
  button.title = ToolTip;

  const icon = document.createElement('d2l-icon');
  icon.setAttribute('icon', ExportIcon);
  icon.style.marginRight = '10px';
  button.prepend(icon);

  const headerRow = document.querySelector('.d2l-page-header-side');

  let bar;
  if (!document.querySelector('div.mm-button-bar')) {
    bar = document.createElement('div');
    bar.classList.add('mm-button-bar');
    bar.style.display = 'inline-block';
    headerRow.insertBefore(bar, headerRow.firstChild);
  } else bar = document.querySelector('div.mm-button-bar');

  const iconButton = document.createElement('d2l-button-icon');
  iconButton.title = ToolTip;
  iconButton.setAttribute('icon', ExportIcon);
  iconButton.style.margin = '0 10px';
  bar.insertAdjacentElement('afterbegin', wrap(iconButton));

  document
    .querySelector('#ContentView')
    .nextElementSibling.querySelector('button')
    ?.insertAdjacentElement('afterend', wrap(button));

  const dialog = document.createElement('mm-dialog');
  dialog.setAttribute('preserve', true);
  dialog.setAttribute('hide-cancel', true);
  dialog.setAttribute('heading', 'User Progress Information');
  dialog.innerHTML = '<mm-loader center indeterminate></mm-loader><p style="text-align:center;">Loading...</p>';
  dialog.hidden = true;
  document.body.append(dialog);
  setTimeout(() => (dialog.hidden = false), 1000);

  const prettyprintduration = seconds => {
    const [h, m, s] = new Date(seconds * 1000).toISOString().substr(11, 8).split(':');
    return `${h > 0 ? h + 'h ' : ''}${m > 0 ? m + 'm ' : ''}${s}s`;
  };

  const prettyprinttimestamp = ts => new Date(ts).toLocaleDateString();

  const style = document.createElement('style');
  style.innerHTML = `
    .progress-table thead tr {
      background-color: #eee;
    }

    .progress-table thead th,
    .progress-table tbody td {
      padding: 5px;
    }

    .progress-table tbody tr {
      text-align: center;
    }

    .progress-table tbody tr:nth-child(even) {
      background-color: #f6f6f6;
    }
  `;
  document.head.append(style);

  Promise.all([
    api(`/d2l/api/le/1.46/${orgid}/classlist/`),
    api(`/d2l/api/le/unstable/${orgid}/content/userprogress/?objectId=${contentid}`, { fresh: true }),
    api(`/d2l/api/lp/1.29/enrollments/orgUnits/${orgid}/users/`),
  ]).then(([classlist, progress, enrollments]) => {
    // Hash our map to prevent doing a .find each time.
    const progressmap = new Map();
    progress.forEach(user => progressmap.set(user.UserId, user));

    const usermap = new Map();
    enrollments.forEach(({ User }) => usermap.set(User.Identifier, User));

    const table = document.createElement('table');
    table.classList.add('progress-table');

    const headers = ['Student ID', 'Student Name', 'Email', 'Times Visited', 'Total Duration', 'Last Visited'];

    const header = document.createElement('thead');
    header.innerHTML = `<tr>${headers.map(hdr => `<th>${hdr}</th>`).join('')}</tr>`;

    const csvRows = [];

    const body = document.createElement('tbody');
    classlist.forEach(student => {
      // Only get students and past students
      if (![103, 120].includes(student.RoleId)) return;

      const report = progressmap.get(parseInt(student.Identifier)) ?? { TotalTime: 0, NumVisits: 0, LastVisited: '' };

      const row = document.createElement('tr');
      row.innerHTML = `
      <td>${student.OrgDefinedId}</td>
      <td>${student.FirstName} ${student.LastName}</td>
      <td>${usermap.get(student.Identifier).EmailAddress}</td>
      <td>${report.NumVisits}</td>
      <td>${prettyprintduration(report.TotalTime)}</td>
      <td>${report.LastVisited.length > 0 ? prettyprinttimestamp(report.LastVisited) : ''}</td>`;

      body.append(row);

      csvRows.push([
        student.OrgDefinedId,
        `${student.FirstName} ${student.LastName}`,
        usermap.get(student.Identifier).EmailAddress,
        report.NumVisits,
        prettyprintduration(report.TotalTime),
        report.LastVisited.length > 0 ? prettyprinttimestamp(report.LastVisited) : '',
      ]);
    });
    table.append(header, body);

    dialog.innerHTML = '';

    const description = document.createElement('p');
    description.innerText = `Below is the user progress information for "${PageName}".`;

    const csv = 'data:text/csv;charset=utf-8,' + [headers.map(hdr => `"${hdr}"`).join(',')].concat(csvRows).join('\n');

    const downloadLink = document.createElement('a');
    downloadLink.classList.add('d2l-link');
    downloadLink.download = `User Progress Report - ${document.title}.csv`;
    downloadLink.href = csv;
    downloadLink.style.marginBottom = '1rem';
    downloadLink.innerText = '[Save as CSV]';
    downloadLink.setAttribute('mm-icon-type', 'action-button');
    downloadLink.setAttribute('mm-icon-offset', '-7:-2');
    dialog.append(description, downloadLink, table);
  });

  button.addEventListener('click', e => dialog.toggleAttribute('open', true));
  iconButton.addEventListener('click', e => dialog.toggleAttribute('open', true));
})();
