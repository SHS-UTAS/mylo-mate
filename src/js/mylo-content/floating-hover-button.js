/**
 * @affects Unit Content
 * @problem The `Edit Web Page` button is at the bottom of the page, requiring lots of scrolling.
 * @solution Add a floating `Edit Web Page` icon beside the content
 * @target Academic and support staff
 * @impact Moderate
 */

import { finder } from '../../lib/finder.js';

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const settings = await FetchMMSettings();
  if (!settings.mylo.showExtraEditButtons) return;

  const hasEditButton =
    Array.from(document.querySelectorAll('button.d2l-button')).find(btn => /edit web page/i.test(btn.innerText)) != null;
  if (!hasEditButton) return;

  const pattern = new URLPattern('/d2l/le/content/:orgid/viewContent/:pageid/View', window.location.origin);
  if (!pattern.test(window.location.href)) return;

  const { orgid, pageid } = pattern.exec(window.location.href).pathname.groups;
  const frame = document.querySelector('.d2l-fileviewer-text iframe');

  const btn = 'floating-edit-button';
  const button = document.createElement('d2l-button-icon');
  button.setAttribute('text', 'Edit from here');
  button.setAttribute('icon', 'tier1:edit');
  button.classList.add(btn);

  button.onclick = () => {
    const sel = button.getAttribute('data-selector');
    const url = new URL(`/d2l/le/content/${orgid}/contentfile/${pageid}/EditFile?fm=0`, window.location.origin);
    url.searchParams.set('sx', sel);
    window.location = url.href;
  };

  const whitelist = [
    'p',
    'ul',
    'ol',
    'table',
    /** 'div.card', */
    'blockquote',
    'iframe',
    'div.video-wrapper',
  ];

  const style = document.createElement('style');
  style.innerHTML = `
    .${btn} {
      position: absolute;
      opacity: 0.3;
      transition: opacity 150ms ease-in-out;
      transform: translate(-50%,-50%);
      background: #ddd;
      border-radius: 0.4em;
      cursor: pointer;
    }

    .${btn}:hover {
      opacity: 1;
    }
  `;
  document.body.append(style);

  const findNearestMatchingComponent = path => {
    path = Array.from(path).reverse();
    for (const el of path) {
      if (el?.parentElement?.matches?.('[block-identifier="content-block"]')) {
        /** If it's an empty element, continue on our way */
        if (!el.querySelector('iframe') && el.innerText.trim().length == 0) continue;

        return el;
      }
    }
    return null;
  };

  const doc = frame.contentDocument;
  doc.addEventListener('mouseover', e => {
    const matching = findNearestMatchingComponent(e.composedPath());
    if (!matching) return;

    const elbox = matching.getBoundingClientRect();
    const framebox = frame.getBoundingClientRect();

    button.style.top = `${elbox.top + framebox.top + window.pageYOffset}px`;
    button.style.left = `${elbox.right + framebox.left + window.pageXOffset}px`;

    const selector = finder(matching, { root: doc.body, seedMinLength: 5 });
    button.setAttribute('data-selector', btoa(selector));

    document.body.append(button);
  });

  document.addEventListener('mouseover', e => {
    if (e.target.matches('iframe')) return;
    if (e.target.matches(`.${btn}`)) return;

    button.removeAttribute('data-selector');
    button.remove();
  });

  doc.addEventListener('mouseout', e => {
    const { toElement } = e;
    if (toElement == null) return;

    button.removeAttribute('data-selector');
    button.remove();
  });
})();
