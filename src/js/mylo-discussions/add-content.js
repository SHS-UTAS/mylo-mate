/**
 * @affects Discussions
 * @problem Viewing discussion posts in bulk is not possible, meaning monitoring posts requires a lot of clicking and waiting.
 * @solution In Grid View add the message of each post beneath each post title, allowing for quick and easy scanning over messages, crucial for efficient monitoring of discussion spaces.
 * @target Academic and support staff
 * @impact High
 * @savings 30 seconds per post viewed
 */

(async function () {
  const IsOER = window.location.host.startsWith('oer.utas');
  let { ou, tid, fid } = window.location.searchParams;

  await until(() => window.top.document.body.getAttribute('mm-abort') != null);
  if (window.top.document.body.getAttribute('mm-abort') == 'true') return;

  if (!fid || fid === '0') {
    fid = await (async () => {
      if (!window.location.searchParams['mm-forum-id']) {
        const forums = await api(`/d2l/api/le/1.28/${ou}/discussions/forums/`);
        const topics = await Promise.all(
          forums.map(async forum => {
            const t = await api(`/d2l/api/le/1.28/${ou}/discussions/forums/${forum.ForumId}/topics/`);
            return t ? t.find(topic => topic.TopicId == tid) : null;
          })
        );
        const f = topics.filter(t => t != null)[0];
        return f ? f.ForumId : null;
      }
      return window.location.searchParams['mm-forum-id'];
    })();
  }

  const store = await FetchMMSettings();
  const defaultState = store.mylo.ViewDiscussionsDefaultState || 'closed';

  function e_on(event, target, callback, context) {
    (context || document).addEventListener(event, e => {
      let el = e.target.matches(target) ? target : e.target.closest(target);
      if (el != null) callback.call(el, e);
    });
  }

  async function GetUserRoles() {
    if (IsOER) return null;
    return api(`/d2l/api/lp/1.31/enrollments/orgUnits/${ou}/users/`);
  }

  const post = id =>
    api(`/d2l/api/le/1.28/${ou}/discussions/forums/${fid}/topics/${tid}/posts/${id}`, { fresh: true, first: true });

  const header_row = document.querySelector('.d2l-grid-actions-container d2l-overflow-group');
  if (!header_row) return;

  const style = document.createElement('style');
  style.innerHTML = `
			.mm-discussion-text, .mm-discussion-text * { white-space: normal; word-break: break-word; } 
			.mm-discussion-text p { margin: 5px 0; } 
			d2l-icon.mm-discussion-chevron  { height: 14px !important; margin-left: 5px; cursor:pointer; }
			.mm-post-chevron .mm-discussion-chevron { display: none; }
			.mm-post-chevron[mm-view-status="open"] .mm-discussion-chevron[mm-show-on="open"] { display: block; }
			.mm-post-chevron[mm-view-status="closed"] .mm-discussion-chevron[mm-show-on="closed"] { display: block; }
			.mm-discussion-text[mm-view-status="closed"] { display: none; }
			.mm-discussion-text img { max-width: 20% !important; }
			.mm-disabled, .mm-disabled span, .mm-disabled d2l-icon { pointer-events: none; color: #888; }
			mm-loader.small { display: inline-block; margin: 0; padding: 0; position: relative; top: 5px; left: -10px; height: 20px; width: 20px; }
		`;
  document.head.appendChild(style);

  // let loader = document.createElement('link');
  // loader.href = chrome.runtime.getURL('css/loader.css');
  // loader.rel = 'stylesheet';
  // loader.type = 'text/css';
  // root.head.appendChild(loader);

  const wrap = elem => {
    const wrapper = document.createElement('div');
    wrapper.classList.add('d2l-grid-action-wrapper', 'd2l-button-group-custom-item');
    wrapper.append(elem);
    return wrapper;
  };

  const show_messages = document.createElement('d2l-button-subtle');
  show_messages.setAttribute('data-testid', 'show-all-messages');
  show_messages.setAttribute('icon', 'tier1:zoom-in');
  show_messages.setAttribute('text', 'Expand all messages');
  show_messages.setAttribute('title', 'Expand all messages');

  const hide_messages = document.createElement('d2l-button-subtle');
  hide_messages.setAttribute('data-testid', 'hide-all-messages');
  hide_messages.setAttribute('icon', 'tier1:zoom-out');
  hide_messages.setAttribute('text', 'Collapse all messages');
  hide_messages.setAttribute('title', 'Collapse all messages');

  show_messages.setAttribute('mm-icon-type', 'action-button');
  show_messages.setAttribute('mm-icon-offset', '6:6');
  hide_messages.setAttribute('mm-icon-type', 'action-button');
  hide_messages.setAttribute('mm-icon-offset', '6:6');

  const messages_loading = document.createElement('div');
  messages_loading.className = 'mm-disabled';
  messages_loading.innerHTML = '<mm-loader class="small" width="20"></mm-loader>Topic posts loading... Please wait...';

  header_row.append(wrap(show_messages), wrap(hide_messages), wrap(messages_loading));

  const users = await GetUserRoles();

  document.querySelectorAll('a[id^="post_"]').forEach(async title => {
    const postID = title.id.split('_')[2];
    let message = 'No data found.';

    const msg = await post(postID);
    if (msg?.Message?.Html) message = msg.Message.Html;
    else if (msg?.Message?.Text) message = msg.Message.Text;

    const handle = document.createElement('div');
    handle.style.display = 'inline-block';
    handle.classList.add('mm-post-chevron');
    handle.setAttribute('mm-post-id', postID);
    handle.setAttribute('mm-view-status', defaultState);
    handle.setAttribute('data-testid', 'show-hide-post-content');
    handle.innerHTML =
      '<d2l-icon mm-show-on="open" class="mm-discussion-chevron" icon="d2l-tier1:zoom-out" title="Collapse message"></d2l-icon><d2l-icon mm-show-on="closed" class="mm-discussion-chevron" icon="d2l-tier1:zoom-in" title="Expand message"></d2l-icon>';
    title.parentNode.appendChild(handle);

    const wrapper = document.createElement('div');
    wrapper.setAttribute('data-testid', 'post-content');
    wrapper.setAttribute('mm-post-id', postID);
    wrapper.classList.add('mm-discussion-text');
    wrapper.setAttribute('mm-view-status', defaultState);
    wrapper.innerHTML = message;
    title.parentNode.appendChild(wrapper);

    const user = users.find(({ User }) => User.Identifier == msg.PostingUserId);
    const author = title.parentNode.nextElementSibling.querySelector('label');

    const StudentProfileLinks = [];
    const badge = document.createElement('span');

    if (!user) {
      badge.innerText = ' [WN]';
      badge.title = 'Withdrawn Student';
      author.append(badge);

      return;
    }

    if (user.Role.Name.includes('Lecturer')) {
      author.style.fontWeight = 'bold';

      badge.innerText = ' [L]';
      badge.title = 'Lecturer';
      author.append(badge);
    } else if (user.Role.Name.includes('Tutor')) {
      author.style.fontWeight = 'bold';
      badge.innerText = ' [T]';
      badge.title = 'Tutor';
      author.append(badge);
    } else if (user.Role.Name.includes('Support')) {
      author.style.fontWeight = 'bold';
      badge.innerText = ' [S]';
      badge.title = 'Support';
      author.append(badge);
    } else if (user.Role.Name.includes('Student')) {
      const link = document.createElement('a');
      link.target = '_blank';
      link.href = `/d2l/le/classlist/userprogress/${msg.PostingUserId}/${ou}/Summary`;
      link.style.marginLeft = '10px';
      link.setAttribute('data-user-id', msg.PostingUserId);
      link.title = `Show user progress for ${user.User.DisplayName}`;

      StudentProfileLinks.push(link);

      const icon = document.createElement('d2l-icon');
      icon.setAttribute('icon', 'tier1:profile-default');
      link.append(icon);

      link.setAttribute('mm-icon-type', 'action-button');
      link.setAttribute('mm-icon-offset', '-4:-2');

      author.append(link);
    }

    let groups = await api(`/d2l/api/lp/1.22/${ou}/groupcategories/`);
    groups = await Promise.all(
      groups.map(async group => {
        const enrollments = await Promise.all(
          group.Groups.map(id => api(`/d2l/api/lp/1.22/${ou}/groupcategories/${group.GroupCategoryId}/groups/${id}`))
        );
        group.Groups = enrollments.map(i => i[0]);
        return group;
      })
    );

    const GetUsersGroupEnrollments = userid => {
      const output = [];
      groups.forEach(category => {
        category.Groups.forEach(group => {
          if (group.Enrollments.includes(parseInt(userid))) {
            output.push({ category, group });
          }
        });
      });
      return output;
    };

    StudentProfileLinks.forEach(link => {
      const enrollments = GetUsersGroupEnrollments(link.getAttribute('data-user-id'));
      if (enrollments.length > 0) {
        const title = [link.title];
        title.push('Enrolled in groups:');
        enrollments.forEach(({ category, group }) => {
          title.push(` - ${category.Name} > ${group.Name}`);
        });
        link.title = title.join('\u000A');
      } else {
        link.title += '\u000ANot enrolled in any groups.';
      }
    });
  });

  show_messages.classList.remove('mm-disabled');
  hide_messages.classList.remove('mm-disabled');

  messages_loading.parentElement.remove();

  show_messages.addEventListener('mousedown', e => {
    document.querySelectorAll('[mm-view-status]').forEach(node => node.setAttribute('mm-view-status', 'open'));
  });
  hide_messages.addEventListener('mousedown', e => {
    document.querySelectorAll('[mm-view-status]').forEach(node => node.setAttribute('mm-view-status', 'closed'));
  });

  e_on(
    'mousedown',
    '.mm-post-chevron',
    function (e) {
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();

      const state = this.getAttribute('mm-view-status');
      const toggle = state == 'open' ? 'closed' : 'open';
      this.setAttribute('mm-view-status', toggle);
      this.nextElementSibling.setAttribute('mm-view-status', toggle);
    },
    document
  );
})();

// This gets all students for a unit:
// await api('/d2l/api/lp/1.20/enrollments/orgUnits/258471/users/?roleId=103')
