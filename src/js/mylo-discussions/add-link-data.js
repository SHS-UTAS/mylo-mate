(function () {
  if (window.LinkDataAdded) return;
  window.LinkDataAdded = true;

  function modifyQueryStrings() {
    // Test if we have the permissions to view as a student.
    //	If we don't, then it means we don't have permissions for much else.
    const isStudent =
      document.querySelector('.d2l-list a').innerText != 'View as Student' &&
      document.querySelector('.d2l-list a').innerText != 'View as Instructor';
    if (isStudent) return;

    const parent = (node, target) => {
      let e;
      for (e = node.parentNode; e !== undefined && !e.matches(target); e = e.parentNode);
      return e;
    };

    // For each of the Topic Headings, find the appropriate element, and two new shortlinks to each heading.
    document.querySelectorAll('div[id^="topicDetailsPlaceholderId"] h3 a.d2l-linkheading-link').forEach(a => {
      let id = parent(a, '.d2l-forum-list-item')
        .parentNode.querySelector('div[data-forum-id]')
        .getAttribute('data-forum-id');
      a.href += (a.href.includes('?') ? '&' : '?') + `mm-forum-id=${id}`;
    });
  }
  modifyQueryStrings();

  const filter_checker = new MutationObserver(modifyQueryStrings);
  filter_checker.observe(document.querySelector('#ForumsTopicsListPartialPlaceHolder'), { childList: true });
})();
