/**
 * @affects Discussions
 * @problem The discussions area require several clicks and page-loads to access the various associated properties pages.
 * @solution Add quick links to common properties for each Discussions topic area (Edit Properties, Edit Restrictions, Edit Assessment).
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  if (window.DiscussionShortcutsAdded) return;
  window.DiscussionShortcutsAdded = true;

  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  function addShortlinks() {
    // Test if we have the permissions to view as a student.
    //	If we don't, then it means we don't have permissions for much else.

    // Get the OrgUnitID code
    let ou = window.location.href.split('/discussions/')[0].split('/').pop();
    // For each of the Topic Headings, find the appropriate element, and two new shortlinks to each heading.
    document.querySelectorAll('div[id^="topicDetailsPlaceholderId"]').forEach(row => {
      let topicID = row.querySelector('div').attributes[0].value;
      let heading = row.querySelector('h3');
      let wrapper = document.createElement('span');
      wrapper.setAttribute('data-testid', 'discussion-quicklinks');
      wrapper.setAttribute('mm-icon-render', 'icon');
      wrapper.setAttribute('mm-icon-type', 'action-navigation');
      wrapper.setAttribute('mm-icon-offset', '-6:-6');
      heading.appendChild(wrapper);
      addQuicklink(
        wrapper,
        `/d2l/lms/discussions/admin/topic_new_properties.d2l?ou=${ou}&tid=${topicID}`,
        'p',
        'Edit Topic Properties'
      );
      addQuicklink(
        wrapper,
        `/d2l/lms/discussions/admin/forum_topic_edit_restrictions.d2l?ou=${ou}&id=${topicID}&type=t&actId=8`,
        'r',
        'Edit Topic Restrictions'
      );
      addQuicklink(
        wrapper,
        `/d2l/lms/discussions/admin/topic_new_scoring.d2l?ou=${ou}&tid=${topicID}&actId=7`,
        'a',
        'Edit Topic Assessment'
      );
    });
  }
  addShortlinks();

  function addQuicklink(parentNode, href, shortcut, title) {
    let newLink = document.createElement('a');
    newLink.href = href;
    newLink.innerText = `[${shortcut}]`;
    newLink.classList.add('vui-link');
    newLink.setAttribute('Title', title);
    parentNode.appendChild(newLink);
  }

  const filter_checker = new MutationObserver(addShortlinks);
  filter_checker.observe(document.querySelector('#ForumsTopicsListPartialPlaceHolder'), { childList: true });
})();
