(async () => {
  const config = await FetchMMSettings();
  if (!config.mylo.ShowBulkEditDiscussionsEditor) return;

  const [, ou] = window.location.pathname.match(/d2l\/le\/([0-9]+)\/discussions/i);
  const btn = document.createElement('d2l-button');
  btn.setAttribute('mm-icon-type', 'action-button');
  btn.setAttribute('mm-icon-offset', '-7:0');
  btn.innerHTML = `
  <div class="d2l-buttonmenu-content">
    <d2l-icon icon="tier1:manage-dates-edit"></d2l-icon>
    <span style="padding-left: calc(1.4rem - 18px);">Bulk Edit Dates</span>
  </div>
  `;

  document.querySelector('#MoreActionsButtonMenu')?.insertAdjacentElement('afterend', btn);

  const modal = document.createElement('mm-dialog');
  modal.toggleAttribute('preserve', true);
  modal.registerNewButton({ primary: false, text: 'Clear all dates', role: 'clear-dates', index: 0 });
  modal.setAttribute('heading', 'Bulk Edit Discussion Dates');
  document.body.append(modal);

  modal.addEventListener('clear-dates', e => {
    e.preventDefault();
    modal.querySelectorAll('d2l-input-date-time-range').forEach(sel => {
      sel.setAttribute('start-value', '');
      sel.setAttribute('end-value', '');
    });
  });

  btn.onclick = async () => {
    modal.innerHTML = `
  <mm-loader center></mm-loader>
  <p style="text-align: center;">Loading...</p>
`;

    modal.toggleAttribute('open', true);

    const forums = await api(`/d2l/api/le/1.52/${ou}/discussions/forums/`, { fresh: true });

    const container = document.createElement('div');
    container.classList.add('date-range-container');

    const style = document.createElement('style');
    style.innerHTML = `
.date-range-wrapper {
    display: grid;
    gap: 1em;
    grid-template-columns: 12em auto;
    grid-template-rows: 1fr 1fr;
    padding: 1em;
}

.name-container {
    grid-row: 1/3;
    grid-column: 1/1;
    align-self: center;
    text-align: right;
    padding-right: 1em;
    font-weight: bold;
}

.availability-container {
    grid-row: 1/1;
    grid-column: 2/2;
}

.unlock-container {
    grid-row: 2/2;
    grid-column: 2/2;
}

.date-range-wrapper.forum-item {
    background-color: #f5f5f5;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

fieldset.forum-container {
    border: thin solid #ccc;
    padding: 1em;
    border-radius: 0.4em;
}

fieldset.forum-container legend {
    font-weight: bold;
    padding: 0 0.5em;
}

fieldset.forum-container:not(:last-of-type) {
    margin-bottom: 2em;
}

.date-range-wrapper:not(:last-of-type) {
    border-bottom: thin solid #ddd;
}

.date-range-wrapper.forum-item .name-container {
    display: none;
}
`;

    for (const forum of forums) {
      const topics = await api(`/d2l/api/le/1.52/${ou}/discussions/forums/${forum.ForumId}/topics/`, { fresh: true });

      /** Create the parent forum editor */
      const fieldset = document.createElement('fieldset');
      fieldset.classList.add('forum-container');
      const legend = document.createElement('legend');
      legend.innerText = forum.Name;
      fieldset.append(legend);

      const wrapper = document.createElement('div');
      wrapper.classList.add('date-range-wrapper', 'forum-item');

      const name = document.createElement('span');
      name.classList.add('name-container');

      const availability = document.createElement('div');
      availability.classList.add('availability-container');

      const unlock = document.createElement('div');
      unlock.classList.add('unlock-container');

      const AvailabilityDateRange = document.createElement('d2l-input-date-time-range');
      AvailabilityDateRange.setAttribute('label', 'Availability');
      AvailabilityDateRange.setAttribute('start-value', forum.StartDate ?? '');
      AvailabilityDateRange.setAttribute('end-value', forum.EndDate ?? '');
      AvailabilityDateRange.setAttribute('auto-shift-dates', 'true');

      AvailabilityDateRange.setAttribute('type', 'forum');
      AvailabilityDateRange.setAttribute('name', 'availability');
      AvailabilityDateRange.setAttribute('id', forum.ForumId);

      const UnlockDateRange = document.createElement('d2l-input-date-time-range');
      UnlockDateRange.setAttribute('label', 'Unlock Dates');
      UnlockDateRange.setAttribute('start-value', forum.PostStartDate ?? '');
      UnlockDateRange.setAttribute('end-value', forum.PostEndDate ?? '');
      UnlockDateRange.setAttribute('auto-shift-dates', 'true');

      UnlockDateRange.setAttribute('type', 'forum');
      UnlockDateRange.setAttribute('name', 'unlock');
      UnlockDateRange.setAttribute('id', forum.ForumId);

      name.innerText = forum.Name;
      availability.append(AvailabilityDateRange);
      unlock.append(UnlockDateRange);

      wrapper.append(name, availability, unlock);
      fieldset.append(wrapper);
      container.append(fieldset);

      for (const topic of topics) {
        const wrapper = document.createElement('div');
        wrapper.classList.add('date-range-wrapper', 'topic-item');

        const name = document.createElement('span');
        name.classList.add('name-container');

        const availability = document.createElement('div');
        availability.classList.add('availability-container');

        const unlock = document.createElement('div');
        unlock.classList.add('unlock-container');

        const AvailabilityDateRange = document.createElement('d2l-input-date-time-range');
        AvailabilityDateRange.setAttribute('label', 'Availability');
        AvailabilityDateRange.setAttribute('start-value', topic.StartDate ?? '');
        AvailabilityDateRange.setAttribute('end-value', topic.EndDate ?? '');
        AvailabilityDateRange.setAttribute('auto-shift-dates', 'true');

        AvailabilityDateRange.setAttribute('type', 'topic');
        AvailabilityDateRange.setAttribute('name', 'availability');
        AvailabilityDateRange.setAttribute('id', topic.TopicId);

        const UnlockDateRange = document.createElement('d2l-input-date-time-range');
        UnlockDateRange.setAttribute('label', 'Unlock Dates');
        UnlockDateRange.setAttribute('start-value', topic.UnlockStartDate ?? '');
        UnlockDateRange.setAttribute('end-value', topic.UnlockEndDate ?? '');
        UnlockDateRange.setAttribute('auto-shift-dates', 'true');

        UnlockDateRange.setAttribute('type', 'topic');
        UnlockDateRange.setAttribute('name', 'unlock');
        UnlockDateRange.setAttribute('id', topic.TopicId);

        name.innerText = topic.Name;
        availability.append(AvailabilityDateRange);
        unlock.append(UnlockDateRange);

        wrapper.append(name, availability, unlock);

        fieldset.append(wrapper);
      }
    }

    container.append(style);

    modal.innerHTML = '';
    modal.append(container);
  };

  modal.addEventListener('ok', async e => {
    const GetRangeValue = (type, id, name, attr) =>
      modal.querySelector(`d2l-input-date-time-range[type="${type}"][id="${id}"][name="${name}"]`)?.getAttribute(attr) ||
      null;

    const CompareValues = (a, b, keys) => keys.every(key => a[key] === b[key]);

    const GetAvailabilityStart = (type, id) => GetRangeValue(type, id, 'availability', 'start-value');
    const GetAvailabilityEnd = (type, id) => GetRangeValue(type, id, 'availability', 'end-value');
    const GetUnlockStart = (type, id) => GetRangeValue(type, id, 'unlock', 'start-value');
    const GetUnlockEnd = (type, id) => GetRangeValue(type, id, 'unlock', 'end-value');

    const PUT = (url, data) => apisend(url, data, { verb: 'put' });

    let changes = 0;
    const forums = await api(`/d2l/api/le/1.52/${ou}/discussions/forums/`, { fresh: true });
    for (const forum of forums) {
      const data = {
        Name: forum.Name,
        Description: forum.Description,
        ShowDescriptionInTopics: forum.ShowDescriptionInTopics, // Added with LE API v1.14
        AllowAnonymous: forum.AllowAnonymous,
        IsLocked: forum.IsLocked,
        IsHidden: forum.IsHidden,
        RequiresApproval: forum.RequiresApproval,
        MustPostToParticipate: forum.MustPostToParticipate,
        DisplayInCalendar: forum.DisplayInCalendar,
        DisplayPostDatesInCalendar: forum.DisplayPostDatesInCalendar,

        StartDate: GetAvailabilityStart('forum', forum.ForumId),
        EndDate: GetAvailabilityEnd('forum', forum.ForumId),
        PostStartDate: GetUnlockStart('forum', forum.ForumId),
        PostEndDate: GetUnlockEnd('forum', forum.ForumId),
      };

      const IsSame = CompareValues(data, forum, ['StartDate', 'EndDate', 'PostStartDate', 'PostEndDate']);
      if (!IsSame) {
        // Push the changes
        await PUT(`/d2l/api/le/1.52/${ou}/discussions/forums/${forum.ForumId}`, data);
        changes++;
      }

      const topics = await api(`/d2l/api/le/1.52/${ou}/discussions/forums/${forum.ForumId}/topics/`, { fresh: true });
      for (const topic of topics) {
        const StartDate = GetAvailabilityStart('topic', topic.TopicId);
        const EndDate = GetAvailabilityEnd('topic', topic.TopicId);
        const UnlockStartDate = GetUnlockStart('topic', topic.TopicId);
        const UnlockEndDate = GetUnlockEnd('topic', topic.TopicId);

        const data = {
          Name: topic.Name,
          Description: { Content: topic.Description.Html, Type: 'Html' },
          AllowAnonymousPosts: topic.AllowAnonymousPosts,
          IsHidden: topic.IsHidden,
          RequiresApproval: topic.RequiresApproval,
          ScoreOutOf: topic.ScoreOutOf,
          IsAutoScore: topic.IsAutoScore,
          IncludeNonScoredValues: topic.IncludeNonScoredValues,
          ScoringType: topic.ScoringType,
          IsLocked: topic.IsLocked,
          MustPostToParticipate: topic.MustPostToParticipate,
          RatingType: topic.RatingType,
          DisplayInCalendar: StartDate || EndDate ? topic.DisplayInCalendar : false,
          DisplayUnlockDatesInCalendar: UnlockStartDate || UnlockEndDate ? topic.DisplayUnlockDatesInCalendar : false,
          GroupTypeId: topic.GroupTypeId,
          StartDate,
          EndDate,
          UnlockStartDate,
          UnlockEndDate,
        };

        const IsSame = CompareValues(data, topic, ['StartDate', 'EndDate', 'UnlockStartDate', 'UnlockEndDate']);
        if (!IsSame) {
          // Push the changes
          await PUT(`/d2l/api/le/1.52/${ou}/discussions/forums/${forum.ForumId}/topics/${topic.TopicId}`, data);
          changes++;
        }
      }
    }

    if (changes > 0) {
      window.location.reload();
    }
  });
})();
