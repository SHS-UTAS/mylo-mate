/**
 * @affects Discussions
 * @problem Part of the unit cleanup process at the beginning of each delivery is to hide the previous delivery's Discussion area. This requires visiting the properties page for each Discussion Area and Topic, a slow, tedious and error-prone process.
 * @solution Add a `Manage Bulk Visibility` button which opens a dialogue showing all discussion forums and topics. Toggling any of the checkboxes allows their visibility to be managed in a single dialogue, greatly streamlining the workflow.
 * @target Academic and support staff
 * @impact High
 * @savings 5 minutes per discussion topic
 */

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const OrgID = window.location.searchParams.ou || /d2l\/le\/([0-9]+)\/discussions\//gi.exec(window.location.href)[1];

  const container = document.querySelector('.d2l-page-actions');
  const button = document.createElement('d2l-button-subtle');
  button.setAttribute('icon', 'tier1:visibility-conditional');
  button.setAttribute('text', 'Bulk Manage Visibility');
  button.setAttribute('mm-icon-type', 'action-select');
  button.setAttribute('mm-icon-offset', '2:2');
  container.prepend(button);

  await customElements.whenDefined('mm-dialog');

  const description = `<p style="margin-top: 0;">Checked discussions will be visible, unchecked discussions will be hidden.</p>`;

  const dialog = document.createElement('mm-dialog');
  document.body.append(dialog);
  dialog.addEventListener('open', () => (document.body.style.overflow = 'hidden'));
  dialog.addEventListener('close', () => (document.body.style.overflow = ''));
  dialog.preserve = true;
  dialog.width = '600px';
  dialog.heading = `Bulk Discussion Visibility Manager`;
  dialog.innerHTML = `${description}<p><mm-loader center></mm-loader></p><p style="text-align:center;">Loading, please wait..</p>`;

  button.addEventListener('click', e => {
    dialog.setAttribute('open', true);
  });

  let style = document.createElement('style');
  style.innerHTML = `
  ul.discussions-list { }
  ul.discussions-list li:first-child, ul.discussions-list li:last-child, ul.discussions-list li ul li { padding: 0; }
  ul.discussions-list li { padding: 10px 0; }
  ul.discussions-list li ul { margin-left: 20px; }
  ul.discussions-list .current-status { font-size: 12px; }`;

  const forums = await Promise.all(
    (
      await api(`/d2l/api/le/1.40/${OrgID}/discussions/forums/`, {
        fresh: true,
      })
    ).map(async forum => {
      forum.Topics = await api(`/d2l/api/le/1.40/${OrgID}/discussions/forums/${forum.ForumId}/topics/`, {
        fresh: true,
      });
      return forum;
    })
  );

  const statusMap = {};
  forums.forEach(forum => {
    statusMap[forum.ForumId.toString()] = !forum.IsHidden;
    forum.Topics.forEach(topic => {
      statusMap[topic.TopicId.toString()] = !topic.IsHidden;
    });
  });

  let list = document.createElement('ul');
  list.classList.add('discussions-list');
  forums.forEach(space => {
    let li = document.createElement('li');

    const checkedChildren = space.Topics.filter(topic => topic.IsHidden);

    let cb = document.createElement('input');
    cb.type = 'checkbox';
    cb.name = space.ForumId;
    cb.checked = !space.IsHidden;
    if (checkedChildren.length > 0 && checkedChildren.length < space.Topics.length) {
      cb.indeterminate = true;
    }

    let item = document.createElement('label');
    item.innerHTML = `${space.Name} <span class="current-status">(Currently ${
      space.IsHidden ? 'hidden' : 'visible'
    })</span>`;
    item.prepend(cb);
    li.append(item);

    if (space.Topics.length > 0) {
      const sub = document.createElement('ul');
      sub.classList.add('subcollection');
      space.Topics.forEach(topic => {
        let li = document.createElement('li');
        let cb = document.createElement('input');
        cb.type = 'checkbox';
        cb.name = topic.TopicId;
        cb.checked = !topic.IsHidden;

        let item = document.createElement('label');
        item.innerHTML = `${topic.Name} <span class="current-status">(Currently ${
          topic.IsHidden ? 'hidden' : 'visible'
        })</span>`;
        item.prepend(cb);
        li.append(item);
        sub.append(li);
      });
      li.append(sub);
    }

    list.append(li);
  });

  list.addEventListener('change', e => {
    const elem = e.target;
    const isSubItem = elem.closest('.subcollection') != null;

    if (!isSubItem) {
      let children = elem.closest('li').querySelectorAll('input[type="checkbox"]');
      children.forEach(el => (el.checked = elem.checked));
    } else {
      let siblings = elem.closest('.subcollection').querySelectorAll('input[type="checkbox"]');
      let checked = Array.from(siblings).filter(cb => cb.checked);
      let parent = elem.closest('.subcollection').closest('li').querySelector('input[type="checkbox"]');
      parent.indeterminate = false;
      if (checked.length == 0) {
        parent.checked = false;
      } else if (checked.length == siblings.length) {
        parent.checked = true;
      } else {
        parent.checked = false;
        parent.indeterminate = true;
      }
    }
  });

  dialog.innerHTML = description;
  dialog.append(style, list);

  dialog.addEventListener('ok', async () => {
    const cbs = Array.from(dialog.querySelectorAll('input[type="checkbox"]'));
    const different = cbs.filter(cb => cb.checked != statusMap[cb.name]);
    const isSubItem = cb => cb.closest('.subcollection') != null;

    // Get the native D2L change request actions.
    const actions = Object.entries(D2L.OR)
      .filter(([k, v]) => k.includes('UpdateTopicVisibility') || k.includes('UpdateForumVisibility'))
      .map(([k, v]) => v.select);
    different.forEach(cb => {
      let filter = isSubItem(cb) ? `topics/${cb.name}/Update` : `forums/${cb.name}/Update`;
      let action = actions.find(action => action.includes(filter));
      if (action) {
        let obj = JSON.parse(action);
        obj.P = [obj.P[0]];
        D2L.LP.Web.Serialization.Json.Deserialize(JSON.stringify(obj))();
      }
    });

    await new Promise(res => setTimeout(res, 500));
    window.location.reload();
  });
})();
