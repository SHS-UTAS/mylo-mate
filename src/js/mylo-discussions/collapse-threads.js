/**
 * @affects Discussions
 * @problem Posts with lots of replies often clutter the screen, and take up a significant amount of room. Collapsing messages one by one is awkward and slow.
 * @solution Add a button to the top of the discussions area, which automatically collapses all expanded message threads.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  let header_row = document.querySelector('.d2l-grid-actions-container d2l-overflow-group');
  if (!header_row) return;

  if (document.querySelector('select[name="SL_filterType"]').value != 1) return;

  const table = document.querySelector('d2l-table-wrapper table');
  let shouldCollapse = true;

  const dictionary = {
    collapse: {
      text: 'Collapse threads',
      title: 'Collapse all open threads',
      icon: 'tier1:chevron-up',
    },
    expand: {
      text: 'Expand threads',
      title: 'Expand all collapsed threads',
      icon: 'tier1:chevron-down',
    },
  };

  let toggle = document.createElement('d2l-button-subtle');
  toggle.setAttribute('data-testid', 'toggle-collapse');
  toggle.setAttribute('icon', dictionary.collapse.icon);
  toggle.setAttribute('text', dictionary.collapse.text);
  toggle.setAttribute('title', dictionary.collapse.title);
  toggle.setAttribute('mm-icon-type', 'action-button');
  toggle.setAttribute('mm-icon-offset', '6:6');
  header_row.append(toggle);

  toggle.addEventListener('click', e => {
    table
      .querySelectorAll(`tbody > tr > td img[title^="${shouldCollapse ? 'collapse row' : 'expand row'}" i]`)
      .forEach(e => e.click());

    const set = shouldCollapse ? dictionary.expand : dictionary.collapse;
    toggle.setAttribute('icon', set.icon);
    toggle.setAttribute('text', set.text);
    toggle.setAttribute('title', set.title);

    shouldCollapse = !shouldCollapse;
  });
})();
