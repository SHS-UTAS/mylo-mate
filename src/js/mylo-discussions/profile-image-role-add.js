/**
 * @affects Discussions
 * @problem When viewing the discussions tool it is useful to know the role of the last poster in a topic.
 * @solution Add a stylised border to the left hand side of the user's profile image so the role can be seen at a glance (Lecturer, Tutor and Support all have different colours and styles).
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  // Find the div `.d2l-user-profile-handle`, and find the `.d2l-user-profile-card-link`.
  // In the [onclick] attribute, it'll link to `D2L.O("__g(x)",0)`, with (x) being some kind of number.
  //	In `D2L.OR[__g(x)][0]`, will be a link to the users profile, which includes their User ID.
  // This gives us a much more accurate way to ensure we have the correct profile.
  //
  // For safety sake, ensure that we don't always assume [0] as the starting index; make sure to grab both arguments from `D2L.O` and use them as is.
  //	This should make it a bit more robust.

  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;
  const IsOER = window.location.host.startsWith('oer.utas');
  const OrgID = window.location.searchParams.ou || /d2l\/le\/([0-9]+)\/discussions\//gi.exec(window.location.href)[1];

  // let store = await new Promise(res => chrome.storage.sync.get(null, res));
  let _cache = { ts: Date.now(), _posts: [], _users: {} };

  async function GetUserRoles() {
    if (IsOER) return null;
    if ((Date.now() - _cache.ts) / 1000 < 60 * 30 && _cache._users.length > 0) {
      res(_cache._users);
      return;
    }

    const RoleType = { LECTURER: 4, TUTOR: 5 };
    const GetFromClassList = async role => {
      let d = new DOMParser().parseFromString(
        await ajax(`/d2l/lms/classlist/classlist.d2l?ou=${OrgID}&tabid=${role}`),
        'text/html'
      );
      return Array.from(d.querySelectorAll('table[summary^="This table is a list"] a[title^="Compose email to"]')).map(a => {
        let [sur, given] = a.innerText.split(',');
        return (given + ' ' + sur).trim();
      });
    };

    const GetWithdrawnStudents = async () => {
      const GetHTML = async url => new DOMParser().parseFromString(await ajax(url), 'text/html');
      const GetStudentsFromPage = async (page = 1, check = true) => {
        let res = [];
        const pageSize = 200;
        let dom = await GetHTML(
          `/d2l/lms/classlist/admin/classlist_report.d2l?ou=${OrgID}&d2l_stateScopes=%7B1%3A%5B%27gridpagenum%27,%27search%27,%27pagenum%27%5D,2%3A%5B%27lcs%27%5D,3%3A%5B%27grid%27,%27pagesize%27,%27htmleditor%27,%27hpg%27%5D%7D&d2l_stateGroups=%5B%27grid%27,%27gridpagenum%27%5D&d2l_statePageId=287&d2l_state_grid=%7B%27Name%27%3A%27grid%27,%27Controls%27%3A%5B%7B%27ControlId%27%3A%7B%27ID%27%3A%27grid_summary%27%7D,%27StateType%27%3A%27%27,%27Key%27%3A%27%27,%27Name%27%3A%27gridRoles%27,%27State%27%3A%7B%27PageSize%27%3A0,%27SortField%27%3A%27%27,%27SortDir%27%3A1%7D%7D,%7B%27ControlId%27%3A%7B%27ID%27%3A%27grid_users%27%7D,%27StateType%27%3A%27%27,%27Key%27%3A%27%27,%27Name%27%3A%27gridUsers%27,%27State%27%3A%7B%27PageSize%27%3A%27${pageSize}%27,%27SortField%27%3A%27%27,%27SortDir%27%3A1%7D%7D%5D%7D&d2l_state_gridpagenum=%7B%27Name%27%3A%27gridpagenum%27,%27Controls%27%3A%5B%7B%27ControlId%27%3A%7B%27ID%27%3A%27grid_users%27%7D,%27StateType%27%3A%27pagenum%27,%27Key%27%3A%27%27,%27Name%27%3A%27gridUsers%27,%27State%27%3A%7B%27PageNum%27%3A${page}%7D%7D%5D%7D&d2l_change=0`
        );
        if (dom.querySelector('table[summary="Withdrawals summary"]')) {
          res = Array.from(dom.querySelectorAll('table[summary="Withdrawals summary"] a[title^="Actions for"]'))
            .filter(
              node =>
                Array.from(node.parentNode.parentNode.querySelectorAll('label')).find(l => l.innerText == 'Student') != null
            )
            .map(a => {
              let [sur, given] = a.parentNode.innerText.split(',');
              return (given + ' ' + sur).trim();
            });
          if (check && dom.querySelector('select[title="Page Number"]')) {
            let pages = Array(dom.querySelectorAll('select[title="Page Number"] option').length - 1)
              .fill()
              .map((v, i) => i + 2);
            let others = await Promise.all(pages.map(page => GetStudentsFromPage(page, false)));
            others.forEach(o => (res = res.concat(o)));
          }
        }
        return res;
      };
      return await GetStudentsFromPage(1);
    };

    return {
      Lecturers: await GetFromClassList(RoleType.LECTURER),
      Tutors: await GetFromClassList(RoleType.TUTOR),
      Students: {
        Current: (await APIRequest(`/d2l/api/lp/1.20/enrollments/orgUnits/${OrgID}/users/?roleId=103`)).map(
          res => res.User.DisplayName
        ),
        StudentView: (await APIRequest(`/d2l/api/lp/1.20/enrollments/orgUnits/${OrgID}/users/?roleId=118`)).map(
          res => res.User.DisplayName
        ),
        Withdrawn: await GetWithdrawnStudents(),
      },
    };
  }

  const UserRoles = await GetUserRoles();

  let style = document.createElement('style');
  style.innerHTML = `
		[mm-staff] .d2l-imagelink, [mm-staff] .d2l-user-profile-card-link .d2l-image  { border-radius: 0 !important; border-top-width: 0 !important; border-bottom-width: 0 !important; }
		[mm-staff="lecturer"] .d2l-imagelink, [mm-staff="lecturer"] .d2l-user-profile-card-link .d2l-image { border-left: 5px green solid !important; }
		[mm-staff="tutor"] .d2l-imagelink, [mm-staff="tutor"] .d2l-user-profile-card-link .d2l-image { border-left: 5px #4098cc dashed !important; }
		[mm-staff="support"] .d2l-imagelink, [mm-staff="support"] .d2l-user-profile-card-link .d2l-image { border-left: 5px grey dotted !important; }
	`;
  document.head.appendChild(style);

  const ApplyImageBorder = () => {
    let users = [];
    if (window.location.href.includes('/discussions/threads/')) {
      users.push({
        name: document.querySelector('.d2l-page-title-view .d2l-textblock').innerText.split('posted')[0].trim(),
        image: document.querySelector('.d2l-imagelink'),
      });
      document.querySelectorAll('.d2l-le-disc-post .d2l-user-profile-handle').forEach(handle => {
        users.push({
          name: handle.parentNode.nextElementSibling.firstElementChild.innerText,
          image: handle.querySelector('.d2l-imagelink'),
        });
      });

      users.forEach(({ name, image }) => {
        if (UserRoles.Students.StudentView.find(sv => sv == name)) {
          // Do nothing if user was in Student View.
        } else if (UserRoles.Lecturers.find(l => l == name)) {
          // The user is a lecturer.
          image.parentNode.parentNode.setAttribute('mm-staff', 'lecturer');
        } else if (UserRoles.Tutors.find(t => t == name)) {
          // The user is a tutor.
          image.parentNode.parentNode.setAttribute('mm-staff', 'tutor');
        } else if (UserRoles.Students.Withdrawn.find(w => w == name)) {
          // The user is a withdrawn student.
          // We don't have a special style for them, so ignore this.
        } else if (UserRoles.Students.Current.find(c => c == name)) {
          // The user is a current student.
          // We don't really want to change the styling here, so we'll do nothing.
        } else {
          // The user isn't in any of these lists, so may be either Auditor, or much more likely, Support.
          image.parentNode.parentNode.setAttribute('mm-staff', 'support');
        }
      });
    } else if (window.location.href.includes('/discussions/topics/')) {
      document.querySelectorAll('.d2l-last-post-by').forEach(postBy => {
        let image = postBy.querySelector('.d2l-imagelink');
        let name = image.parentNode.parentNode.parentNode.nextElementSibling
          .querySelector('.d2l-textblock')
          .lastChild.nodeValue.split('by')[1]
          .trim();

        if (UserRoles.Students.StudentView.find(sv => sv == name)) {
          // Do nothing if user was in Student View.
        } else if (UserRoles.Lecturers.find(l => l == name)) {
          // The user is a lecturer.
          image.parentNode.parentNode.setAttribute('mm-staff', 'lecturer');
        } else if (UserRoles.Tutors.find(t => t == name)) {
          // The user is a tutor.
          image.parentNode.parentNode.setAttribute('mm-staff', 'tutor');
        } else if (UserRoles.Students.Withdrawn.find(w => w == name)) {
          // The user is a withdrawn student.
          // We don't have a special style for them, so ignore this.
        } else if (UserRoles.Students.Current.find(c => c == name)) {
          // The user is a current student.
          // We don't really want to change the styling here, so we'll do nothing.
        } else {
          // The user isn't in any of these lists, so may be either Auditor, or much more likely, Support.
          image.parentNode.parentNode.setAttribute('mm-staff', 'support');
        }
      });
    } else if (window.location.href.includes('/discussions/List')) {
      document.querySelectorAll('.d2l-table-cell-last').forEach(row => {
        let name = row.querySelector('.d2l-last-post-date-container .d2l-textblock');
        if (name != null) {
          name = name.innerText.trim();
          if (UserRoles.Students.StudentView.find(sv => sv == name)) {
            // Do nothing if user was in Student View.
          } else if (UserRoles.Lecturers.find(l => l == name)) {
            // The user is a lecturer.
            row.querySelector('.d2l-user-profile-handle').setAttribute('mm-staff', 'lecturer');
          } else if (UserRoles.Tutors.find(t => t == name)) {
            // The user is a tutor.
            row.querySelector('.d2l-user-profile-handle').setAttribute('mm-staff', 'tutor');
          } else if (UserRoles.Students.Withdrawn.find(w => w == name)) {
            // The user is a withdrawn student.
            // We don't have a special style for them, so ignore this.
          } else if (UserRoles.Students.Current.find(c => c == name)) {
            // The user is a current student.
            // We don't really want to change the styling here, so we'll do nothing.
          } else {
            // The user isn't in any of these lists, so may be either Auditor, or much more likely, Support.
            row.querySelector('.d2l-user-profile-handle').setAttribute('mm-staff', 'support');
          }
        }
      });
    }
  };

  if (document.querySelector('#ForumsTopicsListPartialPlaceHolder') || document.querySelector('#topicContentsPlaceholder')) {
    let mo = new MutationObserver(ApplyImageBorder);
    mo.observe(
      document.querySelector('#ForumsTopicsListPartialPlaceHolder') || document.querySelector('#topicContentsPlaceholder'),
      { childList: true, subtree: true }
    );
  }
  ApplyImageBorder();
})();
