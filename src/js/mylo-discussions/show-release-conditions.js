(async function () {
  const forumtype = 'discussionForums';
  const topictype = 'discussionTopics';
  const ou = window.location.href.match(/d2l\/le\/([0-9]+)\/discussions/i)[1];

  document.querySelectorAll('span[title="Conditionally released"]').forEach(async el => {
    if (el.closest('div[id*="forumDetails"]')) {
      // We're in a forum block, do forum things
      const id = el.closest('div[id*="forumDetails"]').id.split('_').pop();
      const restrictions = await getReleaseConditions(ou, forumtype, id);
      el.innerText = restrictions.join('<br>');
    } else if (el.closest('div[id*="topicDetailsPlaceholder"]')) {
      // We're in a topic block, do topic things
      const id = el.closest('div[id*="topicDetailsPlaceholder"]').id.split('Id').pop();
      const restrictions = await getReleaseConditions(ou, topictype, id);
      el.innerText = restrictions.join('<br>');
    }
  });
})();
