import Cropper from 'https://unpkg.com/cropperjs@1.5.9/dist/cropper.esm.js';
import baseactions from './sidebar-actions.js';
import jss from '../../lib/jss.bund.js';
import preset from '../../lib/jss-preset-default.bund.js';
jss.setup(preset());

const { classes } = jss
  .createStyleSheet({
    imagePreview: {
      position: 'relative',
    },
    recommended: {
      position: 'absolute',
      top: 0,
      right: 0,
      fontSize: 'small',
    },
    container: {
      height: '100%',
      display: 'grid',
      overflow: 'auto',
      padding: '0 1em',
    },
  })
  .attach();

const base = {};
baseactions.forEach(action => {
  base[action.key] = action;
});

const hidealloverride = document.createElement('style');
hidealloverride.innerHTML = `[hidden] { display: none !important; }`;
document.head.append(hidealloverride);

const dialog = document.createElement('mm-prompt');
dialog.setAttribute('preserve', true);
/** dialog.registerNewButton({ text: 'Upload Image', role: 'upload-banner', index: 0 }); */
/** dialog.registerNewButton({ text: 'Crop/Resize', role: 'crop-resize', index: 1 }); */
dialog.alignButtons = 'left';
dialog.confirmText = 'Next';
dialog.addEventListener('open', () => (document.body.style.overflow = 'hidden'));
dialog.addEventListener('close', () => (document.body.style.overflow = ''));

const resizer = document.createElement('mm-prompt');
resizer.setAttribute('preserve', true);
resizer.addEventListener('open', () => (document.body.style.overflow = 'hidden'));
resizer.addEventListener('close', () => (document.body.style.overflow = ''));
resizer.setAttribute('heading', `Crop Banner Image`);
resizer.alignButtons = 'left';
resizer.confirmText = 'Finish';
resizer.setAttribute('max-height', '80%');
resizer.setAttribute('width', '700px');

const loader = document.createElement('div');
loader.innerHTML = `<p><mm-loader center></mm-loader></p><p style="text-align:center;">Loading, please wait..</p>`;

dialog.append(loader);
dialog.setAttribute('heading', `Browse for Page Image`);
dialog.setAttribute('max-height', '80%');
dialog.setAttribute('width', '700px');
document.body.append(dialog, resizer);

const style = document.createElement('style');
style.innerHTML = `ul.file-tree { overflow-y: auto; }

ul.file-tree li .name:before {
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  font-size: inherit;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  padding: 0 7px;
}

ul.file-tree li[data-type] {
  cursor: pointer;
}

ul.file-tree li {
  padding: 2px 0;
  border-radius: 3px;
}

ul.file-tree li {
  max-width: 90%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  min-width: 0;
  align-items: center;
}

ul.file-tree li[active] {
  background-color: rgba(148, 212, 243, 0.6);
  font-weight: bold;
}

ul.file-tree li .name:hover {
  text-decoration: underline;
}

ul.file-tree li .name:before:hover {
  text-decoration: none;
}

ul.file-tree li[data-type="Folder"] > .name {
  font-weight: bold;
}

ul.file-tree li[data-type="Folder"][collapsed] > .name:before {
  content: "\\f114";
}

ul.file-tree li[data-type="Folder"] > .name:before {
  content: "\\f115";
}

ul.file-tree li[data-type="Image"] > .name:before {
  content: "\\f1c5";
}

ul.file-tree li > ul {
  display: block;
  margin-left: 10px;
}

.path-info {
  padding-top: 0.5rem; 
  border-top: 2px solid whitesmoke; 
}
 

.path-info label {
  font-weight: bold;
}

.path-info label.disabled {
  color: #999;
}

.path-preview {
  max-height: 200px;
  max-width: 100%;
  padding-top: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  aspect-ratio: 4/1;
  margin: 0 auto;
}

.path-preview img {
  max-width: 100%;
  max-height: inherit;
}
`;

/** The filter to apply to the folder tree */
const filter = ['Folder', 'Image'];
const orgId = window.location.pathname.match(/\/content\/([\d]+)/)[1];
const coursedata = api(`/d2l/api/lp/1.26/courses/${orgId}`, { first: true });
const fs = new Files(orgId);

const path = document.createElement('div');
path.classList.add('path-info');
const pathTitle = document.createElement('label');
pathTitle.innerText = 'File path';

const pathInput = document.createElement('input');
pathInput.classList.add('d_edt', 'vui-input', 'd2l-edit');
pathInput.readOnly = true;

const pathPreview = document.createElement('div');
pathPreview.classList.add('path-preview');
pathPreview.innerHTML = '<img src="" />';

const scale = document.createElement('span');
scale.classList.add(classes.recommended);
pathPreview.prepend(scale);
pathPreview.classList.add(classes.imagePreview);

let makeDefaultConfirmMessage = `Selecting this option will change the banner image for all new pages in your unit. You can replace the default banner image in the future. Are you sure you want to continue?`;
(async () => {
  const course = await coursedata;
  const images = await GetDirectoryListing(orgId, `${course.Path}images/banners/default`);
  if (images?.length > 0) {
    makeDefaultConfirmMessage = `Selecting this option will change the banner image for all new pages in your unit. The old default banner image will be overwritten. Are you sure you want to continue?`;
  }
})();

const makeDefault = document.createElement('input');
makeDefault.type = 'checkbox';
makeDefault.style.marginRight = '1em';
makeDefault.addEventListener('change', e => {
  // We don't want to show the confirm message if the user is
  // -unchecking- the checkbox. Note, this value is after the change, not before.
  if (!e.target.checked) return;

  e.preventDefault();
  e.target.checked = confirm(makeDefaultConfirmMessage);
});

const mdLabel = document.createElement('label');
mdLabel.innerText = 'Use this banner image for all pages';
mdLabel.title = 'This banner image will be used as the default for all new pages in this unit';
mdLabel.prepend(makeDefault);

const bannerIsDefault = document.createElement('label');
bannerIsDefault.innerText = 'Default banner image is selected';
bannerIsDefault.hidden = true;

const uploadImage = document.createElement('button');
uploadImage.classList.add('d2l-button');
uploadImage.innerText = 'Upload Image';

const uploadIcon = document.createElement('d2l-icon');
uploadIcon.setAttribute('icon', 'tier1:upload');
uploadIcon.style.marginRight = '0.5em';
uploadImage.prepend(uploadIcon);

path.append(/** pathTitle, pathInput, */ uploadImage, pathPreview, mdLabel, bannerIsDefault);

let _ready = false;
let _readyQueue = [];
/**
 * Resolves true when the initial document tree has been created.
 */
const IsReady = () => new Promise(res => (_ready ? res(true) : _readyQueue.push(res)));

const sizeMessage = document.createElement('span');
sizeMessage.style.textAlign = 'right';
sizeMessage.style.width = '100%';
sizeMessage.style.display = 'block';
sizeMessage.style.margin = '-30px -20px 10px -20px';

const fileList = document.createElement('ul');
fileList.hidden = true;
fileList.classList.add('file-tree', 'scroll-shadow');

/**
 * This provides us a way to get the Manage Files tree.
 * Could be useful for generating a basic tree navigator for finding images.
 */
const history = new Map();
export const GetDirectoryListing = async (id, path) => {
  try {
    if (history.has(path)) {
      return history.get(path);
    }

    const response = await CustomRPC(
      `/d2l/common/rpc/dataGrid/dataGrid.d2lfile?ou=${id}&d2l_rh=rpc&d2l_rt=call`,
      'Reload',
      `{'param1':{'Paging':{'PageSize':100000,'PageNumber':1},'DataGridContext':{'Parameters':{'NodeType':'Content','DirectoryId':'${path.replace(
        /\//g,
        '\\/'
      )}','IsShared':'0','NumDirectories':'0','NumFiles':'0','ErrorTypeId':'0'},'Columns':{Name:{'ColumnKey':'Name'},Size:{'ColumnKey':'Size'},Type:{'ColumnKey':'Type'},LastModified:{'ColumnKey':'LastModified'}}},'Rpc':'GetGridContents','RpcSrc':'\/d2l\/lp\/manageFiles\/rpc\/rpc_functions.d2l','Sorting':[{'SortField':'IsDirectory','IsAscending':false,'CollationCultureCode':null},{'SortField':'Name','IsAscending':true,'CollationCultureCode':null}]}}`
    ).then(d => d.text());
    const obj = new Function(`return(${response.replace('while(true){}', '').replace(/'ClassName':[^,]+,/gi, '')});`)();
    const res = obj.Result?.Rows.map(({ RowKey, Subject, Data }) => ({
      Path: RowKey.substr(2),
      Name: Subject,
      Type: Data[2].Text.Value,
      IsDirectory: RowKey.startsWith('d_'),
    }));

    history.set(path, res);
    return res;
  } catch (e) {
    console.error('Error fetching directory listing: ', e);
  }
};

const populate = (list, items) => {
  items.forEach(item => {
    const li = document.createElement('li');
    li.title = item.Name;
    li.setAttribute('data-type', item.Type);
    li.setAttribute('data-key', item.Path);
    if (item.Type == 'Folder') li.setAttribute('collapsed', '');
    li.innerHTML = `<span class="name">${item.Name}</span>`;
    list.append(li);
  });
  return list;
};

const populateSubList = async (list, path, filter) => {
  const parent = list.querySelector(`li[data-key="${path}" i]`);
  if (!parent) return;

  const subelem = parent.querySelector(`ul[data-parent="${path}"]`) || document.createElement('ul');
  if (!subelem.hasAttribute('data-parent')) subelem.setAttribute('data-parent', path);
  subelem.innerHTML = `<li><mm-loader center width="20" style="width: 50px; margin: 0; padding: 10px 20px;"></mm-loader></li>`;
  parent.append(subelem);

  const children = await GetDirectoryListing(orgId, path).then(d =>
    filter.length > 0 ? d.filter(r => filter.includes(r.Type)) : d
  );

  if (children.length == 0) {
    subelem.innerHTML = `<li class="empty">No items</li>`;
    return false;
  } else {
    subelem.innerHTML = ``;
    const subitems = populate(subelem, children);
    parent.append(subitems);
    return true;
  }
};

{
  // Scoping
  const container = document.createElement('div');
  container.classList.add(classes.container);
  container.append(fileList, path);
  dialog.append(style, container);
}

// Start populating the file browser dialog in the background.
const buildTree = async () => {
  _ready = false;

  history.clear();
  fileList.replaceWith(loader);

  // Flush the file list each time
  fileList.innerHTML = '';

  const course = await coursedata;
  const initial = await GetDirectoryListing(orgId, course.Path).then(d =>
    filter.length > 0 ? d.filter(r => filter.includes(r.Type)) : d
  );

  populate(fileList, initial);

  loader.replaceWith(fileList);

  _ready = true;
  _readyQueue.forEach(cb => cb(true));
  _readyQueue = [];
};

// Handle menu entry events in the file browser dialog
dialog.addEventListener('click', e => {
  if (e.target.matches('li[data-type="Folder"] > .name')) {
    const key = e.target.closest('li[data-type="Folder"]').getAttribute('data-key');
    if (dialog.querySelector(`ul[data-parent="${key}"]`)) {
      dialog.querySelector(`ul[data-parent="${key}"]`).toggleAttribute('hidden');
    } else populateSubList(fileList, key, filter);
    e.target.closest('li[data-type="Folder"]').toggleAttribute('collapsed');
  }

  if (e.target.matches('li[data-type]:not([data-type="Folder"]) > .name')) {
    const sel = e.target.closest('li[data-type]:not([data-type="Folder"])');
    fileList.querySelectorAll('li[active]').forEach(li => li.removeAttribute('active'));
    sel.setAttribute('active', '');
    pathInput.value = encodeURI(sel.getAttribute('data-key'));

    const disableCheckbox = /images\/banners\/default\/unit-banner.jpg/i.test(pathInput.value);

    makeDefault.disabled = disableCheckbox;
    makeDefault.parentElement.classList.toggle('disabled', disableCheckbox);

    mdLabel.hidden = disableCheckbox;
    bannerIsDefault.hidden = !disableCheckbox;

    // Set the image preview here
    pathPreview.querySelector('img').src = encodeURI(sel.getAttribute('data-key'));
  }
});

buildTree();

const BrowseForUnitFile = (existingPath = '') => {
  return new Promise(async res => {
    try {
      const doc = tinymce.activeEditor.getDoc();

      const handleRes = async path => {
        const banner = doc.querySelector('.banner-img');
        banner?.removeAttribute('hidden');

        if (makeDefault.checked) {
          makeDefault.checked = false; // For next time

          const { Path } = await coursedata;

          if (!new RegExp(`${Path}images/banners/default/unit-banner.jpg`, 'gi').test(path)) {
            // Delete the files in the folder originally
            const images = await GetDirectoryListing(orgId, `${Path}images/banners/default`, false);
            if (images?.length > 0) {
              for (const image of images) {
                const path = image.Path.split('/').slice(0, -1).join('/');
                await fs.Delete(path, image.Name);
              }
            }

            const img = await fetch(path).then(d => d.blob());

            /** The filename of the image */
            const output = await fs.UploadFile(`${Path}images/banners/default`, `unit-banner.jpg`, img);
            pathInput.value = output;
            res(output);
          } else {
            res(path);
          }
        } else {
          // Return the path value
          res(path);
        }

        // Rebuild the tree for the next visit (in the background for a better user experience)
        buildTree();
      };

      dialog.addEventListener('cancel', () => res(null), { once: true });
      dialog.addEventListener(
        'ok',
        async e => {
          e.preventDefault();
          dialog.dismiss();

          const path = pathInput.value;
          const skipCropping = /images\/banners\/default\/unit-banner.jpg/i.test(path);
          if (skipCropping) {
            return handleRes(path);
          } else {
            const changed = await CropImage(path);
            return handleRes(changed);
          }
        },
        { once: true }
      );

      uploadImage.onclick = async e => {
        const file = document.createElement('input');
        file.type = 'file';
        file.accept = 'image/*';
        file.click();

        file.addEventListener('change', async e => {
          if (e.target.files.length == 0) return;

          dialog.dismiss();

          const src = URL.createObjectURL(e.target.files[0]);
          const changed = await CropImage(src);

          return handleRes(changed);
        });
      };

      pathInput.value = existingPath;

      const disableCheckbox = /images\/banners\/default\/unit-banner.jpg/i.test(pathInput.value);

      makeDefault.disabled = disableCheckbox;
      makeDefault.parentElement.classList.toggle('disabled', disableCheckbox);

      mdLabel.hidden = disableCheckbox;
      bannerIsDefault.hidden = !disableCheckbox;

      pathPreview.querySelector('img').src = existingPath;

      const images = {
        bannerSmall: {
          selector: `.banner-img img`,
          size: '1200px \u{00d7} 300px',
        },
        bannerMedium: {
          selector: `.bg-img-wrapper img`,
          size: '1200px \u{00d7} 400px',
        },
        background: {
          selector: `.fullscreen-splash.overlay-reveal img`,
          size: '1200px \u{00d7} 600px',
        },
      };

      const recommended = Object.values(images).find(img => doc.querySelector(img.selector));
      if (recommended) {
        scale.innerText = `Recommended minimum size: ${recommended.size}`;
      } else {
        scale.innerText = '';
      }

      dialog.setAttribute('heading', `Choose Banner Image`);
      const course = await coursedata;
      dialog.setAttribute('open', true);
      await IsReady();

      fileList.querySelectorAll('li[active]').forEach(el => el.removeAttribute('active'));

      if (existingPath.includes(course.Path)) {
        if (fileList.querySelector(`li[data-key="${new URL(existingPath).pathname}" i]`)) {
          fileList.querySelector(`li[data-key="${new URL(existingPath).pathname}" i]`).toggleAttribute('active', true);
        } else {
          const parts = [];
          new URL(existingPath).pathname
            .replace(course.Path, '')
            .split('/')
            .forEach((part, i, a) => {
              if (i != a.length - 1) parts.push(`${parts[parts.length - 1] || course.Path}${part}/`);
            });

          for (const part of parts) {
            try {
              if (!(await populateSubList(fileList, part, filter))) break;
              fileList.querySelector(`li[data-key="${part}" i]`)?.removeAttribute('collapsed');
            } catch (e) {
              console.error('Error in building existing path tree', e);
            }
          }
          if (fileList.querySelector(`li[data-key="${new URL(existingPath).pathname}" i]`)) {
            fileList.querySelector(`li[data-key="${new URL(existingPath).pathname}" i]`).toggleAttribute('active', true);
          }
        }
      }
      loader.remove();
      fileList.hidden = false;
    } catch (e) {
      console.error('Something went wrong opening the dialog: ', e);
      res(null);
    }
  });
};

if (!document.head.querySelector('link[data-purpose="cropperjs"]')) {
  const link = document.createElement('link');
  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = `chrome-extension://${extensionID}/lib/cropper.css`;
  link.dataset.purpose = 'cropperjs';
  document.head.append(link);
}

const CropImage = image => {
  return new Promise((res, rej) => {
    resizer.setAttribute('heading', 'Crop Banner Image');
    resizer.innerHTML = '';

    const options = document.createElement('div');
    options.style.marginBottom = '1em';
    options.innerHTML = `
    <label class="d2l-input-radio-label">
      <input type="radio" name="ShouldCrop" value="true" checked> Crop image
    </label>
    <label class="d2l-input-radio-label">
      <input type="radio" name="ShouldCrop" value="false"> Don't crop image
    </label>`;

    const container = document.createElement('div');
    resizer.append(options, container);

    const img = document.createElement('img');
    img.src = image;
    img.style.display = 'block';
    img.style.maxWidth = '100%';

    img.style.minHeight = '15em';

    container.append(img);

    resizer.setAttribute('open', true);

    const crop = new Cropper(img, {
      aspectRatio: 4 / 1,
      viewMode: 1,
      autoCropArea: 1,
    });

    options.onchange = e => {
      const ShouldCrop = JSON.parse(options.querySelector('[name="ShouldCrop"]:checked').value);
      if (!ShouldCrop) {
        crop.clear();
        crop.disable();
      } else {
        crop.enable();
        crop.crop();
      }
    };

    const getSize = path =>
      new Promise((res, rej) => {
        const img = document.createElement('img');
        img.onload = () => res({ width: img.naturalWidth, height: img.naturalHeight });
        img.onerror = rej;
        img.src = path;
      });

    resizer.addEventListener(
      'ok',
      async e => {
        // We want to make sure we can control when the dialog disappears.
        e.preventDefault();

        container.replaceWith(loader);

        // Do something to show a loader
        const blob = await new Promise(res => crop.getCroppedCanvas().toBlob(res));

        const c = crop.getCropBoxData();
        const i = crop.getImageData();

        /** If there is less than 5px overall size difference, consider it the same and don't crop the image. */
        const IsFullSizeCropZone = Math.floor(i.width - c.width) + Math.floor(i.height - c.height) < 5;
        const ShouldCrop = JSON.parse(options.querySelector('[name="ShouldCrop"]:checked').value) && !IsFullSizeCropZone;

        /** If the image starts with a blob prefix, we definitely need to upload. So, don't do any of the below checking in that case.  */
        if (!image.startsWith('blob:')) {
          /** If the image is the same size, just return rather than duplicate things. */
          if (!ShouldCrop) {
            resizer.dismiss();
            return res(image);
          }
        }

        const blocks = image.split('/');
        const parts = image.split('.');
        const ext = parts.pop();
        const fn = blocks.pop();

        const name = `${fn.replace(/[^a-z0-9-]*/gi, '')}-${Date.now().toString(15)}.${ext}`;

        const { Path } = await coursedata;
        const output = await fs.UploadFile(`${Path}images/banners`, name, blob);

        // Close the dialog now that we're done.
        resizer.dismiss();

        res(output);
      },
      { once: true }
    );

    resizer.addEventListener('cancel', resizer.dismiss, { once: true });
  });
};

const components = {
  'toggle-lead': {
    key: 'toggle-lead',
    /** title: 'Toggle Lead Style', */
    title: editor => `${editor.selection.getNode()?.classList.contains('lead') ? 'Remove' : 'Add'} Lead Style`,
    description: 'Toggle the Paragraph Lead style',
    action(editor) {
      editor.selection.getNode().classList.toggle('lead');
      document.dispatchEvent(new Event('domchange'));
    },
    targetNode: ['p'],
    display() {
      return true;
    },
  },
  'toggle-lg-ol': {
    key: 'toggle-lg-ol',
    /** title: 'Toggle Large Ordered List', */
    title: editor => `${editor.selection.getNode()?.classList.contains('large-number') ? 'Remove' : 'Add'} Large Numbers`,
    description: 'Toggle the large numbers of an ordered list',
    action(editor) {
      const node = editor.selection.getNode();
      const elem = node.matches('ol') ? node : node.closest('ol');
      if (elem) {
        elem.classList.toggle('large-number');
        document.dispatchEvent(new Event('domchange'));
      }
    },
    targetNode: ['ol'],
    display() {
      return true;
    },
  },
  'toggle-download-style': {
    key: 'toggle-download-style',
    /** title: 'Toggle Download Link Style', */
    title: editor => `${editor.selection.getNode()?.classList.contains('download') ? 'Remove' : 'Add'} Download Link Style`,
    description: 'Toggle the download icon of a link',
    action(editor) {
      const node = editor.selection.getNode();
      const elem = node.matches('a') ? node : node.closest('a');
      if (elem) {
        elem.classList.toggle('download');
        document.dispatchEvent(new Event('domchange'));
      }
    },
    targetNode: ['a[href]'],
    display() {
      return true;
    },
  },
  'toggle-banner-visibility': {
    key: 'toggle-banner-visbility',
    /** title: 'Toggle Banner Visibility', */
    title: editor => `${editor.dom.doc.querySelector('.banner-img')?.hasAttribute('hidden') ? 'Show' : 'Hide'} Banner Image`,
    description: 'Hide or show the banner image',
    action(editor) {
      const banner = editor.dom.doc.querySelector('.banner-img');
      if (banner) {
        banner.toggleAttribute('hidden');
        document.dispatchEvent(new Event('domchange'));
      }
    },
    display(store, editor) {
      return editor.dom.doc.querySelector('.banner-img') != null;
    },
  },
  'browse-page-image': {
    key: 'browse-page-image',
    title: 'Choose Banner Image',
    description: 'Browse the Unit Files for the page image',
    async action(editor) {
      const img = editor.dom.doc.querySelector('.banner-img img,.bg-img-wrapper img');
      if (img) {
        const image = await BrowseForUnitFile(img ? img.src : '');
        if (!image) return;

        if (image.trim().length > 0) {
          const img = editor.dom.doc.querySelector('.banner-img img,.bg-img-wrapper img');
          img.src = `${image}?ts=${Date.now()}`;
          img.setAttribute('data-mce-src', image);
        }
        document.dispatchEvent(new Event('domchange'));
      }
    },
    display(store, editor) {
      return editor.dom.doc.querySelector('.banner-img,.bg-img-wrapper') != null;
    },
  },
  'insert-stuff': {
    key: 'insert-stuff',
    title: 'Insert Stuff',
    description: 'Quickly open the "Insert Stuff" dialog',
    async action(editor, tinymce) {
      editor.execCommand('d2l-isf');
    },
    display(store, editor) {
      return !store.editor.ShowInsertStuffPowerTools;
    },
  },
  'insert-echo360': {
    key: 'insert-echo-video',
    title: 'Insert Echo360 Video',
    description: 'Quickly open the "Insert Stuff" Echo360 dialog',
    async action(editor) {
      const mo = new MutationObserver(() => {
        const frame = document.querySelector('iframe[src="/d2l/tools/blank.html"]');
        if (!frame) return;

        frame.onload = () => {
          frame.contentDocument.querySelector('a[title*="Echo360"]')?.click();
          mo.disconnect();
        };
      });

      mo.observe(document.body, { childList: true });
      editor.execCommand('d2l-isf');
    },
    display(store) {
      return store.editor.ShowInsertStuffPowerTools;
    },
  },
  'insert-h5p': {
    key: 'insert-h5p-video',
    title: 'Insert H5P Component',
    description: 'Quickly open the "Insert Stuff" H5P dialog',
    async action(editor) {
      const mo = new MutationObserver(() => {
        const frame = document.querySelector('iframe[src="/d2l/tools/blank.html"]');
        if (!frame) return;

        frame.onload = () => {
          frame.contentDocument.querySelector('a[title="H5P"]')?.click();
          mo.disconnect();
        };
      });

      mo.observe(document.body, { childList: true });
      editor.execCommand('d2l-isf');
    },
    display(store) {
      return store.editor.ShowInsertStuffPowerTools;
    },
  },
  'wrap-videos': {
    key: 'wrap-videos-with-edit-space',
    title: 'Add Space Around Videos',
    description: 'Automatically add whitespace around videos that have no editable areas already.',
    async action(editor) {
      let affected = 0;
      editor.dom.doc.querySelectorAll('.video-wrapper').forEach(vid => {
        if (vid.previousElementSibling.matches('p') || vid.previousElementSibling.querySelector('p')) return;
        const spacer = document.createElement('p');
        spacer.innerHTML = `<br data-mce-bogus="1" />`;
        vid.insertAdjacentElement('beforebegin', spacer);
        affected++;
      });
      if (affected == 0) alert(`No videos were found in need of spacing.`);
      else alert(`Spacing was added above ${affected} video${plural(affected, 'video', 'videos')}`);
    },
    display() {
      return true;
    },
  },
  'make-image-hyperlink': {
    key: 'image-hyperlink',
    title: editor => `${editor.selection.getNode()?.classList.contains('open-externally') ? 'Remove l' : 'L'}ink to image`,
    description: editor => {
      const removing = editor.selection.getNode()?.classList.contains('open-externally');
      if (removing) {
        return 'Removes the link so nothing happens when the image is clicked';
      } else {
        return 'Adds a link to this image so the image is opened full size in a new tab';
      }
    },
    async action(editor) {
      const elem = editor.selection.getNode();

      const nearestAnchor = elem.closest('a[href]');
      if (
        nearestAnchor &&
        !confirm(
          `Setting this image to open in a new tab will remove the existing link to ${nearestAnchor.href}. Would you like to do this?`
        )
      ) {
        // The image has an anchor parent somewhere, and they don't want to remove it.
        return;
      } else {
        // If there's an anchor parent, replace it with its children (ie. remove the node).
        nearestAnchor?.replaceWith(...nearestAnchor.children);
      }

      elem.classList.toggle('open-externally');
      document.dispatchEvent(new Event('domchange'));
    },
    display: () => true,
    targetNode: ['img'],
  },
  'link-data': {
    key: 'accessible-links',
    title: editor => `${/\(.*\)$/i.test(editor.selection.getNode()?.innerText.trim()) ? 'Remove' : 'Add'} link data`,
    description: 'Toggle accessibilty text at the end of a link, eg. "(pdf, 250kb)"',
    async action(editor) {
      const elem = editor.selection.getNode();

      if (/\(.*\)$/i.test(elem.innerText)) {
        // Remove the text
        const [, name] = elem.innerText.match(/(.*)\s+?\(.*\)$/i);
        elem.innerText = name;
      } else {
        // Add the text
        let url = elem.href;

        if (elem.href.includes('coursefile-')) {
          const [, hash] = url.match(/coursefile-([^_]+)/i) ?? [];
          if (hash) {
            url = atob(hash);

            elem.href = url;
            elem.dataset.mceHref = url;
          }
        }

        const response = await fetch(url, { method: 'head' });
        const type = new URL(response.url).pathname.split('.').pop().toLowerCase();
        const size = humanReadableFileSize(response.headers.get('content-length'), true, 1);

        elem.innerText += ` (${type}, ${size})`;
      }

      document.dispatchEvent(new Event('domchange'));
    },
    display: () => true,
    targetNode: ['a[href*="/content/enforced/"]', 'a[href*="coursefile-"]'],
  },
  'use-default-banner': {
    key: 'use-default-banner',
    title: 'Use Default Banner',
    description: 'Revert back to using the default unit banner',
    async action(editor) {
      const banner = editor.dom.doc.querySelector('.banner-img');
      const img = banner.querySelector('img');

      const { Path } = await coursedata;

      banner.hidden = false;
      img.src = `${Path}images/banners/default/unit-banner.jpg`;
      img.dataset.mceSrc = `${Path}images/banners/default/unit-banner.jpg`;

      document.dispatchEvent(new Event('domchange'));
    },
    display: async (store, editor) => {
      /**
       * if (editor.dom.doc.querySelector('.banner-img:not([hidden]) img[src*="images/banners/default/unit-banner.jpg"]')) {
       *   return false;
       * }
       */

      const { Path } = await coursedata;
      try {
        const res = await fetch(`${Path}images/banners/default/unit-banner.jpg`, { method: 'head' });
        return res.ok;
      } catch (e) {
        return false;
      }
    },
  },
  'toggle-active-tab': {
    key: 'toggle-active-tab',
    title: editor =>
      editor.selection.getNode().classList.contains('active') ? 'Remove default tab selection' : 'Use as default tab',
    description: 'Change or remove the active tab of the selected tab group',
    async action(editor) {
      const node = editor.selection.getNode();
      if (node.classList.contains('active')) {
        node.classList.remove('active');
        node
          .closest('.tabs-wrapper')
          .querySelector('.tab-content')
          .querySelectorAll(':scope > :is(.show,.active)')
          .forEach(el => el.classList.remove('show', 'active'));
      } else {
        node.closest('[role="tablist"]').querySelector('.active')?.classList.remove('active');
        node.classList.add('active');

        const indexOfNewTab = Array.from(node.parentNode.children).indexOf(node);
        const tabContentContainer = node.closest('.tabs-wrapper').querySelector('.tab-content');

        tabContentContainer
          .querySelectorAll(':scope > :is(.show,.active)')
          .forEach(el => el.classList.remove('show', 'active'));

        tabContentContainer.children[indexOfNewTab].classList.add('show', 'active');
      }

      document.dispatchEvent(new Event('domchange'));
    },
    display: () => true,
    targetNode: ['.tabs-wrapper [role="tablist"] a'],
  },
};

/** Categories */
export default [
  {
    uuid: 'b21828be-133b-4a9a-ae22-c48a8cae4691',
    label: 'Page Options',
    children: [
      components['toggle-banner-visibility'],
      components['browse-page-image'],
      components['use-default-banner'],
      components['wrap-videos'],
    ],
    collapsed: [base['strip-tags'], base['links-in-new-tab'], base.formatmarks],
  },
  {
    uuid: 'af4eb64a-b5c3-4987-aa25-c011723b1950',
    label: 'Edit Selected',
    children: [
      components['toggle-lead'],
      components['toggle-lg-ol'],
      components['make-image-hyperlink'],
      base['edit-class'],
      components['toggle-download-style'],
      components['link-data'],
      components['toggle-active-tab'],
    ],
  },
];

export const insertComponents = [components['insert-h5p'], components['insert-echo360'], components['insert-stuff']];
