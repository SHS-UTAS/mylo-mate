const search = Object.fromEntries(Array.from(new URL(window.location.href).searchParams));
const container = window.top.document.querySelector('iframe[src="/d2l/tools/blank.html"]').closest('.d2l-dialog');

container.style.transition = 'min-width 300ms, min-height 300ms';
container.style.minWidth = 0;
container.style.minHeight = 0;

if (search.itemSourceKey == 'D2L.Ext.RemotePlugins.LtiCimIsf_b59feff9-66e0-4f60-828e-922a72777551') {
  // H5P object; make it big!
  container.style.minWidth = '90vw';
  container.style.minHeight = '90vh';
} else {
  // Not H5P object; make it normal sized again!
  container.style.minWidth = 0;
  container.style.minHeight = 0;
}
