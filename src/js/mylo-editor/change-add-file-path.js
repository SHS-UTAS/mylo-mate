(async () => {
  const form = document.querySelector('form[action*="main.d2l"][action*="area=mycomputer"]');
  if (!form) return;

  const path = window.top.document.querySelector('input[name="FilePath"]').value;
  const pathSelector = UI.GetControl('PS_location');
  pathSelector.SetPath(path);
})();
