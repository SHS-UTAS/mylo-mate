(async function () {
  const container = document.querySelector('input[type="hidden"][name="topicIsHidden"]').parentElement;

  const notice = document.createElement('span');
  notice.classList.add('draft-label');
  notice.innerText = ' (draft)';
  container.append(notice);
})();
