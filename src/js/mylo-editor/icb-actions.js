export default [
  {
    key: 'toggle-underrule',
    title: 'Toggle Underrule',
    description: 'Toggle the underrule of the currently selected element',
    action(editor) {
      const node = editor.selection.getNode();

      // Toggles the existence of the class 'page-header' on an element, which will toggle the under-rule of an element.
      if (editor.dom.hasClass(node, 'page-header')) {
        editor.dom.removeClass(node, 'page-header');
      } else {
        editor.dom.addClass(node, 'page-header');
      }
    },
    display() {
      return true;
    },
  },
  {
    key: 'toggletooltip',
    title: 'Create Hover Text',
    description: 'Create or remove hover text from selected text.',
    action(editor) {},
    display(store) {
      return false;
    },
  },
  {
    key: 'legacy-echo-embed',
    title: 'Embed Echo360 (Legacy)',
    description: 'Embed an Echo360 video with its Public Link',
    action(editor) {
      const path = prompt('Please provide an Echo360 Public Link');
      if (!/echo360\.(org|net)\.au\/media\/[^/]+\/public/i.test(path)) {
        alert('Please ensure you have provided an Echo360 public link.');
      } else {
        const code = `<div class=\"video-youtube\"><!-- 16:9 aspect ratio --><div class=\"embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"${path}?autoplay=false&automute=false\"allowfullscreen=\"\"></iframe></div><p class=\"caption\"><a href=\"${path}\" target=\"_blank\">Visit in new window</a> </p></div>`;
        document.dispatchEvent(new CustomEvent('insert-code', { detail: code }));
      }
    },
    display(store) {
      return store.other.ShowEcho360LegacyEmbed;
    },
  },
];
