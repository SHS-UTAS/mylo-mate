/**
 * @affects Content Editor
 * @problem Migrating content from other templates to the D2L templates system is a tedious and time-consuming process of copying and pasting content.
 * @solution Add a button that appears when editing web pages, which, when clicked, automatically converts the page from the old template to the D2L template.
 * @target Academic and support staff
 * @impact High
 * @impact 1 hour per page
 */

(async function () {
  await WaitForTinyMCE();
  const doc = tinymce.activeEditor.dom.doc;
  const orgId = window.location.pathname.match(/\/content\/([\d]+)/)[1];
  const config = await FetchMMSettings();

  /** Add the "Use D2L Basic Template" button */
  const basicTemplateSrc = '/shared/content-template/v3.0/pages/Basic%20Page.html';
  async function GetUnitSettings(id, asURLData = false) {
    const dom = new DOMParser().parseFromString(
      await fetch(`/d2l/le/content/${id}/settings/Open`).then(d => d.text()),
      'text/html'
    );
    const form = dom.querySelector('form');
    const fd = new URLSearchParams(new FormData(form));
    if (asURLData) return fd;

    const items = {};
    for (const [k, v] of fd) {
      items[k] = v;
    }
    return items;
  }

  async function SaveUnitSettings(id, changes) {
    const fd = await GetUnitSettings(id, true);
    for (const [k, v] of Object.entries(changes)) {
      fd.set(k, v);
    }

    fd.append('requestId', Date.now() % 10);
    fd.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
    fd.append('isXhr', true);

    return fetch(`/d2l/le/content/${id}/settings/Save`, { method: 'post', body: fd });
  }

  const useD2Lbutton = document.createElement('button');
  useD2Lbutton.classList.add('d2l-button');
  useD2Lbutton.setAttribute('mm-icon-type', 'action-select');
  useD2Lbutton.setAttribute('mm-icon-offset', '-6:0');
  useD2Lbutton.innerText = 'Change to D2L Template';
  useD2Lbutton.title = 'Clean content and Apply D2L Template';
  useD2Lbutton.hidden = true;

  // If we're already using a D2L template in the content, we don't want this here.
  setInterval(() => {
    useD2Lbutton.toggleAttribute('hidden', doc.querySelector('[mm-template-style="d2l"]') != null);
  }, 500);

  useD2Lbutton.addEventListener('click', async e => {
    const settings = await GetUnitSettings(orgId);
    const D2LIsDefaultTemplate = /^\/shared\/content-template\/[^\/]+\/pages\/$/i.test(settings.defaultTemplatePathSelector);
    const message = `The D2L Template is not the default templating system for this unit. Select OK to change the default to D2L Templates for this unit and apply the default template to this page.`;

    if (!D2LIsDefaultTemplate) {
      if (!confirm(message)) return;
      SaveUnitSettings(orgId, {
        defaultTemplateIsEnabled: '1',
        defaultTemplatePathSelector: '/shared/content-template/v3.0/pages/',
      });
    }

    const res = await fetch(basicTemplateSrc);
    if (res.status == 200) {
      const page = new DOMParser().parseFromString(await res.text(), 'text/html');
      const body = tinymce.activeEditor.getBody();

      const saved = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'hr', 'p', 'table', 'img', 'iframe', 'a'];
      const unwrap = elem => elem.replaceWith(...elem.childNodes);

      const ICBTemplates = ['v3-at/custom', 'icb.utas.edu.au'];

      if (tinymce.activeEditor.dom.doc.head.querySelector(ICBTemplates.map(v => `link[href*="${v}"]`).join(',')) != null) {
        const root = body.querySelector('#main') || body;

        // If we can't find main, just grab the body element itself so we have something to work with.
        const elem = root.cloneNode(true);

        /**
         * Unwrap columns and rows to flatten the tree.
         * Especially useful in the case of a multitude of column containers.
         */
        try {
          elem.querySelectorAll('[class*="col-"]').forEach(container => {
            container.querySelectorAll('.row').forEach(unwrap);
            unwrap(container);
          });
        } catch (e) {
          console.error('Error unwrapping columns and rows:', e);
        }

        /** Find the first heading element, and if it's the first element on the page, use it as the page heading. */
        try {
          const firstChild = elem.querySelector(':scope > h1,:scope > h2,:scope > h3,:scope > h4,:scope > h5,:scope > h6');
          if (firstChild && firstChild.previousElementSibling == null) {
            page.querySelector('[block-identifier="header-block"]').innerText = firstChild.innerText;
            firstChild.remove();
          } else {
            page.querySelector('[block-identifier="header-block"]').innerText = body.querySelector('#header h2').innerText;
          }
        } catch (e) {
          console.error('Error finding the first heading:', e);
        }

        /** Find floating image containers, and process them into the D2L format. */
        try {
          elem.querySelectorAll('.img-left,.img-right,.img-center').forEach(el => {
            const img = el.querySelector('img');
            if (!img) {
              console.log(el);
              return;
            }
            const caption = el.querySelector('.caption');
            const className = el.classList.contains('img-left')
              ? ' class="float-left"'
              : el.classList.contains('img-right')
              ? ' class="float-right"'
              : '';
            el.outerHTML = `<figure${className}><p><img src="${img.src}" alt="${img.alt}" /></p><figcaption>${
              caption ? caption.innerHTML : ''
            }</figcaption>`;
          });
        } catch (e) {
          console.error('Error converting image containers:', e);
        }

        /** Find the tab panels, and convert to D2L format. */
        try {
          elem.querySelectorAll('.dit-tab-panel').forEach(tabpanel => {
            const keys = Array.from(tabpanel.querySelectorAll('ul.nav-tabs a[href]'));

            const title = tabpanel.querySelector('.panel-title');
            if (title) title.classList.remove('panel-title');

            const desc = tabpanel.querySelector('.panel-body');

            tabpanel.outerHTML = `
            ${title ? title.outerHTML : ''}
            ${desc ? desc.innerHTML : ''}
            <div class="tabs-wrapper tabs-horizontal"> 
              <div class="row"> 
                <div class="col-6 col-md-12"> 
                  <div class="list-group flex-md-row text-center" role="tablist"> 
                    ${keys
                      .map(
                        (a, i) =>
                          `<a class="list-group-item list-group-item-action${
                            i == 0 ? ' active' : ''
                          }" data-toggle="list" role="tab">${a.innerText}</a>`
                      )
                      .join('\n')}
                  </div> 
                </div> 
                <div class="col-6 col-md-12"> 
                  <div class="tab-content"> 
                    ${keys
                      .map(
                        (a, i) =>
                          `<div class="tab-pane fade${i == 0 ? ' show active' : ''}" role="tabpanel" tabindex="0">${
                            tabpanel.querySelector(`div[id="${a.getAttribute('href').substr(1)}"]`).innerHTML
                          }</div>`
                      )
                      .join('\n')}
                  </div> 
                </div> 
              </div> 
            </div>`;
          });
        } catch (e) {
          console.error('Error converting tab panels:', e);
        }

        /** Convert attention panels to jumbotrons */
        try {
          elem.querySelectorAll('div.callout-success,div.callout-danger').forEach(panel => {
            const title = panel.querySelector('h4,h3,h2,h1,h5,h6');
            const component = panel.querySelector('.media-body');

            panel.outerHTML = `
            <div class="jumbotron">
              <h2>${title.innerText}</h2>
              ${component.innerHTML}
            </div>
            `;
          });
        } catch (e) {
          /* handle error */
          console.error('Error converting attention panels to jumbotrons:', e);
        }

        /** Find callout and accordion panels, and convert appropriately. */
        try {
          elem.querySelectorAll('div.panel-info,div.panel-warning').forEach(panel => {
            if (panel.querySelector('.panel-collapse')) {
              // This is an accordion
              panel.outerHTML = `
              <p>&nbsp;</p>
              <div class="accordion"> 
                <div class="card"> 
                  <div class="card-header"> 
                    <h2 class="card-title">${panel
                      .querySelector('.panel-title')
                      .innerText.replace(/\(collapsed?\)/i, '')
                      .trim()}</h2> 
                  </div> 
                  <div class="collapse"> 
                    <div class="card-body"> 
                    ${panel.querySelector('.panel-body').innerHTML}
                    </div> 
                  </div> 
                </div> 
              </div>
              <p>&nbsp;</p>
                `;
            } else {
              // This is just a callout.
              panel.outerHTML = `
              <p>&nbsp;</p>
              <div class="card standard"> 
                <div class="card-body"> 
                <h5>${panel.querySelector('.panel-heading').innerText}</h5> 
                  <div class="card-text"> 
                    ${panel.querySelector('.panel-body').innerHTML}
                  </div> 
                </div> 
              </div>
              <p>&nbsp;</p>
                `;
            }
          });
        } catch (e) {
          console.error('Error converting panels and accordions:', e);
        }

        /** Convert icon panels, and attempt to find an appropriately matching icon. */
        try {
          const iconEquiv = {
            'panel-learningOutcomes': undefined,
            'panel-reading': 'fa-book-reader',
            'panel-assessmentItem': 'fa-check-square',
            'panel-discussion': 'fa-comments',
            'panel-reflection': 'fa-lightbulb',
            'panel-question': undefined,
            'panel-lecture': 'fa-play-circle',
            'panel-survey': undefined,
            'panel-activity': undefined,
            'panel-quiz': 'fa-question-circle',
            'panel-web': 'fa-globe',
            'panel-calendar': 'fa-exclamation-circle',
            'panel-experiment': 'fa-hand-paper',
            'panel-thoughtTree': undefined,
            'panel-caseStudy': undefined,
            'panel-collaboration': 'fa-users',
            'panel-portfolio': undefined,
            'panel-pebble': undefined,
            'panel-etivity': 'fa-globe',
            'panel-listen': 'fa-play-circle',
            'panel-overview': undefined,
            'panel-key_points': undefined,
            'panel-atsi': undefined,
            'panel-concept': undefined,
            'panel-checklist': undefined,
          };

          elem.querySelectorAll('div.panel').forEach(panel => {
            if (panel.matches('[class*="dit"]') || panel.closest('[class*="dit"]')) return;
            const type = Array.from(panel.classList.values()).find(val => val.startsWith('panel-'));

            const childWrapper = document.createElement('div');
            panel.querySelectorAll(':scope > .panel-body .media, :scope > .panel-body').forEach(el => {
              el.querySelectorAll('.media-object').forEach(e => e.remove());
              const children = Array.from(el.childNodes).map(node => {
                if (node.nodeName == '#text') {
                  const p = document.createElement('p');
                  p.append(node);
                  return p;
                } else return node;
              });

              childWrapper.append(...children);
            });

            panel.outerHTML = `
            <div class="card card-graphic callout-card">
              <div class="card-body">
                <div class="card-icon">
                  <p><span class="fas ${iconEquiv[type] || 'fa-book-reader'} callout-icon"></span></p>
                </div>
                <div class="card-text">
                  <h5>${panel.querySelector(':scope > .panel-heading')?.innerText ?? 'Callout Heading'}</h5>
                  ${childWrapper.innerHTML}
                </div>
              </div>
            </div>`;
          });
        } catch (e) {
          console.error('Error converting activity callout panels:', e);
        }

        /** Replace spacers with clearfix components. */
        try {
          elem.querySelectorAll('.dit-spacer').forEach(el => (el.outerHTML = '<p class="clearfix clear-fix"><br></p>'));
        } catch (e) {
          console.error('Error replacing ICB spacers:', e);
        }

        /** Find elements that we know don't have an analogue at the moment, and just replace them with plain text. */
        try {
          const elems = elem.querySelectorAll('.dit-tab-panel,.dit-drag-drop,.dit-sorting-with-feedback');
          if (
            elems.length > 0 &&
            !confirm(
              'This page uses ICB interactive objects that are not supported in the D2L Templating system. You may consider creating an equivalent activity using H5P. Select OK to continue and remove the interactive object.'
            )
          ) {
            return;
          } else {
            elems.forEach(parent => (parent.outerHTML = `<p>${parent.innerText}</p>`));
          }
        } catch (e) {
          console.error('Error converting custom ICB interactive elements:', e);
        }

        /** Convert the sortable panels to something a bit neater. */
        try {
          elem.querySelectorAll('.dit-sortable').forEach(panel => {
            panel.replaceWith(panel.querySelector('.panel-title'), panel.querySelector('.dit-sortable'));
          });
        } catch (e) {
          console.error('Error converting sortable panels:', e);
        }

        /** Convert the blockquotes to the standardized format of the D2L template. */
        try {
          elem.querySelectorAll('blockquote').forEach(el => {
            const quote = document.createElement('blockquote');
            const text = document.createElement('p');
            const citation = document.createElement('footer');

            text.innerHTML = el.innerHTML;
            if (el.nextElementSibling?.matches('cite')) {
              citation.append(el.nextElementSibling);
	    } else if (el.nextElementSibling?.querySelector('cite')) {
              citation.append(el.nextElementSibling.querySelector('cite'));
            } else {
              let cite = document.createElement('cite');
              cite.innerText = 'Citation: Source title';
              citation.append(cite);
            }

            quote.append(text, citation);
            el.replaceWith(quote);
          });
        } catch (e) {
          console.error('Error converting blockquotes:', e);
        }

        /** Remove any ID attributes, as they shouldn't be used in this template. */
        try {
          elem.querySelectorAll('[id]').forEach(node => node.removeAttribute('id'));
        } catch (e) {
          console.error('Error removing IDs:', e);
        }

        /** Remove style attributes (but save and underlines!) */
        try {
          elem.querySelectorAll('[style]:not([style*="text-decoration"])').forEach(node => node.removeAttribute('style'));
          elem.querySelectorAll('[style*="text-decoration"]').forEach(node => {
            const decoration = node.style.textDecoration;
            node.removeAttribute('style');
            node.style.textDecoration = decoration;
          });
        } catch (e) {
          console.error('Error processing style attributes:', e);
        }

        /** Find any p or div that just contains a <br>, and strip them. */
        try {
          elem.querySelectorAll('p,div').forEach(node => {
            if (node.innerText.trim().length == 0 && node.innerHTML == '<br>' && !node.classList.contains('clearfix'))
              node.remove();
          });
        } catch (e) {
          console.error('Error removing empty linebreaks:', e);
        }

        /** Strip the footer components out as best we can. */
        try {
          elem.querySelectorAll('a[href="mailto:digital.team@utas.edu.au"]').forEach(mailto => {
            const container = mailto.closest('div.row,div[class*="col-"],#footer,p,div');
            if (container) container.remove();
          });
          elem
            .querySelectorAll(
              'img[src="https://health.utas.edu.au/templates/v3-at/common/img/logos/UTAS_colour_domestic.png"]'
            )
            .forEach(logo => {
              const container = logo.closest('div.row,div[class*="col-"],#footer,p,div');
              if (container) container.remove();
            });
          if (elem.lastElementChild && elem.lastElementChild.matches('hr')) elem.lastElementChild.remove();
        } catch (e) {
          console.error('Error attempting to remove the footer elements:', e);
        }

        /**
         * By default, the ICB float images had a max-width of 40% on the container.
         * The D2L templates don't have this, so some images look massive in comparison.
         * So lets automatically make them 40% of the container (in px, otherwise their container doesn't style correctly).
         */
        try {
          const width = `${Math.round(root.scrollWidth * 0.4)}px`;
          elem.querySelectorAll('.float-left img, .float-right img').forEach(img => (img.style.width = width));
        } catch (e) {
          console.error('Error resizing floating images:', e);
        }

        /**
         * Remove any containers that have the 'box-border' class.
         * This doesn't happen all that often it seems, but when it does it makes content look rather funky.
         */
        try {
          elem.querySelectorAll('.box-border').forEach(node => node.classList.remove('box-border'));
        } catch (e) {
          console.error(e);
        }

        // Put our tweaked content in our new template element, getting rid of any &nbsp;'s that appear twice or more.
        page.querySelector('[block-identifier="content-block"]').innerHTML = elem.innerHTML.replace(/(&nbsp;\s?){2,}/g, ' ');
      } else {
        // Do things if not ICB
        body.querySelectorAll(`*${saved.map(name => `:not(${name})`).join('')}`).forEach(unwrap);
        page.querySelector('[block-identifier="content-block"]').innerHTML = body.innerHTML;
      }

      tinymce.activeEditor.setContent(page.documentElement.outerHTML);
      if (config.editor.ShowConversionPrompt) {
        setTimeout(() => {
          alert(
            'The page has been automatically converted to the D2L templating system. Please review the content to ensure it is correct before saving your changes.'
          );
        }, 250);
      }
    } else {
      alert('The template could not be found.');
    }
  });

  document.querySelector('form > button, form > d2l-dropdown').insertAdjacentElement('afterend', useD2Lbutton);
})();
