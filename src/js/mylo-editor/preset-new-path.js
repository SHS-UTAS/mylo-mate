/**
 * @affects Content Editor
 * @problem Web pages are typically saved to the last place that was selected, or to the root directory of the unit. The Manage Files area thus quickly becomes difficult to maintain and organise.
 * @solution If the module doesn't have a default path set, a path is automatically generated based on the new page's module structure.
 * @target Academic and support staff
 * @impact High
 * @impact 15 minutes per unit
 */

(async function () {
  const settings = await FetchMMSettings();
  if (!settings.editor.PromptNewFilePath) return;

  const id = window.location.pathname.match(/\/content\/([\d]+)\/author/i)[1];
  const parentModule = window.location.searchParams.parentModuleId;

  let modules = new Map();
  let iterate = (node, parentId) => {
    node.Modules.forEach(module => iterate(module, node.ModuleId));
    node.ParentModuleId = parentId;
    modules.set(node.ModuleId, node);
  };
  const toc = await api(`/d2l/api/le/1.43/${id}/content/toc`, { fresh: true, first: true });
  toc.Modules.forEach(module => iterate(module, null));

  let parents = [];
  let node = modules.get(parseInt(parentModule));
  while (node != null) {
    parents.push(node);
    node = modules.get(parseInt(node.ParentModuleId));
  }

  parents = parents.reverse();

  // Only apply if the module doesn't already have a default path.
  if (/\/content\/enforced\/[^\/]+?\/$/i.test(modules.get(parseInt(parentModule)).DefaultPath)) {
    let base = parents[0].DefaultPath.match(/(\/content\/enforced\/.+?\/)/i)[1];
    let path = parents.map(({ Title }) => Title.replace(/([^a-z0-9]+)/gi, '-')).join('/');

    const result = `${base + path}/`;

    const input = document.querySelector('input[name="FilePath"]');
    input.value = result;

    const label = input.previousElementSibling;
    label.title = result;

    const changeText = label.querySelector('strong');
    changeText.innerText = result.replace(changeText.previousSibling.nodeValue, '').slice(0, -1);
  }
})();
