export default [
  {
    key: 'edit-class',
    title: 'Edit Class',
    description: 'Modify the classes of the currently selected element',
    action(editor) {
      let newClass = window.prompt(
        'Please enter new classes, separated by a space',
        editor.dom.getAttrib(editor.selection.getNode(), 'class')
      );

      // Apply the new class string to the element.
      if (newClass !== null) {
        editor.dom.setAttrib(editor.selection.getNode(), 'class', newClass);
      }
    },
    display() {
      return true;
    },
    targetNode: ['[contenteditable="true"] *'],
  },
  {
    key: 'strip-tags',
    title: 'Strip Tags',
    description: 'Remove unnecessary tags from content (e.g. span, header divs)',
    action(editor) {
      // Remove the children of any spans, and any div's
      //  called #header, #body and #footer and replace with their children.
      const blacklist = ['span:not(.fas)', 'div#header', 'div#body', 'div#footer'];

      const dom = editor.dom.doc.querySelector('[block-identifier="content-block"],#main');
      const tags = dom.querySelectorAll(blacklist.join(', '));
      tags.forEach(tag => tag.replaceWith(...tag.childNodes));

      const ignore = ['th', 'iframe[src*="padlet.com" i]'];
      dom
        .querySelectorAll(`[style]${ignore.map(el => `:not(${el})`).join('')}`)
        .forEach(node => node.removeAttribute('style'));

      dom
        .querySelectorAll(`[data-mce-style]${ignore.map(el => `:not(${el})`).join('')}`)
        .forEach(node => node.removeAttribute('data-mce-style'));
    },
    display() {
      return true;
    },
  },
  {
    key: 'links-in-new-tab',
    title: 'Open All Links in New Tab',
    description: 'Force all links in the page to open in a new tab or window',
    async action(editor) {
      // Go through and find all <a> tags, and test if its target isn't `_blank`.
      // In this case, add the target `_blank` to it!
      const e = editor.dom.doc.getElementsByTagName('a');

      // A list of rules to avoid.
      //	To be used in a `:not({{rule}})` fashion.
      const rules = [
        '[href^="#"]', // href is an anchor
        '[download]', // Has a download attr
        '[href^="mailto:"]', // Is a mailto link
        '.leavetarget', // Has a specific class
        '.pdf-link', // Isn't the PDF link in the header
        ':empty', // Make sure it's not an empty container used as an anchor point.
        '[href^="javascript"]',
        '[href^="Javascript"]',
        '[href^="function"]',
        '[href^="onclick"]',
        '[onclick]', // Skip any href that appeaers to run a javascript function.
        '#footer .caption',
      ];

      const store = await FetchMMSettings();
      const all = store.editor.allLinksInNewTab;

      let effected = 0;

      for (let i = 0; i < e.length; i++) {
        let a = e[i];

        let setTarget = !rules.some(rule => a.matches(rule)) && a.matches(`[href^="http"]:not([target="_blank" i])`);
        if (setTarget) {
          a.target = '_blank';
          a.setAttribute('data-target-by', 'mylo-mate');
          effected++;
        }

        if (a.href.includes('ezproxy')) {
          const url = a.href.split('?url=');
          const out = url[1];
          if (out && out.trim().length > 0) {
            let u = url[0] + '?url=' + decodeURIComponent(out.trim());
            a.href = u;
            a.dataset.mceHref = u;
          }
        }
      }

      if (effected == 0) {
        alert('No links needed to change to open in a new window.');
      } else {
        alert(`${effected} ${plural(effected, 'link')} changed to open in a new window.`);
      }
    },
    display() {
      return true;
      // return store?.editor?.allLinksInNewTab ?? false;
    },
  },
  {
    key: 'formatmarks',
    /** title: 'Toggle Format Marks', */
    title: editor => `${editor.dom.doc.body.matches('[mm-features*="formatmarks" i]') ? 'Hide' : 'Show'} Format Marks`,
    description: 'Toggles the markers at the beginning and ends of paragraphs.',
    async action(editor) {
      let flags = new Set((editor.dom.doc.body.getAttribute('mm-features') || '').split(',').map(v => v.trim()));
      if (flags.has(this.key)) flags.delete(this.key);
      else flags.add(this.key);

      const clean = data => Array.from(data).filter(v => v.trim().length > 0);

      editor.dom.doc.body.setAttribute('mm-features', clean(flags).join(','));
      document.dispatchEvent(new Event('domchange'));

      const settings = await FetchMMSettings();
      settings.editor.startWithTogglesOn = flags.has(this.key);
      await SaveMMSettings(settings);
    },
    display() {
      return true;
    },
    runOnStart(store, editor) {
      const addFlags = store?.editor?.startWithTogglesOn ?? false;

      let flags = new Set((editor.dom.doc.body.getAttribute('mm-features') || '').split(',').map(v => v.trim()));
      addFlags ? flags.add(this.key) : flags.delete(this.key);

      const clean = data => Array.from(data).filter(v => v.trim().length > 0);

      editor.dom.doc.body.setAttribute('mm-features', clean(flags).join(','));
      document.dispatchEvent(new Event('domchange'));
    },
  },
];
