/**
 * await WaitForTinyMCE();
 *
 * const doc = tinymce.activeEditor.dom.doc;
 * const body = doc.body.querySelector('[mm-template-style="d2l"]');
 *
 * const orgId = window.location.pathname.match(/\/content\/([\d]+)/)[1];
 * const course = await api(`/d2l/api/lp/1.26/courses/${orgId}`, { first: true });
 *
 * const src = `${course.Path}images/banners/default/unit-banner.jpg`;
 *
 * const useDefaultBanner = document.createElement('button');
 * useDefaultBanner.classList.add('d2l-button');
 * useDefaultBanner.setAttribute('mm-icon-type', 'action-select');
 * useDefaultBanner.setAttribute('mm-icon-offset', '-6:0');
 * useDefaultBanner.innerText = 'Use Default Banner';
 * useDefaultBanner.title = 'Change to default unit banner image';
 * useDefaultBanner.hidden = true;
 *
 * document.querySelector('form > button, form > d2l-dropdown').insertAdjacentElement('afterend', useDefaultBanner);
 *
 * document.addEventListener('domchange', async e => {
 *   const result = await fetch(src);
 *
 *   useDefaultBanner.hidden = !(
 *     result.ok &&
 *     body?.querySelector('.banner-img:not([hidden])') &&
 *     !body?.querySelector('.banner-img:not([hidden]) img')?.src?.includes('images/banners/default/unit-banner.jpg')
 *   );
 * });
 *
 * useDefaultBanner.addEventListener('click', e => {
 *   const img = body.querySelector('.banner-img img');
 *   img.src = src;
 *   img.setAttribute('data-mce-src', src);
 *   document.dispatchEvent(new Event('domchange'));
 * });
 */
