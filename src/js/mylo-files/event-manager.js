(async function () {
  const [data] = eval(document.querySelector('input[name="d2l_controlMap"]').value)
  const currPath = document.querySelector(`input#${data.hdn_currdir[0]}`);

  const observer = new MutationObserver((mut) => {
    document.dispatchEvent(new CustomEvent('filemanager-navigate', { detail: currPath.value }));
  });

  observer.observe(currPath, { attributes: true, attributeFilter: ['value'] });
}());
