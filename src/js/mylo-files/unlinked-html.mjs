const abort = (error = 'Fail condition met') => {
  throw new Error(error);
};

// Check that we have items to append to!
//	If we do, then add our no-repeat flag so that the rest of this script will only run once.
//	If we don't, bail. We're not ready yet.
const items = document.querySelector('ul.da_a');
if (items == null) abort();

const store = await FetchMMSettings();
if (!store.other.ManageFilesFeatures) abort();

const ou = GetOrgId();
const files = new Files(ou);

const addItem = ({ id, icon, title }) => {
  const item = document.createElement('li');
  item.className = 'float_l ' + id;
  item.style.display = 'inline';
  item.setAttribute('mm-icon-type', 'action-button');
  item.setAttribute('mm-icon-offset', '4:10');
  item.innerHTML = `<d2l-button-icon icon="d2l-tier1:${icon}" title="${title}" class="checkbox"></d2l-button-icon>`;
  items.appendChild(item);
  return item;
};

// Add folder, linked and unlinked buttons.
const sniffer = addItem({ id: 'sniffer', icon: 'news', title: 'Unlinked HTML sniffer' });

const loader = document.createElement('mm-loader');

const container = document.createElement('div');
container.style.display = 'flex';
container.style.alignItems = 'center';

const modal = document.createElement('mm-dialog');
modal.toggleAttribute('preserve', true);
modal.setAttribute('width', '80%');
modal.setAttribute('heading', 'Unlinked HTML Sniffer');
modal.append(container);

document.body.append(modal);

/**
 * const list = document.createElement('ul');
 * container.append(list);
 */

const tree = document.createElement('mm-file-tree');
container.append(tree);

const toc = GetUnitModule({ OrgID: ou });

modal.toggleAttribute('open', true);
sniffer.onclick = async e => {
  container.append(loader);
  loader.remove();
};

const linkedcontent = new Set();

const CheckPath = async path => {
  const paths = new Set();

  for await (const page of files.GetFilesIterator(path, 20)) {
    await Promise.all(
      page.map(async ({ IsDirectory, Path, Type }) => {
        if (IsDirectory) {
          const children = await CheckPath(Path);
          /** console.log(Path, children); */
          paths.add([...children]);
        }
        /**
         * else {
         * }
         */
        if (!/web page/i.test(Type)) return;
        if (!/.html?$/i.test(Path)) return;
        paths.add(Path);
      })
    );
  }

  return Array.from(paths)
    .filter(v => v)
    .flat();
};

for (const topic of await toc) {
  linkedcontent.add(topic.Url);
}

const foundfiles = Array.from(await CheckPath('/')).flat();
const unlinked = foundfiles.filter(file => !linkedcontent.has(file));

/** console.log(linkedcontent, foundfiles, unlinked); */

/** console.log('🌴', foundfiles); */
/** tree.data = foundfiles; */
tree.data = unlinked;
