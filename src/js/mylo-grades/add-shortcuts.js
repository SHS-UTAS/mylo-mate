/**
 * @affects Grades
 * @problem Grade items often need individual restrictions added, needing several clicks to reach the Restrictions tab.
 * @solution Add a quicklink to Restrictions next to each Grade Item, as well as either the Calculated or Adjusted Grade item (depending on what the unit is configured to use).
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  try {
    const id = document.querySelector('a[href^="/d2l/home"]').href.split('/').pop();
    let page = new DOMParser().parseFromString(
      await fetch(`/d2l/lms/grades/admin/settings/calculation_options.d2l?ou=${id}`).then(d => d.text()),
      'text/html'
    );
    let state = ['Calculated', 'Adjusted'][page.querySelector('input[name="FinalGradeRelease"]:checked').value];

    let relLink = document.querySelector(`a[title="Edit Final ${state} Grade"]`);

    let a = document.createElement('a');
    a.classList.add('d2l-link');
    a.setAttribute('data-testid', 'final-grade-quicklink');
    a.setAttribute('mm-icon-render', 'icon');
    a.setAttribute('mm-icon-type', 'action-navigation');
    a.setAttribute('mm-icon-offset', '-5:0');
    a.innerText = '[r]';
    a.style.marginLeft = '5px';
    a.href = `/d2l/lms/grades/admin/manage/finalgrade_rests_edit.d2l?objectId=${
      relLink.getAttribute('onclick').match(/[0-9]+/)[0]
    }&ou=${id}`;
    relLink.parentElement.insertBefore(a, relLink.nextElementSibling);
  } catch (e) {
    console.error(e);
  }
})();
