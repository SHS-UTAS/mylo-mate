/**
 * @affects Grades
 * @problem Different colleges and schools have varying rules around grade visibility, and students can acquire grade information through several different MyLO tools.
 * @solution Add a dialog that appears in the top-right corner of the Grades area, indicating what is currently visible or hidden to students in that unit.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const id = document.querySelector('a[href*="/d2l/home/"]').href.split('/').pop();

  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  try {
    let html = await ajax(`/d2l/le/classlist/userprogress/0/${id}/settings/Edit`);
    let dom = new DOMParser().parseFromString(html, 'text/html');

    let flags = { checked: [], unchecked: [] };

    let reportInfo = {};

    let fs = dom.querySelectorAll('fieldset');

    const desiredCheckboxes = [
      'ProvidersSettingsData$ChecklistParam',
      'ProvidersSettingsData`4$ChecklistParam',
      'ProvidersSettingsData`7$ChecklistParam',
    ];
    desiredCheckboxes
      .map(cb => dom.querySelector(`input[type="checkbox"][id="${cb}"]`))
      .forEach(cb => {
        flags[cb.checked ? 'checked' : 'unchecked'].push(cb.parentNode.innerText);
      });

    let cbs = fs[1].querySelectorAll('input[type="checkbox"]');
    flags[cbs[0].checked ? 'checked' : 'unchecked'].push('Display box plots');
    flags[cbs[1].checked ? 'checked' : 'unchecked'].push('Display potential final grade');

    let style = document.createElement('style');
    style.innerHTML = ` .mm-dialog {
			font-size: 14px;
			max-width: 600px;
			border: thin solid grey;
			text-align: right;
			position: sticky;
			top: 240px;
			background: white;
			border-radius: 3px;
			padding: 5px 10px;
			margin-top: -58px;
			float:right;
			right:20px;
		}
		
		@media (max-width: 1270px) {
			.mm-dialog {
				max-width: unset;
				position: relative;
				top: 0;
				left: 0;
				right: 0;
				width: fit-content;
				float: right;
				margin-top:unset;
			}
		}`;
    document.head.appendChild(style);

    let alert = document.createElement('div');
    alert.classList.add('mm-dialog');
    alert.setAttribute('data-testid', 'user-progress-settings-dialog');

    document.querySelector('#d_content_inner').style.position = 'relative';
    document.querySelector('.d2l-action-buttons')?.appendChild(alert);

    if (flags.checked.length > 0) {
      alert.innerHTML += `<p style="margin:0;">The following user progress settings are <strong>visible</strong> to students:<br> ${flags.checked.join(
        '; '
      )};</p>`;
    }
    if (flags.unchecked.length > 0) {
      alert.innerHTML += `<p style="margin:0;">The following user progress settings are hidden from students:<br> ${flags.unchecked.join(
        '; '
      )};</p>`;
    }
    alert.innerHTML += `<p style="margin:0;"> <a class="d2l-link" href="/d2l/le/classlist/userprogress/0/${id}/settings/Edit">Open User Progress Settings</a> <a class="d2l-link more-info" title="More information"><d2l-icon icon="d2l-tier1:help"></d2l-icon></a></p>`;

    let dialog = document.createElement('dialog');
    dialog.innerHTML = await ajax(chrome.runtime.getURL('js/mylo-user-progress/modal-content.html'));

    let close = document.createElement('span');
    close.innerText = 'x';
    close.classList.add('dialog-close');
    close.title = 'Close dialog';
    close.addEventListener('click', e => dialog.close());

    dialog.appendChild(close);
    document.body.appendChild(dialog);

    alert.querySelector('.more-info').addEventListener('click', e => {
      dialog.showModal();
    });
  } catch (e) {
    console.warn('MyLO MATE Error', e);
  }
})();
