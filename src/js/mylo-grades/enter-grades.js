/**
 * @affects Grades
 * @problem It is easy to overlook instances where rounding will affect the letter grade awarded to a student (e.g. 69.7 PP would change to 70 CR).
 * @solution Highlight instances where grade rounding would change a students letter score, making it easy for staff to identify.
 * @target Academic and support staff
 * @impact Moderate
 */

(function () {
  if (window.EnterGradeFix) return;
  window.EnterGradeFix = true;

  chrome.storage.sync.get(null, store => {
    // document.querySelectorAll('#wrapper').forEach(w => w.style.overflow = 'visible');
    document.querySelector('d2l-table-wrapper table').style.paddingRight = '30px';

    if (document.querySelector('select[title="Results Per Page"]')) {
      let orig = document.querySelector('select[title="Results Per Page"]').parent('table');
      let clone = orig.cloneNode(true);

      let orig_wrapper = document.createElement('div');
      let row = document.querySelector('select[title="Results Per Page"]').parent('td');
      let nodes = Array.from(row.childNodes);

      row.appendChild(orig_wrapper);
      nodes.forEach(child => orig_wrapper.appendChild(child));

      orig_wrapper.style.width = 'fit-content';
      orig_wrapper.style.position = 'absolute';
      orig_wrapper.style.right = '-2vw';

      let wrapper = document.createElement('div');
      wrapper.style.position = 'sticky';
      wrapper.style.left = '2.439%';
      wrapper.style.width = '75vw';
      wrapper.style.zIndex = '5';
      wrapper.style.height = '10px';

      wrapper.appendChild(clone);
      clone.setAttribute('data-testid', 'cloned-pagination');
      clone.style.position = 'absolute';
      clone.style.right = '-22.5vw';
      clone.style.width = 'unset';
      clone.style.top = '115px';

      clone.querySelectorAll('select').forEach(sel => {
        let wrapper = document.createElement('div');
        sel.parentNode.insertBefore(wrapper, sel);
        wrapper.appendChild(sel);
        wrapper.setAttribute('mm-icon-type', 'action-select');
        wrapper.setAttribute('mm-icon-offset', '-6:0');
      });

      document.querySelector('.d2l-action-buttons').appendChild(wrapper);

      clone.querySelectorAll('select').forEach(sel =>
        sel.addEventListener('change', e => {
          let o = orig.querySelector(`select[name="${sel.name}"]`);
          o.value = sel.value;
          o.dispatchEvent(new Event('change'));
        })
      );
    }
  });

  let gradesTable = document.querySelector('d2l-table-wrapper.d_gd table');
  gradesTable.querySelectorAll('tr td:last-child').forEach(c => examineCell(c)); // Examine Final Adjusted Grade column
  gradesTable.querySelectorAll('tr td:nth-last-child(2)').forEach(c => examineCell(c)); // Examine Final Calculated Grade column
  function examineCell(cell) {
    let gradeNumber = cell.innerText.split('/').map(item => item.trim()); // Examine text values of the form "74 / 100, DN"
    if (gradeNumber.length != 2 || gradeNumber[0] == '')
      gradeNumber = [...cell.querySelectorAll('input')].map(item => item.value); // Examine multiple input boxes (spreadsheet view)
    if (gradeNumber.length >= 2) {
      const gradeLetterArray = gradeNumber[1].split(',').map(item => item.trim()); // Split string of form "100, DN"
      let gradeLetter = '';
      if (gradeLetterArray.length == 2) {
        // If count is 2 a letter grade exists
        gradeNumber[1] = gradeLetterArray[0]; // Assert new denominator
        gradeLetter = gradeLetterArray[1]; // Store letter grade
      }
      const gradePerc = (gradeNumber[0] / gradeNumber[1]) * 100; // Calculate percentage value (denominator may not be 100)
      if (
        (gradePerc >= 49.5 && gradePerc < 50) ||
        (gradePerc >= 59.5 && gradePerc < 60) ||
        (gradePerc >= 69.5 && gradePerc < 70) ||
        (gradePerc >= 79.5 && gradePerc < 80)
      ) {
        cell.classList.add('MM_roundingError'); // Apply style where rounding will cause an issue
        cell.title = 'Rounding will cause a grade change for this student';
      }
      if (
        gradeLetter != '' &&
        ((gradeLetter == 'NN' && gradePerc >= 50) ||
          (gradeLetter == 'PP' && gradePerc >= 60) ||
          (gradeLetter == 'CR' && gradePerc >= 70) ||
          (gradeLetter == 'DN' && gradePerc >= 80))
      ) {
        cell.classList.add('MM_roundingError'); // Apply style where rounding will has caused an issue (decimal points = 0)
        cell.title = 'Rounding will cause a grade change for this student';
      }
    }
  }
})();
