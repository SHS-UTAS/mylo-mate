(async function () {
  const store = await FetchMMSettings();
  if (!store.mylo.EnableAuditExport) return;
  const ou = window.location.searchParams.ou;
  const gradeItems = document.querySelectorAll('tr:not(.d_ggl1) input[name="GradesList_cb"]');

  const ToLetterScore = number => {
    if (number > 80) return 'HD';
    if (number > 70) return 'DN';
    if (number > 60) return 'CR';
    if (number > 50) return 'PP';
    if (number >= 0) return 'NN';
    return '';
  };

  /**
   * Shuffles array in place. ES6 version
   * @param {Array} a items An array containing the items.
   */
  function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  const HEIRARCHY = {
    HD: 80,
    DN: 70,
    CR: 60,
    PP: 50,
    NN: 0,
  };

  const generateExport = async (icon, loader, id) => {
    loader.hidden = false;
    icon.hidden = true;

    const [gradeitem, grades, classlist, info] = await Promise.all([
      api(`/d2l/api/le/1.46/${ou}/grades/${id}`, { first: true }),
      api(`/d2l/api/le/1.46/${ou}/grades/${id}/values/`, { fresh: true }),
      api(`/d2l/api/le/1.46/${ou}/classlist/paged/`, { fresh: true }).then(d =>
        d.filter(r => [103, 120].includes(r.RoleId))
      ),
      api(`/d2l/api/lp/1.21/enrollments/myenrollments/${ou}`, { first: true }),
    ]);

    const UnitCodes = info.OrgUnit.Name.match(/[A-Z]{3}[0-9]{3}/g);
    const Delivery = info.OrgUnit.Code.split('_')[2];

    const me = await api('/d2l/api/lp/1.28/users/whoami', { first: true });
    const workbook = new ExcelJS.Workbook();
    workbook.creator = `MyLO MATE`;
    workbook.lastModifiedBy = `${me.FirstName} ${me.LastName}`;
    workbook.modified = new Date();

    const sheet = workbook.addWorksheet();
    // sheet.name = 'page';
    sheet.name = (gradeitem.ShortName || gradeitem.Name).replace(/\W/g, '-');
    sheet.addRow(['OrgDefinedId', 'Mark', 'Scheme Symbol']);

    const tally = {};
    grades.forEach(({ User, GradeValue }) => {
      if (!classlist.find(stu => stu.OrgDefinedId == User.OrgDefinedId)) return;

      const mark = ((GradeValue?.PointsNumerator || 0) / (GradeValue?.PointsDenominator || 1)) * 100;
      const grade = GradeValue?.DisplayedGrade || ToLetterScore(mark);
      sheet.addRow([User.OrgDefinedId, mark.toFixed(1), grade]);
      (tally[grade.toUpperCase()] ||= []).push(User);
    });

    const tableCoord = 'E2';

    sheet.addTable({
      name: 'SampleSet',
      ref: tableCoord,
      headerRow: true,
      columns: [{ name: 'Grade' }, { name: 'Total' }, { name: '5% of Grades' }, { name: 'Student Numbers Sampled' }],
      rows: Object.entries(tally)
        .map(([grade, users]) => {
          shuffle(users);
          const sample = users.slice(0, Math.ceil(users.length * 0.05)).map(u => u.OrgDefinedId);
          const sampled = sample.join(', ');
          return [grade, users.length, users.length * 0.05, sampled];
        })
        .sort((a, b) => HEIRARCHY[b[0]] - HEIRARCHY[a[0]]),
    });

    sheet.columns.forEach(col => {
      col.width = col.values.reduce((orig, curr) => Math.max(orig, curr.toString().length), 0) + 2;
    });

    const blob = new Blob([await workbook.xlsx.writeBuffer()]);
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.download = `${(UnitCodes ?? []).join('-')}_${Delivery}_${sheet.name}.xlsx`;
    a.href = url;
    a.click();

    loader.hidden = true;
    icon.hidden = false;
  };

  gradeItems.forEach(cb => {
    const isNumeric = cb.closest('tr').querySelector('.d_gn').innerText == 'Numeric';
    if (!isNumeric) return;

    const cell = cb.closest('tr').querySelector('th');
    const icon = document.createElement('d2l-icon');
    icon.setAttribute('icon', 'tier1:reporting');
    icon.title = `Generate auditing report`;
    icon.style.cursor = 'pointer';

    const loader = document.createElement('mm-loader');
    loader.setAttribute('indeterminate', '');
    loader.title = 'Generating...';
    loader.style.display = 'inline-block';
    loader.style.margin = 0;
    loader.setAttribute('width', '20');
    loader.hidden = true;
    cell.append(icon, loader);

    icon.addEventListener('click', e => generateExport(icon, loader, cb.value.split('_').pop()));
  });
})();
