(async () => {
  /** Build a URL object based on the current location */
  const url = new URL(window.location.href);
  const ou = url.searchParams.get('ou');

  /** Make sure we should run this. */
  // const config = await FetchMMSettings();
  // if (!config.mylo.showGradeRenameNotice) return;

  /** Get a listing of the grades and categories from the current unit. */
  const [unit, grades, categories] = await Promise.all([
    api(`/d2l/api/lp/1.36/enrollments/myenrollments/${ou}`, { first: true }),
    api(`/d2l/api/le/1.60/${ou}/grades/`),
    api(`/d2l/api/le/1.60/${ou}/grades/categories/`),
  ]);

  const period = unit.OrgUnit.Code.split('_')[2];
  if (!period) return;
  const year = parseInt(period.substr(0, 2));
  const delivery = period.substr(2, 2);

  if (year <= 21) return;
  if (year == 22) {
    if (['S1', 'A1', 'A2'].includes(delivery)) return;
  }

  /** Get a reference of the table where the categories and categories */
  const grid = document.querySelector('div.d2l-grid-container table[type="data"]');

  /** Put a CSS stylesheet together to put in the <head> tag */
  const style = document.createElement('style');
  style.innerHTML = `
  .warning { display: inline-flex; align-items: center; margin-left: 0.8em; gap: 0.5em; }
  .warning p { margin: 0; }
  `;
  document.head.append(style);

  /** A list of grade types to ignore, and not warn if unusual */
  const IgnoredGradeTypes = ['Text', 'Calculated', 'Formula'];

  /** Find all grade items that aren't associated with a category */
  const GradesWithoutCategories = grades.filter(g => g.CategoryId === 0);

  /**
   * Identify grades that should be associated with a category.
   *
   * These are identified as any grade item that isn't an ignored type,
   *   and has any weight associated (Weight > 0)
   */
  const GradesThatNeedCategories = GradesWithoutCategories.filter(
    g => g.Weight > 0 && !IgnoredGradeTypes.includes(g.GradeType)
  );

  /** Find all the categories that are weighted; unweighted categories aren't considered for renaming warnings. */
  const CategoriesWithWeights = categories.filter(c => c.Weight > 0);

  /** Helper function to get a table row based on the type and item id */
  const GetRow = (type, id) => grid.querySelector(`tr[data-type="${type}"][data-objid="${id}"]`);

  /** Helper function to generate the warning message. */
  const CreateWarning = msg => {
    const wrapper = document.createElement('div');
    wrapper.classList.add('warning');

    /**
     * const icon = document.createElement('d2l-icon');
     * icon.setAttribute('icon', 'tier1:alarmbell');
     * icon.style.color = '';
     */

    const message = document.createElement('p');
    // message.innerText = 'Grade items are now required to be in a category matching Akari. ';
    // message.innerHTML += msg;
    message.innerHTML = msg;

    const help_icon = document.createElement('a');
    help_icon.innerHTML = `<d2l-icon icon="tier1:help"></d2l-icon>`;
    help_icon.title = 'Further Information';
    help_icon.target = '_blank';
    help_icon.style.marginLeft = '1em';
    help_icon.href = 'https://universitytasmania.sharepoint.com/sites/student-operations/SitePages/My%20Results.aspx';

    message.append(help_icon);
    wrapper.append(message);

    return wrapper;
  };

  /**
   * For every grade item that needs to be associated with a category,
   *  - get the associated row
   *  - build a URL directing to the "Create a new Category" page
   *  - build a URL directing to the "Edit this grade item" page
   *  - create the warning message, including links to the above two pages
   *  - append it to the end of the table cell with the grade item url
   */
  for (const grade of GradesThatNeedCategories) {
    const row = GetRow('item', grade.Id);
    const createHref = `/d2l/lms/grades/admin/manage/category_props_newedit.d2l?objectType=9&ou=${ou}`;
    const editHref = `/d2l/lms/grades/admin/manage/item_props_newedit.d2l?objectId=${grade.Id}&ou=${ou}`;

    const icon = CreateWarning(
      `All grade items must sit within a <strong>grade category</strong> to be compatible with 
			<a class="d2l-link" href="https://universitytasmania.sharepoint.com/sites/student-operations/SitePages/My%20Results.aspx">MyResults</a>, even if there is only one item in the category. 
			
			Select New > Category to <a href="${createHref}" class="d2l-link">create a new category</a>, or edit the grade item to assign it to an existing category. Further information 
			about MyLO grades settings and naming conventions are available on the <a class="d2l-link" href="https://universitytasmania.sharepoint.com/sites/student-operations/SitePages/Getting-your-unit-ready-for-MyResults.aspx">Getting your unit ready for MyResults</a> Intranet page.`
    );
    row.querySelector('th').append(icon);
  }

  /**
   * For every grade item that needs to be associated with a category,
   *  - get the associated row
   *  - build a URL directing to the "Edit this category" page
   *  - build the warning message, including links to the above page
   *  - test to see if the name passes the Regular Expression test for a suitable name
   *      - if yes, append it to the end of the table cell with the grade item url
   *
   *  Minimum name requirement is `AT[0-9]`, and should be expanded following the below pattern:
   *
   *    ^AT[0-9]$                         // Matches "AT1"
   *    ^AT[0-9]_(.*)$                    // Matches "AT2_A category name"
   *    ^AT[0-9]_(.*)_ILO(-[0-9]+)$       // Matches "AT3_I have outcomes_ILO-1" and "AT4_I have multiple outcomes_ILO-1-3-5"
   *    ^AT[0-9]_(.*)_ILO(-[0-9]+)_H?$    // Matches "AT5_I am a hurdle assessment_ILO-2_H"
   */
  for (const category of CategoriesWithWeights) {
    const name_checker = /^AT[0-9]+(_[^_]*?(_ILO(-[0-9])+)*?(_H)?)?$/gi;
    const editHref = `/d2l/lms/grades/admin/manage/category_props_newedit.d2l?objectId=${category.Id}&ou=${ou}`;

    const row = GetRow('cat', category.Id);
    const icon = CreateWarning(`
			All grade categories representing assessment tasks should be named to communicate the task order as per the unit outline. 
			This will ensure compatibility with <a class="d2l-link" href="https://universitytasmania.sharepoint.com/sites/student-operations/SitePages/My%20Results.aspx">MyResults</a>.
		 
			Edit the category name to start with the assessment task (AT) number (e.g. "<strong>AT1_Project Proposal</strong>" for a first assessment task).
			Further information about MyLO grades settings and naming conventions are available on the <a class="d2l-link" href="https://universitytasmania.sharepoint.com/sites/student-operations/SitePages/Getting-your-unit-ready-for-MyResults.aspx">Getting your unit ready for MyResults</a> Intranet page.
		`);

    if (!name_checker.test(category.Name.trim())) {
      row.querySelector('th').append(icon);
    }
  }
})();
