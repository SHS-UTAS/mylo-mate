/**
 * @affects Grades
 * @problem It is difficult to find the assignment, discussion or quiz item associated with a grade item.
 * @solution Add a link to the assignment, discussion or quiz item that the grade item is associated with.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const storage = await FetchMMSettings();
  const id = window.location.searchParams.ou;
  const [types, grades] = await Promise.all([
    api(`/d2l/api/lp/1.21/tools/orgUnits/${id}`),
    api(`/d2l/api/le/1.29/${id}/grades/`),
  ]);

  const ACTION = {
    MANAGE: window.location.href.includes('admin/manage'),
    ENTER: window.location.href.includes('admin/enter'),
  };

  const patterns = {
    Assignments: {
      url: '/d2l/lms/dropbox/admin/mark/folder_submissions_users.d2l?db={tid}&ou={id}',
      api: '/d2l/api/le/1.29/{id}/dropbox/folders/{tid}',
    },
    Discussions: {
      url: '/d2l/lms/discussions/messageLists/frame.d2l?tId={tid}&ou={id}&fId=0&threadId=0&postId=0&groupFilterOption=0',
      api: ['/d2l/api/le/1.29/{id}/discussions/forums/', '/d2l/api/le/1.29/{id}/discussions/forums/{fid}/topics/'],
    },
    Quizzes: {
      url: '/d2l/lms/quizzing/admin/mark/quiz_mark_users.d2l?qi={tid}&ou={id}',
      api: '/d2l/api/le/1.29/{id}/quizzes/{tid}',
    },
  };

  const knownItems = grades
    .filter(grade => grade.AssociatedTool != null)
    .map(grade => {
      grade.GradeType = types.find(t => t.ToolId == grade.AssociatedTool.ToolId);
      return grade;
    })
    .filter(grade => grade.GradeType != null)
    .filter(grade => Object.keys(patterns).includes(grade.GradeType.DisplayName));

  // console.log(types, knownItems);

  knownItems.forEach(item => {
    const tid = item.AssociatedTool.ToolItemId;

    const link = document.createElement('a');
    link.setAttribute('data-testid', 'grade-attached-item');
    link.setAttribute('mm-generated', true);
    link.innerText = '[a]';
    link.style.marginLeft = '5px';
    link.classList.add('d2l-link');
    link.setAttribute('data-item-id', tid);
    link.title = `Go to attached item in ${item.GradeType.DisplayName}.`;
    link.href = patterns[item.GradeType.DisplayName].url.replace(/{tid}/g, tid).replace(/{id}/g, id);

    if (ACTION.MANAGE) {
      const entry = document.querySelector(`a[onclick*="(${item.Id},"]`);
      const row = entry.parentNode;
      row.insertBefore(link, entry.nextElementSibling);
    } else if (ACTION.ENTER) {
      const entry = document.querySelector(`[data-d2l-table-sort-field="go_${item.Id}"]`);
      entry.parentNode.insertBefore(link, entry.nextElementSibling);
    }
  });

  document.querySelectorAll('a.d2l-link[onclick*="gotoNewEditItemProps"]').forEach(a => {
    const obj = parseInt(/Props\(([0-9 ]+)\)/gi.exec(a.getAttribute('onclick'))[1]);
    const title = a.innerText;

    const link = document.createElement('a');
    link.setAttribute('data-testid', 'grade-restriction');
    link.setAttribute('mm-icon-render', 'icon');
    link.setAttribute('mm-icon-type', 'action-navigation');
    link.setAttribute('mm-icon-offset', '-5:0');
    link.title = `Restrictions for ${title}`;
    link.href = `/d2l/lms/grades/admin/manage/item_rests_edit.d2l?ou=${id}&objectId=${obj}&feedback=2`;
    link.innerText = '[r]';
    link.style.marginLeft = '5px';
    link.classList.add('d2l-link');

    a.parentNode.insertBefore(link, a.nextElementSibling);
  });

  const details = await Promise.all(
    knownItems.map(async item => {
      const type = item.GradeType.DisplayName;
      let endpoint = patterns[type].api;
      const tid = item.AssociatedTool.ToolItemId;

      if (!Array.isArray(endpoint)) {
        const res = await api(endpoint.replace(/{tid}/g, tid).replace(/{id}/g, id), { first: true, fresh: true });
        res.TID = tid;
        return res;
      }
      endpoint = endpoint.map(a => a.replace(/{tid}/g, tid).replace(/{id}/g, id));
      if (type === 'Discussions') {
        const forums = await api(endpoint[0], { fresh: true });
        const topics = await Promise.all(forums.map(f => api(endpoint[1].replace(/{fid}/g, f.ForumId), { fresh: true })));
        const res = topics.reduce((v, a) => a.concat(v)).find(t => t.TopicId === tid);
        res.TID = tid;
        return res;
      }
    })
  );

  knownItems.forEach(item => {
    const tid = item.AssociatedTool.ToolItemId;
    const detail = details.find(v => v.TID === tid);
    const shortcut = document.querySelector(`a[data-item-id="${tid}"]`);
    const row = shortcut.closest('tr');

    const ItemIsHidden = detail.IsHidden || detail.IsActive === false || false;
    const GradeItemHidden = JSON.parse(row.getAttribute('data-hidden') || 'false');

    if (storage.mylo.ShowGradeVisibilityMismatch && ItemIsHidden != GradeItemHidden) {
      if (window.location.pathname.includes('admin/enter')) return;
      // There is a mismatch between the visibility states
      const wrapper = document.createElement('div');
      wrapper.style.display = 'inline';

      const icon = document.createElement('d2l-icon');
      icon.setAttribute('icon', 'tier1:grade-visible');
      icon.title = `There is a mismatch between the visibility of the grade item and the tool it is attached to. The grade item is ${
        GradeItemHidden ? 'hidden' : 'visible'
      }, and the ${item.GradeType.DisplayName} area it's attached to is ${ItemIsHidden ? 'hidden' : 'visible'}.`;
      icon.style.margin = '0 5px';

      wrapper.append(icon);
      row.querySelector('th').append(wrapper);
    }

    shortcut.title = `Attached to "${detail.Name}" in ${item.GradeType.DisplayName}.`;
  });
})();
