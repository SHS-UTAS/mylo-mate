/**
 * @affects Grades
 * @problem Staff often need an indication of when a student last accessed MyLO. This information isn't available in Grades, where it is most useful.
 * @solution Add the Last Accessed date beneath each student name in the Enter Grades tool.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const table = document.querySelector('table[summary^="This table is a list of users and their grades."]');
  const rows = table.querySelectorAll('tr:not(.d_gh)');

  const OrgID = window.location.searchParams.ou;
  const classlist = await api(`/d2l/api/le/1.40/${OrgID}/classlist/`);
  const users = classlist.reduce((o, user) => {
    o[user.Identifier] = user;
    return o;
  }, {});

  // console.log(users);
  rows.forEach(row => {
    const [, code] = /\/userprogress\/([\d]+)/i.exec(
      row.querySelector('a[href^="/d2l/le/userprogress"]').getAttribute('href')
    );
    // const [, code] = /ggs\(([0-9]+)\)/i.exec(row.querySelector('th a[onclick*="ggs" i]').getAttribute('onclick'));
    const user = users[code];

    const access = document.createElement('span');
    access.style.display = 'block';
    if (user.LastAccessed != null) {
      // eslint-disable-next-line no-undef
      const d = moment(user.LastAccessed);
      access.innerText = `Last accessed: ${d.format('DD MMMM, YYYY HH:MM')}`;
      access.title = d.fromNow();
      // access.innerText = `Last accessed: ${d.format('LT')} ${d.format('ll')} (${d.fromNow()})`;
    } else {
      access.innerText = 'Last accessed: Never';
    }
    row.querySelector('th').append(access);
  });
})();
