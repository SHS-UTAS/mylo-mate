(async function () {
  const type = 'grades';
  const titleBreak = '\u000A';
  const ou = window.location.searchParams.ou;

  document.querySelectorAll('img[title="Has Release Conditions"]').forEach(async el => {
    const id = el
      .closest('th')
      ?.querySelector('[onclick*="gotoNew" i]')
      ?.getAttribute('onclick')
      ?.match(/([0-9]+)/)[1];
    const restrictions = await getReleaseConditions(ou, type, id);
    if (!restrictions) return;

    const text = [el.title].concat(restrictions.map(v => ` - ${v}`));
    el.title = text.join(titleBreak);
  });
})();
