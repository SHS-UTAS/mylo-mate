/**
 * @affects Groups
 * @problem The bulk group enrollment tool requires groups have unique IDs, but by default they aren't unique when created in MyLO.
 * @solution Provide a tool that automatically makes group IDs unique within MyLO, ready for bulk group enrollment.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  // Temporarily removing this, as it may no longer be needed.
  return;

  const button = ({ text, icon, title = '', parent = null, classes = [], loader = false, testId = '' }) => {
    let btn = document.createElement('d2l-button-subtle');
    btn.title = title;
    btn.setAttribute('icon', icon);
    btn.setAttribute('text', text);
    btn.setAttribute('data-testid', testId);
    btn.setAttribute('mm-icon-type', 'action-button');
    btn.setAttribute('mm-icon-offset', '6:6');
    btn.classList.add(...classes);
    if (loader) {
      let l = document.createElement('mm-loader');
      l.setAttribute('width', 20);
      btn.appendChild(l);
    }
    parent.appendChild(btn);
    return btn;
  };

  if (!document.querySelector('[title="Delete selected groups and categories"]')) return;
  const parent = document.querySelector('[title="Delete selected groups and categories"]').parentNode;

  // D2L edit btn.
  let bulk_edit = button({
    text: 'Unique Group IDs',
    title: 'Makes all Group IDs unique to this unit',
    icon: 'd2l-tier1:group',
    parent,
    classes: ['mm-btn', 'mm-inc-loader'],
    loader: true,
    testId: 'unique-group-ids',
  });

  let ts = Date.now();
  let i = 0;

  // When 'Save' is clicked
  const message =
    'This action will automatically rename all the Group IDs to be completely unique. This process can be time consuming depending on how many groups need to be renamed. Please do not close or refresh this window while this process is running.\n\nDo you wish to proceed?';
  bulk_edit.addEventListener('click', async e => {
    if (confirm(message)) {
      bulk_edit.setAttribute('disabled', true);
      bulk_edit.classList.add('loading');
      let urls = Array.from(document.querySelectorAll('a[href^="group_edit.d2l?ou="]'));
      bulk_edit.querySelector('mm-loader').setAttribute('max', urls.length);

      const categoryId = window.location.searchParams.categoryId || document.querySelector('select[name="category"]').value;

      await Promise.all(
        urls.map(async (link, index) => {
          let body = new FormData();
          body.append('d2l_action', 'Update');
          body.append('d2l_actionparam', '');
          body.append('d2l_hitCode', window.localStorage.getItem('XSRF.HitCodeSeed') + ((new Date().getTime() + 1e8) % 1e8));
          body.append('categoryId', categoryId);
          body.append('d2l_rf', '');
          body.append('name', link.innerText);
          body.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
          body.append('code', `${window.location.searchParams.ou}_${ts}_${index + 1}`);

          await fetch(link.href, { method: 'POST', body });

          i++;
          bulk_edit.querySelector('mm-loader').setAttribute('value', i);
        })
      );

      bulk_edit.removeAttribute('disabled');
      bulk_edit.classList.remove('loading');
      alert(`${urls.length} group IDs made unique.`);
    }
  });
})();
