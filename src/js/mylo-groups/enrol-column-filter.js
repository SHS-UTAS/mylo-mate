(async function () {
  const url = new URL(window.location.href);
  if (!url.searchParams.has('selectedGroups')) return;

  const selected = url.searchParams.get('selectedGroups').split(',');
  const table = document.querySelector('table[type="data"]');
  const firstRow = table.querySelector('tr:not([header])');

  const colmap = [];

  for (let i = 0; i < firstRow.childElementCount; i++) {
    let id = 'header';
    if (i > 0) {
      const cb = firstRow.children[i].children[0];
      id = cb.getAttribute('onclick').match(/EnrollmentChange\(\s*([0-9]+),\s*([0-9]+)\s*\)/i)[1];
    }
    colmap.push(id);
  }

  table.querySelectorAll('tr').forEach(row => {
    Array.from(row.children).forEach((row, i) => row.setAttribute('data-col-id', colmap[i]));
  });

  // Let's take out the header for now.
  colmap.shift();

  colmap.forEach(id => {
    if (!selected.includes(id)) {
      table.querySelectorAll(`[data-col-id="${id}"]`).forEach(node => (node.style.display = 'none'));
    }
  });

  // Remove strange widths from before we had any say in things, that causes unusual overlapping of cells

  for (let el = table; el != null; el = el.closest('[style*="width" i]')) {
    el.style.width = '';
  }

  const otherWideElements = ['.d2l-action-buttons', '.d2l-page-header', 'header'];
  otherWideElements.map(el => document.querySelector(el)).forEach(el => (el.style.width = ''));
})();
