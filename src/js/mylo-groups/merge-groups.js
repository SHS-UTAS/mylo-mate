(async function () {
  const store = await FetchMMSettings();
  if (!store.other.AdvancedGroupEnhancements) return;

  const id = window.location.searchParams.ou;
  const button = ({ text, icon, title = '', parent = null, classes = [], loader = false, testId = '' }) => {
    let btn = document.createElement('d2l-button-subtle');
    btn.title = title;
    btn.setAttribute('icon', icon);
    btn.setAttribute('text', text);
    btn.setAttribute('data-testid', testId);
    btn.setAttribute('mm-icon-type', 'action-button');
    btn.setAttribute('mm-icon-offset', '6:6');
    btn.classList.add(...classes);
    if (loader) {
      let l = document.createElement('mm-loader');
      l.setAttribute('width', 20);
      btn.appendChild(l);
    }
    parent.appendChild(btn);
    return btn;
  };

  const checkbox = (text, title, checked = false) => {
    const label = document.createElement('label');
    label.innerText = text;
    label.title = title;

    const cb = document.createElement('input');
    cb.type = 'checkbox';
    cb.checked = checked;

    label.prepend(cb);
    return { label, checkbox: cb };
  };

  if (!document.querySelector('[text="Delete"]')) return;
  const parent = document.querySelector('[text="Delete"]').parentElement;
  // D2L edit btn.
  let merge = button({
    text: 'Merge Selected',
    title: 'Add all students in checked groups to a single selected group',
    icon: 'tier1:group',
    parent,
    classes: ['mm-btn', 'mm-inc-loader'],
    testId: 'merge-groups',
  });

  merge.toggleAttribute('disabled', true);
  const container = document.querySelector('.d2l-grid-container table[type="list"]');
  container.addEventListener('change', e => {
    merge.toggleAttribute('disabled', getSelectedGroups().length == 0);
  });

  const loader = document.createElement('mm-fullscreen-loading-modal');
  loader.setAttribute('text', 'Loading...');
  loader.setAttribute('indeterminate', true);
  loader.setAttribute('preserve', true);
  document.body.append(loader);

  const getSelectedGroups = () =>
    Array.from(document.querySelectorAll('input[name="gridGroups_cb"]:checked')).map(cb => cb.value.split('_')[1]);

  const dialog = document.createElement('mm-dialog');
  dialog.setAttribute('heading', 'Merge Enrolled Users from Selected Groups');
  dialog.setAttribute('preserve', true);
  dialog.setAttribute('width', '600px');
  dialog.hidden = true;
  document.body.append(dialog);

  let categories = await api(`/d2l/api/lp/1.21/${id}/groupcategories/`, { fresh: true });
  const collection = await Promise.all(
    categories.map(async category => {
      return [
        category,
        await api(`/d2l/api/lp/1.21/${id}/groupcategories/${category.GroupCategoryId}/groups/`, { fresh: true }),
      ];
    })
  );
  const classlist = await api(`/d2l/api/le/1.46/${id}/classlist/`, { fresh: true });

  const description = document.createElement('div');

  const affectedStudents = document.createElement('dl');
  affectedStudents.classList.add('list');

  const groupsmap = new Map();

  const select = document.createElement('select');
  select.classList.add('d2l-select', 'vui-select');
  collection.forEach(([category, groups]) => {
    const group = document.createElement('optgroup');
    group.label = category.Name;

    group.dataset.visibleChildren = groups.length;

    groups.forEach(child => {
      const opt = document.createElement('option');
      opt.value = child.GroupId;
      opt.innerText = child.Name;
      opt.dataset.category = category.GroupCategoryId;
      groupsmap.set(child.GroupId, { ...child, GroupCategoryId: category.GroupCategoryId });
      group.append(opt);
    });
    select.append(group);
  });

  const showallgroups = checkbox('Show all groups', 'Show all groups from all categories', false);
  const move = checkbox('Move students', 'Moves the students out of the groups instead of copying them', false);

  const wrapper = document.createElement('div');
  wrapper.classList.add('mm-group-option-flags');
  wrapper.append(showallgroups.label, move.label);

  dialog.append(description, select, wrapper, affectedStudents);

  affectedStudents.addEventListener('click', e => {
    if (e.target.matches('dt')) {
      affectedStudents.toggleAttribute('collapsed');
    }
  });

  showallgroups.checkbox.addEventListener('change', e => {
    const checked = e.target.checked;
    const selected = getSelectedGroups();
    select.querySelectorAll('optgroup').forEach(category => {
      let visible = 0;
      category.querySelectorAll('option').forEach(opt => {
        let show = checked || selected.includes(opt.value);
        opt.hidden = !show;
        if (show) visible++;
      });
      category.dataset.visibleChildren = visible;
    });
    if (select.selectedOptions[0].hidden) {
      const first = select.querySelector('option:not([hidden])');
      if (first) first.selected = true;
    }
  });

  dialog.hidden = false;
  merge.addEventListener('click', e => {
    if (getSelectedGroups().length == 0) return alert('Please check at least one group to use this tool.');

    const from = getSelectedGroups();
    if (from.length == 1) {
      showallgroups.checkbox.checked = true;
    }
    showallgroups.checkbox.dispatchEvent(new Event('change'));

    const studentsFrom = from.flatMap(id => groupsmap.get(parseInt(id)).Enrollments).filter(IsUnique);

    if (studentsFrom.length == 0) {
      return alert('There are no students to merge out. Please select at least one group with students, and try again.');
    }

    description.innerText = `Select a group to merge the students into:`;
    // description.innerText = `${studentsFrom.length} ${plural(studentsFrom.length, 'student', 'students')} from ${
    //   from.length
    // } ${plural(from.length, 'group', 'groups')} will be added to this group:`;

    affectedStudents.innerHTML = '';

    const style = document.createElement('style');
    style.innerHTML = `
    .mm-group-option-flags {
      margin-top: 10px;
    }

    .mm-group-option-flags label:not(:first-child) {
      padding-left: 10px;
    }

    optgroup[data-visible-children="0"] {
      display: none;
    }

    .student-list {
      list-style: none;
      padding: 0;
      margin: 0;
    }

    .list[collapsed] .student-list {
      display: none;
    }

    .list {
      margin-bottom: 0;
    }

    .list dt {
      cursor: pointer;
    }
    
    .list dt:before {
      display: inline-block;
      content: '- ';
      padding-right: 10px;
    }

    .list[collapsed] dt:before {
      content: '+ ';
    }
    `;

    const desc = document.createElement('dt');
    desc.innerText = 'List of students';

    const students = document.createElement('dd');
    const list = document.createElement('ul');
    list.classList.add('student-list');

    const col = studentsFrom
      .map(id => classlist.find(student => student.Identifier == id))
      .filter(v => v)
      .sort((a, b) => {
        const j = a.LastName.toLowerCase();
        const k = b.LastName.toLowerCase();
        return j < k ? -1 : j > k ? 1 : 0;
      });

    col.forEach(({ FirstName, LastName, OrgDefinedId }) => {
      const li = document.createElement('li');
      li.innerText = `${FirstName} ${LastName} (${OrgDefinedId})`;
      list.append(li);
    });

    const message = document.createElement('p');
    message.innerText = `${studentsFrom.length} ${plural(studentsFrom.length, 'student', 'students')} from ${
      from.length
    } ${plural(from.length, 'group', 'groups')} will be merged into the above group.`;

    students.append(list);
    affectedStudents.append(style, message, desc, students);
    affectedStudents.toggleAttribute('collapsed', true);

    dialog.setAttribute('open', true);
  });

  dialog.addEventListener('ok', async e => {
    merge.setAttribute('disabled', '');

    const from = getSelectedGroups();
    const to = select.selectedOptions[0].value;
    const category = select.selectedOptions[0].dataset.category;

    const studentsFrom = from.flatMap(id => groupsmap.get(parseInt(id)).Enrollments).filter(IsUnique);
    const studentsTo = groupsmap.get(parseInt(to)).Enrollments;
    const toMove = studentsFrom.filter(id => !studentsTo.includes(id)).map(id => ({ UserId: id }));

    const moving = move.checkbox.checked;

    // We subtract one from the 'from' groups count, as we know we aren't removing students from that group.
    // However, if we're only merging from a single group, we're going to end up with a '0',
    //  so we use Math.max to ensure that it's never smaller than 1.
    loader.setAttribute(
      'text',
      `${moving ? 'Moving' : 'Merging'} ${toMove.length} ${plural(toMove, 'student', 'students')} from ${Math.max(
        from.length - 1,
        1
      )} ${plural(Math.max(from.length - 1, 1), 'group', 'groups')}.`
    );

    loader.removeAttribute('indeterminate');

    loader.setAttribute(
      'max',
      toMove.length +
        (moving ? from.filter(id => id != to).flatMap(id => groupsmap.get(parseInt(id)).Enrollments).length : 0)
    );

    loader.setAttribute('open', true);

    let counter = 0;
    const afterEach = () => loader.setAttribute('value', ++counter);

    await apisend(`/d2l/api/lp/1.28/${id}/groupcategories/${category}/groups/${to}/enrollments/`, toMove, {
      asSinglePayload: false,
      afterEach,
    });

    if (moving) {
      await Promise.all(
        from.map(async groupid => {
          if (groupid == to) return; // Don't remove students from the group we're moving to.

          const group = groupsmap.get(parseInt(groupid));
          return Promise.all(
            group.Enrollments.map(UserId =>
              apisend(
                `/d2l/api/lp/1.28/${id}/groupcategories/${group.GroupCategoryId}/groups/${groupid}/enrollments/${UserId}`,
                null,
                {
                  verb: 'DELETE',
                  afterEach,
                }
              )
            )
          );
        })
      );
    }

    merge.removeAttribute('disabled');
    merge.classList.remove('loading');
    window.location.reload();
  });
})();
