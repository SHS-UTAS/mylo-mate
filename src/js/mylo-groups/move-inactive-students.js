/**
 * @affects Groups
 * @problem Inactive students often have to be removed from tutor and marking groups, a task that is tedious to accomplish manually.
 * @solution A tool that finds all students who've never accessed the MyLO unit, and generates bulk enrollment spreadsheets to move them to a designated "Inactive students" group.
 * @target Academic and support staff
 * @impact High
 * @savings 2 hours per group category
 */
(async function () {
  const store = await FetchMMSettings();

  if (!store.other.EdTechsMode || !store.other.GenerateInactiveStudentsSpreadsheet) return;
  if (!document.querySelector('select[name="category"]')) return;

  const { ou } = window.location.searchParams;
  const categoryId = window.location.searchParams.categoryId || document.querySelector('select[name="category"]').value;

  const [groupEnrolments, users, unit] = await Promise.all([
    api(`/d2l/api/lp/1.25/${ou}/groupcategories/${categoryId}/groups/`, { fresh: true }),
    api(`/d2l/api/le/1.40/${ou}/classlist/`, { fresh: true }),
    api(`/d2l/api/lp/1.25/enrollments/myenrollments/${ou}`, { first: true }),
  ]);

  let isGettingUnitCodes = true;
  const unitCodes = new Map();
  const groups = new Map();
  const enrollments = new Map();

  const button = document.createElement('button');
  button.classList.add('d2l-button');
  button.type = 'button';
  button.innerText = 'Generate "Inactive Students" spreadsheet';
  button.disabled = true;
  button.title = `Examines all students enrolled in the groups in this category, and compiles a spreadsheet for each group containing students who haven't yet accessed MyLO`;
  button.setAttribute('mm-icon-type', 'action-select');
  button.setAttribute('mm-icon-offset', '-6:0');

  const loader = document.createElement('mm-loader');
  loader.setAttribute('hidden', true);
  loader.setAttribute('indeterminate', true);
  loader.setAttribute('width', 20);
  loader.style.margin = '0px -10px -5px 10px';
  loader.style.display = 'inline-block';
  button.append(loader);

  const wrapper = document.createElement('li');
  wrapper.classList.add('d2l-action-buttons-item');

  wrapper.append(button);
  document.querySelector('.d2l-action-buttons-list').append(wrapper);

  let dialog = document.createElement('mm-dialog');
  dialog.setAttribute('heading', 'Move Inactive Students');
  dialog.setAttribute('preserve', true);
  document.body.append(dialog);

  let description = document.createElement('p');
  description.innerHTML = `Please select the group to move inactive students to.`;
  description.style.marginTop = 0;
  dialog.append(description);

  let dropdown = document.createElement('select');
  dropdown.classList.add('vui-input', 'd2l-select');

  groupEnrolments.forEach(group => {
    groups.set(group.GroupId, group);
    group.Enrollments.forEach(userId => {
      userId = userId.toString();
      if (enrollments.has(userId)) {
        let set = enrollments.get(userId);
        set.push(group.GroupId);
        enrollments.set(userId, set);
      } else {
        enrollments.set(userId, [group.GroupId]);
      }
    });

    let option = document.createElement('option');
    option.value = group.GroupId;
    option.innerText = group.Name;
    if (/inactive/i.test(group.Name)) {
      option.selected = true;
    }
    dropdown.append(option);
  });

  dialog.append(dropdown);

  button.disabled = false;
  button.addEventListener('click', e => {
    dialog.setAttribute('open', true);
  });

  let groupsToCollect = users
    .filter(user => user.LastAccessed == null) // Get the students who haven't accessed
    .map(student => {
      if (enrollments.has(student.Identifier)) {
        return enrollments.get(student.Identifier);
      }
    }) // Get the groups that they belong to
    .flat() // Flatten arrays
    .filter((v, i, a) => v && a.indexOf(v) === i); // Remove any undefined and make sure values are unique.

  const GetGroupId = id => {
    return new Promise(res => {
      MyLOLayer(`/d2l/lms/group/group_edit.d2l?ou=${ou}&groupId=${id}`, {
        load: ({ frame, dom }) => {
          unitCodes.set(id, dom.querySelector('input[name="code"]').value);
          frame.remove();
          res();
        },
      });
    });
  };

  PromisePool(
    groupsToCollect.map(id => PromisePoolItem(GetGroupId, id)),
    { concurrency: 3 }
  ).then(() => (isGettingUnitCodes = false));

  const saveCSV = (name, data) => {
    let link = document.createElement('a');
    link.setAttribute('href', encodeURI(data));
    link.setAttribute('download', name);
    link.click();
  };

  dialog.addEventListener('ok', async e => {
    button.disabled = true;
    loader.removeAttribute('hidden');
    await until(() => !isGettingUnitCodes);

    const inactive = parseInt(dropdown.value);

    if (!unitCodes.has(inactive)) {
      await GetGroupId(inactive);
    }

    const enroll_headers = `data:text/csv;charset=utf-8,ENROLL,Username,,Role Name,Org Unit Code`;
    const unenroll_headers = `data:text/csv;charset=utf-8,UNENROLL,Username,,Org Unit Code`;

    const enroll_rows = [];
    const unenroll_rows = [];

    // Get all the students who haven't accessed MyLO
    users
      .filter(user => user.LastAccessed == null)
      .forEach(student => {
        if (enrollments.has(student.Identifier)) {
          let enrolledIn = enrollments.get(student.Identifier);
          enrolledIn.forEach(groupId => {
            // If they're already in the chosen 'inactive' group, we don't want to unenroll them from there just to reenroll them again.
            // So, just stop.
            if (parseInt(groupId) == inactive) return;

            enroll_rows.push(`"ENROLL","${student.Username}",,"student","${unitCodes.get(inactive)}"`);
            unenroll_rows.push(`"UNENROLL","${student.Username}",,"${unitCodes.get(groupId)}"`);
          });
        }
      });

    if (enroll_rows.length > 0) {
      saveCSV(`${unit.OrgUnit.Name.replace(/\s/g, '_')}_ENROLL.csv`, `${enroll_headers}\r\n${enroll_rows.join('\n')}`);
    }

    if (unenroll_rows.length > 0) {
      saveCSV(`${unit.OrgUnit.Name.replace(/\s/g, '_')}_UNENROLL.csv`, `${unenroll_headers}\r\n${unenroll_rows.join('\n')}`);
    }

    button.disabled = false;
    loader.setAttribute('hidden', true);
  });
})();
