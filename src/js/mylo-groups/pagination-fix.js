(function () {
  if (window.GroupPaginationFixed) return;
  window.GroupPaginationFixed = true;

  let orig = document.querySelector('select[title="Results Per Page"]');
  if (!orig) return;

  const o = orig.parent('table.d_gact.d_gd');
  o.style.zIndex = 1;
  let clone = o.cloneNode(true);

  clone.querySelectorAll('select').forEach(sel => {
    let wrapper = document.createElement('div');
    sel.parentNode.insertBefore(wrapper, sel);
    wrapper.appendChild(sel);
    wrapper.setAttribute('mm-icon-type', 'action-select');
    wrapper.setAttribute('mm-icon-offset', '-6:0');
  });

  orig.parent('div.d2l-grid-container').insertBefore(clone, document.querySelector('d2l-table-wrapper'));
  clone.querySelectorAll('select').forEach(node =>
    node.addEventListener('change', e => {
      let o = orig.parent('table.d_gact.d_gd').querySelector(`#${node.id}`);
      o.value = node.value;
      o.dispatchEvent(new Event('change'));
    })
  );

  document.querySelector('.d2l-fitted-table').classList.remove('d2l-fitted-table');

  orig
    .parent('table.d_gact.d_gd')
    .querySelectorAll('td.d_gact')
    .forEach(td => (td.style.paddingRight = 0));
  orig.parent('table.d_gact.d_gd').style.width = 'auto';
  // orig.parent('table.d_gact.d_gd').style.float = 'right';
  orig.style.left = '1%';

  clone.querySelectorAll('td.d_gact').forEach(td => (td.style.paddingRight = 0));
  clone.style.width = 'auto';
  // clone.style.float = 'right';
  clone.style.left = '1%';
})();
