(async function () {
  //https://mylo.utas.edu.au/d2l/lms/group/group_enroll.d2l?ou=190777&categoryId=152394&groupId=325924
  const category = document.querySelector('select[name="category"]').value;
  const { ou } = window.location.searchParams;

  document.querySelectorAll('a[href^="group_edit.d2l"]').forEach(link => {
    const { groupId } = new URL(link.href).query;

    const quicklink = document.createElement('a');
    quicklink.href = `/d2l/lms/group/group_enroll.d2l?ou=${ou}&categoryId=${category}&groupId=${groupId}`;
    quicklink.classList.add('d2l-link');
    quicklink.innerText = '[e]';
    quicklink.style.marginLeft = '10px';
    quicklink.target = '_blank';
    quicklink.setAttribute('mm-icon-type', 'action-button');
    quicklink.setAttribute('mm-icon-offset', '0:0');
    quicklink.title = `Visit the "Enrol Users" page for ${link.innerText}`;
    link.insertAdjacentElement('afterend', quicklink);
  });
})();
