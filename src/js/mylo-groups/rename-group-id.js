(async function () {
	if (window.location.searchParams['mm-rename-id']) {
		document.querySelector('input[name="code"]').value = window.location.searchParams['mm-rename-id'];
		const click = () => document.querySelector('d2l-floating-buttons button[primary]').click();
		click();
		setInterval(click, 2000);
	}
})();