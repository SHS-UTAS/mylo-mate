(function () {
  const table = document.querySelector('table[summary^="List of groups in this category"]');
  let columns = table.querySelectorAll('th:not(:first-child)');
  let rows = table.querySelectorAll('tr:not(.d_gh)');

  columns.forEach((col, index) => {
    let wrapper = document.createElement('div');
    wrapper.style.display = 'inline-block';
    wrapper.style.marginRight = '10px';
    wrapper.style.marginTop = '5px';
    wrapper.setAttribute('mm-icon-type', 'action-button');
    wrapper.setAttribute('mm-icon-offset', '-7:-2');

    let cb = document.createElement('input');
    cb.type = 'checkbox';
    cb.classList.add('vui-input');
    cb.classList.add('d_chb');
    cb.title = 'Select all in column';

    wrapper.appendChild(cb);
    col.querySelector('label:nth-child(2)').appendChild(wrapper);

    cb.addEventListener('change', e => {
      table.querySelectorAll(`input[type="checkbox"][data-col="${index + 1}"]`).forEach(r => (r.checked = cb.checked));
    });
  });

  rows.forEach(row => {
    Array.from(row.children).forEach((cell, col) => {
      let cb = cell.querySelector('input[type="checkbox"]');
      if (cb) cb.setAttribute('data-col', col);
    });
  });
})();
