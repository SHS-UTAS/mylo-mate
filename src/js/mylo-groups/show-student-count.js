(async function () {
  const id = window.location.searchParams.ou;
  const root = document.querySelector('a[href*="category_newedit.d2l"]');
  if (!root) return;

  const categoryId = root.href.split('categoryId=')[1].split('&')[0];
  const category = await api(`/d2l/api/lp/1.28/${id}/groupcategories/`, { fresh: true });
  const groups = await api(`/d2l/api/lp/1.28/${id}/groupcategories/${categoryId}/groups/`, { fresh: true });
  const classlist = await api(`/d2l/api/le/1.46/${id}/classlist/`, { fresh: true }).then(d =>
    d.filter(user => user.RoleId == 103)
  );

  const restrictingId = category.find(cat => cat.GroupCategoryId == categoryId).RestrictedByOrgUnitId;

  let restrictedUserPool = null;

  let restrictions = { group: '', category: '' };

  if (restrictingId != null) {
    const restrictedCategoryGroup = category.find(cat => cat.Groups.includes(restrictingId));
    const restrictedBy = await api(
      `/d2l/api/lp/1.28/${id}/groupcategories/${restrictedCategoryGroup.GroupCategoryId}/groups/${restrictingId}`,
      { fresh: true, first: true }
    );

    restrictions = { category: restrictedCategoryGroup.Name, group: restrictedBy.Name };
    restrictedUserPool = restrictedBy.Enrollments.filter(id => {
      const cl = classlist.find(usr => usr.Identifier == id);
      return cl && cl.RoleId == 103;
    });
  }

  const enrolled = new Set(
    groups
      .flatMap(group => group.Enrollments)
      .filter(
        id =>
          classlist.find(cl => cl.Identifier == id) && (restrictedUserPool != null ? restrictedUserPool.includes(id) : true)
      )
  );

  const pool = restrictedUserPool != null ? restrictedUserPool : classlist;

  const difference = pool.length - enrolled.size;

  const text = root.nextElementSibling;
  text.setAttribute('mm-icon-type', 'action-button');
  text.setAttribute('mm-icon-offset', '0:0');

  text.innerHTML = `<span>(${groups.length} ${plural(groups.length, 'group', 'groups')} - ${
    difference > 0 ? `${difference} unenrolled` : 'all students enrolled'
  }${
    restrictedUserPool == null
      ? ''
      : ` <d2l-icon icon="tier1:release-conditions" title="This category is restricted to enrollments from '${restrictions.category}' > '${restrictions.group}'"></d2l-icon>`
  })</span>`;
  text.style.paddingLeft = '5px';
  text.title = `${groups.length} ${plural(groups.length, 'group', 'groups')} in this category, ${enrolled.size}/${
    pool.length
  } ${plural(pool.length, 'student', 'students')} enrolled; ${difference} floaters.`;
})();
