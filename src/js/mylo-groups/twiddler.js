(async () => {
  const store = await FetchMMSettings();
  if (!store.other.AdvancedGroupEnhancements) return;

  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const { ou } = window.location.searchParams;

  const button = ({ text, icon, title = '', parent = null, classes = [], testId = '' }) => {
    let btn = document.createElement('d2l-button-subtle');
    btn.title = title;
    btn.setAttribute('data-testid', testId);
    btn.setAttribute('icon', icon);
    btn.setAttribute('text', text);
    btn.setAttribute('mm-icon-type', 'action-button');
    btn.setAttribute('mm-icon-offset', '6:6');
    btn.classList.add(...classes);
    parent.append(btn);
    return btn;
  };

  const parent = document.querySelector('[text="Delete"]')?.parentElement;
  if (!parent) return;

  // Tweak btn.
  let tweakadd = button({
    text: 'Add Student View',
    title: 'Enroll View Student in all groups',
    icon: 'tier1:share-parent-filled',
    parent,
    classes: ['mm-btn'],
  });

  let tweakremove = button({
    text: 'Remove Student View',
    title: 'Unenroll View Student in all groups',
    icon: 'tier1:share-parent-hollow',
    parent,
    classes: ['mm-btn'],
  });

  const categoryId = window.location.searchParams.categoryId || document.querySelector('select[name="category"]').value;
  tweakadd.addEventListener('click', async e => {
    const users = await api(`/d2l/api/lp/1.31/enrollments/orgUnits/${ou}/users/`, { fresh: true });
    const studentview = users.find(user => user.Role.Id == 118);
    const UserId = studentview.User.Identifier;

    const { Groups } = await api(`/d2l/api/lp/1.31/${ou}/groupcategories/${categoryId}`, { fresh: true, first: true });
    await Promise.all(
      Groups.map(groupId =>
        apisend(`/d2l/api/lp/1.31/${ou}/groupcategories/${categoryId}/groups/${groupId}/enrollments/`, { UserId })
      )
    );

    window.location.reload();
  });

  tweakremove.addEventListener('click', async e => {
    const users = await api(`/d2l/api/lp/1.31/enrollments/orgUnits/${ou}/users/`, { fresh: true });
    const studentview = users.find(user => user.Role.Id == 118);
    const UserId = studentview.User.Identifier;

    const { Groups } = await api(`/d2l/api/lp/1.31/${ou}/groupcategories/${categoryId}`, { fresh: true, first: true });
    await Promise.all(
      Groups.map(groupId =>
        apisend(`/d2l/api/lp/1.31/${ou}/groupcategories/${categoryId}/groups/${groupId}/enrollments/${UserId}`, null, {
          verb: 'delete',
        })
      )
    );

    window.location.reload();
  });
})();
