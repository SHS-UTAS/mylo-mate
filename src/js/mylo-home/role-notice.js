/**
 * @affects MyLO Homepage
 * @problem When enrolled in a unit as an auditor, certain privileges are heavily restricted. A user's role isn't immediately apparent when in a unit, so it can be confusing.
 * @solution Add an alert to the Unit Homepage, notifying the user that they are an Auditor in the unit, also warning that most MyLO MATE functionality will be unavailable whilst viewing in this role.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  await until(() => document.body.getAttribute('mm-user-role') != null);

  if (/auditor/i.test(document.body.getAttribute('mm-user-role'))) {
    let notice = document.createElement('div');
    notice.classList.add('auditor-notice', 'd2l-widget', 'd2l-tile');
    notice.style.backgroundColor = 'var(--d2l-color-citrine)';
    notice.role = 'region';
    notice.innerHTML = `
      <div class="d2l-widget-header">
        <div class="d2l-homepage-header-wrapper">
          <d2l-icon icon="tier1:alert"></d2l-icon>
          <strong>MyLO MATE Notice</strong>
        </div>
        <div class="d2l-clear"></div>
      </div>
      <div class="d2l-widget-content">
        <div class="d2l-widget-content-padding">
          <p>You are an auditor in this unit. MyLO MATE functionality will be unavailable.</p>
          <p>To use MyLO MATE in this unit, please enroll as a Lecturer or Support.</p>
          <div class="d2l-clear"></div>
        </div>
      </div>
    `;
    document.querySelector('.homepage-col-8').prepend(notice);
  }
})();
