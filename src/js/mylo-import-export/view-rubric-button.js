/**
 * @affects MyLO Import/Export
 * @problem One of the most commonly imported items into MyLO are Rubrics, but after a rubric import completes there is no option to jump to the Rubrics tool.
 * @solution Add a button that links directly to the Rubrics page when an import completes successfully.
 * @target Academic and support staff
 * @impact Low
 */

(function () {
  if (window.FrameObserverAdded) return;
  window.FrameObserverAdded = true;

  const until_event = (node, event) => new Promise(res => node.addEventListener(event, res));

  let frame_observer = new MutationObserver(async () => {
    if (
      document.querySelector('.d2l-dialog iframe') &&
      !document.querySelector('.d2l-dialog iframe').contentDocument.querySelector('#mm-view-rubrics')
    ) {
      let frame = document.querySelector('.d2l-dialog iframe');
      await until_event(frame, 'load');

      let doc = frame.contentDocument;
      let orig_btn = doc.querySelector('#D2L_LE_Import_ImportCourse_ViewContentPrimaryButton');
      if (orig_btn == null) return;

      if (frame.contentWindow.mmRubricButtonAdded) return;
      else frame.contentWindow.mmRubricButtonAdded = true;

      let clone = orig_btn.cloneNode(true);
      clone.innerText = 'View Rubrics';
      clone.id = 'mm-view-rubrics';

      let wrapper = document.createElement('div');
      wrapper.style.display = 'inline-block';
      wrapper.setAttribute('mm-icon-type', 'action-button');
      wrapper.setAttribute('mm-icon-offset', '-6:14');
      wrapper.classList.add('d2l-hidden');
      wrapper.appendChild(clone);

      orig_btn.parentNode.insertBefore(wrapper, orig_btn.nextElementSibling);
      clone.addEventListener('click', () => {
        window.location = `/d2l/lp/rubrics/list.d2l?ou=${/ou=([0-9]*)/g.exec(window.location.href)[1]}`;
      });

      let btn_watcher = new MutationObserver(() => {
        wrapper.className = orig_btn.className;
        wrapper.classList.remove('d2l-button');
        clone.className = orig_btn.className;
        orig_btn.hasAttribute('disabled') ? clone.setAttribute('disabled', 'disabled') : clone.removeAttribute('disabled');
      });
      btn_watcher.observe(orig_btn, {
        attributes: true,
        attributeFilter: ['class', 'disabled'],
      });
    }
  });

  frame_observer.observe(document, {
    childList: true,
    subtree: true,
  });
})();
