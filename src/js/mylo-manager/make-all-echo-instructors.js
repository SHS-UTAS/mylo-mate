(async () => {
  const iconsheet = document.createElement('link');
  iconsheet.rel = 'stylesheet';
  iconsheet.href = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css';
  document.head.append(iconsheet);

  const hdr = document.querySelector('#grdStaff thead th:nth-last-child(2)');
  const btn = document.createElement('button');
  btn.style.border = 'thin solid #ccc';
  btn.style.padding = '0.3em 1em';
  btn.style.borderRadius = '3px';
  btn.title = 'Toggle Echo360 Instructor roles';
  btn.setAttribute('role', 'button');

  const icon = document.createElement('span');
  icon.classList.add('fas', 'fa-chalkboard-teacher');
  btn.append(icon);
  hdr.append(btn);

  const getEcho360Value = () => {
    const noneRole = document.querySelectorAll('select[name$="EchoInstructorLevel" i] option[value="0"]:checked').length;

    // If there's no users with the "None" instructor level, then everyone is an instructor.
    // As such, we want everyone to move to the None role
    if (noneRole == 0) return 0;
    // Otherwise, make everyone an instructor.
    else return 2;
  };

  btn.onclick = e => {
    const newValue = getEcho360Value();
    document.querySelectorAll('select[name$="EchoInstructorLevel" i]').forEach(sel => {
      sel.value = newValue;
    });
    e.preventDefault();
    return false;
  };
})();
