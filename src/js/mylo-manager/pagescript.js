/**
 * @affects MyLO Manager
 * @problem Users can be bulk enrolled into units but unit details need to be compiled into a specific spreadsheet format.
 * @solution Add a Bulk User Management (BUM) export tool to MyLO Manager, compiles all the unit details on screen and generates a spreadsheet in the correct format.
 * @target Support staff
 * @impact Moderate
 */

(async () => {
  // If the role is matches an entry in this array, then enable quicklinks. Otherwise, fade the bar out.
  const allowedRoles = ['Support', 'Lecturer', 'UnitCoordinator', 'Auditor', 'Tutor'];

  const container = document.createElement('div');
  container.id = 'BUMdialog';
  container.title = 'Export Bulk User Management (BUM) CSV';
  container.innerHTML = `
<form>
  <fieldset>
    <p>Please enter the usernames you'd like to include, and the role you'd like them to have (comma separated).</p>
    <p>
      <label for="names">Usernames:</label>
      <input type="text" name="names" id="BUMnames" value="" class="text ui-widget-content ui-corner-all" /> 
        
      <select id="BUMrole" role="listbox">
        <option role="option" value="Support" selected>Support</option>
        <option role="option" value="Lecturer">Lecturer</option>
      </select>

      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </p>
    <p>
      <strong>Note</strong> only the rows on the current page will be included.
    </p>
    <p>
      <a id="exportcsv" style="display:none" href="javascript:void()">Export CSV</a>
    </p>
  </fieldset>
</form>`;

  const names = container.querySelector('#BUMnames');
  const roles = container.querySelector('#BUMrole');

  document.body.append(container);

  // Handler for the Bulk User Management (BUM) Tool dialog panel.
  const dialog = $(container).dialog({
    autoOpen: false,
    height: 250,
    width: 400,
    modal: true,
    buttons: {
      Close: function () {
        dialog.dialog('close');
      },
    },
  });

  /**
   * Launched by the #BUMtool button in MyLO Manager. Exports a CSV of units on the page, along with user IDs.
   */
  function doBUM() {
    /** The raw input value */
    const input = names.value.trim();

    // Takes in the comma-separated usernames, and splits and trims then into an array.
    const users = input
      .split(',')
      .map(item => item.trim())
      .filter(v => v.length > 0);

    // If there are users and the Bulk User Management (BUM) names field isn't empty (after getting rid of any whitespace)
    if (users.length > 0) {
      const today = new Date();
      const rows = [];

      // Go through each unit in the grid currently, and get each AccessEnd date.
      // If it's valid, then get the ID of the unit, and give the user a role (Past Support | Support, depending when the unit ends),
      // and add the details to a CSV string.
      document.querySelectorAll('#grdMyUnits > tbody > tr').forEach(el => {
        const accessEndString = el.querySelector('td[aria-describedby="grdMyUnits_AccessEnd"]')?.title;
        if (accessEndString) {
          // Ensure only rows with a valid end date are included
          const accessEnd = $.datepicker.parseDate('dd/M/yy', accessEndString);
          const thisID = el.querySelector('td[aria-describedby="grdMyUnits_Code"]')?.title;
          for (const user of users) {
            rows.push('ENROLL,' + user + ',,' + (accessEnd < today ? 'Past Support' : 'Support') + ',' + thisID);
          }
        }
      });

      // If we have data in our CSV string, add the headers,
      // then generate a Blob and a URL to download it.
      if (rows.length > 0) {
        const csv = 'ENROLL,Username,,Role Name,Org Unit Code\r\n' + rows.join('\r\n');
        const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
        const url = URL.createObjectURL(blob);
        $('#exportcsv', container).attr({ href: url, download: 'QuicklinksBUM.csv' }).fadeIn();
      }
    } else container.querySelector('#exportcsv').hidden = true;
    // There were no valid users, so hide the exportCSV panel.
  }

  // Bulk User Management (BUM) export set up
  if (store.manager.addBUM) {
    $('#grdMyUnits').jqGrid('navButtonAdd', '#pagerMyUnits', {
      caption: 'Bulk User Management (BUM)',
      title: 'Export page for Bulk User Management (BUM) tool',
      buttonicon: 'ui-icon-arrowthickstop-1-n',
      onClickButton: function () {
        dialog.dialog('open');
      },
    }); // Add button for Bulk User Management (BUM) export
    names.onchange = doBUM;

    let btn = document.querySelector('td[title="Export page for Bulk User Management (BUM) tool"] > div');
    btn.setAttribute('mm-icon-type', 'action-select');
    btn.setAttribute('mm-icon-offset', '0:3');
  }
  // end Bulk User Management (BUM) export setup

  if ($('#grdMyUnits').getGridParam('rowNum') != store.manager.recordsPerPage) {
    $('#grdMyUnits').jqGrid('setGridParam', { rowNum: store.manager.recordsPerPage }).trigger('reloadGrid'); // also triggers addQuicklinks
    document.querySelector('#pg_pagerMyUnits .ui-pg-selbox').value = store.manager.recordsPerPage;
  }

  // When the 'Rows to display' value changes, let's send that value to our background script to remember.
  document.querySelector('#pg_pagerMyUnits .ui-pg-selbox').addEventListener('change', e => {
    chrome.runtime.sendMessage(extensionID, { request: 'updateRecord', records: $(this).val() });
  });
})();
