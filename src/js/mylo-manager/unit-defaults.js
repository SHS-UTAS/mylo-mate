(async () => {
  const config = await FetchMMSettings();
  if(!config.other.EdTechsMode) return;

  const style = document.createElement('style');
  style.innerHTML = `
    .UnitRequestTabPage { position: relative; }
    .save-load-config { position: absolute; top: 1em; right: 1em; }

    .save-load-config button { background: none; border: thin solid #ccc; margin: 0 0.5em; padding: 0.2em 0.5em; border-radius: 5px; }
    .save-load-config button:disabled { background: #dadada; color: #aaa; }
    .save-load-config button:before { font-family: "FontAwesome"; font-size: 1.2em; }
    .save-load-config button.save:before { content: '\\f0c7'; }
    .save-load-config button.load:before { content: '\\f093'; margin-right: 0.5em; }
  `;
  document.head.append(style);

  const faculty = document.querySelector('#Faculty');
  const school = document.querySelector('#School');
  const discipline = document.querySelector('#Discipline');

  const DeliveryPeriod = document.querySelector('#DeliveryPeriod');

  const NumUnitOutlines = document.querySelector('#NumUnitOutlines');
  const FormatUnitOutlines = document.querySelector('#FormatUnitOutlines');

  const TimeTable = document.querySelector('#NoTimeTable');
  const ReadingLists = document.querySelector('#ReadingLists');

  const container = document.querySelector('.UnitRequestTabPage');

  const wrapper = document.createElement('div');
  wrapper.classList.add('save-load-config');

  const save = document.createElement('button');
  save.classList.add('save');

  const load = document.createElement('button');
  load.classList.add('load');
  load.disabled = window.localStorage.getItem('mm-unit-default') == null;
  
  save.title = 'Save these selections as my default';
  load.title = 'Apply my saved defaults to this page';
  load.innerText = 'Apply my defaults'

  wrapper.append(load, save);
  container.append(wrapper);

  save.onclick = e => {
    e.preventDefault();

    const o = {
      faculty: faculty.value,
      school: school.value,
      discipline: discipline.value,
      DeliveryPeriod: DeliveryPeriod.value,
      NumUnitOutlines: NumUnitOutlines.value,
      FormatUnitOutlines: FormatUnitOutlines.value,
      TimeTable: TimeTable.checked,
      ReadingLists: ReadingLists.checked,
    };

    window.localStorage.setItem('mm-unit-default', JSON.stringify(o));
    load.disabled = false;

    alert('The selections on this page have been saved. You can use the Apply button for future unit creation.');
  };

  const domchange = (el, test = () => true) =>
    new Promise((res, rej) => {
      const handle = new MutationObserver(() => {
        if (test()) {
          handle.disconnect();
          res();
        }
      });
      handle.observe(el, { childList: true, subtree: true });
    });

  load.onclick = async e => {
    e.preventDefault();

    const o = JSON.parse(window.localStorage.getItem('mm-unit-default'));

    NumUnitOutlines.value = o.NumUnitOutlines;
    FormatUnitOutlines.value = o.FormatUnitOutlines;
    TimeTable.checked = o.TimeTable;
    ReadingLists.checked = o.ReadingLists;
    
    DeliveryPeriod.value = o.DeliveryPeriod,
    DeliveryPeriod.dispatchEvent(new Event('change'));

    faculty.value = o.faculty;
    faculty.dispatchEvent(new Event('change'));
    await domchange(school, () => school.options.length > 1);

    school.value = o.school;
    school.dispatchEvent(new Event('change'));
    await domchange(discipline, () => discipline.options.length > 1);

    discipline.value = o.discipline;
    discipline.dispatchEvent(new Event('change'));
  };
})();
