(async function() {
	const units = await APIRequest('/d2l/api/lp/1.20/enrollments/myenrollments/?orgUnitTypeId=3');	
	const frame = document.querySelector('frame[name="Body"]');
	frame.addEventListener('load', e => {
		const dom = frame.contentDocument;
		let elems = dom.querySelectorAll('input[type="radio"]');
		let ids = Array.from(elems).map(n => n.value);
		let on_page_units = units.filter(u=>ids.includes(u.OrgUnit.Id.toString()));
		
		const exam_days = 21;
		const now = new Date();
		on_page_units.forEach(unit => {
			let start = new Date(unit.Access.StartDate);
			let end = new Date(unit.Access.EndDate);
			let exam_date = new Date(end.getTime() + (1000 * 60 * 60 * 24 * exam_days))
			
			let status = now < start ? 'future'
								 : now >= start && now <= end ? 'active'
								 : now >= end && now <= exam_date ? 'exam'
								 : 'end';
			
			// Find the input again, based on the id of this unit, and give the parent `<tr>` element an attribute.
			Array.from(elems).find(n => n.value == unit.OrgUnit.Id.toString()).parentNode.parentNode.setAttribute('mm-status', status);
		});
		let styles = document.createElement('link');
		styles.href = chrome.runtime.getURL('css/mylo-search.css');
		styles.rel = 'stylesheet';
		styles.type = 'text/css';
		dom.head.appendChild(styles);
	});
})();