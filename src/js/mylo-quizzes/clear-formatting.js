(async function () {
  if (typeof tinymce === 'undefined') return;

  if (!HTMLDocument.prototype.waitFor) {
    HTMLDocument.prototype.waitFor = function (target, cb) {
      if (this.querySelector(target)) {
        cb.call(null, this.querySelector(target));
        return;
      }

      const res = mutList => {
        if (this.querySelector(target)) {
          cb.call(null, this.querySelector(target));
        }
      };
      const obs = new MutationObserver(res);
      obs.observe(this, { childList: true, subtree: true });
    };
  }

  let style = document.createElement('style');
  style.innerHTML = `
	.mce-i-mm_btn:before {
		content: '';
		background-position: 0 0;
		background-repeat: no-repeat;
		width: 18px;
		height: 18px;
		background-size: 18px 18px;
		display: block;
	}
	.mce-i-mm_clear:before {
		background-image: url(https://s.brightspace.com/lib/bsi/10.7.10-daylight.14/images/tier1/close-default.svg);
	}
	
	.mce-i-mm_strip:before {
		background-image: url(https://s.brightspace.com/lib/bsi/10.7.10-daylight.14/images/html-editor/insert-attributes.svg);
	}

	`;
  document.head.appendChild(style);
  await until(() => tinymce.get().length > 0);

  tinymce.get().forEach(async (editor, i) => {
    async function handle() {
      let toolbar = this.bodyElement.parentNode.querySelector('.toolbar');

      let styles_btn = document.createElement('div');
      styles_btn.classList.add('mce-widget');
      styles_btn.classList.add('mce-btn');
      styles_btn.tabindex = -1;
      styles_btn.role = 'button';
      styles_btn.title = 'Clear Styles';
      styles_btn.setAttribute('data-editor', this.id);
      styles_btn.innerHTML = `<button role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-mm_btn mce-i-mm_strip"></i></button>`;

      let plain_text_btn = document.createElement('div');
      plain_text_btn.classList.add('mce-widget');
      plain_text_btn.classList.add('mce-btn');
      plain_text_btn.tabindex = -1;
      plain_text_btn.role = 'button';
      plain_text_btn.title = 'Remove Formatting';
      plain_text_btn.setAttribute('data-editor', this.id);
      plain_text_btn.innerHTML = `<button role="presentation" type="button" tabindex="-1"><i class="mce-ico mce-i-mm_btn mce-i-mm_clear"></i></button>`;

      const width = 50;

      const buttons = [styles_btn, plain_text_btn];

      // console.log('Bar');
      await until(() => toolbar.querySelector('.mce-btn.mce-last') != null);
      // console.log(toolbar.querySelector('.mce-btn.mce-last'));
      buttons.forEach(btn => {
        toolbar.querySelector('.mce-btn.mce-last').parentNode.insertBefore(btn, toolbar.querySelector('.mce-btn.mce-last'));
      });
      toolbar
        .querySelectorAll('[style*="width: 330px"]')
        .forEach(node => (node.style.width = 330 + width * buttons.length + 'px'));

      styles_btn.addEventListener('click', e => {
        const editor = tinymce.get(styles_btn.getAttribute('data-editor'));
        editor.bodyElement.querySelectorAll('[style]').forEach(node => node.removeAttribute('style'));
      });

      plain_text_btn.addEventListener('click', e => {
        const editor = tinymce.get(plain_text_btn.getAttribute('data-editor'));
        editor.bodyElement.innerHTML = editor.bodyElement.innerText;
        // editor.bodyElement.querySelectorAll('[style]').forEach(node => node.removeAttribute('style'));
      });

      // let footer = editor.editorContainer.parentNode.parentNode.querySelector('.d2l-htmleditor-footer .d2l-htmleditor-group');
      // let btn = document.createElement('span');
      // btn.setAttribute('mm-editor', i);
      // btn.className = 'd2l-htmleditor-toolbar-item';
      // btn.innerHTML = `
      // <a class="d2l-htmleditor-button mm-remove-style-tags" title="Remove style attribute from tags" role="button" aria-label="Remove style attribute from tags" tabindex="0">
      // 	<img class="d2l-htmleditor-button-icon" src="/d2l/img/lp/pixel.gif" style="background-image: url(https://s.brightspace.com/lib/bsi/10.7.10-daylight.14/images/html-editor/insert-attributes.svg); background-position: 0 0; background-repeat: no-repeat; width: 18px; height: 18px; background-size: 18px 18px;">
      // </a>`;
      // footer.insertBefore(btn, footer.firstChild);

      // btn.addEventListener('click', function (e) {
      // 	let index = this.getAttribute('mm-editor');
      // 	tinymce.editors[parseInt(index)].dom.doc.querySelectorAll('[style]').forEach(node => node.removeAttribute('style'));
      // });
    }
    if (editor.initialized) await handle.call(editor);
    else editor.on('init', handle.bind(editor));
  });

  document.waitFor('iframe.d2l-dialog-frame', iframe => {
    if (iframe.classList.contains('mmflag-tagged')) return;

    iframe.classList.add('mmflag-tagged');
    iframe.addEventListener('load', function (e) {
      let txt = iframe.contentDocument.querySelector('textarea.d2l-htmleditor-dialog-textarea');
      if (!txt) return;

      let btn = document.createElement('button');
      btn.className = 'd2l-button';
      btn.innerText = 'Remove Style Attributes';
      btn.setAttribute('mm-icon-type', 'action-select');
      btn.setAttribute('mm-icon-offset', '-6:0');
      btn.addEventListener('click', function (e) {
        txt.value = txt.value.replace(/ style="[^]+?"/gim, '');
      });

      iframe.contentDocument.querySelector('.d2l-dialog-footer-container .d2l-dialog-button-group').appendChild(btn);
    });
  });
})();
