/**
 * @affects Rubrics
 * @problem Archived rubrics are hidden by default, requiring some clicking and waiting to make them visible.
 * @solution Optionally override the hidden status, making archived rubrics immediately visible.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  // Get our store
  // const store = await new Promise(res => chrome.storage.sync.get(null, res));
  // Get a reference to the `Archived` checkbox.
  let archived = Array.from(document.querySelectorAll('div[id$="searchOptions"] label'))
    .find(l => l.innerText == 'Archived')
    .parentNode.querySelector('input[type="checkbox"]');

  // if (store.mylo.AlwaysShowArchivedRubrics) {
  // Let's disable the checkbox, and add the [M] icon and a descriptive hover message as to why it's disabled.
  archived.disabled = true;
  archived.parentNode.title = `This has been disabled by MyLO MATE. To re-enable, please disable 'Always show archived rubrics' in your Extension Options`;
  archived.parentNode.setAttribute('mm-icon-type', 'action-button');
  archived.parentNode.setAttribute('mm-icon-offset', '-7:-2');

  // If we want to always see an archived field, and it's not set up already, then make it so.
  if (!archived.checked) {
    archived.checked = true;
    let search = document.querySelector('.dsearch_header d2l-input-search');
    if (search) {
      search.search();
    }
  }
  // If it's already checked, and our setting is enabled, lets delete the `x search results` row.
  else {
    let notice = document.querySelector('table[role="search"] > tbody > tr:nth-child(2)');
    if (notice) notice.remove();
  }
  // }
})();
