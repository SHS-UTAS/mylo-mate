(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const { ou } = window.location.searchParams;

  const button = document.createElement('d2l-button-subtle');
  button.setAttribute('text', 'Set Rubric Status');
  button.setAttribute('icon', 'tier1:rubric');
  button.title = 'Manage status of selected rubrics';
  const deleteMultiple = document.querySelector('d2l-button-icon[text="Delete selected rubrics"]');
  if (deleteMultiple) deleteMultiple.insertAdjacentElement('afterend', button);

  const dialog = document.createElement('mm-dialog');
  dialog.setAttribute('heading', 'Bulk Manage Rubric Status');
  dialog.setAttribute('preserve', true);

  const RubricBase = window.location.origin.includes('d2ldev2')
    ? '0e78be5b-5af6-4f4b-827b-568374e17a1d'
    : 'c0ef8d21-d21d-4bf1-b68f-054dc1a5e6fc';

  const RubricState = [
    ['Under Development', 'Draft'],
    ['Finalised', 'Published'],
    ['Archived', 'Archived'],
  ];

  const selector = document.createElement('select');
  selector.classList.add('vui-input', 'd2l-select');
  RubricState.forEach(([name, value]) => {
    let opt = document.createElement('option');
    opt.innerText = name;
    opt.value = value;
    selector.append(opt);
  });
  const blurb = document.createElement('p');
  dialog.append(blurb, selector);
  document.body.append(dialog);

  button.addEventListener('click', e => {
    const selected = Array.from(
      document.querySelectorAll('table.d2l-table tr:not([header]) input[type="checkbox"]:checked')
    );

    if (selected.length == 0) {
      return alert('Please select at least one rubric before continuing.');
    }

    blurb.innerText = `Please select the rubric status you'd like to change the ${selected.length} selected rubric${
      selected.length == 1 ? '' : 's'
    } to:`;
    dialog.setAttribute('open', true);
  });

  dialog.addEventListener('ok', async e => {
    const selected = Array.from(
      document.querySelectorAll('table.d2l-table tr:not([header]) input[type="checkbox"]:checked')
    ).map(cb => cb.value.split('_')[1]);

    const token = await GetOAuth2Token();
    await Promise.all(
      selected.map(id => {
        const fd = new FormData();
        fd.set('status', selector.value);

        return fetch(`https://${RubricBase}.rubrics.api.brightspace.com/organizations/${ou}/${id}`, {
          method: 'PATCH',
          body: fd,
          headers: { Authorization: `Bearer ${token}` },
        });
      })
    );
    window.location.reload();
  });
})();
