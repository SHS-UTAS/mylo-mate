(async () => {
  const config = await FetchMMSettings();
  if (!config.other.EdTechsMode) return;

  const table = document.querySelector('d2l-table-wrapper table');
  const rubrics = Array.from(table.querySelectorAll(`[href*="edit_structure.d2l"]:not([mm-generated])`));

  const addBaseCells = () => {
    const headerCell = text => {
      const cell = document.createElement('th');
      cell.setAttribute('scope', 'col');
      cell.classList.add('d_hch', 'd_gl');
      cell.innerText = text;
      return cell;
    };

    const index = table.querySelector('tr[header] th:nth-child(3)');
    index.insertAdjacentElement('afterend', headerCell('Score Visibility'));
    index.insertAdjacentElement('afterend', headerCell('Learning Outcomes'));
    index.insertAdjacentElement('afterend', headerCell('Rubric Visibility'));

    const cell = key => {
      const cell = document.createElement('td');
      cell.dataset.key = key;
      return cell;
    };

    table.querySelectorAll('tr:not([header])').forEach(row => {
      const index = row.querySelector('td:nth-of-type(2)');
      index.insertAdjacentElement('afterend', cell('score-visibility'));
      index.insertAdjacentElement('afterend', cell('learning-outcomes'));
      index.insertAdjacentElement('afterend', cell('rubric-visibility'));
    });
  };

  addBaseCells();

  const GetOAuth2Token = async () => {
    const existing = window.localStorage['D2L.Fetch.Tokens'];
    if (existing != null && existing['*:*:*'] != null) {
      const body = JSON.parse(existing['*:*:*']);
      if (body.expires_at < Date.now() / 1000) {
        delete window.localStorage['D2L.Fetch.Tokens'];
      } else return body.access_token;
    }

    if (!window.localStorage['XSRF.Token']) return null;

    const token = await fetch('/d2l/lp/auth/oauth2/token', {
      method: 'post',
      body: 'scope=*:*:*',
      credentials: 'include',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Csrf-Token': window.localStorage['XSRF.Token'],
      }),
    }).then(data => data.json());

    window.localStorage.setItem('D2L.Fetch.Tokens', JSON.stringify({ '*:*:*': token }));
    return token.access_token;
  };

  const prod = 'https://c0ef8d21-d21d-4bf1-b68f-054dc1a5e6fc.rubrics.api.brightspace.com/organizations';
  const headers = { headers: { Authorization: 'Bearer ' + (await GetOAuth2Token()) } };

  const GetProperty = (rubric, key) =>
    rubric.actions.map(action => action.fields?.find(field => field.name == key)).find(v => v);

  const labelKeys = {
    AlwaysVisible: 'Rubric visible',
    NeverVisible: 'Rubric hidden',
    VisibleOnceFeedbackPosted: 'Rubric hidden until feedback released',
  };

  await Promise.all(
    rubrics.map(async link => {
      const search = new URL(link.href).searchParams; //window.location.search;
      const orgId = search.get('ou');
      const rubricId = search.get('rubricId');

      const rubric = await fetch(`${prod}/${orgId}/${rubricId}`, headers).then(d => d.json());
      const props = {
        RubricVisiblity: rubric.properties.visibility,
        IncludeFeedbackInOverallFeedback: !(GetProperty(rubric, 'feedbackCopy')?.value.selected ?? true),
        LearningOutcomes:
          GetProperty(rubric, 'alignment')?.value.find(row => row.selected).id == 'ManualAlignment' ? 'Automatic' : 'Manual',
        ScoreVisible: GetProperty(rubric, 'scoreVisible')?.value ?? true,
      };

      const row = link.closest('tr');
      row.querySelector('td[data-key="rubric-visibility"]').innerText = `${labelKeys[props.RubricVisiblity]}`;
      //${props.IncludeFeedbackInOverallFeedback ? '(include in overall feedback)' : ''}

      row.querySelector('td[data-key="learning-outcomes"]').innerText =
        props.LearningOutcomes == 'ManualAlignment'
          ? 'Manually map to rubric levels'
          : 'Automatically map to percentage scores';

      row.querySelector('td[data-key="score-visibility"]').innerText = `${props.ScoreVisible ? 'Show' : 'Hide'} scores`;
    })
  );
})();
