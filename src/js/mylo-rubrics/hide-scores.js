(async () => {
  await until(() => document.querySelector('d2l-rubric-editor')?.entity);
  const rubric = document.querySelector('d2l-rubric-editor');
  if (/untitled/i.test(rubric.entity.properties.name)) {
    sdqs.querySelectorDeep('#hide-score-checkbox input[type="checkbox"]:not(:checked)')?.click();
  }
})();
