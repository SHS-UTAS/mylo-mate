/**
 * @affects Rubrics
 * @problem The separate Rubric Generator tool works closely with MyLO, so it is useful to have it readily accessible in the Rubrics area.
 * @solution Add a button that opens the Rubric Generator tool in a new tab.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  // await until(() => document.body.getAttribute('mm-unit-status') != null);
  // if (document.body.getAttribute('mm-unit-status') !== "2") return;

  let target = document.querySelector('.d2l-action-buttons button[primary]');
  if (!target) return;

  let generatorBtn = document.createElement('button');
  generatorBtn.setAttribute('data-testid', 'rubric-generator-link');
  generatorBtn.className = 'd2l-button';
  generatorBtn.innerText = 'Rubric Generator';
  generatorBtn.setAttribute('title', 'Open MyLO Rubric Generator in a new window');
  generatorBtn.setAttribute('mm-icon-type', 'action-select');
  generatorBtn.setAttribute('mm-icon-offset', '-6:0');

  target.parentNode.appendChild(generatorBtn);

  generatorBtn.addEventListener('click', () => {
    window.open(`http://www.utas.edu.au/building-elearning/resources/mylo-rubric-generator`, '_blank');
  });
})();
