/**
 * @affects Rubrics
 * @problem The Rubric Generator requires its output to be imported into MyLO but it's not obvious how to accomplish this.
 * @solution Add a button that opens the Import/Export components tool in a new tab, and opens the "Select Component" dialog, simplifying the process for staff.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  if (window.ImportNewRubricLinkAdded) return;
  window.ImportNewRubricLinkAdded = true;

  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  // await until(() => document.body.getAttribute('mm-unit-status') != null);
  // if (document.body.getAttribute('mm-unit-status') !== "2") return;

  let target = document.querySelector('.d2l-action-buttons button[primary]');
  if (!target) return;

  let importBtn = document.createElement('button');
  importBtn.setAttribute('data-testid', 'import-rubric-button');
  importBtn.className = 'd2l-button';
  importBtn.innerText = 'Import Rubric from File';
  importBtn.setAttribute('title', 'Import a rubric zip file exported from MyLO Rubric Generator or from MyLO itself');
  importBtn.setAttribute('mm-icon-type', 'action-select');
  importBtn.setAttribute('mm-icon-offset', '-6:0');

  target.parentNode.insertBefore(importBtn, target.nextElementSibling);

  importBtn.addEventListener('click', () => {
    window.location = `/d2l/lms/importExport/import_export.d2l?ou=${window.location.searchParams.ou}&amp%3bsp=1&mm-action=3,1,1`;
  });
})();
