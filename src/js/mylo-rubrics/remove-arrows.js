(async () => {
  const config = await FetchMMSettings();
  if (!config.mylo.removeScrollerArrows) return;

  setTimeout(() => {
    sdqs.querySelectorAllDeep('d2l-scroll-wrapper,d2l-rubric-criteria-group-editor').forEach(node => {
      node.toggleAttribute('hide-action', true);
      node.style.width = 'fit-content';
    });
  }, 1000);
})();
