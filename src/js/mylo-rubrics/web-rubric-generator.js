(async function () {
  const urlParams = new URLSearchParams(window.location.search);
  const mmrid = urlParams.get('mmrid');
  if (mmrid != null) {
    let data = await new Promise(resolve => chrome.storage.local.get([mmrid], e => resolve(e[mmrid])));
    let loader = document.createElement('script');
    loader.innerHTML = `
		var bin = [${data.join(',')}];
		setTimeout(function() { importZip({ target: { files: [bin] }}); }, 1000);
	`;
    document.body.appendChild(loader);
  }
  //TODO: Figure out how/if/when to remove the data stored in chrome.storage.local?
})();

