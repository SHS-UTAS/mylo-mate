(async function() {
	window.location.params = window.location.params || {};
	window.location.search.substr(1).split('&').map(part => { let o = part.split('='); window.location.params[o[0].toLowerCase()] = o[1]; });

	if(window.location.params.search) {
		let searchField = document.querySelector('input[placeholder="Search for units"]');
		searchField.value = window.location.params.search;
		searchField.nextElementSibling.dispatchEvent(new Event('click'));
	}
})();