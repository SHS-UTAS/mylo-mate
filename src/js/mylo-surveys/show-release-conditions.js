(async function () {
  const type = 'surveys';
  const titleBreak = '\u000A';
  const ou = window.location.searchParams.ou;

  document.querySelectorAll('img[title="Has Release Conditions"]').forEach(async el => {
    const id = new URL(el.closest('th').querySelector('[onclick*="setreturnpoint" i]').href).searchParams.get('si');
    const restrictions = await getReleaseConditions(ou, type, id);
    const text = [el.title].concat(restrictions.map(v => ` - ${v}`));
    el.title = text.join(titleBreak);
  });
})();
