const updatePath = async () => {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  // Make sure that we've got this enabled
  const settings = await FetchMMSettings();
  if (!settings.editor.PromptNewFilePath) return;

  const result = Object.entries(D2L.OR).filter(([, v]) => /file\/main.d2l.*path=/i.test(v.select));
  if (result.length == 0) return;

  // Get the key and object that handles opening the Upload File dialog
  const [key, objstr] = result.at(-1);

  const obj = JSON.parse(objstr.select);

  const id = window.location.pathname.match(/\/content\/([\d]+)\/Home/)[1];
  const parentModule = document
    .querySelector('#d2l_two_panel_selector_main [id^="placeholderid" i]')
    .id.split('placeholderid')[1];

  let modules = new Map();
  let iterate = (node, parentId) => {
    node.Modules.forEach(module => iterate(module, node.ModuleId));
    node.ParentModuleId = parentId;
    modules.set(node.ModuleId, node);
  };

  const toc = await api(`/d2l/api/le/1.43/${id}/content/toc`, { fresh: true, first: true });
  toc.Modules.forEach(module => iterate(module, null));

  let parents = [];
  let node = modules.get(parseInt(parentModule));
  while (node != null) {
    parents.push(node);
    node = modules.get(parseInt(node.ParentModuleId));
  }

  parents = parents.reverse();

  let base = parents[0].DefaultPath.match(/(\/content\/enforced\/.+?\/)/i)[1];
  let path = parents.map(({ Title }) => Title.replace(/([^a-z0-9]+)/gi, '-')).join('/');

  const url = new URL(obj.P[0].P[1].Url, window.location.origin);
  url.searchParams.set('path', `${base + path}/`);

  obj.P[0].P[1].Url = url.pathname + url.search;

  D2L.OR[key].select = JSON.stringify(obj);
};

document.querySelector('#d2l_two_panel_selector_main').addEventListener('dom-changed', e => {
  const change = e.detail;
  if (change == null) return;

  // If the DOM has updated to add children, good chance the page refreshed. Lets update.
  if (change.type == 'childList' && change.addedNodes.length > 0) {
    updatePath();
  }
});

// The event won't fire on page load, so we'll have to run it manually.
updatePath();
