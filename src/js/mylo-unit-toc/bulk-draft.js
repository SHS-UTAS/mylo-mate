(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const monitor = document.querySelector('#d2l_two_panel_selector_main');
  if (!monitor) return;

  const dropdown = document.createElement('d2l-dropdown');
  dropdown.classList.add('d2l-menuflyout', 'd2l-menuflyout-dropdown');
  dropdown.innerHTML = `
  <d2l-button-subtle mm-icon-type="action-button" mm-icon-offset="4:10" class="d2l-dropdown-opener d2l-menuflyout-subtle-opener d2l-menuflyout-opener" title="Modify publish status for all units below" type="button" dir="ltr" aria-haspopup="true" aria-expanded="false">
    <div class="d2l-menuflyout-custom-content">
      <div class="d2l-inline">
        <div class="d2l-textblock d2l-offscreen">Draft Status</div>
        <div class="d2l-textblock d2l-body-small">Content status</div>
      </div>
      <img class="d2l-image" style="background-image: url(https://s.brightspace.com/lib/bsi/20.19.6-45/images/tier1/chevron-down.svg); background-position: 0 0; background-repeat: no-repeat; width: 18px; height: 18px; background-size: 18px 18px;" src="/d2l/img/lp/pixel.gif" alt="">
      <div class="d2l-clear"></div>
    </div>
  </d2l-button-subtle>
  <d2l-dropdown-content class="d2l-menuflyout-dropdown-contents" align="start" no-pointer vertical-offset="12" render-content d2l-dropdown-content dir="ltr" style="--d2l-dropdown-verticaloffset:12px;">
    <ul class="d2l-list">
      <li><a class="d2l-link" href="javascript:void(0);" title="Draft">Draft</a></li>
      <li><a class="d2l-link" href="javascript:void(0);" title="Published">Published</a></li>
    </ul>
  </d2l-dropdown-content>
  `;

  const getContainer = () => document.querySelector('d2l-button-subtle[title*="Modify publish status" i]').parentNode.parentNode;
  const updateOriginalButton = () => {
    const button = document.querySelector('d2l-button-subtle[title*="Modify publish status" i]');
    if (!button) return;

    const status = button.querySelector('div.d2l-body-small');
    if (status.closest('.d2l-datalist-item-content') != null) return;
    if (status.hasAttribute('tweaked')) return;
    status.innerText = `Module status: ${status.innerText}`;
    status.setAttribute('tweaked', true);
  };

  updateOriginalButton();

  const updateStates = (e) => {
    document.querySelectorAll(`ul.d2l-datalist a[title="${e.target.title}" i]`).forEach(a => a.click());
  };

  dropdown.querySelector('a[title="Draft"]').addEventListener('click', updateStates);
  dropdown.querySelector('a[title="Published"]').addEventListener('click', updateStates);

  // dropdown.style.display = 'none';

  function checkBulkEdit() {
    const show = document.querySelector('button[title="Done editing bulk edit"]') != null;
    if (!show) {
      updateOriginalButton();
      return;
    }

    if (!dropdown.isAttached) {
      updateOriginalButton();
      getContainer().prepend(dropdown);
    }
  }

  (new MutationObserver(checkBulkEdit)).observe(monitor, { childList: true, subtree: true });
  checkBulkEdit();
}());
