
(async function () {
  const config = await FetchMMSettings();
  if (!config.mylo.exportToWord) return;

  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const monitor = document.querySelector('#d2l_two_panel_selector_main');
  if (!monitor) return;
  /**
   * Conserve aspect ratio of the original region. Useful when shrinking/enlarging
   * images to fit into a certain area.
   *
   * @param {Number} srcWidth width of source image
   * @param {Number} srcHeight height of source image
   * @param {Number} maxWidth maximum available width
   * @param {Number} maxHeight maximum available height
   * @return {Object} { width, height }
   */
  function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    const resized = { width: srcWidth * ratio, height: srcHeight * ratio };

    if (resized.width > srcWidth || resized.height > srcHeight) return { width: srcWidth, height: srcHeight };

    return resized;
  }

  const unit = /\/le\/content\/([0-9]+)\/Home/gi.exec(window.location.href)[1];
  const styles = await fetch(chrome.runtime.getURL('res/styles.xml')).then(d => d.text());
  /** const domFromUrl = async url => new DOMParser().parseFromString(await fetch(url).then(d => d.text()), 'text/html'); */
  const domFromUrl = async url => {
    const container = document.createElement('iframe');
    container.src = url;

    document.body.append(container);
    return new Promise(res => {
      container.onload = () => res([container, container.contentDocument]);
    });
  };

  const icons = Object.entries({
    'Reading': 'fa-book-reader',
    'Video': 'fa-play-circle',
    'Written Task': 'fa-pencil-alt',
    'Assessment': 'fa-check-square',
    'Quiz': 'fa-question-circle',
    'Group Activity': 'fa-users',
    'Discussion': 'fa-comments',
    'Case Study': 'fa-briefcase',
    'Important': 'fa-exclamation-circle',
    'Practical': 'fa-hand-paper',
    'Intended Learning Outcomes': 'fa-certificate',
    'Reflection': 'fa-lightbulb',
    'In-Person Activity': 'fa-chalkboard-teacher',
    'Web Resource': 'fa-globe',
  });

  const d2mify = dom => {
    const body = dom.querySelector('[block-identifier="content-block"]');
    let listInstance = 0;

    body.querySelectorAll('.clearfix,.clear-fix').forEach(el => el.remove());

    body.querySelectorAll('a[href]').forEach(a => {
      /** This ensures that all the -attributes- are absolute. */
      a.setAttribute('href', a.href);
    });

    const processList = child => {
      const depth = node => {
        let i = -1;
        let n = node;
        while (n && child.contains(n)) {
          if (n.matches('ul,ol')) i++;

          n = n.parentElement;
        }
        return i;
      };

      const replace = parent => {
        let items = [];

        const process = item => {
          listInstance++;
          item.querySelectorAll(':scope > li').forEach(li => {
            const offset = depth(li);

            const replacement = document.createElement('x-li');
            replacement.setAttribute('depth', offset);
            replacement.setAttribute('type', li.parentElement.localName);
            replacement.setAttribute('instance', listInstance);

            const clone = li.cloneNode(true);
            clone.querySelectorAll('ul,ol').forEach(el => el.remove());
            clone.querySelectorAll('p').forEach(el => el.replaceWith(...el.childNodes));

            replacement.innerHTML = clone.innerHTML;

            items.push(replacement);

            li.querySelectorAll(':scope > ol,:scope > ul').forEach(process);
          });
        };

        process(parent);
        return items;
      };

      const items = replace(child);
      return items;
    };

    const parseNode = child => {
      if (child.matches('ul,ol')) {
        child.replaceWith(...processList(child));
      } else if (child.matches('.accordion')) {
        const title = child.querySelector('.card-title')?.innerText ?? 'Callout title';
        const content = child.querySelector('.card-body')?.innerHTML ?? 'Callout content';

        const container = document.createElement('table');
        container.innerHTML = `<tr><td colspan="2">Accordion</td><td>&nbsp;</td></tr><tr><td>${title}</td><td>${content}</td></tr>`;

        container.querySelectorAll('ul,ol').forEach(list => list.replaceWith(...processList(list)));

        child.replaceWith(container);
      } else if (child.matches('.float,.float-left,.float-right,figure')) {
        const img = child.querySelector('img');
        const caption = child.querySelector('figcaption').innerHTML;
        const alt = img?.getAttribute('alt') ?? '';
        const position = child.matches('.float-left') ? ' Left' : child.matches('.float-right') ? ' Right' : '';

        const container = document.createElement('table');
        container.innerHTML = `
  <tr><td>Image${position}</td></tr>
  <tr><td>${img?.outerHTML ?? ''}</td></tr>
  <tr><td>${alt}</td></tr>
  <tr><td>${caption}</td></tr>
`.trim();

        container.querySelectorAll('ul,ol').forEach(list => list.replaceWith(...processList(list)));
        child.replaceWith(container);
      } else if (child.matches('.card-standard')) {
        const header = child.querySelector('.card-body :is(h5,h1,h2,h3,h4,h6)');
        const title = header.innerHTML;
        header?.remove();

        const content = child.querySelector('.card-body').innerHTML;

        const container = document.createElement('table');
        container.innerHTML = `<tr><td>Callout</td></tr><tr><td><h7 data-style-id="Callouttitle">${title}</h7>${content}</td></tr>`;

        container.querySelectorAll('ul,ol').forEach(list => list.replaceWith(...processList(list)));
        child.replaceWith(container);
      } else if (child.matches('.card-graphic')) {
        const header = child.querySelector('.card-body :is(h5,h1,h2,h3,h4,h6)');
        const title = header?.innerHTML;
        header?.remove();

        const content = child.querySelector('.card-body').innerHTML;

        const icon = [...child.querySelector('.card-icon .fas').classList.values()].find(v => v.startsWith('fa-'));
        const type = icons.find(([_, v]) => v == icon)?.[0] ?? 'Icon callout';

        const container = document.createElement('table');
        container.innerHTML = `<tr><td>${type}</td></tr><tr><td>${
          title ? `<h7 data-style-id="Callouttitle">${title}</h7>` : ''
        }${content}</td></tr>`;

        container.querySelectorAll('ul,ol').forEach(list => list.replaceWith(...processList(list)));
        child.replaceWith(container);
      } else if (child.matches('.tabs-wrapper')) {
        const tabs = Array.from(child.querySelectorAll('[role="tablist"] [role="tab"]')).map(v => v.innerHTML);
        const children = Array.from(child.querySelectorAll('.tab-content [role="tabpanel"]')).map(v => v.innerHTML);

        const container = document.createElement('table');
        container.innerHTML = `<tr><td colspan="2">Tabs</td></tr>`;

        for (let i = 0; i < tabs.length; i++) {
          const row = document.createElement('tr');
          row.innerHTML = `<td>${tabs[i].trim()}</td><td>${children[i].trim()}</td>`;
          container.append(row);
        }

        container.querySelectorAll('ul,ol').forEach(list => list.replaceWith(...processList(list)));
        child.replaceWith(container);
      } else if (child.matches('iframe')) {
        const p = document.createElement('p');
        const a = document.createElement('a');
        a.href = child.src;
        a.innerText = child.src;

        p.append(a);

        const sibling = child.parentElement?.nextElementSibling;
        if (sibling?.matches('.video-text')) {
          sibling.remove();
        }

        child.replaceWith(p);
      } else if (child.matches('.jumbotron')) {
        const header = child.querySelector('h5,h1,h2,h3,h4,h6');
        const title = header.innerHTML;
        header.remove();

        const content = child.innerHTML;

        const container = document.createElement('table');
        container.innerHTML = `<tr><td>Jumbotron</td></tr><tr><td><h7 data-style-id="Callouttitle">${title}</h7>${content}</td></tr>`;

        container.querySelectorAll('ul,ol').forEach(list => list.replaceWith(...processList(list)));
        child.replaceWith(container);
      } else if (child.matches('blockquote')) {
        const container = document.createElement('x-blockquote');
        const quote = document.createElement('x-quote');
        const hasCitation = child.querySelector('cite') != null;

        if (hasCitation) {
          const cite = document.createElement('x-cite');

          cite.innerText = child.querySelector('cite')?.innerText;
          child.querySelectorAll('footer,cite').forEach(el => el.remove());
        }

        quote.innerHTML = child.innerHTML;

        container.append(quote);
        if (hasCitation) {
          container.append(cite);
        }

        child.replaceWith(container);
      }

      for (const el of child.children) {
        parseNode(el);
      }
    };

    for (const child of body.children) {
      parseNode(child);
    }

    return dom;
  };

  const toAST = node => {
    const styles = el =>
      Object.fromEntries([...el.computedStyleMap()].map(([property, value]) => [property, value[0].toString()]));

    const allowEmptyTableCells = true;
    const scan = async (el, parent) => {
      let d = { parent };
      for (const child of el.childNodes) {
        d.children = (d.children || []).concat(await scan(child, d)).filter(v => v);
      }

      if (el.nodeName == '#text') {
        d.type = 'text';
        d.content = el.textContent;
        if (d.content.trim().length == 0) return;
      } else if (el.nodeName == 'IMG') {
        d.type = 'img';
        d.content = await fetch(el.src).then(d => d.arrayBuffer());
        /** d.attributes = Object.fromEntries([...el.attributes ?? {}]).map(v => [v.name, v.value])); */
        d.attributes = Object.fromEntries([...el.attributes].map(v => [v.name, v.value]));
        d.styles = styles(el);

        const b = el.getBoundingClientRect();
        d.dimensions = {
          width: b.width,
          height: b.height,
        };
      } else {
        if (allowEmptyTableCells) {
          if (['th', 'td'].includes(el.localName)) {
            if (!d.children?.length) {
              d.children = [{ type: 'text', content: '', parent: d }];
            }
          }
        }

        d.type = el.localName;
        d.attributes = el.attributes ? Object.fromEntries([...el.attributes]?.map(v => [v.name, v.value]) ?? []) : {};
        d.styles = el.computedStyleMap?.() ? styles(el) : {};
      }

      return d;
    };

    return scan(node);
  };

  const { Header, TextRun, Paragraph, SectionType, Table, TableRow, TableCell, ImageRun, ExternalHyperlink } = docx;

  const toDocTree = ast => {
    const skipped = [
      'span',
      'strong',
      'em',
      'b',
      'i',
      'u',
      'div',
      'ul',
      'ol',
      'thead',
      'tbody',
      'tfoot',
      'sup',
      'sub',
      'x-blockquote',
    ];

    const clean = node => {
      if (node.root) node.root = Array.from(node.root).flatMap(clean);
      if (node.children) node.children = node.children.flatMap(clean);

      if (skipped.includes(node.type)) {
        return node.root ?? node.children;
      } else {
        return node;
      }
    };

    const paragraphLike = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'li', 'x-li', 'a', 'x-cite', 'x-quote'];

    const findNearestDocumentFriendlyParent = node => {
      let parent = node.parent;

      while (parent) {
        if (!skipped.includes(parent.type)) return parent;
        parent = parent.parent;
      }

      return null;
    };

    const parse = node => {
      if (node.children) node.children = node.children.flatMap(parse).filter(v => v);
      if (skipped.includes(node.type)) return node.children;

      const children = node.children;

      switch (node.type) {
        case 'text': {
          const parent = node.parent;
          const trueparent = findNearestDocumentFriendlyParent(node);

          const obj = {
            bold: parent.styles['font-weight'] == 'bold' || parseInt(parent.styles['font-weight']) > 400,
            italics: parent.styles['font-style'] == 'italic',
            underline: parent.styles['text-decoration'] == 'underline',
            superScript: parent.type == 'sup',
            subScript: parent.type == 'sub',
            text: node.content,
          };

          if (trueparent?.type == 'a') obj.style = 'Hyperlink';

          const el = new TextRun(obj);

          if (
            !paragraphLike.includes(parent.type) &&
            !paragraphLike.includes(findNearestDocumentFriendlyParent(node)?.type)
          ) {
            return new Paragraph({ children: [el] });
          } else return el;
        }

        case 'p': {
          const classes = node.attributes.class?.split(' ') ?? [];
          const obj = { children };
          if (classes.includes('lead')) {
            obj.style = 'Leadtext';
          }

          return new Paragraph(obj);
        }

        case 'x-quote': {
          return new Paragraph({ children, style: 'Blockquote' });
        }

        case 'x-cite': {
          return new Paragraph({ children, style: 'Captiontext', alignment: docx.AlignmentType.RIGHT });
        }

        case 'h1':
        case 'h2':
        case 'h3':
        case 'h4':
        case 'h5':
        case 'h6':
        case 'h7': {
          if (node.type == 'h1') {
            return new Paragraph({ children, style: 'PageTitle' });
          } else if (node.type == 'h2') {
            return new Paragraph({ children, style: 'ContentHeading' });
          } else if (node.type == 'h3') {
            return new Paragraph({ children, style: 'Contentsub-heading' });
          } else if (node.type == 'h7') {
            return new Paragraph({ children, style: node.attributes['data-style-id'] });
          } else {
            const type = `HEADING_${node.type.substr(-1)}`;
            return new Paragraph({ children, heading: docx.HeadingLevel[type] });
          }
        }

        case 'x-li': {
          const { depth, type, instance } = node.attributes;
          const level = parseInt(depth);

          if (type == 'ul') {
            return new Paragraph({ children, bullet: { level } });
          } else {
            return new Paragraph({ children, numbering: { reference: 'numbering', level, instance: parseInt(instance) } });
          }
        }

        case 'table': {
          return [new Table({ rows: children }), new Paragraph('')];
        }

        case 'tr': {
          return new TableRow({ children });
        }

        case 'th':
        case 'td': {
          const columnSpan = parseInt(node.attributes.colspan ?? '1');
          const rowSpan = parseInt(node.attributes.rowspan ?? '1');
          return new TableCell({ children, rowSpan, columnSpan });
        }

        case 'a': {
          if (!node.attributes.href) return;
          return new ExternalHyperlink({
            child: node.children[0],
            link: node.attributes.href,
          });
        }

        case 'img': {
          const { content, parent } = node;
          const img = new ImageRun({
            data: content,
            transformation: calculateAspectRatioFit(
              Math.max(node.dimensions.width, 10),
              Math.max(node.dimensions.height, 10),
              400,
              400
            ),
          });

          if (
            !paragraphLike.includes(parent.type) &&
            !paragraphLike.includes(findNearestDocumentFriendlyParent(node)?.type)
          ) {
            return new Paragraph({ children: [img] });
          } else {
            /** console.log(parent, findNearestDocumentFriendlyParent(node), img, node); */
            /** return; //img; */
            return img;
          }
        }

        default:
          console.log(node);
          return;
      }
    };

    const result = parse(ast);
    return result;
  };

  const onExport = async e => {
    const btn = e.target;
    btn.toggleAttribute('disabled', true);

    const moduleId = btn.dataset.moduleId;
    const base = `/d2l/api/le/1.55/${unit}/content/modules/${moduleId}`;

    try {
      const module = await api(base, { fresh: true, first: true });
      const topics = await api(`${base}/structure/`, { fresh: true });

      const page = 3;

      const sections = [
        {
          properties: { type: SectionType.CONTINUOUS },
          children: [new Paragraph({ children: [new TextRun(module.Title)], style: 'ModuleTitle' })],
        },
      ].concat(
        await Promise.all(
          topics /** .slice(page - 1, page) */
            .map(async (topic, index) => {
              const [frame, dom] = await domFromUrl(topic.Url);
              try {
                const d2m = await d2mify(dom);
                const body = d2m.querySelector('[block-identifier="content-block"]').parentElement;
                const ast = await toAST(body);
                /** console.log(ast); */
                const docx = toDocTree(ast);
                /** console.log(docx); */

                const opts = {
                  headers: { default: new Header({ children: [new Paragraph(topic.Title)] }) },
                  children: docx,
                };

                // Prevents the module title from being on its own page
                if (index == 0) {
                  opts.properties = { type: SectionType.CONTINUOUS };
                }

                /** return []; */
                return opts;
              } catch (e) {
                console.error(e);
              } finally {
                frame?.remove();
              }
            })
        )
      );

      const doc = new docx.Document({
        title: module.Title,
        creator: 'MyLO MATE',
        externalStyles: styles,
        sections,
        numbering: {
          config: [
            {
              levels: [
                {
                  level: 0,
                  format: docx.LevelFormat.DECIMAL,
                  text: '%1.',
                  alignment: docx.AlignmentType.START,
                  style: {
                    paragraph: {
                      indent: { left: docx.convertInchesToTwip(0.4), hanging: docx.convertInchesToTwip(0.18) },
                    },
                  },
                },
              ],
              reference: 'numbering',
            },
          ],
        },
      });

      docx.Packer.toBlob(doc).then(blob => saveAs(blob, `${module.Title}.docx`));
    } catch (e) {
      console.error(e);
    } finally {
      btn.toggleAttribute('disabled', false);
    }
  };

  const btn = document.createElement('d2l-button');
  btn.innerHTML = `<d2l-icon icon="tier1:file-document"></d2l-icon> Export as .docx`;
  btn.onclick = onExport;

  function addButton() {
    const row = document.querySelector('[id^="placeholderid"]');
    const container = row.children[0];
    const moduleId = row.id.split('placeholderid')[1];

    btn.dataset.moduleId = moduleId;
    container.append(btn);
  }

  const actions = () => addButton();

  let m = new MutationObserver(actions);
  m.observe(monitor, { childList: true });
  actions();
})();
)();
