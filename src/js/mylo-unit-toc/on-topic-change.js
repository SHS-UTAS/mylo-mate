(async () => {
  await until(() => document.querySelector('#d2l_two_panel_selector_main'));
  const container = document.querySelector('#d2l_two_panel_selector_main');

  const onchange = () => document.dispatchEvent(new Event('d2l-topic-changed'));

  const observer = new MutationObserver(onchange);
  observer.observe(container, { childList: true });
  onchange();
})();
