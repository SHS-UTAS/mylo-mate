/**
 * @affects MyLO Table of Contents
 * @problem The Unit Builder tool is extremely helpful when restructuring modules and content but accessing the tool is not intuitive.
 * @solution Add a link directly to the Unit Builder in the top right corner of the Table of Contents.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  if (window.UnitBuilderLinkAdded) return;
  window.UnitBuilderLinkAdded = true;

  const monitor = document.querySelector('#d2l_two_panel_selector_main');
  if (!monitor) return;

  const id = /\/le\/content\/([0-9]+)\/Home/gi.exec(window.location.href)[1];
  const toc = await GetUnitModule({ OrgID: id, Cached: false });

  function addButton() {
    let container = document.querySelector('.d2l-page-header .d2l-button-subtle-group');
    let link = document.createElement('div');
    link.innerHTML = ` <d2l-button-subtle data-testid="unit-builder" text="Unit Builder" mm-icon-type="action-button" mm-icon-offset="4:10" mm-view-as-student="false" icon="d2l-tier1:coursebuilder" href="/d2l/lms/courseBuilder/main.d2l?ou=${document
      .querySelector('.d2l-navigation-s-link')
      .href.split('/')
      .pop()}"></d2l-button-subtle> `;

    let l = link.children[0];
    l.addEventListener('click', e => (window.location = l.getAttribute('href')));
    container.insertBefore(l, container.firstChild);
  }

  function addEditLinks() {
    document.querySelectorAll('a[title$="Web Page" i]').forEach(a => {
      let [, OrgID, TopicID] = /([0-9]+)\/viewContent\/([0-9]+)\//i.exec(a.href);
      a.parentNode.querySelector(
        '.d2l-textblock.d2l-body-small'
      ).innerHTML += ` [<a data-testid="edit-web-page" href="/d2l/le/content/${OrgID}/contentfile/${TopicID}/EditFile?fm=0" title="Edit '${a.innerText}'">e</a>]`;
    });
  }

  function addFileManagerLinks() {
    document.querySelectorAll('.d2l-datalist-item a.d2l-link[href^="/d2l/le/content"]').forEach(a => {
      let [, TopicID] = /viewContent\/([0-9]+)\//i.exec(a.href);
      let item = toc.find(v => v.Identifier == TopicID);
      if (item && item.TypeIdentifier == 'File' && item.Url) {
        const path = item.Url.split('/').slice(0, -1).join('/');
        const file = item.Url.split('/').pop().trim();
        const href = `/d2l/lp/manageFiles/main.d2l?ou=${id}&Path=${path}/&File=${file}`;
        a.nextElementSibling.querySelector(
          '.d2l-textblock'
        ).innerHTML += ` [<a data-testid="show-in-manage-files" title="Show containing folder in Manage Files" target="_blank" href="${href}">f</a>]`;
      }
    });
  }

  function AddUserProgressLink() {
    document.querySelectorAll('.d2l-datalist-item a.d2l-link[href^="/d2l/le/content"]').forEach(a => {
      let [, TopicID] = /viewContent\/([0-9]+)\//i.exec(a.href);
      let item = toc.find(v => v.Identifier == TopicID);
      if (item?.TypeIdentifier == 'File' && item.Url) {
        const ToolTip = `Export User Progress information for "${item.Title}".`;

        const opener = document.createElement('a');
        opener.title = ToolTip;
        opener.innerText = 'u';
        opener.href = '#';
        opener.dataset.pageName = item.Title;
        opener.dataset.url = a.href;

        a.nextElementSibling
          .querySelector('.d2l-textblock')
          .append(document.createTextNode(' ['), opener, document.createTextNode(']'));
      }
    });
  }

  const resources = Promise.all([
    api(`/d2l/api/le/1.46/${id}/classlist/`),
    api(`/d2l/api/lp/1.29/enrollments/orgUnits/${id}/users/`).then(d => new Map(d.map(u => [u.User.Identifier, u.User]))),
  ]);

  const _progress = new Map();
  async function GetUserProgress(content_id) {
    const existing = _progress.get(content_id);
    if (existing) return existing;

    const content = await api(`/d2l/api/le/unstable/${id}/content/userprogress/?objectId=${content_id}`, { fresh: true });
    const mapped = new Map(content.map(user => [user.UserId, user]));

    _progress.set(mapped);
    return mapped;
  }

  const prettyprintduration = seconds => {
    const [h, m, s] = new Date(seconds * 1000).toISOString().substr(11, 8).split(':');
    return `${h > 0 ? h + 'h ' : ''}${m > 0 ? m + 'm ' : ''}${s}s`;
  };

  const prettyprinttimestamp = ts => new Date(ts).toLocaleDateString();

  /**
   * Generates a user report for the provided link.
   * @param name string The dialog heading
   * @param url string The url of the content page
   */
  async function GenerateUserReportDialog(name, url) {
    const dialog = document.createElement('mm-dialog');
    dialog.setAttribute('hide-cancel', true);
    dialog.setAttribute('open', true);
    dialog.setAttribute('heading', 'User Progress Information - ' + name);
    dialog.innerHTML = '<mm-loader center indeterminate></mm-loader><p style="text-align:center;">Loading...</p>';
    document.body.append(dialog);

    const contentid = url.match(/\/(?<id>[0-9]+)\/view$/i)?.groups?.id;

    const [classlist, usermap] = await resources;
    const progressmap = await GetUserProgress(contentid);
    const style = document.createElement('style');
    style.innerHTML = `
    .progress-table thead tr {
      background-color: #eee;
    }

    .progress-table thead th,
    .progress-table tbody td {
      padding: 5px;
    }

    .progress-table tbody tr {
      text-align: center;
    }

    .progress-table tbody tr:nth-child(even) {
      background-color: #f6f6f6;
    }
  `;

    const table = document.createElement('table');
    table.classList.add('progress-table');

    const headers = ['Student ID', 'Student Name', 'Email', 'Times Visited', 'Total Duration', 'Last Visited'];

    const header = document.createElement('thead');
    header.innerHTML = `<tr>${headers.map(hdr => `<th>${hdr}</th>`).join('')}</tr>`;

    const csvRows = [];

    const body = document.createElement('tbody');
    classlist.forEach(student => {
      // Only get students and past students
      if (![103, 120].includes(student.RoleId)) return;

      const report = progressmap.get(parseInt(student.Identifier)) ?? { TotalTime: 0, NumVisits: 0, LastVisited: '' };

      const row = document.createElement('tr');
      row.innerHTML = `
      <td>${student.OrgDefinedId}</td>
      <td>${student.FirstName} ${student.LastName}</td>
      <td>${usermap.get(student.Identifier).EmailAddress}</td>
      <td>${report.NumVisits}</td>
      <td>${prettyprintduration(report.TotalTime)}</td>
      <td>${report.LastVisited.length > 0 ? prettyprinttimestamp(report.LastVisited) : ''}</td>`;

      body.append(row);

      csvRows.push([
        student.OrgDefinedId,
        `${student.FirstName} ${student.LastName}`,
        usermap.get(student.Identifier).EmailAddress,
        report.NumVisits,
        prettyprintduration(report.TotalTime),
        report.LastVisited.length > 0 ? prettyprinttimestamp(report.LastVisited) : '',
      ]);
    });
    table.append(header, body);

    dialog.innerHTML = '';

    const description = document.createElement('p');
    description.innerText = `Below is the user progress information for "${name}".`;

    const csv = 'data:text/csv;charset=utf-8,' + [headers.map(hdr => `"${hdr}"`).join(',')].concat(csvRows).join('\n');

    const downloadLink = document.createElement('a');
    downloadLink.classList.add('d2l-link');
    downloadLink.download = `User Progress Report - ${document.title}.csv`;
    downloadLink.href = csv;
    downloadLink.style.marginBottom = '1rem';
    downloadLink.innerText = '[Save as CSV]';
    downloadLink.setAttribute('mm-icon-type', 'action-button');
    downloadLink.setAttribute('mm-icon-offset', '-7:-2');
    dialog.append(style, description, downloadLink, table);
  }

  const actions = () => {
    addButton();
    addEditLinks();
    addFileManagerLinks();
    AddUserProgressLink();
  };

  let m = new MutationObserver(actions);
  m.observe(monitor, { childList: true });
  actions();

  document.addEventListener('click', e => {
    if (e.target.matches('a[data-page-name][data-url]')) {
      e.preventDefault();
      GenerateUserReportDialog(e.target.dataset.pageName, e.target.dataset.url);
    }
  });
})();
