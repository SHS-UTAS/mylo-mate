(async function () {
  try {
    if (window.location.pathname.startsWith('/d2l/le/manageCourses/search')) return;

    await until(() => document.body.getAttribute('mm-abort') != null);
    if (document.body.getAttribute('mm-abort') == 'true') return;

    const link = document.querySelector('.d2l-navigation-s-link');
    if (!link) return;

    const id = link.href.split('/').pop();
    const now = new Date();
    const exam_days = 21;

    const addIcon = async () => {
      const parent = link.parentNode;

      parent.classList.add('mm-status-checker');
      parent.classList.add('mm-future');
      try {
        try {
          let unit = await api(`/d2l/api/lp/1.25/courses/${id}`, { first: true, fresh: true });
          unit.StartDate = new Date(unit.StartDate);
          unit.EndDate = new Date(unit.EndDate);

          if (now < unit.StartDate) return; // We've already set future as a placeholder. If it's still in the future, do nothing.

          parent.classList.remove('mm-future');
          let exam_period = new Date(unit.EndDate.getTime() + 1000 * 60 * 60 * 24 * exam_days);
          if (now >= unit.StartDate && now < unit.EndDate) {
            parent.classList.add('mm-active');
            document.body.setAttribute('mm-unit-status', 2);
          } else if (now >= unit.EndDate && now < exam_period) {
            parent.classList.add('mm-exam');
            document.body.setAttribute('mm-unit-status', 1);
          } else if (now > exam_period) {
            parent.classList.add('mm-end');
            document.body.setAttribute('mm-unit-status', 0);
          }
        } catch (e) {
          console.error(e);
        }
      } catch (e) {
        console.error('An error occured setting the unit circle colour. ', e);
      }
    };

    if (link.parentNode.classList.contains('d2l-navigation-s-header-logo-area'))
      document.addEventListener('mm-subtitle-added', addIcon);
    else addIcon();
  } catch (e) {
    console.error(e);
  }
})();
