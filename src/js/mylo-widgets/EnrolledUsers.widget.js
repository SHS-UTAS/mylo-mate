/**
 * @affects MyLO Widgets
 * @problem Finding the staff who manage units is a difficult process, often requiring administrative support or manually checking the classlist for each unit.
 * @solution A widget retrieves enrollment information for all units matching search criteria, and provides a downloadable spreadsheet of staff details or a list of names and email addresses that can be copied to clipboard.
 * @target Support staff
 * @impact High
 * @savings 1 hour per unit
 */

export default async function ({ widget, orgid, config }) {
  const completed = widget.querySelector('.completion-message');

  const messages = {
    found: 'Analysis complete. You can now Copy Emails or Download CSV.',
    none: 'Analysis complete. No users were found.',
  };

  const buttons = {
    CSV: widget.querySelector('#csv'),
    Email: widget.querySelector('#email'),
  };

  const clipboard = new ClipboardJS(buttons.Email);
  clipboard.on('success', function (e) {
    e.clearSelection();
    alert('Emails copied to clipboard.');
  });

  clipboard.on('error', function (e) {
    widget.querySelector('.backup-text').removeAttribute('hidden');
    widget.querySelector('#backup_value').value = buttons.Email.getAttribute('data-clipboard-text');
    alert(
      'Automatic copy failed. Close this message, then press Ctrl-C (or Cmd-C on Mac) to copy the values from the textbox below the buttons.'
    );
  });

  const analyse_btn = widget.querySelector('#activate');
  const loader = widget.querySelector('mm-loader');

  const query = widget.querySelector('[name="search"]');
  const sem = widget.querySelector('#semester');
  const year = widget.querySelector('#year');

  const now = new Date();
  for (let i = -1; i < 2; i++) {
    let y = now.getFullYear() + i;
    let o = document.createElement('option');
    o.value = y - 2000;
    o.innerText = y;
    if (i == 0) o.selected = true;
    year.append(o);
  }

  const check_fields = () => {
    analyse_btn.disabled = !(year.value !== '' && sem.value !== '');
    /** widget.querySelector('.backup-text').setAttribute('hidden', true); */
    Object.keys(buttons).forEach(key => (buttons[key].disabled = true));
  };
  query.addEventListener('keyup', check_fields);
  sem.addEventListener('change', check_fields);
  year.addEventListener('change', check_fields);
  /** widget.querySelector('#MM_backup_value').addEventListener('click', e => e.target.select()); */

  analyse_btn.addEventListener('click', async function (e) {
    analyse_btn.disabled = true;
    loader.show();
    completed.innerText = '';

    const timeStart = Date.now();
    const selected = Array.from(widget.querySelectorAll('[name="user-role"]')).filter(el => el.checked);
    const roles = selected.flatMap(v => v.value.split(',').map(v => parseInt(v)));
    const period = year.value + sem.value;
    const term = query.value;

    const units = await api('/d2l/api/lp/1.26/enrollments/myenrollments/?canAccess=true&orgUnitTypeId=3', { fresh: true });
    const searchable_units = units.filter(
      row =>
        (row.OrgUnit.Code.startsWith('NA_') || row.OrgUnit.Code.startsWith('AW_')) &&
        !row.Access.ClasslistRoleName.includes('Student') &&
        row.OrgUnit.Code.split('_')[2] == period &&
        term
          .toLowerCase()
          .split(',')
          .some(terms => row.OrgUnit.Name.toLowerCase().includes(terms.trim()))
    );

    loader.value = 0;
    loader.max = searchable_units.length;

    // Get users from searchable units.
    const userLists = await Promise.all(
      searchable_units.map(async unit => {
        unit.Classlist = await api(`/d2l/api/lp/1.26/enrollments/orgUnits/${unit.OrgUnit.Id}/users/`, {
          fresh: true,
        }).then(d => d.filter(user => roles.includes(user.Role.Id)));
        loader.value++;
        return unit;
      })
    );

    if (userLists.length > 0) {
      completed.innerText = messages.found;
      Object.keys(buttons).forEach(key => (buttons[key].disabled = false));
    } else {
      completed.innerText = messages.none;
      Object.keys(buttons).forEach(key => (buttons[key].disabled = true));
    }

    let addr = new Set();
    userLists.forEach(unit => {
      roles.forEach(id =>
        unit.Classlist.filter(u => u.Role.Id == id).forEach(({ User }) =>
          addr.add(`${User.DisplayName} <${User.EmailAddress}>;`)
        )
      );
    });

    buttons.Email.setAttribute('data-clipboard-text', Array.from(addr.values()).join(''));
    buttons.CSV.addEventListener('click', () => {
      let headings = ['Run Period', 'Unit Name'].concat(selected.map(cb => cb.title));
      let csv = `data:text/csv;charset=utf-8,${headings.join(',')}\r\n`;

      userLists.forEach(
        unit => {
          let vals = [unit.OrgUnit.Period ? unit.OrgUnit.Period : period, `"${unit.OrgUnit.Name}"`];
          vals = vals.concat(
            selected.map(sel => {
              const id = sel.value.split(',').map(v => parseInt(v));
              return `"${unit.Classlist.filter(u => id.includes(u.Role.Id))
                .map(({ User }) => `${User.DisplayName} <${User.EmailAddress}>;`)
                .join(' ')}"`;
            })
          );
          csv += `${vals.join(',')}\r\n`;
        },
        { once: true }
      );

      const link = document.createElement('a');
      link.setAttribute('href', encodeURI(csv));
      link.setAttribute(
        'download',
        `${term.toUpperCase().length > 0 ? term.toUpperCase() + '_' : ''}${period}_StaffMembers-${selected
          .map(cb => cb.title)
          .join(',')}.csv`
      );
      link.click();
    });

    loader.hide();
    analyse_btn.disabled = false;
  });
}
