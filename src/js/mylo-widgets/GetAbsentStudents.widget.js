/**
 * @affects MyLO Widgets
 * @problem Retrieving a list of all absent students usually requires logging a ticket with ITS for them to create a custom report.
 * @solution A widget examines the classlist and identifies all students who haven't yet accessed the MyLO unit.
 * @target Academic and support staff
 * @impact High
 * @savings 48 hours lead time
 */

export default async function ({ widget, orgid, config }) {
  const Analyze = widget.querySelector('#MM_enrolled_users_activate');
  const loader = widget.querySelector('mm-loader');

  const query = widget.querySelector('[name="MM_enrolled_users_search"]');
  const sem = widget.querySelector('#MM_enrolled_users_semester');
  const year = widget.querySelector('#MM_enrolled_users_year');

  const resultText = widget.querySelector('.status');

  const now = new Date();
  for (let i = -1; i < 2; i++) {
    let y = now.getFullYear() + i;
    let o = document.createElement('option');
    o.value = y - 2000;
    o.innerText = y;
    if (i == 0) o.selected = true;
    year.appendChild(o);
  }

  const check_fields = () => {
    Analyze.disabled = !(year.value !== '' && sem.value !== '');
    widget.querySelector('.backup-text').setAttribute('hidden', true);
  };
  query.addEventListener('keyup', check_fields);
  sem.addEventListener('change', check_fields);
  year.addEventListener('change', check_fields);
  widget.querySelector('.backup-values').addEventListener('click', e => e.target.select());

  Analyze.addEventListener('click', async function (e) {
    Analyze.disabled = true;
    resultText.innerText = '';
    loader.show();

    let period = year.value + sem.value;
    let term = query.value;

    let units = await api('/d2l/api/lp/1.20/enrollments/myenrollments/?canAccess=true&orgUnitTypeId=3');
    let searchable_units = units.filter(
      row =>
        (row.OrgUnit.Code.startsWith('NA_') || row.OrgUnit.Code.startsWith('AW_')) &&
        !row.Access.ClasslistRoleName.includes('Student') &&
        row.OrgUnit.Code.split('_')[2] == period &&
        term
          .toLowerCase()
          .split(',')
          .some(terms => row.OrgUnit.Name.toLowerCase().includes(terms.trim()))
    );

    loader.max = searchable_units.length;

    const collection = await Promise.all(
      searchable_units.map(async ({ OrgUnit }) => {
        const classlist = await api(`/d2l/api/le/1.36/${OrgUnit.Id}/classlist/`, { fresh: true });
        const users = await api(`/d2l/api/lp/1.29/enrollments/orgUnits/${OrgUnit.Id}/users/?roleId=103`, {
          fresh: true,
        }).then(d => {
          const map = new Map();
          d.forEach(({ User }) => map.set(User.Identifier, User));
          return map;
        });

        const absent = classlist
          .filter(student => student.LastAccessed == null && student.RoleId === 103)
          .map(student => ({ ...users.get(student.Identifier), ...student }));

        loader.value = loader.value + 1 || 1;
        return { OrgUnit, absent };
      })
    ).then(x => x.filter(y => y.absent.length > 0));

    const csv = ({ absent }) => {
      let headings = ['ID', 'Username', 'Name', 'Email'];
      let csv = `${headings.map(v => `"${v}"`).join(',')}\r\n`;
      absent.forEach(student => {
        csv += `"=""${student.Identifier}""",`;
        csv += `"=""${student.Username}""",`;
        csv += `"=""${student.DisplayName}""",`;
        csv += `"=""${student.EmailAddress}"""`;
        csv += '\r\n';
      });
      csv += `${','.repeat(headings.length - 1)}\r\n`;
      csv += `"Information accurate as of ${new Date().toString()}"`;
      return csv.trim();
    };

    if (collection.length > 0) {
      const zip = new JSZip();
      collection.forEach(item => zip.file(`${item.OrgUnit.Name.replace(/[^a-z0-9 -]/gi, '')}.csv`, csv(item)));
      let blob = await zip.generateAsync({ type: 'blob' });

      saveAs(blob, `${term.trim().length > 0 ? term + '_' : ''}${period}_StudentsWithNoSignInDate.zip`);
      resultText.innerText = `${collection.length} units found with absent students.`;
    } else {
      resultText.innerText = 'No absent students found.';
    }

    loader.hide();
    Analyze.disabled = false;
  });
}
