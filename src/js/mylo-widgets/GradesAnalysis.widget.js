/**
 * @affects MyLO Widgets
 * @problem At the end of semester, staff need to examine student results and identify any potential problems (e.g. a numerical grade that may round up, changing the letter grade).
 * @solution A widget provides useful class information such as charts of grade spread and distribution, and identifies potential problematic numerical grades. It also allows the grade information to be immediately downloaded ready for processing to exams spreadsheets.
 * @target Academic and support staff
 * @impact High
 * @savings 3 hours per unit
 */
export default async function ({ widget, orgid }) {
  const DefaultSort = {
    column: 'name',
    direction: 'asc',
  };

  let MM_dataPerc = [];

  /**
   * A concurrency-driven Promise.all fill-in.
   * @author Yong Jun <https://medium.com/better-programming/implement-your-own-bluebird-style-promise-map-in-js-7c081b7ad02c>
   * @param {Iterable} iterable The object to iterate upon
   * @param {Function} mapper The function to call for each iterable item
   * @param {object} options Allow the providing of any additional options. Currently only supports `concurrency`
   * @param {number} options.concurrency The number of promises to run in parallel
   */
  Promise.map = async function (iterable, mapper, options = {}) {
    let concurrency = options.concurrency || Infinity;

    let index = 0;
    const results = [];
    const pending = [];
    const iterator = iterable[Symbol.iterator]();

    while (concurrency-- > 0) {
      const thread = wrappedMapper();
      if (thread) pending.push(thread);
      else break;
    }

    await Promise.all(pending);
    return results;

    async function wrappedMapper() {
      const next = iterator.next();
      if (next.done) return null;
      const i = index++;
      const mapped = mapper(next.value, i);

      const resolved = await Promise.resolve(mapped);
      results[i] = resolved;
      return wrappedMapper();
    }
  };

  let PanelSettings = {};

  // #region Graph rendering
  const JunkDataRowOffset = 2;
  const ExamDataHeaders = {
    ParentSspNo: 0,
    ParentSpkCd: 1,
    ParentSpkVerNo: 2,
    ParentSspAttNo: 3,
    ParentSpkAbbrTitle: 4,
    SspNo: 5,
    SpkCd: 6,
    SpkVerNo: 7,
    SspAttNo: 8,
    SpkAbbrTitle: 9,
    Year: 10,
    StudyPeriod: 11,
    StudyPeriodDescription: 12,
    StudentId: 13,
    StudentName: 14,
    Grade: 15,
    Mark: 16,
    Comments: 17,
  };
  const ExamDataHeadersNumerical = ['ParentSpkVerNo', 'ParentSspAttNo', 'SpkVerNo', 'SspAttNo', 'Year', 'Mark'];
  const ExamSheetPageTitle = 'FORMAT STUDY_PACKAGE_RESULT STANDARD';
  const ExamSheetHeaderFields = [
    'Parent Ssp No',
    'Parent Spk Cd',
    'Parent Spk Ver No',
    'Parent Ssp Att No',
    'Parent Spk Abbr Title',
    'Ssp No',
    'Spk Cd',
    'Spk Ver No',
    'Ssp Att No',
    'Spk Abbr Title',
    'Year',
    'Study Period',
    'Study Period Description',
    'Student Id',
    'Student Name',
    'Grade',
    'Mark',
    'Comments',
  ];

  const ATTENTION_REQUEST = {
    NONE: 0,
    GRADE_CHANGE: 1,
    MISSING_FROM_MYLO: 2,
    MISSING_FROM_EXAMS: 3,
  };

  const UnitData = await api(`/d2l/api/lp/1.21/enrollments/myenrollments/${orgid}`, { first: true, fresh: true });
  const UnitCodes = UnitData.OrgUnit.Name.match(/[A-Z]{3}[0-9]{3}/g);

  const loader = widget.querySelector('mm-loader');
  const results = widget.querySelector('#MM_resultsCharts');
  const legend = widget.querySelector('.MM_resultsTableLegend');
  legend.style.display = 'none';

  const resultsTable = widget.querySelector('.MM_resultsTable');
  resultsTable.style.display = 'none';

  const resultsTableNode = widget.querySelector('.MM_resultsTable tbody');
  const studentCounter = widget.querySelector('#MM_resultscounter');

  const FileTarget = widget.querySelector('mm-file-zone');

  const isNumeric = n => !isNaN(parseFloat(n)) && isFinite(n);
  const padZero = (n, length = 2) => n.toString().padStart(length, '0');

  const settings = await FetchMMSettings();
  const config = document.createElement('div');
  config.classList.add('config-panel');
  config.innerHTML = `
      <p>
        Should we warn you on all marks close to a letter grade threshold, or only where rounding would cause a letter grade change?
      </p>
      <label title="All marks close to a letter grade threshold will be highlighted for review">
        <input type="radio" name="rounding" value="endingIn9"> 
        Flag all grades with marks ending in 9
        <mm-help></mm-help>
      </label>
      <label title="Only grades where rounding would cause a letter grade change will be highlighted for review">
        <input type="radio" name="rounding" value="roundingIssues"> 
        Only flag rounding issues
        <mm-help></mm-help>
      </label>
    

      <p>
        Should NN grades automatically change to AN, and the mark to 0?
      </p>
      <label title="This will automatically change NN grades to AN, and change the mark to zero">
        <input type="checkbox" name="convert"${settings.widgets.GradesAnalysisAutomaticAN ? ' checked' : ''}> 
        Automatic NN to AN
        <mm-help></mm-help>
      </label>

      <p>
        Group errors at the top of the list?
      </p>
      <label title="Automatically group errors at the top of the page, for easier management.">
        <input type="checkbox" name="group" ${
          /* Note, using != here vs == 'xlsx' means this will be the default if nothing is set either */
          settings.widgets.ResultsMergeGrouping !== false ? 'checked' : ''
        }>
        Group issues
        <mm-help></mm-help>
      </label>

      <p>
        What file format should be exported? 
        Please note, <code>.xlsx</code> is not supported by the Student Management system; 
          if you intend to pass these files on directly to the Exams office, please ensure you select <code>.csv</code>.
      </p>
      <label title="The default Excel format; not supported by the Student Management system.">
        <input type="radio" name="format" value="xlsx" ${settings.widgets.ResultsMergeFormat == 'xlsx' ? 'checked' : ''}> 
        <code>.xlsx</code>
        <mm-help></mm-help>
      </label>
      <label title="Cannot contain any form of rich text formatting; supported by the Student Management system.">
        <input type="radio" name="format" value="csv"${
          /* Note, using != here vs == 'csv' means this will be the default if nothing is set either */
          settings.widgets.ResultsMergeFormat != 'xlsx' ? 'checked' : ''
        }> 
        <code>.csv</code>
        <mm-help></mm-help>
      </label>

      <p>Add Comments column to export? This column will record any existing letter grades that may have previously appeared in the document.</p>
      <p>Please note, This column interferes with the Student Management system, and must be removed before submission to the Exams office.</p>
      <label title="Add a Comments column to the spreadsheet.">
        <input type="checkbox" name="comments" ${
          /* We want the unset default to be false, so use a strict equality operator */
          settings.widgets.ResultsMergeExportComments ? 'checked' : ''
        }>
        Add Comments Column
        <mm-help></mm-help>
      </label>
      `;
  config.querySelector(`input[value="${settings.widgets.GradesAnalysisDefaultWarning}"]`).checked = true;
  config.addEventListener('change', e => {
    const rounding = config.querySelector('[name="rounding"]:checked').value;
    const format = config.querySelector('[name="format"]:checked').value;
    const convert = config.querySelector('[name="convert"]').checked;
    const grouping = config.querySelector('[name="group"]').checked;
    const comments = config.querySelector('[name="comments"]').checked;

    const data = {
      GradesAnalysisDefaultWarning: rounding,
      GradesAnalysisAutomaticAN: convert,
      ResultsMergeGrouping: grouping,
      ResultsMergeFormat: format,
      ResultsMergeExportComments: comments,
    };

    // For use in the general widget script.
    PanelSettings = data;

    // To persist between uses
    SaveMMSettings({ widgets: data });
  });

  const controlPanel = document.createElement('mm-dialog');
  controlPanel.heading = 'Settings';
  controlPanel.preserve = true;
  controlPanel.maxWidth = '650px';
  controlPanel.style.zIndex = 1e5;
  controlPanel.append(config);

  const openControlPanel = document.createElement('d2l-button-subtle');
  openControlPanel.setAttribute('icon', 'tier1:gear');
  openControlPanel.setAttribute('text', 'Settings');
  openControlPanel.style.position = 'absolute';
  openControlPanel.style.top = '5px';
  openControlPanel.style.right = '10px';
  openControlPanel.addEventListener('click', e => controlPanel.show());

  const style = document.createElement('style');
  style.innerHTML = `
      .config-panel { font-size: 0.9em; }
      .config-panel code { font-size: inherit; }
      .config-panel label { user-select: none; display: block; margin-left: 2em; font-size: 0.75em; }
      .config-panel p { font-size: 0.8em; }
      .clean-table { width: 100%; margin-top: 2rem; }
      .clean-table thead tr { 
        background-color: #eee; 
        position: sticky;
        top: -1px;
        z-index: 10;
      }
      .clean-table thead th, .clean-table tbody td { padding: 5px; }
      .clean-table tbody tr { text-align: center; height: 60px; }
      .clean-table tbody tr:nth-child(even) { background-color: #f6f6f6; }
      .mark-number { max-width: 4rem; text-align: center; padding-right: 0 !important; }
      .mark-letter { max-width: 4rem; text-align: center; text-transform: uppercase; }
      td[data-key="mgrade"] { position: relative; }
      .mark-raw { position: absolute; right: 0; bottom: 0; font-size: small; font-weight: normal; margin: 3px; padding: 0; line-height: 1em; }
      input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button { margin-left: 0em; right: 5px; position: relative; }

      /** Handle the error colours **/
      .clean-table tbody.showOn9 tr.warn:nth-child(even), 
      .clean-table tbody.showOn9 tr.warn:nth-child(odd) { background-color: rgba(250,100,100,0.5); }
      .clean-table tbody tr.error td { box-shadow: inset 0 0 0 1px red; color: #d00; font-weight: bold; }
      .clean-table tbody tr.missing td { box-shadow: inset 0 0 0 1px red; }
      .clean-table tbody tr.closeToPassing { background-color: #ffc; }
      .clean-table tbody tr.error td[data-purpose="mylo-data"]:before,
      .clean-table tbody tr.error td[data-purpose="mylo-data"]:after { content: '**'; }

      /** Handle column visibility **/
      .clean-table.hide-exams [data-key="egrade"] { display: none; }

      /** Handle sort icons **/
      .sort-col-icon { display: none; }
      
      th[data-dir="asc"] .sort-col-icon[data-dir="asc"],
      th[data-dir="desc"] .sort-col-icon[data-dir="desc"] { display: inline-block; }
    `;
  document.head.append(style);
  document.body.append(controlPanel);

  const loadGroups = async () => {
    try {
      const container = widget.querySelector('.target-groups');
      const categories = await api(`/d2l/api/lp/1.18/${orgid}/groupcategories/`, { fresh: true });

      await Promise.all(
        categories.map(async category => {
          category.Groups = await api(`/d2l/api/lp/1.18/${orgid}/groupcategories/${category.GroupCategoryId}/groups/`, {
            fresh: true,
          });

          return category;
        })
      );

      for (const category of categories) {
        const heading = document.createElement('p');
        heading.classList.add('heading-text');
        heading.innerText = category.Name;

        container.append(heading);

        for (const group of category.Groups) {
          const cb = document.createElement('d2l-input-checkbox');
          cb.dataset.category = category.GroupCategoryId;
          cb.dataset.group = group.GroupId;
          cb.dataset.enrollments = group.Enrollments.join(',');
          cb.innerText = group.Name;

          container.append(cb);
        }
      }
    } catch (e) {
      console.error(e);
      alert(
        'Something went wrong while fetching the group categories. Please refresh and try again.\n\nIf this error continues, please contact MyLO MATE support.'
      );
    }
  };

  let MM_barChart, MM_pieChart;
  const MM_chartBgs = [
    'rgba(255, 99, 132, 0.2)',
    'rgba(255, 159, 64, 0.2)',
    'rgba(255, 205, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(201, 203, 207, 0.2)',
  ];
  const MM_chartBorders = [
    'rgb(255, 99, 132)',
    'rgb(255, 159, 64)',
    'rgb(255, 205, 86)',
    'rgb(75, 192, 192)',
    'rgb(54, 162, 235)',
    'rgb(153, 102, 255)',
    'rgb(201, 203, 207)',
  ];

  let MM_dataLbls = ['NN', 'PP', 'CR', 'DN', 'HD'];
  let MM_dataPts = [0, 0, 0, 0, 0];
  let resultsCounter = 0;
  let MM_bFirst = true;

  const groupCount = widget.querySelectorAll('#MM_showGroups input:checked').length;
  let gradeTitlesSet = false;
  let bErrorsFound = false;

  function setStudentStyle(studentID, td) {
    let bValid = true;
    if (studentID.length != 6) bValid = false;
    else {
      for (let i = 0; i < studentID.length; i++) {
        if (!isNumeric(studentID[i])) bValid = false;
      }
    }
    if (!bValid) {
      td.classList.add('MM_invalid');
      td.setAttribute('title', 'Invalid Student ID');
    }
  }

  function setGradeColumnTitles(GradeString) {
    if (!gradeTitlesSet) {
      gradeTitlesSet = true;
      let tableHeads = widget.querySelectorAll('#MM_resultsTable .MM_resultsTable thead th');
      tableHeads[2].innerHTML = GradeString + ' Denominator';
      tableHeads[3].innerHTML = GradeString + ' Numerator (rounded)';
      tableHeads[4].innerHTML = GradeString + ' Scheme Symbol';
    }
  }

  function resetChartData() {
    MM_dataLbls = ['NN', 'PP', 'CR', 'DN', 'HD', 'Empty'];
    MM_dataPts = MM_dataLbls.map(v => 0);
    studentCounter.innerText = '';
    resultsTable.style.display = 'none';
  }

  function flushCharts() {
    resetChartData();
    if (MM_barChart != null) MM_barChart.destroy();
    if (MM_pieChart != null) MM_pieChart.destroy();
    MM_bFirst = true;
  }

  function updateChartData(newItem) {
    if (newItem == null) return;
    let bFound = false;
    for (let i = 0; i < MM_dataLbls.length; i++) {
      if (MM_dataLbls[i] == newItem) {
        MM_dataPts[i] += 1;
        bFound = true;
      }
    }
    if (!bFound) {
      MM_dataLbls.push(newItem);
      MM_dataPts.push(1);
    }
    let dataSum = MM_dataPts.reduce((a, b) => a + b, 0);
    MM_dataPerc = [];
    MM_dataPts.forEach(dataPt => MM_dataPerc.push(Math.floor((dataPt / dataSum) * 100)));

    const bar = widget.querySelector('#MM_barChartContainer');
    const pie = widget.querySelector('#MM_pieChartContainer');
    const gradeType = widget.querySelector('input[name="grade-type"]:checked').value;

    if (MM_bFirst) {
      MM_bFirst = false;
      MM_barChart = new Chart(bar, {
        type: 'bar',
        data: {
          labels: MM_dataLbls,
          datasets: [
            {
              data: MM_dataPts,
              backgroundColor: MM_chartBgs,
              borderColor: MM_chartBorders,
              borderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            display: false,
          },
          aspectRatio: 16 / 9,
          title: {
            display: true,
            text: `Grades by Student Numbers (${gradeType})`,
            padding: 40,
            fontSize: 20,
          },
          tooltips: {
            enabled: false,
          },
          scales: {
            yAxes: [
              {
                display: true,
                scaleLabel: {
                  labelString: `Number of Students`,
                  display: true,
                },
                ticks: {
                  beginAtZero: true,
                },
              },
            ],
          },
          plugins: {
            datalabels: {
              anchor: 'end',
              align: 'end',
              display: true,
              font: {
                weight: 'bold',
              },
              formatter: Math.round,
            },
          },
        },
      });

      MM_pieChart = new Chart(pie, {
        type: 'pie',
        data: {
          labels: MM_dataLbls,
          datasets: [
            {
              label: `Student Grades by Percentage`,
              data: MM_dataPerc,
              backgroundColor: MM_chartBgs,
              borderColor: MM_chartBorders,
            },
          ],
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          title: {
            display: true,
            text: `Grades by Percentage (${gradeType})`,
            fontSize: 20,
          },
          aspectRatio: 16 / 9,
          tooltips: {
            enabled: true,
          },
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: 20,
              bottom: 20,
            },
          },
          plugins: {
            datalabels: {
              anchor: 'end',
              borderColor: function (context) {
                return context.dataset.borderColor;
              },
              borderRadius: 25,
              borderWidth: 1,
              color: 'black',
              display: function (context) {
                return context.dataset.data[context.dataIndex] > 0;
              },
              backgroundColor: 'white',
              font: {
                weight: 'bold',
              },
              formatter: function (value, context) {
                return value + '%';
              },
            },
          },
        },
      });
    }

    MM_barChart.update();
    MM_pieChart.data.datasets[0].data = MM_dataPerc;
    MM_pieChart.update();
  }

  let GradeItems = {};
  const getEnrolments = () => {
    const cbs = Array.from(widget.querySelectorAll('.target-groups d2l-input-checkbox'));
    const users = cbs.filter(cb => cb.checked).flatMap(cb => cb.dataset.enrollments.split(','));
    return [...new Set(users)];
  };

  const GetGrades = async () => {
    GradeItems = {};
    loader.show();
    results.setAttribute('hidden', true);
    resultsTableNode.innerHTML = '';

    const groupsSelected = getEnrolments();

    flushCharts();

    studentCounter.dataset.studentCount = 0;

    // let res = await api(`/d2l/api/le/1.37/${id}/classlist/`)
    let res = await api(`/d2l/api/le/1.37/${orgid}/classlist/`, { fresh: true }).then(res =>
      res.filter(
        user =>
          (user.RoleId === 103 || user.RoleId === 120) &&
          (groupsSelected.length == 0 || groupsSelected.includes(user.Identifier))
      )
    );

    let rows = document.createElement('tbody');
    res.forEach(student => {
      let row = document.createElement('tr');
      for (let index = 0; index < 5; index++) {
        let cell = document.createElement('td');

        if (index === 0) {
          cell.innerHTML = student.OrgDefinedId;
          setStudentStyle(student.OrgDefinedId, cell);
        } else if (index === 1) {
          cell.classList.add('name');
          cell.innerHTML = `${student.FirstName} ${student.LastName}`;
        }

        row.appendChild(cell);
      }

      GradeItems[student.Identifier] = {
        student,
      };

      row.dataset.identifier = student.Identifier;
      studentCounter.dataset.studentCount++;

      rows.appendChild(row);
    });

    resultsTableNode.innerHTML = rows.innerHTML;
    loader.hide();
    results.removeAttribute('hidden');

    legend.style.display = 'table';
    resultsTable.style.display = 'table';

    let numberBasedDisplayGrades = [];

    const getLetterGrade = data => {
      if (data.DisplayedGrade != null && data.DisplayedGrade != '') {
        // If we have a display grade, and it doesn't start with a number, then return that by itself.
        if (!/^\d/.test(data.DisplayedGrade)) return data.DisplayedGrade;
        // If we get to here, our display grade starts with a number, and might be returning the numerator and denominator.
        // In this case, log it for warning purposes and continue getting our results.
        else numberBasedDisplayGrades.push(data);
      }

      const gradeVal = Math.round((data.PointsNumerator / data.PointsDenominator) * 100);

      return gradeVal < 50
        ? 'NN'
        : gradeVal < 60
        ? 'PP'
        : gradeVal < 70
        ? 'CR'
        : gradeVal < 80
        ? 'DN'
        : gradeVal >= 80
        ? 'HD'
        : 'Empty';
    };

    let resultsCounter = 0;
    let gradeType = widget.querySelector('input[name="grade-type"]:checked').value;

    const endpoints = ['calculated', 'adjusted'];

    let allRowsProcess = await Promise.map(
      res,
      async student => {
        try {
          const userId = student.Identifier;
          const row = widget.querySelector(`#MM_resultsTable .MM_resultsTable tbody tr[data-identifier="${userId}"]`);

          const [calculated, adjusted] = await Promise.all(
            endpoints.map(type =>
              api(`/d2l/api/le/1.26/${orgid}/grades/final/values/${userId}?gradeType=${type}`, {
                fresh: true,
                first: true,
              })
            )
          );

          const studentData = gradeType == 'calculated' ? calculated : adjusted;

          if (student == null) debugger;
          if (calculated == null) debugger;
          if (adjusted == null) debugger;

          calculated.DisplayedGrade = getLetterGrade(calculated);
          adjusted.DisplayedGrade = getLetterGrade(adjusted);

          GradeItems[userId].calculated = calculated;
          GradeItems[userId].adjusted = adjusted;

          resultsCounter += 1;
          const rc = studentCounter.dataset.studentCount;
          studentCounter.innerHTML = `${resultsCounter} of ${rc} result${rc == 1 ? '' : 's'}${
            groupCount > 0 ? ` from ${groupCount} group${groupCount == 1 ? '' : 's'}` : ''
          } processed. `;

          const cells = row.querySelectorAll('td');
          const d = studentData.PointsDenominator;
          let n = studentData.PointsNumerator ?? 0;
          cells[2].innerHTML = d;
          cells[3].innerHTML = Math.round(n);

          if (Math.round(n) != n) {
            const prev = document.createElement('span');
            prev.classList.add('previous-grade');
            prev.innerText = n?.toFixed(2);
            cells[3].prepend(prev);
          }

          cells[4].innerHTML = getLetterGrade(studentData);
          setGradeColumnTitles(studentData.GradeObjectName);
          if (d != 100) {
            if (d > 0) n = (n / d) * 100;
          }
          if (Math.floor(n) != Math.round(n)) {
            if (Math.round(n).toString().slice(-1) == 0 && Math.round(n) > 49 && Math.round(n) <= 80) {
              cells[3].setAttribute('title', `Rounding ${n} to ${Math.round(n)} will cause a grade change`);
              cells[3].classList.add('MM_gradeChange');
            }
          }
          n = Math.round(n);
          if (n >= 47 && n < 80) {
            if (n.toString().slice(-1) == '9') row.classList.add('MM_is9');
            else {
              if (n == 47 || n == 48) row.classList.add('MM_is4748');
            }
          }
          if (n < 47) row.classList.add('MM_isUnder47');
          if (n >= 80) row.classList.add('MM_isOver80');
          updateChartData(cells[4].innerHTML);
        } catch (e) {
          console.error(e);
          if (!bErrorsFound)
            alert(
              'Something went wrong while fetching the student data. Please refresh and try again.\n\nIf this error continues, please contact MyLO MATE support.'
            );
          bErrorsFound = true;
        }
      },
      { concurrency: 10 }
    );

    if (numberBasedDisplayGrades.length > 0) {
      let warning = document.createElement('div');
      warning.innerText = `${numberBasedDisplayGrades.length} grade${
        numberBasedDisplayGrades.length == 1 ? '' : 's'
      } automatically converted to letter grade (was displayed as number grade). This may indicate that students can see grade information.`;
      warning.classList.add('warning');
      results.insertAdjacentElement('beforebegin', warning);
    }

    widget.querySelector('#MM_DownloadGrades').disabled = false;
    widget.querySelector('#MM_MergeGrades').disabled = false;
    FileTarget.toggleAttribute('ready', true);
  };

  function AsSpreadsheet(headers, rows) {
    let zip = new JSZip();
    zip.file(
      '[Content_Types].xml',
      `<?xml version="1.0" encoding="utf-8"?><Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types"><Default Extension="xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml" /><Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml" /><Override PartName="/xl/worksheets/sheet.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" /></Types>`
    );
    zip.file(
      '_rels/.rels',
      `<?xml version="1.0" encoding="utf-8"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="/xl/workbook.xml" Id="R8fd13dbf32d24a9b" /></Relationships>`
    );
    zip.file(
      'xl/workbook.xml',
      `<?xml version="1.0" encoding="utf-8"?><x:workbook xmlns:x="http://schemas.openxmlformats.org/spreadsheetml/2006/main"><x:sheets><x:sheet name="Grades" sheetId="1" r:id="R1d37fa14c65e421f" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" /></x:sheets></x:workbook>`
    );
    zip.file(
      'xl/_rels/workbook.xml.rels',
      `<?xml version="1.0" encoding="utf-8"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" Target="/xl/worksheets/sheet.xml" Id="R1d37fa14c65e421f" /></Relationships>`
    );
    let xml = `
    <?xml version="1.0" encoding="utf-8"?>
    <x:worksheet xmlns:x="http://schemas.openxmlformats.org/spreadsheetml/2006/main">
      <x:sheetData>
        <x:row>
        ${headers.map(text => `<x:c t="inlineStr"><x:is><x:t>${text}</x:t></x:is></x:c>`).join('\n')}
        </x:row>
        ${rows
          .map(
            row => `<x:row>${row.map(text => `<x:c t="inlineStr"><x:is><x:t>${text}</x:t></x:is></x:c>`).join('\n')}</x:row>`
          )
          .join('\n')}
      </x:sheetData>
    </x:worksheet>
    `;

    zip.file('xl/worksheets/sheet.xml', xml.trim());
    return zip.generateAsync({
      type: 'blob',
    });
  }

  async function DownloadGrades() {
    const headers = [
      'OrgDefinedId',
      'Last Name',
      'First Name',
      'Calculated Final Grade Numerator',
      'Calculated Final Grade Denominator',
      'Calculated Final Grade Scheme Symbol',
      'Adjusted Final Grade Numerator',
      'Adjusted Final Grade Denominator',
      'Adjusted Final Grade Scheme Symbol',
    ];

    const body = Object.values(GradeItems).map(row => [
      row.student.OrgDefinedId != null ? row.student.OrgDefinedId : '',
      row.student.LastName != null ? row.student.LastName : '',
      row.student.FirstName != null ? row.student.FirstName : '',
      row.calculated.PointsNumerator != null ? row.calculated.PointsNumerator : '',
      row.calculated.PointsNumerator != null && row.calculated.PointsDenominator != null
        ? row.calculated.PointsDenominator
        : '',
      row.calculated.PointsNumerator != null && row.calculated.DisplayedGrade != null ? row.calculated.DisplayedGrade : '',
      row.adjusted.PointsNumerator != null ? row.adjusted.PointsNumerator : '',
      row.adjusted.PointsNumerator != null && row.adjusted.PointsDenominator != null ? row.adjusted.PointsDenominator : '',
      row.adjusted.PointsNumerator != null && row.adjusted.DisplayedGrade != null ? row.adjusted.DisplayedGrade : '',
    ]);

    const now = new Date();
    const blob = await AsSpreadsheet(headers, body);
    const unitName = document.querySelector('div.d2l-navigation-s-header-logo-area a.d2l-navigation-s-link').innerText;
    const filename = `${unitName}_GradesExport_${now.getFullYear()}-${padZero(now.getMonth() + 1)}-${padZero(
      now.getDate()
    )}-${padZero(now.getHours())}-${padZero(now.getMinutes())}.xlsx`;

    saveAs(blob, filename);
  }

  /** widget.querySelector('#MM_grades_analysis_loadGroups').addEventListener('click', loadGroups); */
  /** widget.querySelector('#MM_grades_analysis_clearGroups').addEventListener('click', ClearGroups); */
  loadGroups();

  widget.querySelector('.MM_grades_analysis_activate').addEventListener('click', GetGrades);
  widget.querySelector('#MM_DownloadGrades').addEventListener('click', DownloadGrades);

  const showhideusers = widget.querySelector('.show-student-name');

  showhideusers.addEventListener('change', e => {
    const hide = widget.querySelector('input[name="display-student-name"]:checked').value == 'hidden';
    widget.querySelector('table[show-students]').setAttribute('show-students', hide ? 'false' : 'true');
  });

  showhideusers.dispatchEvent(new Event('change'));

  widget.querySelector('.student-select').addEventListener('change', e => {
    if (e.target.matches('[type="radio"]')) {
      widget.querySelector('.student-select .target-groups').hidden = e.target.value == 'all';
    }
  });

  // #endregion

  /*************************
   *                       *
   *      Merge Grades     *
   *                       *
   *************************/

  // interface MyLOData {
  //   student_id: string; // must pad to compare with leading zeros
  //   username: string;
  //   last_name: string;
  //   first_name: string;
  //   email: string;
  //   calculated_numerator: number;
  //   calculated_denominator: number;
  //   calculated_symbol: string;
  //   adjusted_numerator: number;
  //   adjusted_denominator: number;
  //   adjusted_symbol: string;
  // }

  /**
   * interface SpreadsheetData {
   *  ParentSspNo: number;
   *  ParentSpkCd: string;
   *  ParentSpkVerNo: number;
   *  ParentSspAttNo: number;
   *  ParentSpkAbbrTitle: string;
   *  SspNo: number;
   *  SpkCd: string;
   *  SpkVerNo: number;
   *  SspAttNo: number;
   *  SpkAbbrTitle: string;
   *  Year: number;
   *  StudyPeriod: string;
   *  StudyPeriodDescription: string;
   *  StudentId: number;
   *  StudentName: string;
   *  Grade: string;
   *  Mark: number;
   *  Comments: string;
   * }
   */

  let revertingGrades = false;
  /** The files received from the exams office */
  // let examsFiles: ExcelFile[] = [];

  function getGrade(n) {
    const rounded = Math.round(n);
    if (rounded < 50) return 'NN';
    else if (rounded >= 50 && rounded <= 59) return 'PP';
    else if (rounded >= 60 && rounded <= 69) return 'CR';
    else if (rounded >= 70 && rounded <= 79) return 'DN';
    else if (rounded >= 80 && rounded <= 100) return 'HD';
    else return '?';
  }

  /** Get MyLO gradebook data. */
  function GetMyLOData(code) {
    return GradeItems;
  }

  // Errors
  // Grade change alert - DN -> HD for student ID #123456 in abc123.
  // The following student ID couldn't be found in abc123: 123456

  // Exams File Structure
  // FORMAT STUDY_PACKAGE_RESULT STANDARD
  // Parent Ssp No; Parent Spk Cd; Parent Spk Ver No; Parent Ssp Att No; Parent Spk Abbr Title; Ssp No; Spk Cd; Spk Ver No; Ssp Att No; Spk Abbr Title; Year; Study Period; Study Period Description; Student Id; Student Name; Grade; Mark;

  // Note match by Student Id column, save anything already in Mark column and put it in next column along; rounding errors should be put there too
  let missing = [];
  async function parseExamData({ filename, origtype, rows }) {
    let errors = [];
    missing = [];

    const gradeType = widget.querySelector('input[name="grade-type"]:checked').value;
    const CheckAdjustedTypeFirst = gradeType === 'adjusted';

    const UnitCode = rows[0].SpkCd;

    const UniqueUnitCodesInExamsSheet = Array.from(new Set(rows.flatMap(row => row.SpkCd)));
    const Codes = UnitCodes ?? [];

    if (Codes.length == 0 && UniqueUnitCodesInExamsSheet.length == 1) {
      const msg = `This unit does not have a unit code in the title, while the exam whitesheet refers to '${UniqueUnitCodesInExamsSheet[0]}'. Please double-check the next screen to confirm this is correct.`;
      alert(msg);
    } else if (!UnitCodes?.some(code => UniqueUnitCodesInExamsSheet.includes(code))) {
      // This unit isn't in the unit codes of the exams spreadsheet.
      return alert("This unit doesn't appear in the unit codes of the provided Exams spreadsheet.");
    }

    let usersInSelectedGroups = new Set();
    Array.from(widget.querySelectorAll('#MM_showGroups input:checked'))
      .map(element => element.dataset.enrollments.split(','))
      .flat()
      .forEach(id => usersInSelectedGroups.add(id.toString()));

    const classlist = await api(`/d2l/api/le/1.37/${orgid}/classlist/`).then(res =>
      res.filter(({ RoleId }) => [103, 120].includes(RoleId))
    );

    const UserMap = new Map();
    rows.forEach((row, i) => UserMap.set(row.StudentId.toString(), i));

    /** The dataset extracted from the MyLO Gradebook (excluding Student View) */
    const gradebook = GetMyLOData(UnitCode);

    // If we can't find the dataset.
    if (!gradebook) {
      errors.push(`Could not access MyLO data for ${UnitCode}.`);
      return;
    }

    const ExamStudentIds = Array.from(new Set(rows.flatMap(row => row.StudentId.toString())));

    // Find any students missing from the exams list.
    const missing_from_exams = classlist.filter(
      student => !ExamStudentIds.includes(student.OrgDefinedId.toString().padStart(6, '0'))
    );

    // If there's students missing from the exams list, inform what students there are.
    if (missing_from_exams.length > 0) {
      missing_from_exams.forEach(user => {
        if (gradebook[parseInt(user.Identifier)] != null)
          missing.push({ _state: ATTENTION_REQUEST.MISSING_FROM_EXAMS, ...gradebook[parseInt(user.Identifier)] });
      });
      errors.push(
        `The following student ${plural(
          missing_from_exams.length,
          'ID',
          'IDs'
        )} couldn't be found in the exams file for ${UnitCode}: ${missing_from_exams.map(s => s.OrgDefinedId).join(', ')}`
      );
    }

    /** Find any students in the exams spreadsheet that aren't in the gradebook. */
    const missing_from_mylo = ExamStudentIds.filter(
      id => classlist.find(student => student.OrgDefinedId.toString().padStart(6, '0') == id) == null
    );

    const data = [];

    // If there's students who aren't in MyLO but appear in the gradebook, inform what students were found.
    if (missing_from_mylo.length > 0) {
      missing_from_mylo.forEach(id => {
        const examRow = rows[UserMap.get(id.toString().padStart(6, '0'))];
        data.push(examRow);
        missing.push({ id, _state: ATTENTION_REQUEST.MISSING_FROM_MYLO });
      });

      errors.push(
        `The following student ${plural(
          missing_from_mylo.length,
          'ID',
          'IDs'
        )} couldn't be found in the MyLO data for ${UnitCode}: ${missing_from_mylo.map(missing => missing).join(', ')}`
      );
    }

    // Loop through each row of MyLO data
    classlist.forEach(student => {
      // If the student doesn't appear in the currently selected group, stop here.
      // This will also prevent errors appearing regarding students in the exam document and not the classlist.
      // if (usersInSelectedGroups.size > 0 && !usersInSelectedGroups.has(student.Identifier.toString())) return;

      /** The exams spreadsheet row data. */
      const examRow = rows[UserMap.get(student.OrgDefinedId.toString().padStart(6, '0'))];
      const gradeBookRow = gradebook[parseInt(student.Identifier)];

      //if (!examRow) console.log('Missing exam row: ', student, rows);
      //if (!gradeBookRow) console.log('Missing gradebook row: ', student, gradebook);

      // console.log(gradebook, student.Identifier, parseInt(student.Identifier));

      // If the grade item wasn't found, return here and bail.
      if (!examRow) return;
      if (!gradeBookRow) return;

      examRow._prev = Object.assign({}, examRow);
      examRow._mylo = gradeBookRow;

      if (examRow._prev.Grade.length > 0) console.log(examRow);

      /** The letter grade retrieved from MyLO. */
      let myloLetterGrade = '';

      /** The rounded mark value. */
      let Mark = 0;

      let Scoring;

      // If we want to check the Adjusted column, make sure that the PointsNumerator isn't null.
      // If it is, fall back to the Calculated grade.
      if (CheckAdjustedTypeFirst && gradeBookRow.adjusted?.PointsNumerator != null) {
        Scoring = gradeBookRow.adjusted;
      } else {
        if (CheckAdjustedTypeFirst) {
          // If we're checking the adjusted type first, but we're here, the first `if` failed.
          // Therefore, let's record an error.
          errors.push('Requested Adjusted grades, but students adjusted grade was null: ', gradeBookRow);
        }

        Scoring = gradeBookRow.calculated;
      }

      // console.log(Scoring);

      // set mark to adjusted grade, unless adjusted is zero or checkbox is set
      Mark = Math.round((Scoring.PointsNumerator * 100) / Scoring.PointsDenominator);
      // The letter grade is the calculated value.
      myloLetterGrade = Scoring.DisplayedGrade;

      // The expected grade is what would come from
      const expectedGrade = getGrade(Mark);

      const CommentCellValues = [];

      /**
       * If the gradebook has a letter grade in it already, then just use that and don't touch anything else.
       */
      if (examRow._prev.Grade == '') {
        /**
         * If the MyLO Letter grade is valid (ie. 'HD', 'DN', 'CR', 'PP', or 'NN')
         * but it's different from the provided letter grade, then log the error. (PP -> CR)
         */
        if ('NNPPCRDNHD'.includes(myloLetterGrade)) {
          if (expectedGrade != myloLetterGrade) {
            // errors.push Grade change alert error
            errors.push(
              `Grade change alert - ${myloLetterGrade} -> ${expectedGrade} for student ID #${student.OrgDefinedId} in ${UnitCode}.`
            );

            // append discrepancy to last column (cellAfterMarkCell should be an array of letter grades)
            CommentCellValues.push(myloLetterGrade);
          }
          // Set the Grade column to the expected grade (only if it's NN->HD)
          examRow.Grade = expectedGrade;
        }
        examRow.Mark = Mark;
        examRow.isExamMark = false;
      } else {
        examRow.Mark = 0;
        examRow.isExamMark = true;
      }

      examRow._mylo.Scoring = Scoring;

      // If there's already a grade item in the spreadsheet,
      //  we'll put that in the final column.
      // if (examRow.Grade?.length > 0) {
      //   // append any existing content to last column
      //   CommentCellValues.push(examRow.Grade);
      // }

      // If we want to replace NNs with blank, and their mark is < 50.
      // EDU requirement, all NN should be set to exams last val and zeroed
      // if (ReplaceNNwithANandBlank && Mark < 50) {
      //   examRow.Grade = CommentCellValues[CommentCellValues.length - 1];
      //   Mark = '';
      // }

      examRow.Comments = CommentCellValues.join(', ');

      data.push(examRow);
    });

    ToUserForProcessing(filename, data);
  }

  async function ToUserForProcessing(filename, rows) {
    const dialog = document.createElement('mm-dialog');
    dialog.setAttribute('heading', 'Review issues');
    dialog.setAttribute('open', '');
    dialog.setAttribute('confirm-text', 'Export');
    dialog.setAttribute('max-width', '1100px');

    const desc = document.createElement('p');
    desc.innerText = `Please review these students and select Export when all grades are as intended. No MyLO grade information is modified.`;

    const GetStatus = mark => {
      // If it's close to a letter grade threshold
      const willRoundUp = [59, 69, 79].includes(mark);

      // If it's close to a pass
      const nearPass = [48, 49].includes(mark);

      return { willRoundUp, nearPass };
    };

    const CheckRow = row => {
      const field = row.querySelector('.mark-number');
      const mark = parseInt(field.value);
      const { willRoundUp, nearPass } = GetStatus(mark);

      if (willRoundUp) row.title = 'This mark ends in 9 and is close to a letter grade threshold.';
      else if (nearPass) row.title = 'This mark is close to a passing grade.';
      else if (row.hasAttribute('warning-title')) row.title = row.getAttribute('warning-title');
      else row.title = '';

      row.classList.toggle('warn', willRoundUp);
      row.classList.toggle('closeToPassing', nearPass);
    };

    const OnMarkChange = e => {
      const row = e.target.closest('tr');
      const NN2AN = config.querySelector('[name="convert"]').checked;
      const gradeRegion = row.querySelector('[data-purpose="potential-grade"]');
      // const RecognisedGrade = JSON.parse(row.dataset.recognisedGrade);

      // If we're auto converting NN to AN, then we want to check if the value is less than 50.
      // If the checkbox is unselected, AND the value is less than 50, set it to zero.
      // In any other case, use the original value.
      const orig = parseInt(e.target.value);
      const reverting = revertingGrades;
      const dataset = parseInt(row.dataset.value);
      let number = orig;

      if (JSON.parse(row.dataset.isExamGrade ?? 'false') && orig == 0) {
        // If we're not auto-converting NN grades, save the number to the dataset.
        // This prevents overriding the known grade with a zero (raequires more math to unset).
        row.dataset.value = 0;

        // If this was autoconverted, set the letter grade to 'AN'.
        // Otherwise, use our normal metric.
        gradeRegion.value = row.dataset.letterGrade;
      } else {
        if (NN2AN && orig < 50) number = 0;
        if (reverting && dataset != null) number = dataset;
        if (!Number.isNaN(number) && (number < 0 || number > 100)) number = MathContain(number, 0, 100);

        e.target.value = (reverting && dataset == null) || Number.isNaN(number) ? '' : number;
        // e.target.toggleAttribute('readonly', NN2AN && number === 0);

        // if (RecognisedGrade) {
        //   gradeRegion.toggleAttribute('readonly', number >= 50);
        // }

        // If we're not auto-converting NN grades, save the number to the dataset.
        // This prevents overriding the known grade with a zero (raequires more math to unset).
        if (!NN2AN) row.dataset.value = number;

        // If this was autoconverted, set the letter grade to 'AN'.
        // Otherwise, use our normal metric.
        gradeRegion.value = NN2AN && number === 0 ? 'AN' : getGrade(number);
      }
      CheckRow(row);
    };

    const table = document.createElement('tbody');
    const headers = [
      {
        key: 'sid',
        text: 'Student ID',
        resource: o => o.data.StudentId,
      },
      {
        key: 'name',
        text: 'Name',
        resource: o => o.data._mylo.student.LastName,
      },
      {
        key: 'mgrade',
        text: 'MyLO Grade',
        tooltip: 'These grades are from the MyLO Grades tool',
        resource: o => o.data.NumberMark,
      },
      {
        key: 'egrade',
        text: 'Exams Grade',
        tooltip: 'These grades are already in the exams spreadsheet',
        resource: o => o.data.StudentId,
      },
      {
        key: 'markout',
        text: 'Export Mark',
        tooltip: 'The mark to be sent to the Exams office',
        resource: o => o.row.querySelector(`[data-key="markout"] input`).value,
      },
      {
        key: 'gradeout',
        text: 'Export Grade',
        tooltip: 'The letter grade to be sent to the Exams office',
        resource: o => o.row.querySelector(`[data-key="gradeout"] input`).value,
      },
    ];

    const gradeType = widget.querySelector('input[name="grade-type"]:checked').value;
    const CheckAdjustedTypeFirst = gradeType === 'adjusted';

    /** The dataset extracted from the MyLO Gradebook (excluding Student View) */
    // const gradebook = GetMyLOData(UnitCode);
    // const gradeBookRow = gradebook[parseInt(student.Identifier)];

    missing.forEach(row => {
      if (row._state == ATTENTION_REQUEST.MISSING_FROM_EXAMS) {
        const Scoring = CheckAdjustedTypeFirst && !row.adjusted?.PointsNumerator ? row.adjusted : row.calculated;
        const tr = document.createElement('tr');
        tr.classList.add('missing');
        tr.title = 'This student could not be found in the exams spreadsheet.';
        tr.innerHTML = `<td>${row.student.OrgDefinedId.toString().padStart(6, '0')} <mm-help></mm-help></td>
        <td>${row.student.FirstName} ${row.student.LastName}</td>
        <td>${Scoring.DisplayedGrade} (${((Scoring.PointsNumerator * 100) / Scoring.PointsDenominator).toFixed(
          2
        )})  <small class="mark-raw">${(Scoring.PointsNumerator ?? 0).toFixed(2)}/${Scoring.PointsDenominator}</small></td>
        <td>MISSING</td>
        <td></td>
        <td></td>`;
        table.append(tr);
      }
    });

    const collection = [];
    rows
      .map(row => {
        if (!row._mylo) {
          // console.warn('Student missing from MyLO gradebook: ', row);
          return {
            ...row,
            _mylo: { student: { LastName: row.StudentName, FirstName: '' } },
            NumberMark: null,
            Scoring: { DisplayedGrade: '' },
            errorState: 6,
            class: ['missing'],
          };
        }

        const Scoring = row._mylo.Scoring;
        row.NumberMark = (Scoring.PointsNumerator * 100) / Scoring.PointsDenominator;
        row.class = [];

        const { willRoundUp, nearPass } = GetStatus(parseInt(row.Mark));
        row.errorState = 0;

        if (willRoundUp) {
          row.class.push('warn');
          row.errorState = 1;
        }
        if (nearPass) {
          row.class.push('closeToPassing');
          row.errorState = 1;
        }

        // If the mark was rounded up to a 50/60/70/80,
        // this indicates that rounding will change the letter score.
        if (row.NumberMark < row.Mark && [50, 60, 70, 80].includes(row.Mark)) {
          row.class.push('error');
          row.errorState = 3;
        }

        return row;
      })
      .filter(a => a) // Get rid of null values.
      .forEach(row => {
        const NN2AN = config.querySelector('[name="convert"]').checked;
        const RecognisedGrade = 'NNPPCRDNHD'.includes(row.Grade);

        const r = document.createElement('tr');
        r.classList.add(...(row.class ?? []));
        r.dataset.value = row.Mark;
        r.dataset.id = row.StudentId;
        r.dataset.recognisedGrade = RecognisedGrade;
        r.dataset.isExamGrade = row.isExamMark ?? false;
        r.dataset.letterGrade = row.Grade ?? false;

        if (row.errorState == 3) {
          // Rounding error
          r.title = 'Rounding this mark will cause a letter grade change. Please confirm this is intended.';
        } else if (row.errorState == 6) {
          r.title = 'This student could not be found in the MyLO classlist.';
        } else if (row._prev?.Grade) {
          r.title = 'The exams spreadsheet contains a grade from student management for this student';
        }

        if (r.title.length > 0) r.setAttribute('warning-title', r.title);
        r.innerHTML = `
        <td data-key="sid">${row.StudentId.toString().padStart(6, '0')} ${
          r.title.length > 0 ? '<mm-help></mm-help>' : ''
        }</td>
        <td data-key="name">${row._mylo.student.FirstName} ${row._mylo.student.LastName}</td>
        <td data-key="mgrade" data-purpose="mylo-data">${
          row.errorState == 6
            ? 'MISSING'
            : `${row._mylo.Scoring.DisplayedGrade} (${isNaN(row.NumberMark) ? 0 : row.NumberMark?.toFixed(2)})`
        } <small class="mark-raw">${row._mylo.Scoring?.PointsNumerator?.toFixed(2) ?? 0}/${
          row._mylo.Scoring?.PointsDenominator ?? 0
        }</small></td>
        <td data-key="egrade">${row.errorState == 6 ? row.Grade : row._prev?.Grade ?? ''}</td>
        <td data-key="markout"><input type="number" value="${
          row.errorState == 6 && row.Grade != ''
            ? 0
            : (row._prev?.Grade && !'NNPPCRDNHD'.includes(row._prev?.Grade) ? 0 : null) ??
              (RecognisedGrade && NN2AN && (isNaN(row.Mark) || row.Mark < 50) ? 0 : row.Mark)
        }" min="0" max="100" class="d2l-edit vui-edit mark-number" /></td>
        <td data-key="gradeout"><input type="text" value="${
          (!'NNPPCRDNHD'.includes(row._prev?.Grade) ? row._prev?.Grade : false) ||
          (RecognisedGrade && NN2AN && (isNaN(row.Mark) || row.Mark < 50) ? 'AN' : row.Grade)
        }" minLength="2" maxLength="2" class="d2l-edit vui-edit mark-letter" data-purpose="potential-grade"/></td>
        `;
        table.append(r);
        CheckRow(r);

        collection.push({ data: row, row: r });
      });

    const tbl = document.createElement('table');
    tbl.classList.add('clean-table');
    tbl.innerHTML = `<thead><tr>${headers
      .map(
        hdr => `
        <th${hdr.tooltip ? ` title="${hdr.tooltip}"` : ''} data-key="${hdr.key}">
          <a class="d2l-link" data-purpose="sort">  
            ${typeof hdr === 'string' ? hdr : hdr.text} 
            <d2l-icon 
              class="sort-col-icon" data-dir="asc" 
              data-target="${hdr.key}" title="Sorted by ${hdr.text ?? hdr} (asc)" 
              icon="tier1:arrow-toggle-up">
            </d2l-icon>
            <d2l-icon 
              class="sort-col-icon" data-dir="desc" 
              data-target="${hdr.key}" title="Sorted by ${hdr.text ?? hdr} (desc)" 
              icon="tier1:arrow-toggle-down">
            </d2l-icon>
          </a>
          ${hdr.tooltip ? '<mm-icon></mm-icon>' : ''} 
        </th>`
      )
      .join('')}</tr></thead>`;
    // tbl.querySelector('.sort-col-icon[data-target="name"][data-dir="desc"]').toggleAttribute('active', true);

    // Make sure it hasn't already been applied somehow.
    tbl.querySelectorAll('th[data-dir]').forEach(th => th.removeAttribute('data-dir'));
    // Add the default sort rule (found at the top of the document).
    tbl.querySelector(`th[data-key="${DefaultSort.column}"]`).setAttribute('data-dir', DefaultSort.direction);

    tbl.addEventListener('click', e => {
      if (!e.target.matches('a[data-purpose="sort"]')) return;

      const hdr = e.target.closest('th');

      tbl.querySelectorAll('th[data-dir]').forEach(th => {
        if (th != hdr) th.removeAttribute('data-dir');
      });

      const isAsc = hdr.dataset.dir != 'desc';
      hdr.dataset.dir = isAsc ? 'desc' : 'asc';

      sort();
    });

    tbl.append(table);

    const ShouldShowExamsCol =
      missing.concat(
        rows.filter(row => {
          try {
            return row.errorState === 6 || row._prev?.Grade?.toString().trim().length > 0;
          } catch (e) {
            console.error(e, row);
            return false;
          }
        })
      ).length > 0;
    tbl.classList.toggle('hide-exams', !ShouldShowExamsCol);

    table.addEventListener('change', e => {
      if (e.target.matches('.mark-number')) {
        OnMarkChange(e);
      }
    });

    const placeholder = document.createElement('tbody');
    placeholder.innerHTML = `<tr style="height: 100%;"><td colspan="${headers.length}"><mm-loader center indeterminate width="30"></mm-loader></td></tr>`;
    function sort() {
      tbl.replaceChild(placeholder, table);

      const sorter = tbl.querySelector('th[data-dir]');
      const header = headers.find(hdr => hdr.key == sorter.dataset.key);

      const ShouldGroupErrors = config.querySelector('[name="group"]').checked;
      const rows = collection
        .sort((a, b) => {
          const left = header.resource(a);
          const right = header.resource(b);
          const dir = left > right ? 1 : left < right ? -1 : 0;
          return dir * (sorter.dataset.dir == 'asc' ? 1 : -1);
        })
        .sort((a, b) => {
          if (ShouldGroupErrors) {
            if (a.data.errorState > b.data.errorState) return -1;
            if (a.data.errorState < b.data.errorState) return 1;
          }
        })
        .map(d => d.row);

      table.append(...rows);

      tbl.replaceChild(table, placeholder);
    }

    // When we toggle the "Group Errors" cb, re-sort the rows.
    const GroupErrors = config.querySelector('[name="group"]');
    GroupErrors.addEventListener('change', e => sort());
    if (GroupErrors.checked) sort();

    config.addEventListener('change', e => {
      const rounding = config.querySelector('[name="rounding"]:checked').value;
      const convert = config.querySelector('[name="convert"]').checked;

      table.classList.toggle('showOn9', rounding == 'endingIn9');
      table.querySelectorAll('input.mark-number').forEach(input => {
        if (parseInt(input.value) < 50) {
          revertingGrades = !convert;
          input.dispatchEvent(new Event('change', { bubbles: true }));
          revertingGrades = false;
        }
      });
    });

    dialog.append(style, desc, tbl);
    desc.append(openControlPanel);
    document.body.append(dialog);
    // Make sure all our transient changes take effect, along with saving our unscoped settings to PanelSettings
    config.dispatchEvent(new Event('change'));

    table.classList.toggle('showOn9', config.querySelector('input[type="radio"]:checked').value == 'endingIn9');
    dialog.addEventListener('ok', () => {
      let data = [].concat(rows);
      data.forEach(row => {
        const r = table.querySelector(`tr[data-id="${row.StudentId}"]`);
        row.Mark = r.querySelector('.mark-number').value;
        row.Grade = r.querySelector('.mark-letter').value;
      });
      save(filename, data);
    });
  }

  const workbookToCSV = sheet => {
    let csv = [];
    sheet._rows.forEach(row =>
      csv.push(
        row.values
          .filter(v => v)
          .map(v => `"${v}"`)
          .join(',')
      )
    );
    return csv.join('\r\n');
  };

  async function save(filename, rows) {
    const format = PanelSettings.ResultsMergeFormat;
    const comments = PanelSettings.ResultsMergeExportComments;

    const me = await api('/d2l/api/lp/1.28/users/whoami', { first: true });
    const workbook = new ExcelJS.Workbook();
    workbook.creator = `MyLO MATE`;
    workbook.lastModifiedBy = `${me.FirstName} ${me.LastName}`;
    workbook.modified = new Date();

    const sheet = workbook.addWorksheet();
    sheet.name = filename.split('.').slice(0, -1).join('.'); // Use the filename but drop the extension.
    sheet.addRow([ExamSheetPageTitle]);
    sheet.addRow(ExamSheetHeaderFields);
    rows.forEach(row => {
      sheet.addRow(
        Object.keys(ExamDataHeaders).map(
          key =>
            (ExamDataHeadersNumerical.includes(key)
              ? isNaN(parseInt(row[key], 10))
                ? null
                : parseInt(row[key], 10)
              : row[key].toString()) ?? ''
        )
      );
    });

    sheet.columns.forEach(col => {
      if (col.number <= 13) {
        // Column A:M
        col.width = 1;
      } else {
        // Columns N:<end>

        // Find the longest value in the column
        let longest = col.values.reduce((o, a) => {
          const len = a.toString().trim().length;
          return len > o ? len : o;
        }, 0);

        col.width = longest + 4;
      }
    });

    if (!comments) {
      /**
       * If we don't want the Comments column, remove it here.
       * It's easier to remove it now, rather than conditionally add it.
       */
      sheet.spliceColumns(18, 1);
    }

    // Save as either CSV or XLSX, depending on the format selected in the Settings panel.
    const blob = new Blob([format == 'xlsx' ? await workbook.xlsx.writeBuffer() : workbookToCSV(workbook.getWorksheet(1))]);
    const url = URL.createObjectURL(blob);
    // window.open(url, '_blank'); // Open blob in new tab for debugging.
    const a = document.createElement('a');
    a.download = `${sheet.name}-merge.${format}`;
    a.href = url;
    a.click();
  }

  async function ProcessSheet(file) {
    const ext = file.name.split('.').pop().toLowerCase();
    switch (ext) {
      case 'csv': {
        return ProcessCSV(file);
      }
      case 'xlsx': {
        return ProcessXLSX(file);
      }
      default: {
        return alert('Unknown file type.');
      }
    }
  }

  async function ProcessCSV(file) {
    let data = {
      filename: file.name,
      origtype: 'csv',
      rows: [],
    };

    const csv = await new Promise(res =>
      Papa.parse(file, { complete: r => res(r), skipEmptyLines: true, dynamicTyping: true })
    ).then(d => {
      d.data = d.data.filter(v => v.filter(Boolean).length > 0);
      return d;
    });

    if (csv.data[1][0] != 'Parent Ssp No') {
      alert('Document not recognised.');
      return {};
    }

    for (let i = JunkDataRowOffset; i < csv.data.length; i++) {
      const row = csv.data[i];
      data.rows.push({
        ParentSspNo: row[0] ?? '',
        ParentSpkCd: row[1] ?? '',
        ParentSpkVerNo: row[2] ?? '',
        ParentSspAttNo: row[3] ?? '',
        ParentSpkAbbrTitle: row[4] ?? '',
        SspNo: row[5] ?? '',
        SpkCd: row[6] ?? '',
        SpkVerNo: row[7] ?? '',
        SspAttNo: row[8] ?? '',
        SpkAbbrTitle: row[9] ?? '',
        Year: row[10] ?? '',
        StudyPeriod: row[11] ?? '',
        StudyPeriodDescription: row[12] ?? '',
        StudentId: (row[13] ?? '').toString().padStart(6, '0'),
        StudentName: row[14] ?? '',
        Grade: row[15] ?? '',
        Mark: row[16] ?? '',
        Comments: row[17] ?? '',
      });
    }

    return data;
  }

  async function ProcessXLSX(file) {
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(await file.arrayBuffer());

    const sheet = workbook.getWorksheet(1);

    const header = sheet.getRow(2);
    if (header.getCell(1).value != 'Parent Ssp No') {
      alert('Document not recognised.');
      return {};
    }

    let data = {
      filename: file.name,
      origtype: 'xlsx',
      rows: [],
    };

    sheet.eachRow((row, index) => {
      if (index <= JunkDataRowOffset) return;
      data.rows.push({
        ParentSspNo: row.getCell(1).value ?? '',
        ParentSpkCd: row.getCell(2).value ?? '',
        ParentSpkVerNo: row.getCell(3).value ?? '',
        ParentSspAttNo: row.getCell(4).value ?? '',
        ParentSpkAbbrTitle: row.getCell(5).value ?? '',
        SspNo: row.getCell(6).value ?? '',
        SpkCd: row.getCell(7).value ?? '',
        SpkVerNo: row.getCell(8).value ?? '',
        SspAttNo: row.getCell(9).value ?? '',
        SpkAbbrTitle: row.getCell(10).value ?? '',
        Year: row.getCell(11).value ?? '',
        StudyPeriod: row.getCell(12).value ?? '',
        StudyPeriodDescription: row.getCell(13).value ?? '',
        StudentId: (row.getCell(14).value ?? '').toString().padStart(6, '0'),
        StudentName: row.getCell(15).value ?? '',
        Grade: row.getCell(16).value ?? '',
        Mark: row.getCell(17).value ?? '',
        Comments: row.getCell(18).value ?? '',
      });
    });

    return data;
  }

  const HandleFiles = async files => {
    if (files.length > 1) {
      alert('Please select one file only.');
      return;
    } else if (files.length == 0) {
      alert('Please select a file.');
      return;
    } else {
      let examsFiles = await Promise.all(Array.from(files).map(file => ProcessSheet(file)));

      if (examsFiles?.length > 0) parseExamData(examsFiles[0]);
    }
  };

  widget.querySelector('#MM_MergeGrades').addEventListener('click', e => {
    const dialog = document.createElement('mm-dialog');
    dialog.setAttribute('open', 'true');
    dialog.toggleAttribute('file-target', true);
    dialog.toggleAttribute('ready', true);
    dialog.setAttribute('max-width', '550px');
    dialog.setAttribute('heading', 'Select the Exam Office White Sheet');

    const description =
      'Please choose the matching Exam Office White Sheet for this unit, then select OK to save the merged exams file.';

    const desc = document.createElement('p');
    desc.innerText = description;

    desc.append(openControlPanel);

    const file = document.createElement('mm-file-zone');
    file.accept = '.xlsx,.csv';
    file.toggleAttribute('draganddrop', true);
    file.toggleAttribute('clicktobrowse', true);
    file.toggleAttribute('showDropZone', true);

    dialog.addEventListener('ok', e => HandleFiles(file.files));
    dialog.append(desc, file);
    document.body.append(dialog);
  });

  widget.addEventListener('file-drop', ({ detail: { files } }) => {
    if (FileTarget.hasAttribute('ready')) {
      HandleFiles(files);
    }
  });

  window.addEventListener('resize', () => {
    window.requestAnimationFrame(() => {
      for (const chart of Object.values(Chart.instances)) {
        chart.resize();
      }
    });
  });
}
