import { CheckPages } from './CheckForLTI.mod.mjs';

export default async function ({ widget, orgid, config }) {
  const start = widget.querySelector('[role="start-scan"]');
  const loader = widget.querySelector('mm-loader');
  const table = widget.querySelector('table[data-type="results"]');

  const filter = widget.querySelector('#filter');
  const semester = widget.querySelector('#semester');
  const year = widget.querySelector('#year');

  const now = new Date();
  for (let i = -1; i < 2; i++) {
    let y = now.getFullYear() + i;
    let o = document.createElement('option');
    o.value = y - 2000;
    o.innerText = y;
    if (i == 0) o.selected = true;
    year.appendChild(o);
  }

  start.addEventListener('click', async () => {
    table.querySelectorAll('tbody').forEach(el => el.remove());

    loader.innerText = 'Fetching enrollments...';
    loader.indeterminate = true;
    loader.hidden = false;

    const units = await api(`/d2l/api/lp/1.31/enrollments/myenrollments/?canAccess=true&orgUnitTypeId=3`, {
      fresh: true,
    }).then(units => {
      const search = filter.value.trim() || '.*';
      const period = year.value + semester.value;
      return units.filter(
        row => row.OrgUnit.Code.split('_')[2] == period && new RegExp(search, 'gi').test(row.OrgUnit.Name)
      );
    });

    loader.value = 0;
    loader.max = units.length;
    loader.innerText = 'Fetching tables of content';

    const topics = await Promise.all(
      units.map(async unit => {
        const topics = await GetUnitModule({ OrgID: unit.OrgUnit.Id });
        loader.value++;

        return topics
          .filter(topic => topic.TypeIdentifier == 'File' && topic.Url?.endsWith('.html'))
          .map(topic => {
            topic.UnitData = unit;
            return topic;
          });
      })
    ).then(d => d.flat());

    const urls = topics
      .filter(v => v.Url)
      .map(t => {
        const href = new URL(t.Url, window.location.origin).href;
        return { href, key: t.TopicId };
      });

    if (urls.length == 0) {
      loader.hidden = true;
      const msg = document.createElement('tbody');
      msg.innerHTML = `<tr><td colspan="2">No pages found.</td></tr>`;
      table.append(msg);
      return;
    }

    const filters = [
      'bbcollab.com/recording/',
      'padlet.com',
      'discussions/List',
      'discussions/messageLists',
      'pebblepad.com.au',
      'rl.talis.com/3/utas/lists/',
    ];
    const tags = ['a', 'iframe'];

    const ignore = ['padlet.com?ref=embed'];

    const keys = {
      Collaborate: 'bbcollab.com/recording/',
      Padlet: 'padlet.com',
      Discussions: ['discussions/List', 'discussions/messageLists'],
      PebblePad: 'pebblepad.com.au',
    };

    loader.value = 0;
    loader.max = urls.length;
    loader.innerText = 'Checking content';
    let matches = 0;

    const check = CheckPages(urls, { filters, tags, ignore, poolsize: Math.min(20, urls.length) });
    check.subscribe(
      result => {
        loader.value++;
        if (result.matches.length > 0) {
          const topic = topics.find(t => t.TopicId == result.key);
          const id = topic.UnitData.OrgUnit.Id;

          matches++;

          // Find the relevant container.
          let body = table.querySelector(`tbody[data-orgid="${id}"]`);

          if (!body) {
            body = document.createElement('tbody');
            body.dataset.orgid = id;

            const header = document.createElement('tr');
            header.innerHTML = `<th colspan="2">${topic.UnitData.OrgUnit.Name}</th>`;
            body.append(header);

            table.append(body);
          }

          const counts = [];
          for (const [label, urls] of Object.entries(keys)) {
            const u = AsArray(urls);
            const matches = u.flatMap(url =>
              result.matches.filter(match => match.toLowerCase().includes(url.toLowerCase()))
            );

            if (matches.length > 0) {
              counts.push(`<strong>${label}</strong>: ${matches.length}`);
            }
          }

          const row = document.createElement('tr');
          row.innerHTML = `
        <td><a class="d2l-link" href="/d2l/le/content/${id}/viewContent/${result.key}/View" target="_blank">${
            topic.Title
          }</a></td>
        <td>${counts.join('<br>')}</td>`;

          body.append(row);
        }
      },
      err => {
        loader.value++;
        console.error(err);
      },
      () => {
        loader.hidden = true;
        if (matches == 0) {
          const msg = document.createElement('tbody');
          msg.innerHTML = `<tr><td colspan="2">No results found.</td></tr>`;
          table.append(msg);
        }
      }
    );
  });
}
