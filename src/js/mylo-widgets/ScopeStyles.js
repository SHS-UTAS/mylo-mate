let scopedStyles = document.querySelectorAll('style[widget-scope]');
scopedStyles.forEach(style => {
	let target = style.getAttribute('widget-scope');
	let stylesheet = Object.values(document.styleSheets).find(css => css.ownerNode == style);
	Array.from(stylesheet.rules).forEach(rule => { if (!rule.selectorText.trim().startsWith(`[mm-widget-name="${target}"]`)) { rule.selectorText = `[mm-widget-name="${target}"] ${rule.selectorText}` } });
});