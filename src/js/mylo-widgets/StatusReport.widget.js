/**
 * @affects MyLO Widgets
 * @problem When preparing a unit starting from a previous delivery there are many steps that need to be followed. This process is tedious and repetitive, and sometimes steps are be missed.
 * @solution
 *  A widget examines the unit, stepping through the common checks to ensure that the unit is adhering to certain standards.
 *  These tests currently include checking whether a unit outline exists, ensuring the unit outline is not a placeholder document, whether the ICB template exists in this unit, what the current default template is (and offers to set to D2L if none set), checking what templates are being used in the HTML content (and warning on those that don't match the default template), whether there are announcements that were published more than 1 month before the open date of the unit, whether there are any students that aren't assigned to a group, whether there are any hidden assessment folders, whether the reading lists have been populated.
 * @target Academic and support staff
 * @impact High
 * @savings 3 hours per unit
 */

import jss from '../../lib/jss.bund.js';
import { CheckPages } from './CheckForLTI.mod.mjs';

export default async function ({ widget, orgid, config }) {
  const { Path, Name: UnitName } = await api(`/d2l/api/lp/1.26/courses/${orgid}`, { first: true });
  const metaTag = document.head.querySelector('meta[name="mylo-mate"]');
  const extensionID = metaTag ? metaTag.getAttribute('content') : '';
  const loader = widget.querySelector('mm-loader');
  const heading = widget.querySelector('.heading');
  const unitname = widget.querySelector('.unitname');
  const timestamp = widget.querySelector('.timestamp');
  const progressText = widget.querySelector('.progress-text');
  const results = widget.querySelector('#results_text');
  const run = widget.querySelector('#activate');

  pdfjsLib.GlobalWorkerOptions.workerSrc = `chrome-extension://${extensionID}/lib/pdf.worker.min.js`;

  const { classes } = jss
    .createStyleSheet({
      listHeading: {
        'cursor': 'pointer',
        '&:hover': {
          textDecoration: 'underline',
        },
      },
      listItemParagraph: {
        display: 'inline-grid',
        margin: '0 !important',
      },

      listItemBreadcrumb: {
        fontSize: 'smaller',
        color: '#888',
        display: 'block',
      },
    })
    .attach();

  const RemoteFileExists = url =>
    fetch(url, {
      method: 'head',
      redirect: 'error',
    })
      .then(d => d.status == 200)
      .catch(() => false);

  const HelpNode = text => {
    let help = document.createElement('d2l-icon');
    help.setAttribute('icon', 'd2l-tier1:help');
    help.title = text;
    return help;
  };

  const hidden = item => (item.IsHidden ? ' <d2l-icon icon="tier1:visibility-hide" title="Item is hidden"></d2l-icon>' : '');

  const result = ({ title, desc, help, content, warning }) => {
    const block = document.importNode(widget.querySelector('#outcome-text').content, true);
    const header = block.querySelector('.outcome-header');
    /** const description = block.querySelector('.outcome-description'); */
    const result = block.querySelector('.outcome-result');

    header.innerText = title;
    header.appendChild(HelpNode(help));

    /** description.innerText = desc; */

    result.innerHTML = '';

    if (typeof content == 'string') result.innerHTML = content;
    else result.append(content);

    return block;
  };
  /**
   *
   *   const newResultText = (title, description, text, isWarn, asHTML) => {
   *     let block = document.importNode(widget.querySelector('#outcome-text').content, true);
   *     block.querySelector('.outcome-header').innerText = title;
   *     block.querySelector('.outcome-header').appendChild(HelpNode(description));
   *
   *     if (asHTML) {
   *       block.querySelector('.outcome-result').innerText = '';
   *       block.querySelector('.outcome-result').append(text);
   *     } else block.querySelector('.outcome-result').innerText = text;
   *
   *     if (isWarn) block.querySelector('.outcome-result').classList.add('warn');
   *     return block;
   *   };
   *
   *   const newResultList = (
   *     title,
   *     description,
   *     items,
   *     optionalText,
   *     isWarn,
   *     collapseable = false,
   *     isCollapsed = false,
   *     highlightSummaryInstead = false
   *   ) => {
   *     let block = document.importNode(widget.querySelector('#outcome-list').content, true);
   *     const icon = document.createElement('d2l-icon');
   *     icon.style.marginRight = '10px';
   *     icon.setAttribute('icon', `tier1:arrow-${isCollapsed ? 'expand' : 'collapse'}`);
   *
   *     const heading = block.querySelector('.outcome-header');
   *     heading.classList.add(classes.listHeading);
   *
   *     if (!title) heading.remove();
   *     else {
   *       heading.innerText = title;
   *       heading.prepend(icon);
   *     }
   *
   *     if (description) heading.appendChild(HelpNode(description));
   *
   *     if (optionalText != null) block.querySelector('.outcome-optional-text').innerText = optionalText;
   *     else block.querySelector('.outcome-optional-text').remove();
   *
   *     const list = block.querySelector('.outcome-result');
   *     if (isWarn) {
   *       if (highlightSummaryInstead && optionalText != null)
   *         block.querySelector('.outcome-optional-text').classList.add('warn');
   *       else list.classList.add('warn');
   *     }
   *
   *     list.toggleAttribute('collapsed', collapseable && isCollapsed);
   *     heading.parentNode.toggleAttribute('collapsed', collapseable && isCollapsed);
   *
   *     heading.addEventListener('click', e => {
   *       const collapsed = list.toggleAttribute('collapsed');
   *       icon.setAttribute('icon', `tier1:arrow-${collapsed ? 'expand' : 'collapse'}`);
   *       heading.parentNode.toggleAttribute('collapsed', collapsed);
   *     });
   *
   *     items.forEach(item => {
   *       let li = document.createElement('li');
   *       if (typeof item == 'string') li.innerHTML = item;
   *       else li.append(item);
   *       list.append(li);
   *     });
   *     return block;
   *   };
   */

  const basicTemplateSrc = '/shared/content-template/v3.0/pages/Basic%20Page.html';
  async function GetUnitSettings(id) {
    const dom = new DOMParser().parseFromString(
      await fetch(`/d2l/le/content/${id}/settings/Open`).then(d => d.text()),
      'text/html'
    );
    const form = dom.querySelector('form');
    const fd = new URLSearchParams(new FormData(form));
    const items = {};
    for (const [k, v] of fd) {
      items[k] = v;
    }
    return items;
  }

  function iterate(base) {
    let output = [],
      start = base;
    const loop = node => {
      if (node.Modules.length > 0) node.Modules.forEach(loop);
      if (node.Topics.length > 0) {
        output = output.concat(
          node.Topics.map(t => {
            t.TopicName = node.Title;
            return t;
          })
        );
      }
    };
    if (Array.isArray(start)) start.forEach(loop);
    else loop(start);
    return output;
  }

  let info = {};
  let unitInfo = await api(`/d2l/api/lp/1.21/enrollments/myenrollments/${orgid}`, { first: true });
  const UnitOutline = {
    Single: 1,
    PerUnitCode: 2,
    OnCampusOffCampus: 3,
  };
  let codeSplit = unitInfo.OrgUnit.Code.split('_');
  let UnitCodes = unitInfo.OrgUnit.Name.match(/[A-Z]{3}[0-9]{3}/g);

  while (codeSplit.length < 9) codeSplit.push('0');
  info.unitOutlines = parseInt(codeSplit[6]) + 1;
  // Unit Outline UnitCodes are:
  //	1: 'Single Outline',
  //	2: 'One per Unit Code',
  //	3: 'OnCampus/OffCampus'

  info.readingLists = codeSplit[7] != '0';

  const CurrentTemplate = async () => {
    const settings = await GetUnitSettings(orgid);
    if (/^\/shared\/content-template\/[^\/]+\/pages\/$/i.test(settings.defaultTemplatePathSelector)) {
      return 'D2L';
    } else if (/^\/content\/enforced\/[^\/]+\/template\/$/i.test(settings.defaultTemplatePathSelector)) {
      return 'ICB';
    } else {
      return null;
    }
  };

  const checks = [
    {
      title: 'Unit Outline',
      description: 'Confirms that the UnitOutline.pdf file exists, and is not a placeholder.',
      callback: async () => {
        // Test that the file exists at all.
        let exists = false;
        let UnitOutlineType = info.unitOutlines;
        if (UnitOutlineType == UnitOutline.Single) {
          exists = await RemoteFileExists(`${Path}UnitOutline/UnitOutline.pdf`);
        }
        if (UnitOutlineType == UnitOutline.PerUnitCode) {
          exists = await Promise.all(
            UnitCodes.map(code => RemoteFileExists(`${Path}UnitOutline/${code}_UnitOutline.pdf`))
          ).then(v => !v.some(v => !v));
        }
        if (UnitOutlineType == UnitOutline.OnCampusOffCampus) {
          exists = await Promise.all(
            ['OnCampus', 'OffCampus'].map(code => RemoteFileExists(`${Path}UnitOutline/${code}_UnitOutline.pdf`))
          ).then(v => !v.some(v => !v));
        }

        // The file doesn't exist, so lets just fail here.
        if (!exists)
          return {
            exists,
            valid: false,
          };

        // The file exists, so lets test validity
        let valid = false;
        const TestIfValidUnitOutline = async path => {
          if (!(await RemoteFileExists(path))) return false;

          let pdf = await pdfjsLib.getDocument(`${path}?ts=` + Date.now()).promise;
          let str = '';

          for (let i = 0; i < pdf.numPages; i++) {
            str += (await (await pdf.getPage(i + 1)).getTextContent()).items.map(item => item.str).join('');
          }
          let text = str.trim().toLowerCase();
          return !text.includes('unit outline unavailable');
        };

        try {
          let UnitOutlineType = info.unitOutlines;
          if (UnitOutlineType == UnitOutline.Single) {
            valid = [await TestIfValidUnitOutline(`${Path}UnitOutline/UnitOutline.pdf`)];
          }
          if (UnitOutlineType == UnitOutline.PerUnitCode) {
            valid = await Promise.all(
              UnitCodes.map(code => TestIfValidUnitOutline(`${Path}UnitOutline/${code}_UnitOutline.pdf`))
            ).then(v => !v.some(v => !v));
          }
          if (UnitOutlineType == UnitOutline.OnCampusOffCampus) {
            valid = await Promise.all(
              ['OnCampus', 'OffCampus'].map(code => TestIfValidUnitOutline(`${Path}UnitOutline/${code}_UnitOutline.pdf`))
            ).then(v => !v.some(v => !v));
          }
        } catch (e) {
          console.error('Error processing PDF file.', e);
        }

        return {
          valid,
          exists,
        };
      },
      render: async function (data) {
        const obj = {
          title: this.title,
          help: this.description,
          content: '',
          warning: false,
        };

        if (!data.exists) {
          obj.content = `Unit ${plural(data.length, 'outline', 'outlines')} missing.`;
          obj.warning = true;
        } else if (!data.valid) {
          obj.content = `Unit ${plural(data.length, 'outline', 'outlines')} contain placeholder text.`;
          obj.warning = true;
        } else {
          obj.content = `Unit outline${data.length > 1 ? 's' : ''} valid.`;
        }

        return result(obj);
      },
    },
    {
      title: 'Available templates',
      description: `A list of what template options are found to be available in this unit.`,
      callback: async () => {
        return {
          d2l: await RemoteFileExists(basicTemplateSrc),
          icb: await RemoteFileExists(`${Path}template/template.html`),
        };
      },
      render: async function (data) {
        let found = [];
        if (data.icb) found.push('ICB Template');
        if (data.d2l) found.push('D2L Template');

        return result({
          title: this.title,
          help: this.description,
          content: `${found.length} ${plural(found.length, 'template', 'templates')} found. ${found.join(', ')}`,
          warning: false,
        });
      },
    },
    {
      title: 'Default template',
      description: `What template is currently selected as the default.`,
      callback: async () => {
        const template = await CurrentTemplate();
        return template == 'D2L' ? 'D2L Template' : template == 'ICB' ? 'ICB Template' : 'None';
      },
      render: async function (data) {
        if (data == 'None') {
          if (store.editor.AlwaysShowTemplateChangeButton || !(await RemoteFileExists(`${Path}template/template.html`))) {
            async function GetUnitSettings(id, asURLData = false) {
              const dom = new DOMParser().parseFromString(
                await fetch(`/d2l/le/content/${id}/settings/Open`).then(d => d.text()),
                'text/html'
              );
              const form = dom.querySelector('form');
              const fd = new URLSearchParams(new FormData(form));
              if (asURLData) return fd;

              const items = {};
              for (const [k, v] of fd) {
                items[k] = v;
              }
              return items;
            }

            async function SaveUnitSettings(id, changes) {
              const fd = await GetUnitSettings(id, true);
              for (const [k, v] of Object.entries(changes)) {
                fd.set(k, v);
              }

              fd.append('requestId', Date.now() % 10);
              fd.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
              fd.append('isXhr', true);

              return fetch(`/d2l/le/content/${id}/settings/Save`, { method: 'post', body: fd });
            }

            const useD2Lbutton = document.createElement('d2l-button-subtle');
            useD2Lbutton.text = 'Set Default to D2L Template';
            useD2Lbutton.addEventListener('click', async e => {
              await SaveUnitSettings(orgid, {
                defaultTemplateIsEnabled: '1',
                defaultTemplatePathSelector: '/shared/content-template/v3.0/pages/',
              });
              useD2Lbutton.text = 'Settings saved';
              useD2Lbutton.disabled = true;
            });
            data = useD2Lbutton;
          }
        }
        return result({ title: this.title, help: this.description, content: data, warning: false });
      },
    },
    {
      title: 'Template in use',
      description: `Checks for any HTML files in the Unit Content that aren't using a template.`,
      callback: async () => {
        try {
          /** A collection of what pages are using what template. */
          const using = { d2l: [], icb: [], none: [] };

          /** Get the HTML files from the Unit Content */
          const toc = await api(`/d2l/api/le/1.43/${orgid}/content/toc?ignoreModuleDateRestrictions=true`, {
            fresh: true,
            first: true,
          });
          const files = iterate(toc.Modules).filter(
            item => item.TypeIdentifier == 'File' && item.Url && item.Url.endsWith('.html')
          );

          const sanitize = url => url.replace(/%/g, '%25');

          await Promise.all(
            files.map(async item => {
              try {
                const src = await fetch(sanitize(item.Url)).then(d => d.text());
                const dom = new DOMParser().parseFromString(src, 'text/html');
                if (dom.head.querySelector('link[href*="templates/v3-at/"],link[href*="icb.utas.edu.au/v3.3"]'))
                  using.icb.push(item);
                else if (dom.querySelector('[mm-template-style="d2l"]')) using.d2l.push(item);
                else using.none.push(item);
              } catch (e) {
                console.error('Error checking for template in "%s". ', item.Url, e);
              }
            })
          );
          return using;
        } catch (e) {
          throw e;
        }
      },
      render: async function ({ none, icb, d2l }) {
        // The current default template.
        // const currentTemplateConfig = await CurrentTemplate();

        const totalPages = none.length + icb.length + d2l.length;
        let container = document.createElement('div');

        const countHidden = items => items.filter(v => v.IsHidden).length;

        const renderblock = (description, items) => {
          const invisible = countHidden(items);
          const block = document.createElement('div');
          block.innerHTML = `
						<p> 
							${items.length}/${totalPages} ${plural(totalPages, 'page', 'pages')} ${description}.
							${invisible > 0 ? ` (${invisible} hidden)` : ''}
						</p>

						<ul>
							${items
                .map(
                  item =>
                    `
									<li>
										<a href="/d2l/le/content/${orgid}/viewContent/${item.TopicId}/View" target="_blank">
											${item.Title}
										</a>${hidden(item)}
									</li>`
                )
                .join('\n')}
							
						</ul>
					`;

          return block;
        };

        if (none.length > 0) container.append(renderblock('have no recognised template', none));
        if (icb.length > 0) container.append(renderblock('use the ICB template', icb));
        if (d2l.length > 0) container.append(renderblock('use the D2L template', d2l));

        return result({
          title: this.title,
          help: this.description,
          content: container,
        });
      },
    },
    {
      title: 'Old announcements',
      description: `Checks for any announcements published more than 1 month before the open date of the unit.`,
      callback: async () => {
        let news = await api(`/d2l/api/le/1.28/${orgid}/news/`);
        let thisUnit = (await api(`/d2l/api/lp/1.21/enrollments/myenrollments/${orgid}`))[0];
        return news.filter(
          item =>
            item.IsPublished && moment(item.StartDate).isBefore(moment(thisUnit.Access.StartDate).subtract(1, 'months'))
        );
      },
      render: async function (data) {
        if (data.length == 0) {
          return result({ title: this.title, help: this.description, content: 'No old announcements found.' });
        }

        let announcements = data.map(
          item => `<a href="/d2l/le/news/${orgid}/${item.Id}/view" target="_blank">${item.Title}</a>`
        );

        const container = document.createElement('div');

        const msg = document.createElement('p');
        msg.innerText = `${announcements.length} old ${plural(announcements.length, 'announcement')} found.`;

        const list = document.createElement('ul');
        list.append(
          ...announcements.map(link => {
            const li = document.createElement('li');
            li.innerHTML = link;
            return li;
          })
        );

        container.append(msg, list);

        return result({
          title: this.title,
          help: this.description,
          content: container,
        });
      },
    },
    {
      title: 'Groupless Students',
      description: `Checks for any students who aren't enrolled in a manually created group.`,
      callback: async () => {
        const ignoredGroups = ['Campus Groups', 'Mode Groups', 'Unit Groups'];
        let users = (await api(`/d2l/api/lp/1.21/enrollments/orgUnits/${orgid}/users/?roleId=103`)).map(({ User }) => User);
        let groupCategories = (await api(`/d2l/api/lp/1.21/${orgid}/groupcategories/`)).filter(
          category => !ignoredGroups.includes(category.Name)
        );
        let groupsWithMissingUsers = [];
        await Promise.all(
          groupCategories.map(async category => {
            let usersInGroups = [];
            let groups = await api(`/d2l/api/lp/1.21/${orgid}/groupcategories/${category.GroupCategoryId}/groups/`);
            groups.forEach(group => {
              group.Enrollments.forEach(id => (!usersInGroups.includes(id) ? usersInGroups.push(parseInt(id)) : null));
            });

            let missingUsers = users.filter(user => !usersInGroups.includes(parseInt(user.Identifier)));
            if (missingUsers.length > 0) {
              groupsWithMissingUsers.push({
                category,
                missingUsers,
              });
            }
            return true;
          })
        );

        return groupsWithMissingUsers;
      },
      render: async function (data) {
        if (data.length == 0)
          return result({ title: this.title, help: this.description, content: 'All students assigned to custom groups.' });

        let groups = data.map(item => {
          return `<a target="_blank" href="/d2l/lms/group/group_enroll_expanded.d2l?ou=${orgid}&categoryId=${item.category.GroupCategoryId}&d2l_statePageId=1638&d2l_state_search=%7B%27Name%27%3A%27search%27,%27Controls%27%3A%5B%7B%27ControlId%27%3A%7B%27ID%27%3A%27search_groups%27%7D,%27StateType%27%3A%27%27,%27Key%27%3A%27%27,%27State%27%3A%7B%27DoSearch%27%3Atrue,%27IsSuggestedSearch%27%3Afalse,%27PrimaryTextValue%27%3A%27%27,%27PrimaryTextFields%27%3A%5B%27FirstName%27,%27LastName%27,%27UserName%27,%27OrgDefinedId%27%5D,%27OptionData%27%3A%5B%5D%7D%7D,%7B%27ControlId%27%3A%7B%27ID%27%3A%27search_groups%27%7D,%27StateType%27%3A%27expanded%27,%27Key%27%3A%27%27,%27State%27%3A%7B%27IsExpanded%27%3Atrue%7D%7D,%7B%27ControlId%27%3A%7B%27ID%27%3A%27cb_enrolled%27%7D,%27StateType%27%3A%27%27,%27Key%27%3A%27%27,%27State%27%3A%7B%27IsChecked%27%3Afalse,%27Val%27%3A%271%27%7D%7D,%7B%27ControlId%27%3A%7B%27ID%27%3A%27cb_notenrolled%27%7D,%27StateType%27%3A%27%27,%27Key%27%3A%27%27,%27State%27%3A%7B%27IsChecked%27%3Atrue,%27Val%27%3A%271%27%7D%7D%5D%7D&d2l_stateGroups=%5B%27search%27,%27grid%27,%27gridpagenum%27%5D" title="${item.missingUsers.length} students not assigned in ${item.category.Name}">(${item.missingUsers.length}) ${item.category.Name}</a>`;
        });

        const container = document.createElement('div');

        const msg = document.createElement('p');
        msg.innerText = `${groups.length} ${plural(groups.length, 'group is', 'groups are')} missing some students.`;

        const list = document.createElement('ul');
        list.append(
          ...groups.map(group => {
            const li = document.createElement('li');
            li.innerHTML = group;
            return li;
          })
        );

        container.append(msg, list);

        return result({ title: this.title, help: this.description, content: container });
      },
    },
    {
      title: 'Hidden assessment folders',
      description: `Lists any assessment folders that are hidden from students.`,
      callback: async () => {
        let folders = await api(`/d2l/api/le/1.28/${orgid}/dropbox/folders/`);
        return folders.filter(folder => folder.IsHidden);
      },
      render: async function (data) {
        if (data.length == 0)
          return result({ title: this.title, help: this.description, content: 'No assessment folders hidden.' });
        let folders = data.map(
          folder =>
            `<a href="/d2l/lms/dropbox/admin/modify/folder_newedit_restrictions.d2l?db=${folder.Id}&ou=${orgid}" title="Edit settings for ${folder.Name}">${folder.Name}</a>`
        );

        const container = document.createElement('div');

        const msg = document.createElement('p');
        msg.innerText = `${folders.length} assessment ${plural(folders.length, 'folder')} hidden.`;

        const list = document.createElement('ul');
        list.append(
          ...folders.map(folder => {
            const li = document.createElement('li');
            li.innerHTML = folder;
            return li;
          })
        );

        container.append(msg, list);

        return result({ title: this.title, help: this.description, content: container });
      },
    },
    {
      title: 'Broken Content Topics',
      description: 'Find Content Topics that are now appearing as "[Broken Topic]"',
      callback: async () => {
        const toc = await api(`/d2l/api/le/1.52/${orgid}/content/toc`, { fresh: true, first: true });
        if (!toc) return { toc: {}, broken: [] };

        const flatten = root => {
          const output = new Map();
          const loop = (node, parent) => {
            /**
             * If we're storing a module, save it based on the Module ID.
             * If we're storing a topic, save it based on the Topic ID.
             */
            node.ParentModuleId = parent;
            if (node.ModuleId) output.set(node.ModuleId, node);
            if (node.TopicId) output.set(node.TopicId, node);

            /** If we have modules and/or topics, iterate over each one. */
            node.Modules?.forEach(el => loop(el, node.ModuleId));
            node.Topics?.forEach(el => loop(el, node.ModuleId));
          };

          if (Array.isArray(root)) root.forEach(loop);
          else loop(root);

          return output;
        };

        const flat = flatten(toc);
        const topics = flat.filter(([key, value]) => value.TopicId);
        const broken = topics.filter(([k, v]) => v.IsBroken);

        return { modules: flat, broken };
      },
      render: async function ({ modules, broken }) {
        if (broken.size == 0) {
          return result({ title: this.title, help: this.description, content: 'No broken content topics found.' });
        }

        const makeReadable = item => {
          const element = document.createElement('p');

          const link = document.createElement('a');
          link.innerText = `[Broken Topic] (formerly "${item.Title}")`;
          link.innerHTML += hidden(item);
          link.classList.add('d2l-link');
          link.title = 'Open topic in new tab';
          link.target = '_blank';
          link.href = `/d2l/le/content/${orgid}/Home?itemIdentifier=D2L.LE.Content.ContentObject.ModuleCO-${item.ParentModuleId}`;

          element.classList.add(classes.listItemParagraph);

          const breadcrumbs = document.createElement('span');
          breadcrumbs.classList.add('breadcrumb');

          const trail = [];
          for (let node = modules.get(item.ParentModuleId); node != null; node = modules.get(node.ParentModuleId)) {
            trail.push(node);
          }

          trail.reverse();

          breadcrumbs.innerText = 'Found in: ' + trail.map(el => el.Title).join(' > ');
          element.append(link, breadcrumbs);

          return element;
        };

        const container = document.createElement('div');

        const msg = document.createElement('p');
        msg.innerText = `${broken.size} broken ${plural(broken.size, 'link')} identified.`;

        const list = document.createElement('ul');
        list.append(
          ...[...broken.values()].map(makeReadable).map(link => {
            const li = document.createElement('li');
            li.append(link);
            return li;
          })
        );

        container.append(msg, list);

        return result({ title: this.title, help: this.description, content: container });
      },
    },
    {
      title: 'Links that change between deliveries',
      description: 'Identifies pages with links that may need to be updated for each delivery.',
      callback: () => {
        return new Promise(async resolve => {
          /** loader.innerText = 'Loading contents...'; */
          /** loader.indeterminate = true; */
          /** loader.hidden = false; */

          const topics = await GetUnitModule({ OrgID: orgid }).then(topics =>
            topics.filter(topic => topic.TypeIdentifier == 'File' && topic.Url?.endsWith('.html'))
          );

          const urls = topics
            .filter(v => v.Url)
            .map(t => {
              const href = new URL(t.Url.replace(/%20/g, '%2520'), window.location.origin).href;
              return { href, key: t.TopicId };
            });

          const filters = [
            'bbcollab.com/recording/',
            'padlet.com',
            'discussions/List',
            'discussions/messageLists',
            'pebblepad.com.au',
          ];
          const tags = ['a', 'iframe'];
          const ignore = ['padlet.com?ref=embed'];

          const keys = {
            Collaborate: 'bbcollab.com/recording/',
            Padlet: 'padlet.com',
            Discussions: ['discussions/List', 'discussions/messageLists'],
            PebblePad: 'pebblepad.com.au',
          };

          /** loader.indeterminate = false; */
          /** loader.value = 0; */
          /** loader.max = urls.length; */
          /** loader.innerText = 'Checking content'; */

          let data = [];
          const check = CheckPages(urls, { filters, tags, ignore });
          check.subscribe(
            result => {
              if (result.matches.length > 0) {
                const topic = topics.find(t => t.TopicId == result.key);

                const counts = [];
                for (const [label, urls] of Object.entries(keys)) {
                  const u = AsArray(urls);
                  const matches = u.flatMap(url =>
                    result.matches.filter(match => match.toLowerCase().includes(url.toLowerCase()))
                  );

                  if (matches.length > 0) {
                    counts.push(`<strong>${label}</strong>: ${matches.length}`);
                  }
                }

                data.push(
                  `<a class="d2l-link" href="/d2l/le/content/${orgid}/viewContent/${result.key}/View" target="_blank">${
                    topic.Title
                  }</a>${hidden(topic)} | <span>${counts.join('<br>')}</span>`
                );
              }
            },
            console.error,
            () => resolve(data)
          );
        });
      },
      render: async function (results) {
        if (results.length == 0)
          return result({ title: this.title, help: this.description, content: 'No links identified.' });

        const container = document.createElement('div');

        const msg = document.createElement('p');
        msg.innerText = `${results.length} ${plural(results.length, 'link')} identified.`;

        const list = document.createElement('ul');
        list.append(
          ...results.map(result => {
            const li = document.createElement('li');
            li.innerHTML = result;
            return li;
          })
        );

        container.append(msg, list);

        return result({ title: this.title, help: this.description, content: container });
      },
    },
  ];

  if (info.readingLists) {
    checks.push({
      title: 'Reading Lists',
      description: `Confirms that the Reading List is available.`,
      callback: async () => {
        const GetReadingListURL = code => `https://utas.rl.talis.com/units/${code.toLowerCase()}.html`;
        const get = url => new Promise(res => chrome.runtime.sendMessage(extensionID, { request: 'dom', url }, res));

        return Promise.all(
          UnitCodes.map(async code => {
            const payload = await get(GetReadingListURL(code));
            if (!payload) return false;

            let dom = new DOMParser().parseFromString(payload, 'text/html');
            return !dom.querySelector('#main-content-container').innerText.includes('no lists here yet');
          })
        );
      },
      render: async function (data) {
        let BadReadingLists = data.filter(v => !v).length;
        let GoodReadingLists = data.filter(v => v).length;

        const obj = {
          title: this.title,
          help: this.description,
          content: '',
          warning: false,
        };

        if (GoodReadingLists == 0) {
          const text = document.createElement('p');
          text.innerHTML =
            'No reading lists were found. <a class="d2l-link" href="https://www.utas.edu.au/library/teach/reading-lists" target="_blank">Learn how to create a Reading List here.</a>';

          obj.content = text;
          obj.warning = true;
          return result(obj);
        }

        if (BadReadingLists == 0) {
          obj.content = 'All reading lists are available.';
          obj.warning = false;
          return result(obj);
        }

        obj.content = `${BadReadingLists} reading ${plural(BadReadingLists, 'list', 'lists')} unavailable.`;
        obj.warning = true;
        return result(obj);
      },
    });
  }

  run.addEventListener('click', async e => {
    let start = Date.now();
    run.disabled = true;
    loader.hidden = false;
    results.innerHTML = '';

    heading.hidden = false;
    heading.querySelector('h2').innerText = 'Status Report';
    unitname.innerText = UnitName;
    timestamp.innerText = moment().format('DD MMM YYYY, hh:mma');

    for (const test of checks) {
      try {
        progressText.innerText = `Checking "${test.title}"`;
        let res = await test.callback();
        const render = await test.render.call(test, res);
        if (render) results.append(render);
      } catch (e) {
        console.error(e);
      }
    }

    progressText.innerText = `${checks.length} ${plural(checks.length, 'test')} completed in ${moment
      .duration(Date.now() - start)
      .humanize()}.`;
    loader.hidden = true;
    run.disabled = false;
  });
}
