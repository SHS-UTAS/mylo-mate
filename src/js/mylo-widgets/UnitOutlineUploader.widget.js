/**
 * @affects MyLO Widgets
 * @problem Replacing the Unit Outline can be slow and tedious with the Manage Files tool, and is easy for staff to do incorrectly.
 * @solution A widget streamlines the process, automatically renaming the file as necessary and adding the new unit outline to the correct location.
 * @target Academic and support staff
 * @impact High
 * @savings 15 minutes per unit
 */

const HashUnitRef = unit => `${unit.unitCode}--${unit.studyPeriod}`;

const AkariPilotUnits = fetch('/shared/Images/Info_widget/AkariUnitOutlines_StageOneUnitSetup.js')
  .then(d => d.text())
  .then(txt => Array.from(txt.matchAll(/({.*})/gi)).map(match => JSON.parse(match[0])))
  .then(units => new Set(units.map(HashUnitRef)));

/**
 * Test if this unit is using an online Akari outline.
 *
 * @param {string[]} metadata The normalised unit code metadata
 * @param {string[]} codes The codes from the unit name
 * @return {Promise<boolean>} Returns true when the unit is flagged for Akari.
 */

const IsAkariUnit = async (metadata, codes) => {
  // Get the unit outline format configured in MyLO Manager
  const outlineType = JSON.parse(metadata.at(8));
  if (outlineType === null) return false;

  // If the unit is already configured to use Akari, we can just respond here.
  if (outlineType === 5) return true;

  // Find the delivery period. If we don't have one, we may be in a non-award/sandpit unit.
  // As these don't have unit outlines, we can just respond false now.
  const delivery = metadata.at(2);
  if (!delivery) return false;

  // Get the units that are part of the Akari pilot.
  // This responds with a Set containing hashed values of the pilot list.
  // This has ensures that we have an efficient lookup.
  const pilotUnits = await AkariPilotUnits;

  // Hash the codes for this specific unit in the same way that the Akari pilot unit is hashed.
  const hashedCodes = codes.map(code => HashUnitRef({ unitCode: code, studyPeriod: delivery }));

  // Return whether the hash appeared in the pilot lookup.
  return hashedCodes.some(hash => pilotUnits.has(hash));
};

/**
 * Check the unit outline status, and report the status.
 * @param {HTMLElement} widget The widget component
 * @param {string[]} metadata The normalised unit code metadata
 * @param {string[]} codes The codes from the unit name
 */
const CheckUnitOutlineStatus = async (widget, metadata, codes) => {
  const container = widget.querySelector('.outline-status');
  const loader = container.querySelector('d2l-loading-spinner');

  const outlineTypeLabel = document.createElement('div');
  outlineTypeLabel.classList.add('mm-description-with-label');

  const outlineStatusLabel = document.createElement('div');
  outlineStatusLabel.classList.add('mm-description-with-label');

  // Show the loader
  loader.toggleAttribute('hidden', false);

  const isAkari = await IsAkariUnit(metadata, codes);

  const UnitOutlineType = JSON.parse((codes && metadata.at(6)) || null);
  const UnitOutlineLabelFormatter = type => {
    if (UnitOutlineType === null) return 'This unit does not use a unit outline.';

    const label = [
      `a single ${type} unit outline`,
      `one ${type} unit outline per unit code`,
      `a ${type} unit outline for on-campus and off-campus`,
    ].at(UnitOutlineType);

    return `This unit uses ${label}.`;
  };

  const outlineKey = isAkari ? 5 : JSON.parse(metadata.at(-1));
  const outlineFormat = [
    'This unit does not use a unit outline.',
    UnitOutlineLabelFormatter('PDF'),
    UnitOutlineLabelFormatter('HTML'),
    UnitOutlineLabelFormatter('DOCX'),
    UnitOutlineLabelFormatter('HTML'),
    UnitOutlineLabelFormatter('Akari'),
  ].at(outlineKey);

  outlineTypeLabel.innerHTML = `
		<strong>Unit Outline Type</strong>
		<span>${outlineFormat}</span>
	`;

  container.prepend(outlineTypeLabel);

  // If the unit isn't configured for a unit outline, nothing to check or display.
  if (outlineKey === 0 || UnitOutlineType === null) {
    loader.toggleAttribute('hidden', true);
    return;
  }

  const { testAllOutlines } = await import('../mylo/unit-outline-tests.mjs');
  const outlines = await testAllOutlines().then(outlines =>
    outlines.map(({ outline, url, ok }) => ({ label: outline.innerText.trim(), href: url, ok })),
  );

  if (UnitOutlineType === 0) {
    const outline = outlines[0];
    outlineStatusLabel.innerHTML = `
			<strong>Unit Outline Status</strong>
			<span class="vertical-align">
				<d2l-icon icon="tier1:${outline.ok ? 'check-circle' : 'alert'}" class="uo-status--${outline.ok ? 'ok' : 'warn'}"></d2l-icon> 
				<a href="${outline.href}" target="_blank" class="d2l-link">Unit outline is ${outline.ok ? 'available' : 'unavilable'}.</a>
			</span>
		`;
  } else {
    outlineStatusLabel.innerHTML = `
			<strong>Unit Outline Status</strong>
			${outlines
        .map(
          outline =>
            `<span class="vertical-align">
							<d2l-icon icon="tier1:${outline.ok ? 'check-circle' : 'alert'}" class="uo-status--${outline.ok ? 'ok' : 'warn'}"></d2l-icon> 
							<a href="${outline.href}" target="_blank" class="d2l-link">${outline.label} is ${
                outline.ok ? 'available' : 'unavilable'
              }.</a>
						</span>`,
        )
        .join('\n')}
		`;
  }

  container.append(outlineStatusLabel);
  loader.toggleAttribute('hidden', true);
};

export default async function ({ widget, orgid, config }) {
  const UnitData = await api(`/d2l/api/lp/1.21/enrollments/myenrollments/${orgid}`, { first: true, fresh: true });
  const { Path } = await api(`/d2l/api/lp/1.26/courses/${orgid}`, { first: true, fresh: true });
  const UnitCode = UnitData.OrgUnit.Code;
  const content = widget.querySelector('#results_text');
  const loader = widget.querySelector('mm-loader');
  const container = widget.querySelector('#buttons');

  container.innerHTML = '';

  const totalUnitMetadataSize = 9;
  const UnitMetadata = UnitCode.split('_');
  UnitMetadata.push(...Array.from({ length: totalUnitMetadataSize - UnitMetadata.length }).fill(null));

  const UnitCodes = UnitData.OrgUnit.Name.match(/[A-Z]{3}[0-9]{3}/g);
  const UnitOutline = {
    Single: 0,
    PerUnitCode: 1,
    OnCampusOffCampus: 2,
  };

  // Start checking the unit outline status.
  CheckUnitOutlineStatus(widget, UnitMetadata, UnitCodes);

  const IsAkariOutlineUnit = await IsAkariUnit(UnitMetadata, UnitCodes);
  if (IsAkariOutlineUnit) {
    const helpguide =
      'https://www.utas.edu.au/mylo/staff/staff-resources/using-mylo-for-the-first-time/akari-unit-outlines-within-mylo';

    widget.querySelector('mm-widget-control').innerHTML = `
			<p class="vertical-align">
				<d2l-icon icon="tier1:alert" style="color: darkorange; font-size: 1.25em; --d2l-icon-height: 1em; --d2l-icon-width: 1em;"></d2l-icon>
				<span>
					This unit is configured to use Akari unit outlines. Please see the <a href="${helpguide}" target="_blank" class="d2l-link">help guide</a> for more information.
				</span>
			</p>
		`;

    return;
  }

  const outlineFormat = JSON.parse(UnitMetadata.at(-1));
  const extension = [
    // None type. This should be quite uncommon, although
    // setting it to default to pdf protects us for Non-Award units.
    '.pdf',

    // PDF file types
    '.pdf',

    // HTML file types
    '.html',

    // MS Word Documents
    '.docx',

    // Content formats
    '.html',
  ].at(outlineFormat ?? 0);

  const fileInput = document.createElement('input');
  fileInput.type = 'file';
  fileInput.accept = extension;

  const ManageFiles = new Files(orgid);

  const PerformUploadProcess = async ({ code }) => {
    content.innerHTML = '';
    widget.querySelectorAll('#buttons button').forEach(btn => btn.setAttribute('disabled', true));

    const manageFilesUrl = `/d2l/lp/manageFiles/main.d2l?ou=${orgid}&Path=/UnitOutline`;

    try {
      const archiveExisting = widget.querySelector('[name="archive"]').checked;
      const filename = code ? `${code}_UnitOutline${extension}` : `UnitOutline${extension}`;
      const files = fileInput.files;

      const dir = '/UnitOutline';
      const file = `${dir}/${filename}`;

      if (files.length == 0) {
        widget.querySelectorAll('#buttons button').forEach(btn => btn.removeAttribute('disabled'));
        return;
      }

      if (!files[0].name.endsWith(extension)) {
        // Wrong file type for the unit. Let them know, and we'll revert.
        alert(`The file ${files[0].name} doesn't match the required file format of ${extension}. Please try again.`);

        // Clear the file input.
        fileInput.value = null;

        // Reset the buttons.
        widget.querySelectorAll('#buttons button').forEach(btn => btn.removeAttribute('disabled'));

        // Stop processing any more logic.
        return;
      }

      loader.removeAttribute('hidden');

      // If we have an existing unit outline, lets handle it.
      if (await ManageFiles.FileExists(file)) {
        if (archiveExisting) {
          // We'll also hold onto the name, just in case something goes wrong.
          const ts = Date.now();

          const name = `${code ? `${code}_` : ''}UnitOutline-${ts}${extension}`;
          await ManageFiles.Rename(dir, filename, name);
        } else {
          // Let's delete it.
          await ManageFiles.Delete(dir, filename);
        }
      }

      /** This will automatically create the necessary directory structure as well */
      await ManageFiles.UploadFile(dir, filename, files[0]);

      const newOutlinePath = `${Path}UnitOutline/${filename}?${Date.now()}`;
      content.innerHTML = `
				<p class="success">Unit outline uploaded successfully.</p>
				<p><a href="${newOutlinePath}" target="_blank">View Unit Outline</a></p>
				<p><a href="${manageFilesUrl}" target="_blank">View in Manage Files</a></p>
			`;
    } catch (e) {
      console.error(e);
      content.innerHTML = `
				<p class="warn">
					Unit outline uploader encountered an error. Please check the <a href="${manageFilesUrl}" target="_blank">Manage Files</a> area.
				</p>
			`;
    } finally {
      loader.setAttribute('hidden', true);
      widget.querySelectorAll('#buttons button').forEach(btn => btn.removeAttribute('disabled'));
    }
  };

  const UnitOutlineType = JSON.parse((UnitCodes && UnitMetadata.at(6)) || null);

  /**
   * Button factory.
   * @param {object} props The button configuration
   * @param {string} props.label The buttons label
   * @param {string|null} props.code The unit code this applies to
   * @param {number} props.type The outline type.
   * @return {HTMLElement} The button output
   */
  const makeButton = props => {
    const btn = document.createElement('d2l-button');
    btn.innerText = props.label;
    btn.toggleAttribute('primary', true);
    btn.setAttribute('unit-code', props.code);
    btn.setAttribute('outline-type', props.type);
    btn.onclick = () => {
      fileInput.addEventListener('change', () => PerformUploadProcess(props), { once: true });
      fileInput.click();
    };
    return btn;
  };

  const buttons = [];

  switch (UnitOutlineType) {
    case UnitOutline.Single:
      // Show a single code button.
      buttons.push({ label: `Upload ${UnitCodes[0]} Unit Outline`, code: UnitCodes[0], type: UnitOutline.Single });
      break;

    case UnitOutline.OnCampusOffCampus:
      // Add a button for On Campus and Off Campus outlines.
      buttons.push(
        { label: `Upload On Campus Unit Outline`, code: 'OnCampus', type: UnitOutline.OnCampusOffCampus },
        { label: `Upload Off Campus Unit Outline`, code: 'OffCampus', type: UnitOutline.OnCampusOffCampus },
      );
      break;

    case UnitOutline.PerUnitCode:
      // Add a button for each unit code.
      for (const code of UnitCodes) {
        buttons.push({ label: `Upload ${code} Unit Outline`, type: UnitOutline.PerUnitCode, code });
      }
      break;

    case null:
    default:
      // If there's no unit outline type, we should just use a generic label.
      buttons.push({ label: `Upload Unit Outline`, code: null, type: UnitOutline.Single });
      break;
  }

  container.append(...buttons.map(makeButton));
}
