/**
 * @affects MyLO Widgets
 * @problem Word clouds are a fun, engaging and visual way to provide feedback on discussion or page content.
 * @solution A widget generates a word cloud, allowing customisation based on selected discussion topics and/or unit content.
 * @target Academic and support staff
 * @impact Low
 * @savings Unmeasureable
 */

export default async function ({ widget, orgid, config }) {
  const metaTag = document.head.querySelector('meta[name="mylo-mate"]');
  const extensionID = metaTag ? metaTag.getAttribute('content') : '';

  const discussion_list = widget.querySelector('#discussion_topics');
  const content_list = widget.querySelector('#content_types');
  const dialog = widget.querySelector('mm-widget-result .results');
  const checkedTypes = [];

  pdfjsLib.GlobalWorkerOptions.workerSrc = `chrome-extension://${extensionID}/lib/pdf.worker.min.js`;
  const fullSizeCanvas = document.createElement('canvas');
  fullSizeCanvas.height = '1500';
  fullSizeCanvas.width = '1500';

  let word_list = null;

  const LightestColour = 'aa';
  const DarkestColour = '11';

  let cloud = null;

  const content = {
    html: { label: 'HTML content', extensions: ['html', 'htm'], items: [] },
    docx: { label: 'DOCX content', extensions: ['docx'], items: [] },
    pdf: { label: 'PDF content', extensions: ['pdf'], items: [] },
  };

  const handleDiscussionsCheckboxClick = e => {
    if (!e.target.hasAttribute('data-type')) return;
    if (e.target.getAttribute('data-type') == 'forum') {
      discussion_list
        .querySelectorAll(`[data-type="topic"][data-forum-id="${e.target.getAttribute('data-forum-id')}"]`)
        .forEach(cb => (cb.checked = e.target.checked));
    } else if (e.target.getAttribute('data-type') == 'topic') {
      const id = e.target.getAttribute('data-forum-id');
      let stamp = 0;

      const parent = discussion_list.querySelector(`[data-type="forum"][data-forum-id="${id}"]`);
      const cbs = discussion_list.querySelectorAll(`[data-type="topic"][data-forum-id="${id}"]`);

      cbs.forEach(cb => (stamp += cb.checked ? 1 : 0));

      if (stamp === 0) {
        parent.checked = false;
        parent.indeterminate = false;
      } else if (stamp === cbs.length) {
        parent.checked = true;
        parent.indeterminate = false;
      } else {
        parent.checked = false;
        parent.indeterminate = true;
      }
    }
  };

  const fetchTopics = async () => {
    let forums = [];
    const forumsAPI = await api(`/d2l/api/le/1.22/${orgid}/discussions/forums/`);
    await Promise.all(
      forumsAPI.map(async forum => {
        const topics = await api(`/d2l/api/le/1.22/${orgid}/discussions/forums/${forum.ForumId}/topics/`);
        forums.push({
          id: forum.ForumId,
          name: forum.Name,
          topics: topics.map(t => ({ id: t.TopicId, name: t.Name })),
        });
      })
    );
    forums = forums.sort((a, b) => a.id - b.id);

    Array.from(discussion_list.children).forEach(c => c.remove());
    forums.forEach(f => {
      const li = document.createElement('li');

      const cb = document.createElement('d2l-input-checkbox');
      cb.type = 'checkbox';
      cb.setAttribute('data-type', 'forum');
      cb.setAttribute('data-forum-id', f.id);
      cb.addEventListener('change', handleDiscussionsCheckboxClick);
      cb.innerText = f.name;

      li.appendChild(cb);
      discussion_list.appendChild(li);

      if (f.topics.length > 0) {
        const sublist = document.createElement('ul');
        f.topics.forEach(t => {
          const li = document.createElement('li');
          const cb = document.createElement('d2l-input-checkbox');
          cb.type = 'checkbox';
          cb.setAttribute('data-type', 'topic');
          cb.setAttribute('data-forum-id', f.id);
          cb.setAttribute('data-topic-id', t.id);
          cb.addEventListener('change', handleDiscussionsCheckboxClick);
          cb.innerText = t.name;

          li.appendChild(cb);
          sublist.appendChild(li);
        });
        li.appendChild(sublist);
      }
    });
  };

  function iterate(base) {
    let output = [],
      start = base;
    const loop = node => {
      if (node.Modules.length > 0) node.Modules.forEach(loop);
      if (node.Topics.length > 0) output = output.concat(node.Topics);
    };
    if (Array.isArray(start)) start.forEach(loop);
    else loop(start);
    return output;
  }

  const fetchContent = async () => {
    const toc = iterate(
      await api(`/d2l/api/le/1.28/${orgid}/content/toc?ignoreModuleDateRestrictions=true`, { first: true }).then(
        d => d.Modules
      )
    );

    for (let i = 0, j = toc.length; i < j; i++) {
      const item = toc[i];
      if (item.TypeIdentifier != 'File') continue;
      if (item.Url == null) continue;
      const ext = item.Url.split('.').pop().toLowerCase();
      for (const key in content) {
        if (content[key].extensions.includes(ext)) {
          content[key].items.push(item);
        }
      }
    }

    Array.from(content_list.children).forEach(c => c.remove());
    for (const key in content) {
      const li = document.createElement('li');
      const cb = document.createElement('d2l-input-checkbox');
      cb.type = 'checkbox';
      cb.setAttribute('data-type', 'content');
      cb.setAttribute('data-format', key);
      cb.innerText = `${content[key].label} (${content[key].items.length} items)`;

      li.appendChild(cb);
      content_list.appendChild(li);
    }
  };

  fetchContent();
  fetchTopics();

  const word_limit = 200;
  const min_word_size = 3;

  const maxWordAppearanceSize = 50;

  let buffer = [];
  let top_words = {};
  let blacklist = [];
  const common = await fetch(`chrome-extension://${extensionID}/js/mylo-widgets/CommonWords.json`).then(d => d.json());

  const Html2Text = html => {
    const c = document.createElement('textarea');
    c.innerHTML = html.replace(/<[^>]+>/g, '');
    return c.innerText.toLowerCase().replace(/[^a-z]/gi, ' ');
  };

  const getPosts = async (forum, topic) => {
    const posts = await api(`/d2l/api/le/1.22/${orgid}/discussions/forums/${forum}/topics/${topic}/posts/`);
    return posts.filter(p => !p.IsDeleted).map(p => Html2Text(p.Message.Html));
  };

  const updateWordList = async words => {
    for (let i = 0, j = words.length; i < j; i++) {
      const word = words[i].trim().toLowerCase();
      if (word.length < min_word_size) continue; // If the word is less than a certain length, ignore it.
      if (blacklist.includes(word)) continue; // If it's a blacklist word, ignore it.
      if (common.includes(word)) continue; // If it's a common word, ignore it.
      if (word.match(/[a-z]+/i) == null) continue; // Doesn't contain any letters. This makes sure we don't just get punctuation or numbers.
      top_words[word] = (top_words[word] || 0) + 1;
    }

    const list = document.createElement('tbody');
    const top = Object.entries(top_words)
      .sort((a, b) => b[1] - a[1])
      .slice(0, word_limit);
    for (let i = 0, j = top.length; i < j; i++) {
      const tr = document.createElement('tr');
      const word = top[i][0];
      tr.dataset.word = word;
      tr.innerHTML = `<td class="word">${word}</td><td class="frequency">${top[i][1]}</td><td class="remove" title="Click to ignore and regenerate">&times;</td>`;
      list.appendChild(tr);
    }

    window.requestAnimationFrame(() => {
      if (word_list?.querySelector('tbody')) word_list?.querySelector('tbody').remove();
      word_list?.append(list);
    });
  };

  const updateWordCloud = async () => {
    const top = Object.entries(top_words);
    if (top.length > 0) {
      top.sort((a, b) => b[1] - a[1]);

      const cloudWords = top.slice(0, word_limit);

      const minSize = cloudWords[cloudWords.length - 1][1];
      const maxSize = cloudWords[0][1];

      const scale = (maxWordAppearanceSize / maxSize) * 4;

      fullSizeCanvas.addEventListener('wordcloudstart', () => {});
      fullSizeCanvas.addEventListener('wordclouddrawn', () =>
        cloud.getContext('2d').drawImage(fullSizeCanvas, 0, 0, cloud.width, cloud.height)
      );
      fullSizeCanvas.addEventListener('wordcloudstop', () => {
        cloud.getContext('2d').drawImage(fullSizeCanvas, 0, 0, cloud.width, cloud.height);
      });

      if (!widget.querySelector('input[name="cloud-shape"]:checked')) {
        widget.querySelector('input[name="cloud-shape"]').checked = true;
      }

      WordCloud(fullSizeCanvas, {
        list: cloudWords,
        // gridSize: Math.round(16 * 1500 / 1024),
        // weightFactor: size => size * scale,
        weightFactor: size => size * scale, // Do something clever here, to help bolster the size to the full canvas?
        // shape: 'circle',
        shape: widget.querySelector('input[name="cloud-shape"]:checked').value,
        fontFamily: 'Impact, Times, serif',
        color: (word, weight, fontSize, distance, theta) => {
          const min = parseInt(LightestColour, 16);
          const max = parseInt(DarkestColour, 16);
          const pc = (weight - minSize) / (maxSize - minSize);
          const color = Math.round((max - min) * pc + min).toString(16);
          return `#${color.repeat(3)}`;
        },
        rotateRatio: 0.5,
        rotationSteps: 2,
      });
    } else {
      setTimeout(() => alert('No available content found.'), 10);
    }
  };

  const badThingsToHaveInAWord = /[.,\/#!$%\^&\*;:{}=\\_`~()<>\[\]0-9]/g;
  const updateCloudData = async text => {
    const words = text
      .split(/\s/g)
      .map(t => t.trim())
      .filter(v => v.length > 0 && v.match(badThingsToHaveInAWord, ' ') == null);
    buffer = buffer.concat(words);
    updateWordList(words);
    return true;
  };

  const getFileContent = async (type, file) => {
    const errors = [];
    const PlainText = {
      async pdf(file) {
        try {
          let pdf = await pdfjsLib.getDocument(SafeURL(file)).promise;
          let str = '';

          for (let i = 0; i < pdf.numPages; i++) {
            const page = await pdf
              .getPage(i + 1)
              .then(d => d.getTextContent())
              .then(d => d.items.map(item => item.str).join(''));
            str += page;
          }

          return str.trim();
        } catch (e) {
          errors.push(file);
          console.error(file, e);
          return '';
        }
      },
      async docx(file) {
        // Turns a URL into a zip file
        const unzip_docx = url =>
          new Promise((resolve, reject) =>
            JSZipUtils.getBinaryContent(url, (err, content) => {
              if (err) {
                reject(err);
              } else {
                const z = new JSZip();
                z.loadAsync(content).then(() => resolve(z));
              }
            })
          );
        try {
          const zip = await unzip_docx(SafeURL(file));

          // Gets the document text from the zip file.
          const doc = await zip.files['word/document.xml'].async('string');

          // Turns the XML doc into a plain string.
          const str = Array.from(new DOMParser().parseFromString(doc, 'text/xml').querySelectorAll('t'))
            .map(t => t.textContent.trim())
            .join('');
          return str.trim();
        } catch (e) {
          errors.push(file);
          console.error(file, e);
          return '';
        }
      },
      async html(file) {
        try {
          const dom = new DOMParser().parseFromString(await ajax(file), 'text/html');
          const str = dom.body.innerText;
          // str += '\r\n\r\n';
          // dom.querySelectorAll('a[href]').forEach(a => str += a.href + '\r\n');
          return str.trim();
        } catch (e) {
          errors.push(file);
          console.error(e);
          return '';
        }
      },
    };

    return PlainText[type](file);
  };

  const generateWordCloud = async () => {
    let promises = [];
    content_list.querySelectorAll('[data-format]').forEach(cb => {
      const key = cb.getAttribute('data-format');
      if (cb.checked) checkedTypes.push(key);
      if (cb.checked && content[key].items.length > 0) {
        promises = promises.concat(
          content[key].items.map(async f => {
            const t = await getFileContent(key, f.Url);
            updateCloudData(t);
            return true;
          })
        );
      }
    });

    const discussionCheckboxes = Array.from(discussion_list.querySelectorAll('[data-type="topic"]')).filter(v => v.checked);
    if (discussionCheckboxes.length > 0) {
      checkedTypes.push('discussions');
    }
    discussionCheckboxes.forEach(cb => {
      promises.push(
        (async function () {
          const p = await getPosts(cb.getAttribute('data-forum-id'), cb.getAttribute('data-topic-id'));
          p.forEach(updateCloudData);
          return true;
        })()
      );
    });

    return Promise.all(promises);
  };

  let blacklist_field = null;
  const handleRegenerateCloud = () => {
    blacklist = (blacklist_field.value || '')
      .split('\n')
      .map(v => v.trim())
      .filter(v => v.length > 0);
    top_words = {};
    updateWordList(buffer);
    updateWordCloud();
  };

  widget.querySelector('[data-type="run"]').addEventListener('click', async e => {
    dialog.querySelector('.cloud').toggleAttribute('loading', true);
    const cbs = Array.from(widget.querySelectorAll('.checkboxes d2l-input-checkbox')).filter(v => v.checked);

    if (cbs.length == 0) {
      return alert('Please select at least one content source.');
    }

    dialog.hidden = false;

    buffer = [];
    top_words = {};
    blacklist = [];

    await generateWordCloud();

    cloud = dialog.querySelector('canvas');
    word_list = dialog.querySelector('table.word-list');
    word_list.onclick = e => {
      if (e.target.matches('.remove')) {
        const word = e.target.closest('tr').dataset.word;
        const list = new Set(
          (blacklist_field.value || '')
            .split('\n')
            .map(v => v.trim())
            .filter(v => v.length > 0)
        );
        list.add(word);

        blacklist_field.value = Array.from(list.values()).join('\n');
        handleRegenerateCloud();
      }
    };

    blacklist_field = dialog.querySelector('.blacklist .field');

    dialog.querySelector('[role="regen"]').addEventListener('click', handleRegenerateCloud);
    dialog.querySelector('.cloud').toggleAttribute('loading', false);
    updateWordCloud();

    const unit = await api(`/d2l/api/lp/1.22/enrollments/myenrollments/${orgid}`, { first: true });
    dialog.querySelector('[data-action="save"]').addEventListener('click', () => {
      const a = document.createElement('a');
      a.href = fullSizeCanvas.toDataURL('image/png');
      a.download = `${unit.OrgUnit.Name.replace(/ /g, '-').toLowerCase()}-wordcloud-${checkedTypes.join('-')}.png`;
      a.click();
      dialog.toggleAttribute('open', false);
    });
  });
}
