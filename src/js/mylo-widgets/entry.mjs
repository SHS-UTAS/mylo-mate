import {
  faInfoCircle,
  faSearch,
  faLink,
  faUserGraduate,
  faUsers,
  faSchool,
  faChartBar,
  faFileUpload,
  faCloud,
  faBookOpen,
} from '../../lib/fas.svg.js';

/** Builds the general structure of the modal UI */
await until(() => document.body.getAttribute('mm-unit-code'));

const sandpit = document.body.getAttribute('mm-unit-code').split('_')[1] == 'SP';
let widgets = [
  { key: 'AboutWidgets', title: 'Learn About Widgets', icon: faInfoCircle, show: true },
  { key: 'SearchUnit', title: 'Search Unit', icon: faSearch },
  { key: 'ContentChecker', title: 'Check Links', icon: faLink },
  { key: 'GradesAnalysis', title: 'Analyse Grades', icon: faUserGraduate },
  { key: 'StatusReport', title: 'Unit Status Report', icon: faChartBar },
  { key: 'UnitOutlineUploader', title: 'Upload Unit Outline', icon: faFileUpload },
  { key: 'WordCloud', title: 'Word Cloud', icon: faCloud },
];

if (sandpit) {
  widgets.push({ key: 'EnrolledUsers', title: 'Get Enrolled Staff', icon: faUsers });
  widgets.push({ key: 'GetAbsentStudents', title: 'Get Absent Students', icon: faSchool });
  widgets.push({ key: 'IdentifyLTIContent', title: 'Identify LTI Links in Units', icon: faBookOpen });
}

const dashboard = document.createElement('mm-widget-dashboard');
const opener = document.querySelector('[mm-purpose="absence-widget-body" i] [mm-install-status="installed"]') ?? (() => {
	const container = document.createElement('div');
	container.classList.add('d2l-widget', 'd2l-tile', 'd2l-custom-widget');
	container.innerHTML = `
	<div class="d2l-widget-header">
		<div class="d2l-homepage-header-wrapper">
			<h2 class="d2l-heading vui-heading-4" id="d2l_1_4_43">MyLO MATE Checker</h2>
		</div>
		<div class="d2l-homepage-header-menu-wrapper">
			<d2l-dropdown-context-menu no-auto-open="" text="Actions for MyLO MATE Checker" id="d2l_1_90_797" class="d2l-contextmenu-ph d2l-contextmenu-ph-dropdown d2l_1_78_366" data-contextmenuid="d2l_1_5_492" data-placeholderkey="d2l_1_91_561"></d2l-dropdown-context-menu>
			<div id="d2l_1_92_742" class="d2l-floating-container d2l-hidden ">
				<template id="d2l_1_5_492" class="d2l-contextmenu-template" data-floating-container-id="d2l_1_92_742" data-dropdown-content-id="d2l_1_93_71">
					<d2l-menu class="d2l-menu-mvc d2l-contextmenu">
						<d2l-menu-item text="Collapse this widget" id="d2l_1_5_492_CollapseWidgetAction"></d2l-menu-item>
						<d2l-menu-item text="Expand this widget" hidden="" id="d2l_1_5_492_ExpandWidgetAction"></d2l-menu-item>
					</d2l-menu>
				</template>
			</div>
			<div class="d2l-clear"></div>
		</div>
		<div class="d2l-clear"></div>
	</div>
	<d2l-expand-collapse-content expanded="" class="d2l-widget-content" id="d2l_1_29_97">
		<div class="d2l-widget-content-padding">
			<div class=" d2l-htmlblock-wc">
				<d2l-html-block no-deferred-rendering="">
					<div>
						<div mm-purpose="absence-widget-body"></div>
					</div>
				</d2l-html-block>
			</div>
			<div class="d2l-clear"></div>
		</div>
	</d2l-expand-collapse-content>
	`;

	document.querySelector('.homepage-col-4')?.prepend(container);
	return container.querySelector('[mm-purpose="absence-widget-body" i]');
})();

opener.innerHTML = `<div class="d2l-widget-content"></div>`;

const heading = opener.closest('.d2l-widget').querySelector('.d2l-widget-header h2');
heading.innerText = 'MyLO MATE Widgets';

const openbtn = document.createElement('button');
openbtn.classList.add('d2l-button');
openbtn.toggleAttribute('primary');
openbtn.innerText = 'Open Widgets Dashboard';
openbtn.title = 'MyLO MATE includes many widgets that provide extra functionality and saving staff time.';

openbtn.addEventListener('click', e => dashboard.open());

opener.append(openbtn);
document.body.append(dashboard);

const unit = await api(`/d2l/api/lp/1.33/courses/${GetOrgId()}`, { first: true }).then(data => {
  return {
    Name: data.Name,
    Code: data.Code,
    Path: data.Path,
    OrgId: parseInt(data.Identifier),
    Delivery: {
      Name: data.Semester.Name,
      Code: data.Semester.Code,
    },
    IsSandpit: data.Department?.Code == 'SP',
    IsActive: data.IsActive,
    IsExpired: new Date(data.EndDate) < new Date(), // If the end date is older than today
  };
});

for (const widget of widgets) {
  /** console.log(widget, dashboard, unit); */
  try {
    dashboard.registerWidget({
      key: widget.key,
      name: widget.title,
      icon: widget.icon,
      src: `js/mylo-widgets/${widget.key}.widget.html`,
      showAtStart: !!widget.show,
      unit,
    });
  } catch (e) {
    console.error(`Error registering widget ${widget.title}`, e);
    /**
     * console.dir(dashboard);
     * console.error(widget, unit, e);
     */
  }
}
