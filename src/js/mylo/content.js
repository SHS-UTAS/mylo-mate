/**
 * @affects MyLO
 * @problem Some pages are extremely long, and constantly scrolling up and down can get exhausting.
 * @solution Add `Jump to Top` and `Jump to Bottom` buttons in the bottom right corner of the screen.
 * @target Anyone regularly scrolling pages, such as Assignment Submissions
 * @impact Low
 */

(async function () {
  // The scrollheight must be x times larger than the current viewpanel to appear.
  const bottom_scrollheight_multiplier = 0.5;

  // Get the settings as an object.
  const store = await FetchMMSettings();

  // Grab some references, do some math.
  let wrapper = document.body;
  let scroller = window;

  let scrollHeight = wrapper.scrollHeight;
  let innerHeight = scroller.innerHeight;

  // If we're in the manage files area, we're going to handle the scrolling a little bit differently.
  // So let's just bail here, so we don't double up.
  if (window.location.href.includes('/d2l/lp/manageFiles/main.d2l?ou=')) return;

  // Just a flag to tell if we're in the discussions area.
  const discussions = window.location.href.includes('/d2l/lms/discussions/messageLists/frame.d2l');

  // This is to see if we have accessibility mode enabled.
  if (store.other.makeAccessible && !document.body.classList.contains('mm-access-friendly'))
    document.body.classList.add('mm-access-friendly');

  // If we have the Back to top buttons enabled in our settings area, let's go ahead.
  if (store.mylo.BackToTop) {
    // Decide whether we want to show the Jump to Bottom button here. If we're in the discussions area, just always show it.
    //const show_bottom = discussions ? true : scrollHeight > innerHeight * bottom_scrollheight_multiplier;
    const show_bottom = true;

    // Basically a getter. Will provide updated data, instead of the value it was when we first store it.
    const height = () => ((document.querySelector('d2l-floating-buttons[_floating]') || { clientHeight: 0 }).clientHeight) + 10;

    // Create a container for holding our jumping buttons. They're very bouncy.
    let container = document.createElement('div');
		container.classList.add('floating-jump-buttons');
    container.style.bottom = height() + 'px';

    // Create a new button that will act as our "Back to Top",
    // and give it some properties.
    let top = document.createElement('button');
    top.id = 'top';
    top.classList.add('btn');
    if (!show_bottom) top.classList.add('solo');
    top.setAttribute('mm-icon-type', 'action-button');
    top.setAttribute('mm-icon-show', 'false');
    top.title = 'Back to Top';
    top.setAttribute('aria-hidden', 'true');
    top.setAttribute('is', 'd2l-icon-button');
    top.innerHTML = `<d2l-icon class="style-scope d2l-icon-button d2l-icon-0" icon="d2l-tier1:chevron-up"></d2l-icon><span class="style-scope d2l-icon-button"></span>`;
    container.appendChild(top);

    // When the button is clicked, scroll the window back to the top.
    top.addEventListener('click', function () {
      scroller.scrollTo(0, 0);
    });

    if (show_bottom) {
      // This will be our "Jump to Bottom" button
      let bottom = document.createElement('button');
      bottom.id = 'bottom';
      bottom.className = 'btn';
      bottom.setAttribute('mm-icon-type', 'action-button');
      bottom.setAttribute('mm-icon-show', 'false');
      bottom.title = 'Jump to Bottom';
      bottom.setAttribute('aria-hidden', 'true');
      bottom.setAttribute('is', 'd2l-icon-button');
      bottom.innerHTML = `<d2l-icon class="style-scope d2l-icon-button d2l-icon-0" icon="d2l-tier1:chevron-down"></d2l-icon><span class="style-scope d2l-icon-button"></span>`;
      container.appendChild(bottom);

      // When the button is clicked, scroll the window back to the top.
      bottom.addEventListener('click', function () {
        scroller.scrollTo(0, wrapper.scrollHeight);
      });
    }

    document.body.appendChild(container);

    // If this _isn't_ the discussions area...
    if (!discussions) {
      // Listen for classname changes on the bottom bar, if it exists.
      const buttonBar = document.querySelector('d2l-floating-buttons');
      if (buttonBar) {
        const obs = new MutationObserver(() => (container.style.bottom = height() + 'px'));
        obs.observe(buttonBar, { attributes: true, attributeFilter: ['_floating'] });
      }

      // Boundaries to appear in.
      //	When window.scrollY is above `invisible`px, the button will disappear.
      //  When window.scrollY is between `invisible`px and `fadeIn`px, it will set the opacity
      //  to between 0 and 1, where 0 is 200px and 1 is 300px.
      let invisible = 200;
      let fadeIn = 300;

      // Store the last known value of window.scrollY
      let Y = null;

      // Sets the opacity of the Back to Top button
      const update = () => {
        if (Y < invisible) {
          top.style.opacity = 0;
          return;
        }
        if (Y > fadeIn) {
          top.style.opacity = 1;
          return;
        }
        if (Y > invisible && Y < fadeIn) {
          let scale = ((Y - invisible) * 100) / (fadeIn - invisible);
          top.style.opacity = scale / 100;
        }
      };

      // This is a bit noisy, but the basic breakdown here is that it observes the `scroll` event.
      //	However, the scroll event occurs an incredible amount of times, often far more than necessary.
      //	To prevent any crashing issues, we hook into the animationFrame system, so that we only actually do anything when the UI has changed, which cuts down the number of logic cycles significantly.
      let ticking = false;
      scroller.addEventListener('scroll', e => {
        if (!ticking) {
          // On the next animation frame, let's check to see if we've actually scrolled up or down.
          // If we have, update the visibility of our buttons.
          window.requestAnimationFrame(() => {
            let oldY = Y;
            Y = window.pageYOffset;
            if (oldY !== window.pageYOffset) update();
            ticking = false;
          });
          ticking = true;
        }
      });

      // Let's run our first button visibility update.
      // We also want to make sure that when our window resizes, we reassess the necessity of our buttons.
      update();
      window.addEventListener('resize', update);
    } else {
      // If this _is_ the discussions area, we're going to need to handle how we inject the buttons differently.
      // This is because the buttons area is old, and still uses nested framesets.

      // The codebase below is pretty well self explanantory, though one note.
      //	Due to the age of framesets, they don't have scrollTo functions.
      //	To circumvent this, we've added good, old-fashioned anchors at the top and bottom, and jump to them when our buttons are clicked.
      const frame = document
        .querySelector('iframe')
        .contentDocument.querySelector('frame[src*="frame_right"]')
        .contentDocument.querySelector('frame#FRAME_list');
      const body = frame.contentDocument.querySelector('body');

      const goToAnchor = (frameName, anchor) => {
        let a = document.createElement('a');
        a.target = frameName;
        a.href = '#' + anchor;
        body.appendChild(a);
        a.click();
        a = null;
      };
      let topAnchor = document.createElement('a');
      topAnchor.name = 'top';
      let bottomAnchor = document.createElement('a');
      bottomAnchor.name = 'bottom';
      body.appendChild(bottomAnchor);
      body.insertBefore(topAnchor, body.firstChild);

      // When the button is clicked, scroll the window back to the top.
      top.addEventListener('click', function () {
        goToAnchor('FRAME_list', 'top');
      });
      bottom.addEventListener('click', function () {
        goToAnchor('FRAME_list', 'bottom');
      });
    }
  }
})();
