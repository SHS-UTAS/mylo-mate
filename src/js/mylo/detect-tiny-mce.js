(async () => {
  const handle = () => {
    if (window.tinymce) {
      document.dispatchEvent(new Event('TinyMCE_Init'));
    } else {
      window.requestAnimationFrame(handle);
    }
  };

  window.requestAnimationFrame(handle);
})();
