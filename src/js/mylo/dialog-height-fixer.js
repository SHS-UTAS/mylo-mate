/**
 * @affects MyLO
 * @problem Dialogs are sometimes too small causing scrollbars to appear and hiding content.
 * @solution Whenever we detect a dialog appearing on-screen, resize it to make more of the content immediately visible.
 * @target Anyone using dialogs.
 * @impact Low
 */

(async function () {
  // This script listens for new dialog frames being added to the DOM.
  // Basically, we set a height of either a) the window's height, minus 280px for padding, or b) the dialogs scroll height (whichever is smaller).
  // This is powered with a MutationObserver; whenever it detects that a dialog is part of the changed nodes, it will affect all nodes on screen.

  const FrameHeightFixer = async frame => {
    await until(
      () =>
        typeof (document.querySelector('iframe') && document.querySelector('iframe').contentDocument
          ? document.querySelector('iframe').contentDocument.querySelector('[role="main"]')
          : undefined) != 'undefined'
    );
    let f = document.querySelector('iframe');
    const adjust_height = () =>
      (f.parentElement.style.height =
        Math.min(
          window.innerHeight - 280,
          (f.contentDocument.querySelector('[role="main"]') || { scrollHeight: 1000 }).scrollHeight
        ) + 'px');
    adjust_height();
    f.addEventListener('load', adjust_height);
  };

  const FrameHeightFinder = e => {
    if (
      e.some(change =>
        Array.from(change.addedNodes).some(node => node && typeof node.matches == 'function' && node.matches('.ddial_o'))
      )
    ) {
      document.querySelectorAll('.ddial_o').forEach(FrameHeightFixer);
    }
  };

  const o = new MutationObserver(FrameHeightFinder);
  o.observe(document.body, { childList: true });
})();
