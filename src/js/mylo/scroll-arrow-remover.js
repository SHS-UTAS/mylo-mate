/**
 * @affects MyLO
 * @problem Pages with wide tables and lots of columns usually have a scrollbar, often hidden off the bottom of the page, requiring the user to scroll down.
 * @solution Remove the width constraints of the table, allowing it to take up as much room as it needs. This also allows the window scrollbar to appear when required.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  const store = await new Promise(res => chrome.storage.sync.get(null, res));
  if (!store.mylo.removeScrollerArrows) return;

  // This just finds the scroller arrows, and deletes them.
  if (document.querySelector('d2l-table-wrapper')) {
    const container = document.querySelector('#d_content.d2l-max-width');
    if (container) container.classList.remove('d2l-max-width');

    const scroller = document.querySelector('d2l-table-wrapper').shadowRoot.querySelector('d2l-scroll-wrapper');
    if (scroller) {
      scroller.removeAttribute('h-scrollbar');
      const style = scroller.shadowRoot.querySelector('style');
      if (style) {
        style.innerHTML += `
        .wrapper {     
          overflow-x: visible;
          width: fit-content;
          padding-right: 30px;
        }`;
      }
    }
  }
})();
