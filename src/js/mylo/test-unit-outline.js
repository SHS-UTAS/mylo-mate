async function main() {
  const code = await until(() => document.body.getAttribute('mm-unit-code'));

	// Make sure we have a delivery period.
  const delivery = code.split('_').at(2);
  if (!delivery) return [];

	// Don't show in pre-2024 units.
  const year = parseInt(delivery.slice(0, 2));
  if (year < 24) return [];

  // Find all the links that open a unit outline.
  const outlines = document.querySelectorAll('a[onclick*="openunitoutline" i]');

  const { testAllOutlines } = await import('./unit-outline-tests.mjs');

  // Find the first link in the unit outline container.
  // This will be either the link to a single unit outline,
  // or the handle to open the multiple outline dropdown.
  const parent = outlines[0]?.closest('.UnitOutline').querySelector('a');
	if(!parent) return [];

  const statuses = await testAllOutlines();

  // Find links that didn't respond with an 'ok'.
  const issues = statuses.filter(status => !status.ok);

  // If there aren't any issues, there's nothing more to do.
  if (issues.length === 0) return [];

  // Convert the link labels to a string that will be shown in the tooltip.
  const labels = issues.map(issue => ` - ${issue.outline.innerText}`);

  // Build an alert icon (⚠️) that will have more information on hover.
  const icon = document.createElement('d2l-icon');
  icon.setAttribute('icon', 'tier1:alert');
  icon.style.color = 'orange';
  icon.style.marginLeft = '0.5em';
  icon.style.marginBottom = '0.25em';

  // If there's only unit outline, have a basic "Couldn't be found" message.
  // Otherwise, list how many couldn't be found, and which ones were failing.
  icon.title =
    issues.length === 1
      ? `Unit outline could not be found.`
      : [`${issues.length} unit outlines could not be found.`, ...labels].join('\n');

  // Insert the icon after the link anchor we identified earlier.
  parent.insertAdjacentElement('afterend', icon);
}

// Run the parser.
main();
