/**
 * The response for a Unit Outline test
 * @typedef {object} OutlineResult
 * @property {HTMLElement} outline The reference to the unit outline.
 * @property {boolean} ok Whether the check was successful.
 * @property {number} status The status code of the HTTP check.
 * @property {string} url The URL that was checked.
 */

/**
 * The return value of `testAllOutlines()`.
 * @typedef {Promise<OutlineResult>} OutlineTestReturnValue
 */

/**
 * Test a unit outline and return the HTTP status.
 *
 * @param {HTMLElement} outline The unit outline
 * @return {Promise<{ outline: HTMLElement; ok: boolean; status: number }>} The HTTP response for the provided link.
 */
export async function testUnitOutline(outline) {
  // Get the onclick value.
  const attr = outline.getAttribute('onclick');

  // Get the URL parameter passed to the onclick method.
  const url = attr.match(/javascript:openunitoutline\('(?<URL>.*?)'\)/i)?.groups?.URL;

  // If the URL couldn't be retrieved from the onclick event, things went wrong.
  if (!url) return { outline, status: -1, ok: false, url };

  // Test the URL we found.
  const request = await fetch(url, { method: 'HEAD' }).catch(err => {
    console.error(err);
    return { ok: false, status: 401 };
  });

  // Return the status of the page.
  return { outline, ok: request.ok, status: request.status, url };
}

/**
 * Test all unit outlines found within the Unit Information Widget.
 * @return {OutlineTestReturnValue} The results of the Unit Outline tests.
 */
export async function testAllOutlines() {
  // Find all the links that open a unit outline.
  const outlines = document.querySelectorAll('a[onclick*="openunitoutline" i]');

  // If there aren't any unit outlines, we don't have anything we need to do here.
  if (outlines.length === 0) [];

  // Check the status of each unit outline found.
  return Promise.all(Array.from(outlines).map(testUnitOutline));
}
