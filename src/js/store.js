export function get() {
  return new Promise(res => {
    try {
      const ext = import.meta.url.match(/extension:\/\/([^/]+?)\//i)[1];
      if (chrome.storage) {
        chrome.storage.sync.get(null, res);
      } else if (chrome.runtime.sendMessage) {
        chrome.runtime.sendMessage(ext, { request: 'store' }, res);
      }
    } catch (e) {
      console.error(e);
      return {};
    }
  });
}
