/**
 * @affects UTAS website
 * @problem Link checking is tedious, time-consuming and difficult to do without specialist tools.
 * @solution Add a popup dialog that can scan all links on the current page, and report back if any have issues.
 * @target Web Page Maintainers
 * @impact High
 * @savings 15 minutes per page
 */

(async function () {
  const container = document.createElement('div');
  container.id = 'mm-inpage-checker';

  const baseNode = document.querySelector('.l-base-page--main,#main,#content,body');

  if (document.querySelector(`#${container.id}`)) {
    document.querySelector(`#${container.id}`).remove();
  }

  if (baseNode.id == 'main') {
    const fontawesome = document.createElement('link');
    fontawesome.href = 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css';
    fontawesome.rel = 'stylesheet';
    container.append(fontawesome);
  }

  const style = document.createElement('style');
  style.innerHTML = `
  .hilite-red { background-color: #900000; color: white !important; display: inline-block; border-radius: 2px; } 
  @-webkit-keyframes flash { 50% { opacity: 0 } }
  .flash { -webkit-animation-name: flash; -webkit-animation-duration: 1s; -webkit-animation-iteration-count: 3; -webkit-animation-timing-function: ease-in-out; }
  .mm-inpage-link-checker [hidden] { display: none !important; }
  .mm-inpage-link-checker { 
    position: fixed;
    top: 30px;
    right: -400px;
    width: 350px;
    min-height: 100px;
    max-height: 400px;
    background-color: white;
    border: thin solid grey;
    z-index: 100000;
    box-shadow: 0 0 6px black;
    transition: right 0.3s ease-in-out;
    padding: 20px;
    box-sizing: border-box;
  }
  
  .mm-inpage-link-checker [data-role="close-dialog"] {
    position: absolute;
    top: 6px;
    right: 6px;
    font-size: 15px;
    font-weight: bold;
    cursor: pointer;
    padding: 0 5px;
  }

  .mm-inpage-link-checker .mm-inpage-link-checker--title {
    font-size: 18px;
    margin: 0;
    padding: 0;
    border-bottom: thin solid #ccc;
  }
  
  .mm-inpage-link-checker .mm-inpage-link-checker--title small {
    font-size: 11px;
    margin-left: 10px;
    font-family: Arial;
  }
  
  .mm-inpage-link-checker button.btn {
    margin: 10px 0;
  }
  
  .mm-inpage-link-checker .mm-inpage-link-checker--results {
    border-top: thin solid #ccc;
    margin-top: 5px;
    padding-top: 20px;
    overflow-y: auto;
    max-height: 275px;
    display: block;
  }

  .mm-inpage-link-checker .mm-inpage-link-checker--results ul {
    list-style: none;
    padding: 0;
  }
  
  .mm-inpage-link-checker .mm-inpage-link-checker--results a {
    cursor: pointer;
  }

  .mm-inpage-link-checker .mm-inpage-link-checker--results .loader {
    display: block;
    text-align: center;
    font-size: 20px;
    overflow: hidden;
  }

  .mm-inpage-link-checker .mm-inpage-link-checker--results .loader::before {animation: loader 1s infinite linear; display:block;}
  @keyframes loader {
      0% { transform: rotate(0deg) }
      100% { transform: rotate(360deg); }
  }
  `;

  const panel = document.createElement('div');
  panel.classList.add('mm-inpage-link-checker');

  const close = document.createElement('span');
  close.innerText = 'x';
  close.setAttribute('data-role', 'close-dialog');
  close.title = 'Close link checker';
  close.addEventListener('click', e => {
    panel.style.right = '';
    setTimeout(() => container.remove(), 500);
  });

  const title = document.createElement('h2');
  title.innerText = 'Link Checker';
  title.classList.add('mm-inpage-link-checker--title');

  const subtitle = document.createElement('small');
  subtitle.innerText = 'Powered by MyLO MATE';

  title.append(subtitle);

  const content = document.createElement('div');
  content.classList.add('mm-inpage-link-checker--content');

  const button = document.createElement('button');
  button.classList.add('btn', 't-red', 'btn-primary');
  button.innerText = 'Check links on page';

  const results = document.createElement('div');
  results.classList.add('mm-inpage-link-checker--results');
  results.hidden = true;

  const loader = document.createElement('span');
  loader.classList.add('loader', 'fa', 'fa-circle-o-notch');
  loader.setAttribute('aria-hidden', true);

  const notice = document.createElement('p');

  const items = document.createElement('ul');
  results.append(loader, notice, items);

  button.addEventListener('click', async e => {
    results.hidden = false;

    notice.innerText = '';
    items.innerHTML = '';
    loader.hidden = false;
    button.disabled = true;

    baseNode.querySelectorAll('.hilite-red').forEach(node => node.classList.remove('hilite-red'));

    const links = Array.from(baseNode.querySelectorAll('a[href^="http"]'));
    notice.style.textAlign = 'center';
    notice.style.padding = '10px 0';
    notice.innerText = `Checking ${links.length} link${links.length == 1 ? '' : 's'}...`;

    const hrefs = links.map(a => a.href);
    const statuses = await new Promise(res => chrome.runtime.sendMessage({ request: 'check-status', payload: hrefs }, res));

    const bad = links.filter(a => statuses[a.href] != 200);

    notice.style.textAlign = '';
    notice.style.padding = '';
    if (bad.length == 0) {
      notice.innerText = 'All links appear to be working successfully.';
    } else {
      notice.innerText = `${bad.length} out of ${links.length} link${
        links.length == 1 ? '' : 's'
      } responded unusually, and may need investigation. Click below to scroll to the affected link in the page.`;

      bad.forEach(a => {
        const item = document.createElement('li');
        const ref = document.createElement('a');
        ref.innerText = `${a.innerText || `[${a.firstElementChild.localName}]`} (${statuses[a.href]})`;

        a.classList.add('hilite-red');

        ref.addEventListener('click', e => {
          if (a.closest('section.is-inactive:not(.is-active)')) {
            a.closest('section.is-inactive').querySelector('header a').click();
          } else if (a.closest('section.accordion-group')) {
            const isClosed = a.closest('section.accordion-group').querySelector('header a').classList.contains('collapsed');
            if (isClosed) a.closest('section.accordion-group').querySelector('header a').click();
          }
          // a.scrollIntoView({ behavior: 'smooth' });
          a.scrollIntoView({ behaviour: 'smooth', block: 'center', inline: 'nearest' });
          a.addEventListener('animationend', () => {
            a.classList.remove('flash');
          });
          a.classList.add('flash');
        });

        item.append(ref);
        items.append(item);
      });
    }
    button.disabled = false;
    loader.hidden = true;
  });

  content.append(button, results);

  panel.append(close, title, content);

  container.append(style, panel);
  document.body.append(container);
  setTimeout(() => (panel.style.right = '30px'), 100);
})();
