const stylesheet = document.createElement('style');
stylesheet.innerHTML = `
  table.sortable thead th,
  table.sortable thead td {
    cursor: pointer;
  }
  table.sortable thead :is(th,td):is([sort]):after {
    display: inline-block;
  }

  table.sortable thead th[sort="asc"]:after,
  table.sortable thead td[sort="asc"]:after {
    content: '\\25B4';
  }

  table.sortable thead th[sort="desc"]:after,
  table.sortable thead td[sort="desc"]:after {
    content: '\\25BE';
  }
`;

const MakeTableSortable = table => {
  // Don't repeat yourself!
  if (table.__sorted) return;

  if (!stylesheet.isConnected) document.head.append(stylesheet);

  const thead = table.querySelector('thead');
  const tbody = table.querySelector('tbody');

  const headings = Array.from(thead.querySelectorAll('th,td'));
  table.querySelector('thead').addEventListener('click', e => {
    if (!e.target.matches?.('th,td')) return;
    const hdr = e.target;

    const dir = hdr.getAttribute('sort') == 'desc' ? 'asc' : 'desc';
    thead.querySelectorAll('[sort]').forEach(el => el.removeAttribute('sort'));
    hdr.setAttribute('sort', dir);

    const colIndex = headings.indexOf(hdr);

    const rows = Array.from(tbody.querySelectorAll('tr'));

    rows.sort((a, b) => {
      let i = a.querySelector(`td:nth-of-type(${colIndex + 1})`);
      let j = b.querySelector(`td:nth-of-type(${colIndex + 1})`);

      const x = i.querySelector('input,select')?.value ?? i.innerText;
      const y = j.querySelector('input,select')?.value ?? j.innerText;

      return x.localeCompare(y);
    });

    if (dir == 'asc') rows.reverse();

    // Put the remove down here, to prevent a flash in the event that it's a while to sort,
    // and also means that it won't remove if something fails above
    rows.forEach(r => r.remove());
    tbody.append(...rows);
  });

  table.__sorted = true;
};

const mo = new MutationObserver(changes =>
  changes
    .filter(change => change.target.matches('table'))
    .map(v => v.target)
    .map(MakeTableSortable)
);

mo.observe(document.body, { attributes: true, attributeFilter: ['class'], childList: true, subtree: true });

document.querySelectorAll('table.sortable').forEach(MakeTableSortable);
