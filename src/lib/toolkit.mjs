const { Subject } = rxjs;

export const ThreadedWorker = (fn, { args = null, imports, asPromise }) => {
  imports = AsArray(imports)
    ?.map(im => `'${im}'`)
    .join(', ');
  const im = imports ? `importScripts(${imports})\n` : '';

  const src = `${im}
const fn = ${fn.toString()};
self.onmessage = async (e) => {
  const response = await fn(...e.data);
  self.postMessage({ event: 'end', response });
}`;

  const blob = new Blob([src], { type: 'text/javascript' });
  const url = URL.createObjectURL(blob);
  const worker = new Worker(url);

  if (asPromise) {
    return new Promise(res => {
      const msgs = [];
      worker.onmessage = e => {
        if (e.data.event == 'end') {
          res([...msgs, e.data.response]);
          worker.terminate();
        } else {
          msgs.push(e.data);
        }
      };

      worker.postMessage(args);
    });
  } else {
    const subj = new Subject();
    worker.onmessage = e => {
      if (e.data.event == 'end') {
        if (e.data.response) subj.next(e.data.response);
        subj.complete();
        worker.terminate();
      } else {
        subj.next(e.data);
      }
    };
    worker.postMessage(args);
    return subj;
  }
};
